﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using DAS.Application.Constants;
using DAS.Application.Enums;
using DAS.Application.Enums.DasKTNN;
using DAS.Application.Interfaces;
using DAS.Application.Interfaces.DasKTNN;
using DAS.Application.Models.CustomModels;
using DAS.Application.Models.MobileApiModel;
using DAS.Application.Models.ViewModels.DasKTNN;
using DAS.Domain.Models.DASKTNN;
using DAS.Infrastructure.ContextAccessors;
using DAS.Utility;
using DAS.Utility.BuildCondition;
using DAS.Utility.CustomClass;
using DAS.Utility.LogUtils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using static DAS.Application.Enums.DasKTNN.EnumTableInfo;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Configuration;
using System.IdentityModel.Tokens.Jwt;
using DAS.Domain.Enums;

namespace DAS.MobileApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DynamicController : BaseController
    {
        //private readonly ILogger<DynamicController> _logger;
        private readonly ITableInfoApiConfigServices _tableInfoApiConfigService;
        private readonly ILoggerManager _logger;
        private readonly IUserService _iUserService;
        private readonly IAccountService _iAccountService;
        private readonly ITemplateServices _templateServices;
        private readonly IUserPrincipalService _userPrincipalService;
        private readonly IDynamicDBService _iDynamicDBService;
        private readonly ITableInfoServices _iTableInfoService;
        private readonly ISchemaInfoService _iSchemaInfoService;
        private readonly IInputInfoServices _inputInfoServices;
        private readonly ITanXuatTruyCapApiService _tanXuatTruyCapApiService;
        private readonly ISharedAppService _sharedAppService;
        private readonly ISoucreInfoService _soucreInfoService;
        private readonly ISendNotificationServices _notifyClientService;
        private readonly IConfiguration _configuration;
        public DynamicController(ITemplateServices templateServices, ILoggerManager logger,
            IUserPrincipalService userPrincipalService, IUserService iUserService, IDynamicDBService iDynamicDBService, ITableInfoServices iTableInfoService,
            IInputInfoServices inputInfoServices, ITableInfoApiConfigServices TableInfoApiConfigService, ISchemaInfoService SchemaInfoService
            , ISendNotificationServices notifyClientService, IAccountService iAccountService,
            ITanXuatTruyCapApiService tanXuatTruyCapApiService, ISharedAppService sharedAppService,
            IConfiguration configuration, ISoucreInfoService soucreInfoService)
        {
            _templateServices = templateServices;
            _userPrincipalService = userPrincipalService;
            _iUserService = iUserService;
            _iDynamicDBService = iDynamicDBService;
            _iTableInfoService = iTableInfoService;
            _inputInfoServices = inputInfoServices;
            _logger = logger;
            _tableInfoApiConfigService = TableInfoApiConfigService;
            _iSchemaInfoService = SchemaInfoService;
            _notifyClientService = notifyClientService;
            _iAccountService = iAccountService;
            _tanXuatTruyCapApiService = tanXuatTruyCapApiService;
            _sharedAppService = sharedAppService;
            _configuration = configuration;
            _soucreInfoService = soucreInfoService;
        }
        [HttpPost]
        [Authorize]
        [Route("TestPost/{id}")]
        public async Task<IActionResult> TestPost(string id)
        {
            return CustJSonResult(new ServiceResultSuccess
            {
                // Data = data
            });
        }
        [HttpGet]
        [Authorize]
        [Route("TestGet")]
        public async Task<IActionResult> TestGet()
        {
            return CustJSonResult(new ServiceResultSuccess
            {
                Data = "test"
            });
        }
        [HttpPost]
        [Authorize]
        [Route("GetItems/{id}")]
        public async Task<IActionResult> GetItems(string id, [FromBody]VMParamAPI data)
        {
            try
            {
               // SaveLogAccessApi("GetItems", "Lấy danh sách thông tin");
                //Lấy các tham số theo API
                var idapi = 0;
                int.TryParse(id, out idapi);
                var configAPI = await _tableInfoApiConfigService.Update(idapi);
                if (configAPI == null)
                {
                    return CustJSonResult(new ServiceResultError("Cấu hình API không tồn tại"));
                }
                var stshare = CheckStatuShareApp(_userPrincipalService.UserId, configAPI);
                if (stshare.Code == CommonConst.Error)
                {
                    return CustJSonResult(stshare);
                }
                if (configAPI.Status == (int)EnumCommon.Status.InActive)
                {
                    return CustJSonResult(new ServiceResultError("API không đã dừng hoạt động"));
                }
                if (configAPI.Type == (int)TableInfoApiConfigType.GetTableInfo)
                {
                    return CustJSonResult(new ServiceResultError("Cấu hình API không tồn tại"));
                }
                if (configAPI.StartDate != null && configAPI.StartDate.Value > DateTime.Now)//
                {
                    return CustJSonResult(new ServiceResultError("API hiện tại chưa đến thời hạn sử dụng"));
                }
                if (configAPI.EndDate != null && configAPI.EndDate.Value < DateTime.Now)//
                {
                    return CustJSonResult(new ServiceResultError("API hiện tại đã quá hạn"));
                }
                var tableInfor = await _iTableInfoService.Get(configAPI.IDTable.Value);
                if (Utils.IsEmpty(tableInfor))
                {
                    return CustJSonResult(new ServiceResultError("Bảng không tồn tại"));
                }
                var schema = await _iSchemaInfoService.Get(tableInfor.IDSchema);
                if (Utils.IsEmpty(schema))
                {
                    return CustJSonResult(new ServiceResultError("Database không tồn tại"));
                }
                SaveLogAccessApi(configAPI.Name, "Lấy danh sách thông tin bảng "+ tableInfor.Name);
                data = BindAPIParam(configAPI, data);
                data.databaseName = schema.Code;
                data.tableName = tableInfor.DbName;
                //
                var servicereult = new ServiceResult();
                servicereult.Code = CommonConst.Success;
                var result = await _iDynamicDBService.Getitems(data);
                if (result.IsSuccess)
                    return CustJSonResult(new ServiceResultSuccess
                    {
                        Data = new
                        {
                            Items = Utils.ConvertDataTabletoStringReplaceChaLevel(result.Result),
                            TotalItems = result.TotalItems
                        }
                    });
                else
                    return CustJSonResult(new ServiceResultError(result.Message));
            }
            catch (Exception ex)
            {
                return CustJSonResult(new ServiceResultError(ex.Message));
            }
        }
        [HttpPost]
        [Authorize]
        [Route("GetItem/{id}")]
        public async Task<IActionResult> GetItem(string id, [FromBody]VMParamAPI data)
        {
            try
            {
                //SaveLogAccessApi("GetItem", "Lấy thông tin");
                //Lấy các tham số theo API
                var idapi = 0;
                int.TryParse(id, out idapi);
                var configAPI = await _tableInfoApiConfigService.Update(idapi);
                if (configAPI == null)
                {
                    return CustJSonResult(new ServiceResultError("Cấu hình API không tồn tại"));
                }
                var stshare = CheckStatuShareApp(_userPrincipalService.UserId, configAPI);
                if (stshare.Code == CommonConst.Error)
                {
                    return CustJSonResult(stshare);
                }
                if (configAPI.Status == (int)EnumCommon.Status.InActive)
                {
                    return CustJSonResult(new ServiceResultError("API không đã dừng hoạt động"));
                }
                if (configAPI.Type == (int)TableInfoApiConfigType.GetTableInfo)
                {
                    return CustJSonResult(new ServiceResultError("Cấu hình API không tồn tại"));
                }
                if (configAPI.StartDate != null && configAPI.StartDate.Value > DateTime.Now)//
                {
                    return CustJSonResult(new ServiceResultError("API hiện tại chưa đến thời hạn sử dụng"));
                }
                if (configAPI.EndDate != null && configAPI.EndDate.Value < DateTime.Now)//
                {
                    return CustJSonResult(new ServiceResultError("API hiện tại đã quá hạn"));
                }
                var tableInfor = await _iTableInfoService.Get(configAPI.IDTable.Value);
                if (Utils.IsEmpty(tableInfor))
                {
                    return CustJSonResult(new ServiceResultError("Bảng không tồn tại"));
                }
                var schema = await _iSchemaInfoService.Get(tableInfor.IDSchema);
                if (Utils.IsEmpty(schema))
                {
                    return CustJSonResult(new ServiceResultError("Database không tồn tại"));
                }
                SaveLogAccessApi(configAPI.Name, "Lấy thông tin bảng " + tableInfor.Name);
                data.databaseName = schema.Code;
                data.tableName = tableInfor.DbName;
                data = BindAPIParam(configAPI, data);
                //
                var servicereult = new ServiceResult();
                servicereult.Code = CommonConst.Success;
                var result = await _iDynamicDBService.Getitem(data);

                if (result.IsSuccess)
                    return CustJSonResult(new ServiceResultSuccess
                    {
                        Data = new
                        {
                            Items = Utils.ConvertDataTabletoStringReplaceChaLevel(result.Result),
                            LstTach = Utils.ConvertDataTabletoStringReplaceChaLevel(result.LstTach),
                            LstGop = Utils.ConvertDataTabletoStringReplaceChaLevel(result.LsTGop)
                        }
                    });
                else
                    return CustJSonResult(new ServiceResultError(result.Message));
            }
            catch (Exception ex)
            {
                return CustJSonResult(new ServiceResultError(ex.Message));
            }
        }
        [HttpPost]
        [Authorize]
        [Route("AddItem")]
        public async Task<IActionResult> AddItem([FromBody]VMParamAPI data)
        {
            try
            {
                //SaveLogAccessApi("Additem", "Thêm dữ liệu");
                var dbName = data.databaseName;
                var appname = data.appname;
                var urlcallback = data.urlcallback;
                var tableName = data.tableName; //Utils.GetString(DATA, "tableName");
                //var tableInfo = await _iTableInfoService.GetTableByName_Schema(tableName, dbName);
                //if (Utils.IsEmpty(tableInfo))
                //{
                //    return CustJSonResult(new ServiceResultError("Bảng không tồn tại"));
                //}
                //Lấy các tham số theo API
                var idapi = 0;
                int.TryParse(data.apiid, out idapi);
                var configAPI = await _tableInfoApiConfigService.Update(idapi);
                if (configAPI == null)
                {
                    return CustJSonResult(new ServiceResultError("Cấu hình API không tồn tại"));
                }
                var stshare = CheckStatuShareApp(_userPrincipalService.UserId, configAPI);
                if (stshare.Code == CommonConst.Error)
                {
                    return CustJSonResult(stshare);
                }
                if (configAPI.Status == (int)EnumCommon.Status.InActive)
                {
                    return CustJSonResult(new ServiceResultError("API không đã dừng hoạt động"));
                }
                if (configAPI.Type == (int)TableInfoApiConfigType.GetTableRecord)
                {
                    return CustJSonResult(new ServiceResultError("Cấu hình API không tồn tại"));
                }
                if (configAPI.StartDate != null && configAPI.StartDate.Value > DateTime.Now)//
                {
                    return CustJSonResult(new ServiceResultError("API hiện tại chưa đến thời hạn sử dụng"));
                }
                if (configAPI.EndDate != null && configAPI.EndDate.Value < DateTime.Now)//
                {
                    return CustJSonResult(new ServiceResultError("API hiện tại đã quá hạn"));
                }
                var tableInforMap = await _iTableInfoService.Get(configAPI.IDTable ?? 0);
                if (Utils.IsEmpty(tableInforMap))
                {
                    return CustJSonResult(new ServiceResultError("Bảng danh mục gốc không tồn tại, vui lòng kiểm tra lại cấu hình lấy thông tin bảng"));
                }
                var schemaMap = await _iSchemaInfoService.Get(tableInforMap.IDSchema);
                if (Utils.IsEmpty(schemaMap))
                {
                    return CustJSonResult(new ServiceResultError("Database không tồn tại"));
                }
                var tableInforTemp = await _iTableInfoService.Get(configAPI.IDTempTable ?? 0);
                if (Utils.IsEmpty(tableInforTemp))
                {
                    return CustJSonResult(new ServiceResultError("Bảng chờ duyệt trong cấu hình API không tồn tại, vui lòng kiểm tra lại cấu hình lấy thông tin bảng"));
                }
                var schemaTemp = await _iSchemaInfoService.Get(tableInforTemp.IDSchema);
                if (Utils.IsEmpty(schemaTemp))
                {
                    return CustJSonResult(new ServiceResultError("Database không tồn tại"));
                }
                SaveLogAccessApi(configAPI.Name, "Thêm dữ liệu bảng" + tableInforTemp.Name);
                // xử lý dữ liệu đầu vào
                //var properties = Utils.GetString(DATA, "properties");
                var hashProperties = data.properties;// JsonConvert.DeserializeObject<Hashtable>(properties);
                var hjson = (Hashtable)hashProperties.Clone();
                foreach (object key in hjson.Keys)
                {
                    if (key.ToString().ToUpper() == "MADANHMUC")
                    {
                        hjson[key.ToString()] = "";
                        break;
                    }
                }
                hashProperties["DataJson"] = Utils.SerializeHasTable(hjson);
                var columns = (await _iTableInfoService.GetColumnByIDTable(tableInforTemp.ID)).ToList();
                //Thêm sẵn các cột appname,urlcallback
                if (!string.IsNullOrEmpty(urlcallback))
                {
                    hashProperties["UrlCallBack"] = urlcallback;
                }
                if (!string.IsNullOrEmpty(appname))
                {
                    hashProperties["AppName"] = appname;
                }
                hashProperties["IDTable"] = tableInforTemp.ID.ToString();
                hashProperties["Schemaname"] = schemaMap.Code;
                hashProperties["Tablename"] = tableInforMap.DbName;
                //Mã danh mục mặc định  = trống yêu cầu ngày 08/11
                foreach (object key in hashProperties.Keys)
                {
                    if (key.ToString().ToUpper() == "MADANHMUC")
                    {
                        hashProperties[key.ToString()] = DateTime.Now.Ticks.ToString();
                        break;
                    }
                }
                //Xác định nếu là bảng tạm thì đưa tất cả property vào trong
                if (columns.Exists(t => t.DataType == (int)EnumTableInfo.ColumnDataType.TableLink))
                {
                    var columnLink = columns.FirstOrDefault(t => t.DataType == (int)EnumTableInfo.ColumnDataType.TableLink);
                    if (Utils.IsNotEmpty(columnLink) && hashProperties.ContainsKey(columnLink.DbName))
                    {
                        var vlLinks = Utils.Deserialize<List<VMTableLinkValue>>(hashProperties[columnLink.DbName].ToString());
                        if (Utils.IsNotEmpty(vlLinks))
                        {
                            var idRecordRefs = new List<string>();
                            var idTablesRef = new List<string>();
                            foreach (var item in vlLinks)
                            {
                                var tablelink = await _iTableInfoService.GetTableByName_Schema(item.tableName, item.databaseName);
                                if (Utils.IsEmpty(tablelink))
                                {
                                    return CustJSonResult(new ServiceResultError("Bảng liên kết không tồn tại"));
                                }
                                idTablesRef.Add(tablelink.ID.ToString());
                                idRecordRefs.Add(item.value);
                            }
                            hashProperties["IDTableRef"] = Utils.Serialize(idTablesRef);
                            hashProperties["IDRecordRef"] = Utils.Serialize(idRecordRefs);
                        }
                    }
                }
                var result = await _inputInfoServices.Save(hashProperties);
                if (result.IsSuccess)
                {
                    SendNotification(tableInforMap, tableInforTemp.ID);
                    return CustJSonResult(new ServiceResultSuccess());
                }
                else
                {
                    return CustJSonResult(new ServiceResultError(result.Message));
                }
            }
            catch (Exception ex)
            {
                return CustJSonResult(new ServiceResultError(ex.Message));
            }
        }
        [HttpPost]
        [Authorize]
        [Route("UpdateItem")]
        public async Task<IActionResult> UpdateItem([FromBody]VMParamAPI data)
        {
            try
            {
               // SaveLogAccessApi("Updateitem", "Cập nhật thông tin");
                var dbName = data.databaseName;// Utils.GetString(DATA, "databaseName");
                var tableName = data.tableName;// Utils.GetString(DATA, "tableName");
                var appname = data.appname;
                var urlcallback = data.urlcallback;
                var id = data.id;
                //var tableInfo = await _iTableInfoService.GetTableByName_Schema(tableName, dbName);
                //if (Utils.IsEmpty(tableInfo))
                //{
                //    return CustJSonResult(new ServiceResultError("Bảng không tồn tại"));
                //}
                // check bản ghi tồn tại
                var it = _iDynamicDBService.GetFirstById(dbName, tableName, id);
                if (it == null || it.Rows.Count == 0)
                {
                    return CustJSonResult(new ServiceResultError("Bản ghi cần cập nhật không tồn tại"));
                }
                // xử lý dữ liệu đầu vào
                var hashProperties = data.properties; ;// Utils.GetString(DATA, "properties");
                if (hashProperties.Count == 0)
                {
                    return CustJSonResult(new ServiceResultError("Dữ liệu thuộc tính cột không được để trống"));
                }
                //var hashProperties = JsonConvert.DeserializeObject<Hashtable>(properties);
                //Lấy các tham số theo API
                var idapi = 0;
                int.TryParse(data.apiid, out idapi);
                var configAPI = await _tableInfoApiConfigService.Update(idapi);
                if (configAPI == null)
                {
                    return CustJSonResult(new ServiceResultError("Cấu hình API không tồn tại"));
                }
                var stshare = CheckStatuShareApp(_userPrincipalService.UserId, configAPI);
                if (stshare.Code == CommonConst.Error)
                {
                    return CustJSonResult(stshare);
                }
                if (configAPI.Status == (int)EnumCommon.Status.InActive)
                {
                    return CustJSonResult(new ServiceResultError("API không đã dừng hoạt động"));
                }
                if (configAPI.Type == (int)TableInfoApiConfigType.GetTableRecord)
                {
                    return CustJSonResult(new ServiceResultError("Cấu hình API không tồn tại"));
                }
                if (configAPI.StartDate != null && configAPI.StartDate.Value > DateTime.Now)//
                {
                    return CustJSonResult(new ServiceResultError("API hiện tại chưa đến thời hạn sử dụng"));
                }
                if (configAPI.EndDate != null && configAPI.EndDate.Value < DateTime.Now)//
                {
                    return CustJSonResult(new ServiceResultError("API hiện tại đã quá hạn"));
                }
                var tableInforMap = await _iTableInfoService.Get(configAPI.IDTable.Value);
                if (Utils.IsEmpty(tableInforMap))
                {
                    return CustJSonResult(new ServiceResultError("Bảng không tồn tại"));
                }
                var schemaMap = await _iSchemaInfoService.Get(tableInforMap.IDSchema);
                if (Utils.IsEmpty(schemaMap))
                {
                    return CustJSonResult(new ServiceResultError("Database không tồn tại"));
                }
                var tableInforTemp = await _iTableInfoService.Get(configAPI.IDTempTable ?? 0);
                if (Utils.IsEmpty(tableInforTemp))
                {
                    return CustJSonResult(new ServiceResultError("Bảng không tồn tại"));
                }
                var schemaTemp = await _iSchemaInfoService.Get(tableInforTemp.IDSchema);
                if (Utils.IsEmpty(schemaTemp))
                {
                    return CustJSonResult(new ServiceResultError("Database không tồn tại"));
                }
                SaveLogAccessApi(configAPI.Name, "Cập nhật dữ liệu bảng" + tableInforTemp.Name);
                var columns = (await _iTableInfoService.GetColumnByIDTable(tableInforTemp.ID)).ToList();
                //Thêm sẵn các cột appname,urlcallback
                if (!string.IsNullOrEmpty(urlcallback))
                {
                    hashProperties["UrlCallBack"] = urlcallback;
                }
                if (!string.IsNullOrEmpty(appname))
                {
                    hashProperties["AppName"] = appname;
                }
                hashProperties["Schemaname"] = schemaMap.Code;
                hashProperties["Tablename"] = tableInforMap.DbName;
                //Xác định nếu là bảng tạm thì đưa tất cả property vào trong
                hashProperties["DataJson"] = JsonConvert.SerializeObject(data.properties);
                //
                hashProperties["IDTable"] = tableInforTemp.ID.ToString();
                hashProperties["ID"] = id;
                if (columns.Exists(t => t.DataType == (int)EnumTableInfo.ColumnDataType.TableLink))
                {
                    var columnLink = columns.FirstOrDefault(t => t.DataType == (int)EnumTableInfo.ColumnDataType.TableLink);
                    if (Utils.IsNotEmpty(columnLink) && hashProperties.ContainsKey(columnLink.DbName))
                    {
                        var vlLinks = Utils.Deserialize<List<VMTableLinkValue>>(hashProperties[columnLink.DbName].ToString());
                        hashProperties[columnLink.DbName] = "";
                        if (Utils.IsNotEmpty(vlLinks))
                        {
                            var idRecordRefs = new List<string>();
                            var idTablesRef = new List<string>();
                            foreach (var item in vlLinks)
                            {
                                var tablelink = await _iTableInfoService.GetTableByName_Schema(item.tableName, item.databaseName);
                                if (Utils.IsEmpty(tablelink))
                                {
                                    return CustJSonResult(new ServiceResultError("bảng liên kết không tồn tại"));
                                }
                                idTablesRef.Add(tablelink.ID.ToString());
                                idRecordRefs.Add(item.value);
                            }
                            hashProperties["IDTableRef"] = Utils.Serialize(idTablesRef);
                            hashProperties["IDRecordRef"] = Utils.Serialize(idRecordRefs);
                        }
                    }
                }
                var result = await _inputInfoServices.Update(hashProperties);
                if (result.IsSuccess)
                {
                    return CustJSonResult(new ServiceResultSuccess());
                }
                else
                {
                    return CustJSonResult(new ServiceResultError(result.Message));
                }
            }
            catch (Exception ex)
            {
                return CustJSonResult(new ServiceResultError(ex.Message));
            }
        }

        [HttpPost]
        [Authorize]
        [Route("GetInfoTable/{id}")]
        public async Task<IActionResult> GetInfoTable(string id)
        {
            try
            {
                //SaveLogAccessApi("GetInforTable", "Lấy thông tin bảng");

                VMParamAPI data = new VMParamAPI();
                //Lấy các tham số theo API
                var idapi = 0;
                int.TryParse(id, out idapi);
                var configAPI = await _tableInfoApiConfigService.Update(idapi);
                if (configAPI == null)
                {
                    return CustJSonResult(new ServiceResultError("Cấu hình API không tồn tại"));
                }
                var stshare = CheckStatuShareApp(_userPrincipalService.UserId, configAPI);
                if (stshare.Code == CommonConst.Error)
                {
                    return CustJSonResult(stshare);
                }
                if (configAPI.Status == (int)EnumCommon.Status.InActive)
                {
                    return CustJSonResult(new ServiceResultError("API không đã dừng hoạt động"));
                }
                if (configAPI.Type == (int)TableInfoApiConfigType.GetTableRecord)
                {
                    return CustJSonResult(new ServiceResultError("Cấu hình API không tồn tại"));
                }
                if (configAPI.StartDate != null && configAPI.StartDate.Value > DateTime.Now)//
                {
                    return CustJSonResult(new ServiceResultError("API hiện tại chưa đến thời hạn sử dụng"));
                }
                if (configAPI.EndDate != null && configAPI.EndDate.Value < DateTime.Now)//
                {
                    return CustJSonResult(new ServiceResultError("API hiện tại đã quá hạn"));
                }
                var tableInfor = await _iTableInfoService.Get(configAPI.IDTable.Value);
                if (Utils.IsEmpty(tableInfor))
                {
                    return CustJSonResult(new ServiceResultError("Bảng không tồn tại"));
                }
                var schema = await _iSchemaInfoService.Get(tableInfor.IDSchema);
                if (Utils.IsEmpty(schema))
                {
                    return CustJSonResult(new ServiceResultError("Database không tồn tại"));
                }
                SaveLogAccessApi(configAPI.Name, "Lấy thông tin bảng" + tableInfor.Name);
                data.databaseName = schema.Code;
                data.tableName = tableInfor.DbName;
                data = BindAPIParam(configAPI, data);
                var servicereult = new ServiceResult();
                servicereult.Code = CommonConst.Success;
                var result = _iDynamicDBService.GetInfoTable(data);
                if (result.IsSuccess)
                    return CustJSonResult(new ServiceResultSuccess
                    {
                        Data = new
                        {
                            appname = "",
                            urlcallback = "",
                            apiid = idapi.ToString(),
                            properties = data.properties
                        }
                    });
                else
                    return CustJSonResult(new ServiceResultError(result.Message));
            }
            catch (Exception ex)
            {
                return CustJSonResult(new ServiceResultError(ex.Message));
            }
        }
        #region Đăng nhập Share App
        [HttpPost]
        [Route("login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody] VMMobileReaderLogin model)
        {
            if (!ModelState.IsValid)
            {
                return JSErrorModelStateByLine();
            }
            if (model.Password.Length > 255 || model.Password.Length < 6)
            {
                ModelState.AddModelError("Password", "Mật khẩu có tối thiểu 6 ký tự và tối đa 255 ký tự");
                return JSErrorModelStateByLine();
            }

            if (model.UserName.Length > 50)
            {
                ModelState.AddModelError("UserName", "Tên đăng nhập không được vượt quá 255 ký tự");
                return JSErrorModelStateByLine();
            }
            var serviceResult = await _sharedAppService.Authenticate(model);
            if (serviceResult.Code == CommonConst.Success && serviceResult.Data != null)
            {
                #region Bearer token
                var shareApp = (SharedApp)serviceResult.Data;
                // create claims token
                var claimsToken = new List<Claim>
                {
                    new Claim(ClaimTypes.NameIdentifier, shareApp.ID.ToString()),
                    new Claim(CustomClaimTypes.Username, shareApp.Username),
                    new Claim(CustomClaimTypes.FullName, shareApp.Name),
                };
                //Microsoft.IdentityModel.Tokens
                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Tokens:Key"]));
                var signingCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                var tokenSecurity = new JwtSecurityToken(
                    _configuration["Tokens:Issuer"],
                    _configuration["Tokens:Audience"],
                    claimsToken,
                    expires: DateTime.Now.AddSeconds(shareApp.AccessTokenExpiryTime),
                    signingCredentials: signingCredentials);
                #endregion
                // create claims
                var token = new JwtSecurityTokenHandler().WriteToken(tokenSecurity);
                return CustJSonResult(new ServiceResultSuccess("Đăng nhập thành công", new
                {
                    Id = shareApp.ID,
                    UserName = shareApp.Username,
                    Name = shareApp.Name,
                    Token = token
                }));
            }
            return CustJSonResult(new ServiceResultError("Đăng nhập không thành công"));
        }
        #endregion
        [HttpPost]
        [AllowAnonymous]
        [Route("GetSourceData")]
        public async Task<IActionResult> GetSourceData()
        {
            try
            {
                var result = _soucreInfoService.GetListSourceAPI().Result;
                return CustJSonResult(new ServiceResultSuccess
                {
                    Data = new
                    {
                        SQLSERVER=result.FirstOrDefault(t => t.TypeSource == (int)EnumSoucreInfo.TypeSoucre.SQLServer).SourceInfors ,
                        ORACLE= result.FirstOrDefault(t => t.TypeSource == (int)EnumSoucreInfo.TypeSoucre.Oracle).SourceInfors ,
                        CASSANDA =result.FirstOrDefault(t => t.TypeSource == (int)EnumSoucreInfo.TypeSoucre.Cassanda).SourceInfors ,
                        MYSQL = result.FirstOrDefault(t => t.TypeSource == (int)EnumSoucreInfo.TypeSoucre.MySQL).SourceInfors ,
                        API = result.FirstOrDefault(t => t.TypeSource == (int)EnumSoucreInfo.TypeSoucre.API).SourceInfors ,
                        FILECSV =  result.FirstOrDefault(t => t.TypeSource == (int)EnumSoucreInfo.TypeSoucre.FileCSV).SourceInfors ,
                        FILEXML = result.FirstOrDefault(t => t.TypeSource == (int)EnumSoucreInfo.TypeSoucre.FileXML).SourceInfors ,
                    }
                }
                    );
            }
            catch (Exception ex)
            {
                return CustJSonResult(new ServiceResultError(ex.Message));
            }
        }

        [HttpPost]
        [Authorize]
        [Route("GetItemsByScript/{id}")]
        public async Task<IActionResult> GetItemsByScript(string id, [FromBody]VMScriptParamAPI data)
        {
            try
            {
                // SaveLogAccessApi("GetItems", "Lấy danh sách thông tin");
                //Lấy các tham số theo API
                var idapi = 0;
                int.TryParse(id, out idapi);
                var configAPI = await _tableInfoApiConfigService.Update(idapi);
                if (configAPI == null)
                {
                    return CustJSonResult(new ServiceResultError("Cấu hình API không tồn tại"));
                }
                var stshare = CheckStatuShareApp(_userPrincipalService.UserId, configAPI);
                if (stshare.Code == CommonConst.Error)
                {
                    return CustJSonResult(stshare);
                }
                if (configAPI.Status == (int)EnumCommon.Status.InActive)
                {
                    return CustJSonResult(new ServiceResultError("API không đã dừng hoạt động"));
                }
                if (configAPI.Type != (int)TableInfoApiConfigType.RunScriptSQL)
                {
                    return CustJSonResult(new ServiceResultError("Cấu hình API không tồn tại"));
                }
                if (configAPI.StartDate != null && configAPI.StartDate.Value > DateTime.Now)//
                {
                    return CustJSonResult(new ServiceResultError("API hiện tại chưa đến thời hạn sử dụng"));
                }
                if (configAPI.EndDate != null && configAPI.EndDate.Value < DateTime.Now)//
                {
                    return CustJSonResult(new ServiceResultError("API hiện tại đã quá hạn"));
                }
                //var tableInfor = await _iTableInfoService.Get(configAPI.IDTable.Value);
                //if (Utils.IsEmpty(tableInfor))
                //{
                //    return CustJSonResult(new ServiceResultError("Bảng không tồn tại"));
                //}
                //var schema = await _iSchemaInfoService.Get(tableInfor.IDSchema);
                //if (Utils.IsEmpty(schema))
                //{
                //    return CustJSonResult(new ServiceResultError("Database không tồn tại"));
                //}
                SaveLogAccessApi(configAPI.Name, "Lấy dữ liệu thông qua API Script");
                //data = BindAPIParam(configAPI, data);
                //data.databaseName = schema.Code;
                //data.tableName = tableInfor.DbName;
                // tính toán lại script
                var script = configAPI.Description;
                script=script.Replace("{columns}",data.column);
                if(data.values !=null && data.values.Count>0)
                {
                    foreach (var item in data.values.Keys)
                    {
                        script= script.Replace("{"+item.ToString()+"}", data.values[item].ToString());
                    }
                }    
                var servicereult = new ServiceResult();
                servicereult.Code = CommonConst.Success;
                var result =  _iDynamicDBService.ExecuteSql(script);
                if (result!=null)
                    return CustJSonResult(new ServiceResultSuccess
                    {
                        Data = new
                        {
                            Items = Utils.ConvertDataTabletoStringReplaceChaLevel(result),
                            TotalItems = result.Rows.Count
                        }
                    });
                else
                    return CustJSonResult(new ServiceResultError("không có dữ liệu"));
            }
            catch (Exception ex)
            {
                return CustJSonResult(new ServiceResultError(ex.Message));
            }
        }

        #region ------ hàm ---------
        private VMParamAPI BindAPIParam(VMTableInfoApiConfig apiconfig, VMParamAPI data)
        {
            try
            {
                var result = new List<QueryAPI>();
                var conditions = apiconfig.Conditions;
                foreach (var item in apiconfig.Conditions)
                {
                    var opr = "";
                    switch (item.Operator)
                    {
                        case (int)CondOperator.Equal:
                            opr = "=";
                            break;
                        case (int)CondOperator.NotIn:
                            opr = "NOT IN";
                            break;
                        case (int)CondOperator.In:
                            opr = "IN";
                            break;
                        case (int)CondOperator.Greater:
                            opr = ">";
                            break;
                        case (int)CondOperator.GreaterOrEqual:
                            opr = ">=";
                            break;
                        case (int)CondOperator.LowerOrEqual:
                            opr = "<=";
                            break;
                        case (int)CondOperator.Lower:
                            opr = "<";
                            break;
                        case (int)CondOperator.NotLike:
                            opr = "NOT LIKE";
                            break;
                        case (int)CondOperator.Like:
                            opr = "LIKE";
                            break;
                        default:
                            break;
                    }
                    result.Add(new QueryAPI
                    {

                        column = apiconfig.Columns.FirstOrDefault(t => t.ID == item.IDColumn).DbName,
                        operators = opr,
                        value = item.Value,
                        clause = " AND "
                    });
                }
                //Mặc định theo query của  cấu hình API
                if (Utils.IsNotEmpty(result))
                {
                    data.query = result;
                }
                // orderby
                if (Utils.IsNotEmpty(apiconfig.OrderBy))
                {
                    data.orderBy = apiconfig.OrderBy.Split(",").ToList();
                }
                if (apiconfig.Limit != null && apiconfig.Limit.Value > 0)
                {
                    data.rowLimit = apiconfig.Limit.Value;
                }
                // View Columns
                if (Utils.IsNotEmpty(apiconfig.OrderBy))
                {
                    data.orderBy = apiconfig.OrderBy.Split(",").ToList();
                }
                if (Utils.IsNotEmpty(apiconfig.ArrViewColumn))
                {
                    var arrViewColumns = new List<string>();
                    foreach (var item in apiconfig.ArrViewColumn)
                    {
                        arrViewColumns.Add(apiconfig.Columns.FirstOrDefault(t => t.ID == item).DbName);
                    }
                    data.viewColumns = arrViewColumns;
                }
                return data;
            }
            catch (Exception ex)
            {
                throw new LogicException("Cấu hình API sai, vui lòng kiểm tra lại " + ex.Message);
            }

        }

        private void SendNotification(TableInfo table,int idtabletemp=0)
        {
            var users = _iAccountService.GetUserByModule((int)EnumModule.Code.INPUTINFOTEMP, (int)EnumPermission.Type.Approve).Result;
            if (Utils.IsNotEmpty(users))
            {
                var url = $"/InputInfoTemp/?IDTable={idtabletemp}";
                var noidung = $"1 dữ liệu đã được phát sinh vào bảng {table.Name} cần bạn duyệt";
                _notifyClientService.PushToUsers(users.Select(t => t.ID).Distinct().ToArray(), noidung, url, 0, 0, 0, 0, 0, 0, null); //Don't need await
            }
        }

        private void SaveLogAccessApi(string apiname, string content)
        {
            _tanXuatTruyCapApiService.Save(apiname, content);
        }
        private ServiceResult CheckStatuShareApp(int id, VMTableInfoApiConfig vmapi)
        {
            var shareapp = _sharedAppService.Get(id).Result;
            if (shareapp == null || shareapp.Status == (int)EnumSoucreInfo.TypeStatus.DungHoatDong)
                return new ServiceResultError("Trạng thái app đã dừng hoạt động");
            var shareappconfigs = vmapi.SharedAppConfigs.ToList();
            if (shareappconfigs == null || !shareappconfigs.Exists(t => t.IDSharedApp == id))
                return new ServiceResultError("App hiện tại không được quyền truy cập API");
            return new ServiceResultSuccess();
        }
        #endregion
    }
}