﻿using DAS.Application.Models.ViewModels;
using System.Threading.Tasks;

namespace DAS.Application.Interfaces
{
    public interface IProfileCategoryServices
    {
        Task<VMTreeProfileCategory> GetTree(long id, int type);
    }
}