﻿using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels;
using DAS.Domain.Models.DAS;
using DAS.Domain.Models.DASNotify;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAS.Application.Interfaces
{
    public interface IHomeServices
    {
        Task<IEnumerable<VMDashBoardPlan>> GetProcessingCollectionPlan(HomeCondition condition);
        Task<VMDashBoardStorage> GetStatisticalStorageByYear(HomeCondition condition);
        Task<VMDashBoardProfile> ProfileAndDocByStatus(HomeCondition condition);
        Task<VMDashBoardExpiryDate> StatisticalExpiryDate(HomeCondition condition);
        Task<List<SelectListItem>> GetDataTypeDashBoard(HomeCondition condition);
        Task<PaginatedList<VMNotification>> GetListNotificationPaging(NotificationCondition condition);
        Task<int> TotalUnreadNotification();
        Task<VMNotification> GetNotificationByUserId(int userId);
        Task<bool> ReadNotification(int id);
    }
}
