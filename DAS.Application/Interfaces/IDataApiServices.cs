﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAS.Application.Models.CustomModels;
using DAS.Application.Models.MobileApiModel;
using DAS.Application.Models.ViewModels;
using Microsoft.AspNetCore.Http;
using DAS.Application.Enums;
using System.Collections;

namespace DAS.Application.Interfaces
{
    public interface IDataApiServices
    {
        Task<ServiceResult> Register(VMMobileReaderRegister model);
        Task<ServiceResult> Authenticate(VMMobileReaderLogin loginModel);
        Task<ServiceResult> ChangePassword(VMMobileChangePassword model);
    }
}
