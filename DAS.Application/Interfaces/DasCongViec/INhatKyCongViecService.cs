﻿using System.Collections.Generic;
using DAS.Application.Models.ViewModels;
using System.Threading.Tasks;
using DAS.Domain.Models.DAS;
using DAS.Application.Models.CustomModels;
using DAS.Domain.Interfaces.DAS;
using Microsoft.AspNetCore.Http;
using DAS.Application.Models.Param;
using System;
using DAS.Domain.Models.DasCongViec;

namespace DAS.Application.Interfaces.DasCongViec
{
    public interface INhatKyCongViecService
    {
        /// <summary>
        /// Lấy Tất cả danh sách công việc
        /// </summary>
        /// <returns></returns>
        Task<List<VMCongViec>> GetListAllCongViec();
        Task<PaginatedList<VMNhatKyCongViec>> SearchListConditionPagging(NhatKyCongViecCondition condition);
        Task<ServiceResult> Create(VMNhatKyCongViec nhatkycongviec);
        Task<ServiceResult> Update(VMNhatKyCongViec nhatkycongviec);
        /// <summary>
        /// Lấy chi tiết các thông tin liên quan đến nhật ký công việc
        /// </summary>
        /// <param name="nhatkycongviec"></param>
        /// <returns></returns>
        Task<VMNhatKyCongViec> GetDetail(int id);
        /// <summary>
        /// Xóa nhật ký công việc
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ServiceResult> Delete(int id);
        /// <summary>
        /// Lấy danh sách công việc tồn của 1 nhật ký công việc
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<CongViecTonModel> GetTon(NhatKyCongViec item);
        Task<CongViecTonModel> GetTon(DateTime ngay,int ca);
        Task<ServiceResult> SendApprove(int id,int iduser);
        Task<ServiceResult> Approve(VMNhatKyCongViec nhatkycongviec);

        Task<List<VMBaoCaoTongHop>> BaoCaoTongHop(NhatKyCongViecCondition condition);
        Task<List<VMBaoCaoChiTiet>> BaoCaoChiTiet(NhatKyCongViecCondition condition);

    }
}
