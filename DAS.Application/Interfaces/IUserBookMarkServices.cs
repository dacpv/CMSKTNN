﻿using System.Collections.Generic;
using DAS.Application.Models.ViewModels;
using System.Threading.Tasks;
using DAS.Domain.Models.DAS;
using DAS.Application.Models.CustomModels;
using DAS.Domain.Interfaces.DAS;
using Microsoft.AspNetCore.Http;
namespace DAS.Application.Interfaces
{
    public interface IUserBookMarkServices
    {
        Task<VMUserBookMark> GetBookMark();
        Task<ServiceResult> ChangeBookMark( List<int> modules);
        Task<ServiceResult> AddBookMark(int idModule);
        Task<ServiceResult> RemoveBookMark(int idModule);
    }
}
