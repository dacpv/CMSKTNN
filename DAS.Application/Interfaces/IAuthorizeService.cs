﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAS.Application.Interfaces
{
    public interface IAuthorizeService
    {
        Task<bool> CheckPermission(int CodeModule, int[] Permissions);
    }
}
