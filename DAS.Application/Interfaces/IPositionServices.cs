﻿using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels;
using DAS.Domain.Models.DAS;
using DAS.Utility.CustomClass;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAS.Application.Interfaces
{
    public interface IPositionServices
    {
        Task<IEnumerable<Position>> GetsActive();
        Task<PaginatedList<VMPosition>> SearchByConditionPagging(PositionCondition positionCondition);
        Task<ServiceResult> Create(VMPosition vmPosition);
        Task<ServiceResult> Update(VMPosition vmPosition);
        Task<ServiceResult> Delete(int id);
        Task<ServiceResult> Delete(IEnumerable<int> ids);
        Task<List<SelectListItemTree>> GetPostionByTree(VMPosition vMPosition);
        Task<Position> Get(object id);
        Task<IEnumerable<Position>> Gets();
        Task<IEnumerable<VMPosition>> GetListByCondition(PositionCondition positionCondition, bool getParents = false);
    }
}
