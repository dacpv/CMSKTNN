﻿using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels;
using DAS.Domain.Models.DAS;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAS.Application.Interfaces
{
    public interface IStorageServices : IBaseMasterService<Storage>
    {
        Task<VMIndexProfileAndDoc> SearchProfileAndDocByConditionPaging(SearchProfileCondition condition);


        Task<VMIndexDocCatalogingProfile> PlanDocDetailIndex(SearchProfileCondition condition);
        Task<VMIndexDocCatalogingProfile> PlanDocDetailIndexNoPaging(SearchProfileCondition condition);
        Task<VMSearchProfileDoc> GetDocCollect(int IDDoc);
        Task<VMIndexProfileAndDoc> PortalSearch(SearchProfileCondition condition);
        Task<IEnumerable<ProfileTemplate>> GetListProfileTemplate(List<int> ids);
    }
}
