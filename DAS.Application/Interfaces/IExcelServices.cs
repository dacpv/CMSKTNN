﻿using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels;
using DAS.Domain.Models.DAS;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace DAS.Application.Interfaces
{
    public interface IExcelServices
    {
        Task<ServiceResult> ExportExcel(ExportExtend excelExtend, string sheetName, bool isAdjust = true);
        Task<ServiceResult> ExportExcelCus(ExportExtend2 exportExtend, string sheetName, bool isAdjust = true);
    }
}
