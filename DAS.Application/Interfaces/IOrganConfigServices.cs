﻿using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels;
using DAS.Domain.Models.DAS;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAS.Application.Interfaces
{
    public interface IOrganConfigServices : IBaseMasterService<OrganConfig>
    {
        Task<PaginatedList<VMOrganConfig>> SearchByConditionPagging(OrganConfigCondition condition, bool isExport = false);
        Task<VMUpdateOrganConfig> GetOrganConfig(int id);
        Task<object> GetConfigByCode(string code, int idOrgan = 0);
        Task<ServiceResult> CreateOrganConfig(VMUpdateOrganConfig model);
        Task<ServiceResult> UpdateOrganConfig(VMUpdateOrganConfig model);
        Task<ServiceResult> DeleteOrganConfig(int id);
        Task<ServiceResult> Deletes(IEnumerable<int> ids);
    }
}
