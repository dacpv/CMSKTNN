﻿using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels;
using DAS.Domain.Models.DAS;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAS.Application.Interfaces
{
    public interface IUserLogServices
    {
        Task<ServiceResult> LogActionLogin(long userID, string userName,string accesstoken);
        Task<ServiceResult> LogActionLogout();
        Task<PaginatedList<VMLogInfo>> GetCRUDLogByCondition(LogInfoCondition condition, bool isExport = false);
        Task<VMLogInfoStatistic> GetUserLogByCondition(LogInfoCondition condition, bool isExport = false);
        Task<VMLogInfo> GetChartCRUDLogByCondition(LogInfoCondition condition);
        Task<VMLogInfoStatistic> GetCRUDLogByConditionErrol(LogInfoCondition condition, bool isExport = false);
         Task<VMLogInfo> Get(object id);
    }
}
