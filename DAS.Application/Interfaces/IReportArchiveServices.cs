﻿using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels;
using DAS.Domain.Models.DAS;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace DAS.Application.Interfaces
{
    public interface IReportArchiveServices
    {
        Task<VMIndexReportSendArchive> ReportSendArchivePaging(ReportSendArchiveCondition condition, bool isExport = false);
        Task<VMIndexReportReceiveArchive> ReportReceiveArchivePaging(ReportReceiveArchiveCondition condition, bool isExport = false);
    }
}
