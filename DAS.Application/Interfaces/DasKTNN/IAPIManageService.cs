﻿using System.Collections.Generic;
using DAS.Application.Models.ViewModels;
using System.Threading.Tasks;
using DAS.Domain.Models.DAS;
using DAS.Application.Models.CustomModels;
using DAS.Domain.Interfaces.DAS;
using Microsoft.AspNetCore.Http;
using DAS.Application.Models.Param;
using System;
using DAS.Domain.Models.DasCongViec;
using DAS.Domain.Models.DASKTNN;
using DAS.Application.Models.ViewModels.DasKTNN;

namespace DAS.Application.Interfaces.DasKTNN
{
    public interface IAPIManageService
    {
        VMAPIReport Search(APIReportCondition condition);
       
    }
}
