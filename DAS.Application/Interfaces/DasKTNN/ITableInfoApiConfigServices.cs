﻿using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels;
using DAS.Application.Models.ViewModels.DasKTNN;
using DAS.Domain.Models.DASKTNN;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAS.Application.Interfaces
{
    public interface ITableInfoApiConfigServices
    {
        Task<IEnumerable<TableInfoApiConfig>> GetsList();
        Task<TableInfoApiConfig> Get(int id);
        Task<VMIndexTableInfoApiConfig> SearchByConditionPagging(TableInfoApiConfigCondition TableInfoApiConfigCondition);
        Task<VMTableInfoApiConfig> Create();
        Task<ServiceResult> Save(VMUpdateTableInfoApiConfig vmTableInfoApiConfig);
        Task<VMTableInfoApiConfig> Update(int? id);
        Task<ServiceResult> Change(VMUpdateTableInfoApiConfig vmTableInfoApiConfig);
        Task<ServiceResult> Delete(int id);
        Task<ServiceResult> Delete(IEnumerable<int> ids);
        Task<VMTableInfoApiConfig> GetColumnConfig(Hashtable data);
    }
}