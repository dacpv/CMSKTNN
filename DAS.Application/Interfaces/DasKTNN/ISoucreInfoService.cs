﻿using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels;
using DAS.Application.Models.ViewModels.DasKTNN;
using DAS.Domain.Models.DASKTNN;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAS.Application.Interfaces
{
    public interface ISoucreInfoService
    {
        Task<IEnumerable<SoucreInfo>> GetsList();
        Task<PaginatedList<SoucreInfo>> SearchByConditionPagging(SoucreInfoCondition TableInfoCondition);
        VMSoucreInfo Create(int Type);
        Task<VMSoucreInfo> Create(Hashtable data);
        Task<VMSoucreInfo> Update(int? id);
        Task<VMSoucreInfo> Update(Hashtable data);
      
        Task<ServiceResult> Delete(int id);
        Task<ServiceResult> Delete(IEnumerable<int> ids);
        Task<SoucreInfo> Get(int id);
        Task<IEnumerable<VMSourceInfoAPI>> GetListSourceAPI();
    }
}