﻿using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels;
using DAS.Application.Models.ViewModels.DasKTNN;
using DAS.Domain.Models.DASKTNN;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAS.Application.Interfaces
{
    public interface ITableInfoServices
    {
        Task<IEnumerable<TableInfo>> GetsList();
        Task<IEnumerable<TableInfo>> GetsListShowMenu();
        Task<PaginatedList<VMTableInfo>> SearchByConditionPagging(TableInfoCondition TableInfoCondition);
        Task<VMTableInfo> Create();
        Task<ServiceResult> Create(VMUpdateTableInfo vmTableInfo);
        Task<VMTableInfo> Update(int? id);
        Task<ServiceResult> Update(VMUpdateTableInfo vmTableInfo);
        Task<ServiceResult> UpdateColumns(VMUpdateTableInfo vmTableInfo);
        Task<ServiceResult> Delete(int id);
        Task<ServiceResult> Delete(IEnumerable<int> ids);
        Task<TableInfo> Get(int id);
        Task<IEnumerable<TableInfo>> GetTables(params int[] ids);
        Task<ServiceResult> DeleteBySchema(params int[] idSchema);

        Task<IEnumerable<ColumnTableInfo>> GetColumnByIDTable(params int[] idTables);
        Task<IEnumerable<TableInfo>> GetTableByIDSchema(params int[] idSchemas);
        Task<VMColumnTableInfo> CreateColumn(int? idTable);
        Task<ServiceResult> CreateColumn(VMUpdateColumnTable data);
        Task<VMColumnTableInfo> UpdateColumn(int? id);
        Task<ServiceResult> UpdateColumn(VMUpdateColumnTable data);
        Task<ServiceResult> DeleteColumn(int id);
        Task<ServiceResult> DeleteColumn(IEnumerable<int> ids);
        Task<List<SchemaInfo>> GetAllListSchema();
        Task<List<GroupInfo>> GetAllListGroup();
        Task<TableInfo> GetTableByName_Schema(string tbname,string schemaname);
        Task<VMTableInfo> IsClone(int id);
        Task<ServiceResult> Clone(VMCloneTableInfo data);
        Task<VMTableInfoStatistic> Statistic(TableInfoCondition condition);

        Task<List<SoucreInfo>> GetAllListSource(int? type = null);

        Task<ColumnTableInfo> GetColumnOrderDefaul(int idtable);
    }
}