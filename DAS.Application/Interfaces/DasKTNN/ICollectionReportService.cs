﻿using System.Threading.Tasks;
using DAS.Application.Models.ViewModels.DasKTNN;

namespace DAS.Application.Interfaces.DasKTNN
{
    public interface ICollectionReportService
    {
        Task<VMMainCollectionReport> SearchByCondition(CollectionReportCondition condition);
        Task<VMMainCollectionReport> StatisticBySoucre(CollectionReportCondition condition);
    }
}
