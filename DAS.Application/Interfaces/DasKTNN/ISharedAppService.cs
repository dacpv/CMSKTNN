﻿using DAS.Application.Models.CustomModels;
using DAS.Application.Models.MobileApiModel;
using DAS.Application.Models.ViewModels;
using DAS.Application.Models.ViewModels.DasKTNN;
using DAS.Domain.Models.DASKTNN;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAS.Application.Interfaces
{
    public interface ISharedAppService
    {
        Task<IEnumerable<SharedApp>> GetsList();
        Task<PaginatedList<SharedApp>> SearchByConditionPagging(SharedAppCondition TableInfoCondition);
        Task<VMSharedApp> Create();
        Task<VMSharedApp> Create(Hashtable data);
        Task<VMSharedApp> Update(int? id);
        Task<VMSharedApp> Update(Hashtable data);
      
        Task<ServiceResult> Delete(int id);
        Task<ServiceResult> Delete(IEnumerable<int> ids);
        Task<SharedApp> Get(int id);
        Task<ServiceResult> Authenticate(VMMobileReaderLogin loginModel);



        Task<VMSharedAppStatisticIndex> SharedAppStats(SharedAppStatsCondition condition);
        Task<VMSharedAppStatisticIndex> AppByApiStats(SharedAppStatsCondition condition);
        Task<VMSharedAppStatisticIndex> SharedAppFrequencyStats(SharedAppStatsCondition condition);
    }
}