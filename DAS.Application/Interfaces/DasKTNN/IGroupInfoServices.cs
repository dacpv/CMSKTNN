﻿using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels;
using DAS.Application.Models.ViewModels.DasKTNN;
using DAS.Domain.Models.DASKTNN;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAS.Application.Interfaces
{
    public interface IGroupInfoService
    {
        Task<IEnumerable<GroupInfo>> GetsList();
        Task<IEnumerable<GroupTableInfo>> GetsListTable();
        Task<PaginatedList<GroupInfo>> SearchByConditionPagging(GroupInfoCondition TableInfoCondition);
        Task<VMGroupInfo> Create();
        Task<VMGroupInfo> Create(Hashtable data);
        Task<VMGroupInfo> Update(int? id);
        Task<VMGroupInfo> Update(Hashtable data);
      
        Task<ServiceResult> Delete(int id);
        Task<ServiceResult> Delete(IEnumerable<int> ids);
        Task<GroupInfo> Get(int id);
    }
}