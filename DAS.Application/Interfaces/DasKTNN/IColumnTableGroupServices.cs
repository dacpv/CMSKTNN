﻿using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels;
using DAS.Application.Models.ViewModels.DasKTNN;
using DAS.Domain.Models.DASKTNN;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAS.Application.Interfaces
{
    public interface IColumnTableGroupServices
    {
        Task<IEnumerable<ColumnTableGroup>> GetsList();
        Task<ColumnTableGroup> Get(int id);
        Task<VMIndexColumnTableGroup> SearchByConditionPagging(ColumnTableGroupCondition ColumnTableGroupCondition);
        Task<VMColumnTableGroup> Create();
        Task<ServiceResult> Save(VMUpdateColumnTableGroup vmColumnTableGroup);
        Task<VMColumnTableGroup> Update(int? id);
        Task<ServiceResult> Change(VMUpdateColumnTableGroup vmColumnTableGroup);
        Task<ServiceResult> Delete(int id);
        Task<ServiceResult> Delete(IEnumerable<int> ids);
    }
}