﻿using DAS.Application.Models.CustomModels;
using DAS.Application.Models.Param;
using DAS.Application.Models.ViewModels;
using DAS.Application.Models.ViewModels.DasKTNN;
using System.Threading.Tasks;

namespace DAS.Application.Interfaces
{
    public interface IImportDataServices
    {
        Task<VMImportData> Index(int? idTable); 
        Task<VMSyncData> Sync(VMSyncData data);
        Task<VMSyncData> SaveSync(VMSyncData data);

        Task<VMImportData> GetHeader(VMImportData importData); 
        Task<ServiceResult> Import(VMImportData importData);
        Task<ServiceResult> DownloadTemplate(int IDTable);
    }
}