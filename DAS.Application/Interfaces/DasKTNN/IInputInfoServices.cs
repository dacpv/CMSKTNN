﻿using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels.DasKTNN;
using DAS.Domain.Models.DASKTNN;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAS.Application.Interfaces
{
    public interface IInputInfoServices
    {
        Task<VMInputInfo> Index(InputInfoCondition TableInfoCondition, int IDTable, Hashtable hashtable);
        Task<VMInputInfo> Create(int IDTable);
        Task<VMInputInfo> Save(Hashtable data);
        Task<VMInputInfo> SaveMerge(Hashtable data);
        Task<List<ColumnTableInfo>> GetColumnTables(params int[] idTables);
        Task<ServiceResult> DownloadTemplate(int IDTable);
        Task<VMInputInfo> Update(int IDTable, int ID);
        Task<VMInputInfo> Update(Hashtable data);
        Task<VMInputInfo> SaveDetach(Hashtable data);
        Task<VMInputInfo> ApproveData(Hashtable data);
        Task<VMInputInfo> Delete(Hashtable data);
        Task<VMInputInfo> Deletes(Hashtable data, int[] ids);
        Task<VMLeftTree> GetTableLeftTree(InputInfoCondition TableInfoCondition);
        Task<VMLeftTree> SearchByCondition(InputInfoCondition TableInfoCondition);
        Task<VMLeftTree> GetInfoDataDetail(InputInfoCondition TableInfoCondition);
        Task<VMLeftTree> GetLienKetNguoc(InputInfoCondition TableInfoCondition);
        Task<VMLeftTree> CreateInInformationLevel(int IDTable);
        Task<ServiceResult> GetTableRecords(Hashtable DATA);
        Task<ServiceResult> GetSeparateRecords(Hashtable DATA);
        Task<VMSyncData> SyncData(int? idTable);
        Task<ServiceResult> Synchronization(VMSyncData vmSyncData,  Dictionary<int, int> columns);
        Task<ServiceResult> SyncRecord(VMSyncData vmSyncData);
        Task<VMSyncData> GetColumnMapper(VMSyncData data);
        Task<VMSyncData> CheckMapColumn(int idTabel, int idRecord);
        Task<VMSyncData> GetDataTemp(int idTable, int idRecord);
        Task<ServiceResult> Reject(VMSyncData data);
    }
}