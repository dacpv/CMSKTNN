﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels;
using DAS.Domain.Models.DASKTNN;

namespace DAS.Application.Interfaces.DasKTNN
{
    public interface ISchemaInfoService
    {
        Task<PaginatedList<VMSchemaInfo>> SearchByConditionPagging(SchemaInfoCondition condition);
        Task<List<SchemaInfo>> GetAllList();

        Task<SchemaInfo> Get(object id);
        Task<VMSchemaInfo> GetByID(int? id);
        Task<ServiceResult> Save(VMSchemaInfo vmSchemaInfo);
        Task<ServiceResult> Change(VMSchemaInfo vmSchemaInfo);
        Task<ServiceResult> Delete(int id);
        Task<ServiceResult> Delete(IEnumerable<int> ids);
    }
}
