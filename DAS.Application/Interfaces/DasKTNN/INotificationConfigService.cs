﻿using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels.DasKTNN;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAS.Application.Interfaces.DasKTNN
{
    public interface INotificationConfigService
    {
        Task<VMNotificationConfig> SearchByCondition(NotificationConfigCondition condition);
        Task<Dictionary<int, string>> GetList();
        Task<NotificationConfig> GetByID(object id);
        Task<ServiceResult> EnableNotification(Hashtable Data);
        Task<ServiceResult> Save(NotificationConfig vmModel);
        Task<ServiceResult> Update(NotificationConfig vmModel);
        Task<ServiceResult> Delete(int id);
        Task<ServiceResult> Deletes(int[] ids);
    }
}
