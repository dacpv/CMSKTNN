﻿using System.Collections.Generic;
using DAS.Application.Models.ViewModels;
using System.Threading.Tasks;
using DAS.Domain.Models.DAS;
using DAS.Application.Models.CustomModels;
using DAS.Domain.Interfaces.DAS;
using Microsoft.AspNetCore.Http;
using DAS.Application.Models.Param;
using System;
using DAS.Domain.Models.DasCongViec;
using DAS.Domain.Models.DASKTNN;
using System.Data;
using DAS.Utility.BuildCondition;
using DAS.Application.Models.ViewModels.DasKTNN;
using System.Collections;
using DAS.Utility.CustomClass;
using Dapper;

namespace DAS.Application.Interfaces.DasKTNN
{
    public interface IDynamicDBService
    {
        IEnumerable<TableTest> GetAll();

        #region Schema
        int IsCheckSchema(string nameSchema);
        bool CreateSchema(string nameSchema, string password);
        bool DeleteSchema(string nameSchema);
        bool DeleteSchema(string[] nameSchemas);
        #endregion

        #region Table
        bool CreateTable(string schemaName, TableInfo table, IEnumerable<ColumnPathInfo> columnPaths, params VMColumnTableInfo[] columns);
        bool CreateTable(string schemaName, string tableName, params string[] columns);
        bool DropTable(string schemaName, string tableName);
        bool AlertTable(string schemaName, TableInfo table, ColumnTableInfo[] oldColumns, ColumnTableInfo[] columns);
        bool RenameTable(string schemaName, string oldName, string newName);

        bool CloneTable(string schemaName, string tableName, string newSchemaName, string newTableName);
        int CheckTableExists(string schemaName, string tableName);
        bool CloneTableCustom(string schemaName, string tableName, string newSchemaName, string newTableName, bool isGetAll, int[] idRecords, int level);
        #endregion

        #region Column
        bool CreateColumn(string schemaName, TableInfo table, IEnumerable<ColumnPathInfo> columnPaths, params VMColumnTableInfo[] columns);
        bool AlertColumn();
        bool DropColumn(string schemaName, TableInfo table, IEnumerable<ColumnPathInfo> columnPaths, params ColumnTableInfo[] columns);
        bool AddColumnIdentityFormat(string schemaName, TableInfo table, ColumnTableInfo column);
        bool UpdateIdentityColumns(string schemaName, TableInfo table, IEnumerable<ColumnTableInfo> columns);
        #endregion

        #region Data
        DataTable GetFirstByIds(string Schema, string tablename, object Id);
        DataTable GetListShowTable(string Schema, string tablename, string columns = "*");
        DataTable Search(List<CondParam> condparams, string Schema, string tablename, string columns = "*", string orderby = "", Pagination page = null);
        int Count(List<CondParam> condparams, string Schema, string tablename, string columns = "*", string orderby = "", int limit = 0);
        DataTable SearchPage(List<CondParam> condparams, string Schema, string tablename, int page, int pageSize, ref Int64 TotalRow, bool checkApproveData = false, string columns = "*", string orderby = "ID asc");
        DataTable GetFirstById(string Schema, string tablename, object Id);
        DataTable GetListByField(string Schema, string tablename, string columnName, object Id);

        DataTable GetFirstByField(string Schema, string tablename, string columnName, object value, bool ignoreCase = false);
        DataRow GetFirstRowByField(string Schema, string tablename, string columnName, object value);
        DataTable GetFirst(string Schema, string tablename, string orderBy = "", params CondParam[] condparams);
        bool InsertData(string sql, DynamicParameters param, ref int Idinserted);
        bool InsertData(string sql, object param);
        bool UpdateData(string sql, object param);
        bool DeleteData(string sql, object param);
        bool DeleteDataByIDRecordLink(string schemaName, string tableName, params int[] idRecord);
        bool TruncateTable(string schemaName, string tableName);

        bool InsertTableLinkData(string schema, string dbName, LinkTableModel tableLinkData);
        int GetCount(string Schema, string tablename, params CondParam[] condparams);
        #endregion

        #region Get DataAPI
        Task<VMResultAPISearch> Getitems(VMParamAPI data);
        Task<VMResultAPISearch> Getitem(VMParamAPI data);
        #endregion
        VMResultAPISearch GetInfoTable(VMParamAPI data);
        #region Get Info Table

        #endregion
        bool IsExists(string Schema, string tablename, params CondParam[] condparams);
        DataTable ExecuteSql(string sql);
    }
}
