﻿using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels;
using DAS.Domain.Models.DAS;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace DAS.Application.Interfaces
{
    public interface IResetPasswordService : IBaseMasterService<ResetPassword>
    {
        Task<ServiceResult> ResetPasswordRequest(int userID, string token);
        Task<ServiceResult> CreateResetPasswordToken(int userID, string token);
        Task<ServiceResult> ResetPasswordAction(VMResetPassword model, string token);
    }
}
