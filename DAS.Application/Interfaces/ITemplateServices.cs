﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels;
using DAS.Domain.Models.DAS;
namespace DAS.Application.Interfaces
{
    public interface ITemplateServices : IBaseMasterService<Template>
    {
        Task<ServiceResult> Create(VMTemplate model);
        Task<ServiceResult> Update(VMTemplate model);
        Task<VMTemplate> GetTemplate(int id);
        Task<ServiceResult> DeleteListTemplate(int[] ids);
        Task<PaginatedList<VMTemplate>> SearchListTemplateConditionPagging(TemplateCondition condition);
        Task<IEnumerable<Template>> GetActiveTemplate();
        Task<IEnumerable<VMTemplateParam>> GetTemplateParam(int id);
    }
}
