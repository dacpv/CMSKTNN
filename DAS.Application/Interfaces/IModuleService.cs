﻿using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels;
using DAS.Domain.Models.DAS;
using DAS.Utility.CustomClass;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAS.Application.Interfaces
{
    public interface IModuleService
    {
        Task<IEnumerable<Module>> Gets();
        Task<bool> ReUpdateModule(int IDparent = 1, int IDChannel = 0);
        Task<IEnumerable<Module>> GetModuleForCurrentUser();
        Task<IEnumerable<Module>> GetsActive();
        Task<PaginatedList<VMModule>> SearchByConditionPagging(ModuleCondition Condition);
        Task<ServiceResult> Create(VMModule vmModule);
        Task<ServiceResult> Update(VMModule vmModule);
        Task<ServiceResult> Delete(int id);
        Task<ServiceResult> Deletes(IEnumerable<int> ids);
        Task<List<SelectListItemTree>> GetModuleByTree(VMModule vMModule);
        Task<List<SelectListItem>> GetListIcon(VMModule vMModule);
        Task<Module> Get(object id);
        public List<SelectListItem> GetModuleCodeDrd(string moduleCode);
    }
}
