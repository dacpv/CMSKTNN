﻿using DAS.Application.Models.CustomModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAS.Application.Interfaces
{
    public interface IIPAddressClientServices
    {
        Task<ServiceResult> GetPublicIPAddress();
    }
}
