﻿using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels;
using DAS.Domain.Models.DAS;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAS.Application.Interfaces
{
    public interface ISystemConfigServices : IBaseMasterService<SystemConfig>
    {
        Task<PaginatedList<VMSystemConfig>> SearchByConditionPagging(SystemConfigCondition condition, bool isExport = false);
        Task<VMUpdateSystemConfig> GetSystemConfig(int id);
        Task<ServiceResult> CreateSystemConfig(VMUpdateSystemConfig model);
        Task<ServiceResult> UpdateSystemConfig(VMUpdateSystemConfig model);
        Task<ServiceResult> DeleteSystemConfig(int id);
        Task<ServiceResult> Deletes(IEnumerable<int> ids);
        Task<IEnumerable<SystemConfig>> GetsActive();
    }
}
