﻿using DAS.Application.Enums.DasKTNN;
using DAS.Utility;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAS.Application.Models.CustomModels
{
    public class APIConfigConditionModel
    {
        public int IDColumn { get; set; }
        public int Operator { get; set; }
        public string Value { get; set; }
    }
}