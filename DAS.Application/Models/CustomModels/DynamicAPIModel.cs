﻿using DAS.Application.Enums.DasKTNN;
using DAS.Utility;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAS.Application.Models.CustomModels
{
    public class DynamicAPIModel
    {
        public int ID { get; set; }
        public int Type { get; set; }
        public string Url { get; set; }
        public string Method { get; set; }
    }
}