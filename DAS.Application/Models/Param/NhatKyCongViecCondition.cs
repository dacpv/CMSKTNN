﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAS.Application.Models.Param
{
   public class NhatKyCongViecCondition
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int ApprovedBy { get; set; }
        public int Status { get; set; }
        public int IDUser { get; set; }
        public string Keyword { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public NhatKyCongViecCondition()
        {
            PageIndex = 1;
            PageSize = 10;
        }
    }
}
