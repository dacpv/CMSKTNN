﻿using DAS.Application.Models.ViewModels.DasKTNN;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAS.Application.Models.Param
{
    public class ImportDataCondition
    {
        public IEnumerable<VMImportDataHeaderMapper> HeaderMappers { get; set; }
        public IFormFile FileImport { get; set; }
        public int IDTable { get; set; }
    }

}
