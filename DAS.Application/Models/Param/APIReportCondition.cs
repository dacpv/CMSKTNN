﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAS.Application.Models.Param
{
   public class APIReportCondition
    {
        public int apiId { get; set; }

        public string Keyword { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        
    }
}
