﻿using DAS.Domain.Models.DASKTNN;
using System;
using System.Collections.Generic;

namespace DAS.Application.Models.ViewModels.DasKTNN
{
    public class VMSharedAppStatisticIndex
    {
        public int? Index { get; set; }
        public string PreFix { get; set; }
        public PaginatedList<VMSharedAppStatistic> SharedAppStatistics { get; set; }
        public IEnumerable<SharedApp> SharedApps { get; set; } = new List<SharedApp>();
        public PaginatedList<SharedApp> SharedAppPagings { get; set; } = new PaginatedList<SharedApp>();
        public object ChartData { get; set; }
        public SharedAppStatsCondition  Condition { get; set; }
    }

    public class VMSharedAppStatistic : TanXuatTruyCapAPI
    {
        public int Count { get; set; }
        public string AppName { get; set; }
    }

    public class SharedAppStatsCondition
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int RecordCount { get; set; }
        public int IDSharedApp { get; set; }
        public string Keyword { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public SharedAppStatsCondition()
        {
            PageIndex = 1;
            PageSize = 10;
        }
    }


}
