﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAS.Application.Models.ViewModels.DasKTNN
{
    public class VMUpdateTableInfo :VMBaseTableInfo
    {
        public int IsConfig { get; set; } 
        public int IsDetail { get; set; } 

        public IEnumerable<VMUpdateColumnTable> ColumnTables { get; set; } //For detail, update, create
    }
}