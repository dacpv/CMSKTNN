﻿using DAS.Domain.Models.DASKTNN;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAS.Application.Models.ViewModels.DasKTNN
{
    public class VMImportData
    {
        public IEnumerable<TableInfo> Tables { get; set; }

        [Display(Name = "Bảng cần Import", Prompt = "Bảng cần Import")]
        [Required(ErrorMessage = "Vui lòng chọn bảng cần Import dữ liệu")]
        public int? IDTable { get; set; }


        [Display(Name = "Dòng header Import", Prompt = "Dòng header Import")]
        [Required(ErrorMessage = "Vui lòng nhập dòng header Import")]
        public int? HeaderRow { get; set; } = 1;

        [Display(Name = "Dòng dữ liệu import", Prompt = "Dòng dữ liệu import")]
        [Required(ErrorMessage = "Vui lòng nhập dòng dữ liệu Import")]
        public int? DataRow { get; set; } = 2;


        [Display(Name = "Số dòng dữ liệu tối đa", Prompt = "Số dòng dữ liệu tối đa")]
        public int? MaxRow { get; set; }

        [Display(Name = "Sheet import", Prompt = "Sheet import")]
        public int? Sheet { get; set; }

        [Display(Name = "Bỏ qua các dòng có tên trùng nhau", Prompt = "Bỏ qua các dòng có tên trùng nhau")]
        public int? IsUniqueName { get; set; }

        [Display(Name = "File import", Prompt = "File import")]
        [Required(ErrorMessage = "Vui lòng chọn file dữ liệu import")]
        public IFormFile FileImport { get; set; }
        public IEnumerable<VMImportDataHeaderMapper> HeaderMappers { get; set; }
        public Dictionary<int, string> Headers { get; set; }
        public Dictionary<int, string> DicSheet { get; set; }
        
        /// <summary>
        /// Import từ màn hình danh sách nhập liệu
        /// </summary>
        public int IsQuickImport { get; set; }
        public int IsMapCol { get; set; }

        //Các bảng được tham chiếu
        public List<TableInfo> TableRefs { get; set; }
    }
    public class VMImportDataHeaderMapper
    {
        public ColumnTableInfo ColumnTableInfo { get; set; }


        /// <summary>
        /// Danh sách cột tham chiếu mã & label
        /// </summary>
        public List<ColumnTableInfo> ColumnRefs { get; set; }

        /// <summary>
        /// ColumnTableInfo.ID
        /// </summary>
        public int ColumnID { get; set; } 

        /// <summary>
        /// Thứ tự cột excel cần map
        /// </summary>
        public int HeaderIndex { get; set; }

        /// <summary>
        /// Tiêu đề cột excel cần map
        /// </summary>
        public string HeaderTitle { get; set; } 

        /// <summary>
        /// Các bản ghi ko đc trùng nhau
        /// </summary>
        public byte IsUnique { get; set; }

        /// <summary>
        /// ID giá trị tham chiếu
        /// </summary>
        public int IDColumnRef { get; set; } 
    }
}