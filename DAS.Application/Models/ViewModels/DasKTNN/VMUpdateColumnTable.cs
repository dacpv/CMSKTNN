﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace DAS.Application.Models.ViewModels.DasKTNN
{
    public class VMUpdateColumnTable : VMBaseColumnTableInfo
    {
        public int? Index { get; set; }
        public bool IsDelete { get; set; }
        public bool IsUpdate { get; set; }
         
        public bool IsDetail { get; set; }
        
        public IEnumerable<SelectListItem> DlInputTypes { get; set; } = new List<SelectListItem>();
        public IEnumerable<SelectListItem> DlCategoryTypes { get; set; } = new List<SelectListItem>();
        public IEnumerable<SelectListItem> DlDefaultValueTypes { get; set; } = new List<SelectListItem>();

        public IEnumerable<VMColumnPathInfo> ColumnPaths { get; set; }
    }
}