﻿using DAS.Application.Models.CustomModels;
using DAS.Domain.Models.DASKTNN;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace DAS.Application.Models.ViewModels.DasKTNN
{
    public class VMTableLink : LinkTableModel
    {
        public int? Index { get; set; }
        public string PreFix { get; set; }
        public IEnumerable<SelectListItem> DlSchemaRef { get; set; } = new List<SelectListItem>();
        public IEnumerable<TableInfo> Tables { get; set; } = new List<TableInfo>();
    }
    public class VMTableLinkValue
    {
        public string databaseName { get; set; }
        public string tableName { get; set; }
        public string value { get; set; }

    }
}
