﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAS.Application.Models.ViewModels.DasKTNN
{
    public class VMUpdateColumnTableGroup :VMBaseColumnTableGroup
    {
        public int IsConfig { get; set; } 
        public int IsDetail { get; set; } 

        public IEnumerable<VMUpdateColumnTable> ColumnTables { get; set; } //For detail, update, create
    }
}