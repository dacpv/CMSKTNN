using DAS.Domain.Models.DASKTNN;
using System.Collections.Generic;

namespace DAS.Application.Models.ViewModels
{
    public class VMSharedApp
    {
        public bool IsUsed { get; set; }
        public int IsConfig { get; set; }
        public int? IsPrimaryKey { get; set; }
        public int Action { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public IEnumerable<SharedApp> sharedApps { get; set; } = new List<SharedApp>();
        public Dictionary<int, string> dictableInfos { get; set; }
        public SharedApp  sharedApp { get; set; }
    }
    public class SharedAppCondition
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public string Keyword { get; set; }
        public string DBName { get; set; }
        public int IDSchema { get; set; }

        public SharedAppCondition()
        {
            PageIndex = 1;
            PageSize = 10;
        }
    }
}