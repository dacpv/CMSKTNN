﻿using System.ComponentModel.DataAnnotations;

namespace DAS.Application.Models.ViewModels.DasKTNN
{
    public class VMBaseTableInfo
    {
        public int ID { get; set; }

        [Display(Name = "Tên CSDL", Prompt = "Tên CSDL")]
        [Required(ErrorMessage = "Tên trường không được để trống")]
        public int? IDSchema { get; set; }

        [Display(Name = "Nguồn dữ liệu", Prompt = "Nguồn dữ liệu")]
        public int IDSourceInfo { get; set; }

        [Display(Name = "Tên bảng dữ liệu", Prompt = "Tên bảng dữ liệu")]
        [Required(ErrorMessage = "Tên trường không được để trống")]
        [MaxLength(200, ErrorMessage = "{0} không được quá {1} ký tự")]
        public string Name { get; set; }

        [Display(Name = "Tên bảng trong CSDL", Prompt = "Tên bảng trong CSDL (Không có khoảng trắng và những ký tự đặc biệt)")]
        [Required(ErrorMessage = "Tên trường không được để trống")]
        [MaxLength(100, ErrorMessage = "{0} không được quá {1} ký tự")]
        [RegularExpression(@"^[a-zA-Z_$][a-zA-Z_$0-9]*$", ErrorMessage = "Tên trường nhập vào không hợp lệ (Không có khoảng trắng và những ký tự đặc biệt)")]
        public string DbName { get; set; }

        [Display(Name = "Nhãn hiển thị", Prompt = "Nhãn hiển thị")]
        [Required(ErrorMessage = "Tên trường không được để trống")]
        [MaxLength(100, ErrorMessage = "{0} không được quá {1} ký tự")]
        public string Title { get; set; }

        [Display(Name = "Mô tả", Prompt = "Mô tả")]
        [MaxLength(500, ErrorMessage = "{0} không được quá {1} ký tự")]
        public string Description { get; set; }

        [Display(Name = "Hiển thị ở menu")]
        public int IsMenu { get; set; }

        [Display(Name = "Kiểu hiển thị dữ liệu")]
        public int ListType{ get; set; }

        public int Status { get; set; }

        [Display(Name = "Yêu cầu duyệt dữ liệu")]
        public int IsRequireApprove { get; set; }

        [Display(Name = "Là bảng tạm")]
        public int IsTempTable { get; set; }

    }
}