﻿using DAS.Domain.Models.DASKTNN;
using System.Collections.Generic;

namespace DAS.Application.Models.ViewModels.DasKTNN
{
    public class VMIndexColumnTableGroup
    {
        public IEnumerable<TableInfo> TableInfos { get; set; } = new List<TableInfo>();
        public PaginatedList<VMColumnTableGroup> ColumnTableGroups { get; set; } 
        public ColumnTableGroupCondition SearchParam { get; set; } 
    }
}