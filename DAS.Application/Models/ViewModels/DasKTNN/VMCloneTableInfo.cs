﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAS.Application.Models.ViewModels.DasKTNN
{
    public class VMCloneTableInfo 
    {
        public int ID { get; set; } 
        public int IsCloneAllRecord { get; set; } 

        [Display(Name = "Tên bảng dữ liệu mới", Prompt = "Tên bảng dữ liệu mới")]
        [Required(ErrorMessage = "Tên trường không được để trống")]
        [MaxLength(200, ErrorMessage = "{0} không được quá {1} ký tự")]
        public string CloneName { get; set; }
         
        [Display(Name = "Tên bảng trong CSDL mới", Prompt = "Tên bảng trong CSDL (Không có khoảng trắng và những ký tự đặc biệt)")]
        [Required(ErrorMessage = "Tên trường không được để trống")]
        [MaxLength(100, ErrorMessage = "{0} không được quá {1} ký tự")]
        [RegularExpression(@"^[a-zA-Z_$][a-zA-Z_$0-9]*$", ErrorMessage = "Tên trường nhập vào không hợp lệ (Không có khoảng trắng và những ký tự đặc biệt)")]
        public string CloneDbName { get; set; }

        public IEnumerable<VMUpdateColumnTable> ColumnTables { get; set; } //For detail, update, create


        [Range(1, int.MaxValue, ErrorMessage = "Cấp độ sao chép không được nhỏ hơn 1")]
        public int? CloneLevel { get; set; }

        public int[] IDRecordClones { get; set; }
    }
}