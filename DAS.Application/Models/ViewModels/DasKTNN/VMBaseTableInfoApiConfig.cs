﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DAS.Application.Models.ViewModels.DasKTNN
{
    public class VMBaseTableInfoApiConfig
    {
        public int ID { get; set; }

        [Display(Name = "CSDL", Prompt = "CSDL")]
        public int? IDSchema { get; set; }

        [Display(Name = "Danh mục gốc", Prompt = "Danh mục gốc")]
        public int? IDTable { get; set; }

        [Display(Name = "Tên cấu hình", Prompt = "Tên cấu hình")]
        [Required(ErrorMessage = "Tên trường không được để trống")]
        [MaxLength(250, ErrorMessage = "{0} không được quá {1} ký tự")]
        public string Name { get; set; }

        [Display(Name = "Mô tả", Prompt = "Mô tả")]
        public string Description { get; set; }


        [Display(Name = "Ngày hiệu lực", Prompt = "Ngày hiệu lực")]
        [MaxLength(500, ErrorMessage = "{0} không được quá {1} ký tự")]
        public string StrStartDate { get; set; }

        [Display(Name = "Ngày hết hiệu lực", Prompt = "Ngày hết hiệu lực")]
        [MaxLength(500, ErrorMessage = "{0} không được quá {1} ký tự")]
        public string StrEndDate { get; set; }

        [Display(Name = "Cột hiển thị", Prompt = "Cột hiển thị")]
        [MaxLength(2000, ErrorMessage = "{0} không được quá {1} ký tự")]
        public string ViewColumn { get; set; }

        
        [Display(Name = "Điều kiện", Prompt = "Điều kiện")]
        public string Condition { get; set; }


        [Display(Name = "Sắp xếp theo", Prompt = "Sắp xếp theo")]
        [MaxLength(1000, ErrorMessage = "{0} không được quá {1} ký tự")]
        public string OrderBy { get; set; }


        [Display(Name = "Giới hạn bản ghi", Prompt = "Giới hạn bản ghi")]
        public int? Limit { get; set; }


        [Display(Name = "Ngày hiệu lực", Prompt = "Ngày hiệu lực")]
        public DateTime? StartDate { get; set; }

        [Display(Name = "Ngày hết hiệu lực", Prompt = "Ngày hết hiệu lực")]
        public DateTime? EndDate { get; set; }


        [Display(Name = "Loại API", Prompt = "Loại API")]
        [Required(ErrorMessage = "Tên trường không được để trống")]
        public int? Type { get; set; }

        [Display(Name = "Bảng chờ duyệt", Prompt = "Bảng chờ duyệt")]
        public int? IDTempTable { get; set; }

        [Display(Name = "Trạng thái", Prompt = "Trạng thái")]
        public int Status { get; set; }

        public string ParamJson { get; set; }
    }
}