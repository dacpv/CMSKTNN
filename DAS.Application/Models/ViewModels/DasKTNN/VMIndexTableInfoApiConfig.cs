﻿using DAS.Application.Models.CustomModels;
using DAS.Domain.Models.DASKTNN;
using System.Collections.Generic;

namespace DAS.Application.Models.ViewModels.DasKTNN
{
    public class VMIndexTableInfoApiConfig
    {
        public IEnumerable<TableInfo> TableInfos { get; set; } = new List<TableInfo>();
        public PaginatedList<VMTableInfoApiConfig> Configs { get; set; } 
        public TableInfoApiConfigCondition SearchParam { get; set; }
        public Dictionary<int, string> ApiTypes { get; set; }
        public DynamicAPIModel[] DynamicAPIs { get;  set; } = new DynamicAPIModel[0];
    }
}