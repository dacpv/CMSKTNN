﻿using DAS.Application.Models.CustomModels;
using DAS.Domain.Models.DASKTNN;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace DAS.Application.Models.ViewModels.DasKTNN
{
    public class VMTableInfoStatistic
    {
        public int? Index { get; set; }
        public string PreFix { get; set; }
        public IEnumerable<SelectListItem> DlSchemaRef { get; set; } = new List<SelectListItem>();

        public IEnumerable<VMTableItemStatistic> TableStatistics { get; set; }
        public object ChartData { get; set; }
        public PaginatedList<VMTableInfo> Tables { get; internal set; }
    }

    public class VMTableItemStatistic : VMTableInfo
    {
        public int RecordCount { get; set; }
    }
    public class VMSoucreInfoStatistic : SoucreInfo 
    {
        public int RecordCount { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
    }
    
}
