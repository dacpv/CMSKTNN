﻿using DAS.Domain.Models.DASKTNN;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace DAS.Application.Models.ViewModels.DasKTNN
{
    public class VMMainCollectionReport
    {
        public List<CollectionReport> lstCollectionReports { get; set; }
        public PaginatedList<CollectionReport> CollectionReports { get; set; }
        public CollectionReportCondition Condition { get; set; }
        public List<int> Months { get; set; }
        public List<int> Years { get; set; }
        public string DataChart { get; set; }
        public int? Index { get; set; }
        public string PreFix { get; set; }
        public IEnumerable<SelectListItem> DlSchemaRef { get; set; } = new List<SelectListItem>();

        public IEnumerable<VMSoucreInfoStatistic> TableStatistics { get; set; }
        public object ChartData { get; set; }
        public PaginatedList<VMSoucreInfoStatistic> Tables { get; internal set; }
    }

    public class CollectionReportCondition
    {
        public int Month { get; set; }
        public int Year { get; set; }
        public CollectionReportCondition()
        {
            PageIndex = 1;
            PageSize = 10;
        }
        public string Keyword { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}
