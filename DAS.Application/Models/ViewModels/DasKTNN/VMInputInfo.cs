﻿using DAS.Domain.Models.DAS;
using DAS.Domain.Models.DASKTNN;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Data;

namespace DAS.Application.Models.ViewModels.DasKTNN
{
    public class VMInputInfo
    {
        public IEnumerable<SelectListItem> DlSchemaRef { get; set; } = new List<SelectListItem>();
        public IEnumerable<TableInfo> Tables { get; set; } = new List<TableInfo>();
        public IEnumerable<ColumnTableInfo> columnTables { get; set; }
        public IEnumerable<ColumnTableInfo> columnTableAlls { get; set; }
        public IEnumerable<VMTableLink> tableLinks { get; set; } = new List<VMTableLink>();
        public IEnumerable<User> Users { get; set; }
        public InputInfoCondition inputInfoCondition { get; set; }
        public DataTable dataFirst { get; set; }
        public PaginatedList<DataTable> datapage { get; set; }
        public DataTable datas { get; set; }
        public DataTable dataDropdows { get; set; }
        public SchemaInfo schemaInfo { get; set; }
        public TableInfo tableInfo { get; set; }
        public bool IsUsed { get; set; }
        public int Action { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public int IsConfig { get; set; }
        public int? IsPrimaryKey { get; set; }
        public Dictionary<int, string> DictDataType { get; set; } = new Dictionary<int, string>();
        public Dictionary<int, string> DictSchema { get; set; } = new Dictionary<int, string>();
        public Dictionary<int, string> DictTable { get; set; } = new Dictionary<int, string>();
        public Dictionary<int, string> DictColumn { get; set; } = new Dictionary<int, string>();
        public List<TableOptionSelect> DataParent_Refer { get; set; }// Chưa tất cả dữ liệu tham chiếu và refer
        public int IDInserted { get; set; }
        public Dictionary<string, string> DicRecordData { get; set; }
        public bool IsTach { get;  set; }
        public int DfCreateType { get;  set; } 
    }
    public class TableOptionSelect
    {
        public string DBName { get; set; }
        /// <summary>
        /// Tên giá trị cột tham chiếu ( thường là ID)
        /// </summary>
        public string NameColumnValue { get; set; }
        /// <summary>
        /// Tên giá trị cột hiển thị tham chiếu ( thường là Name)
        /// </summary>
        public string NameColumnText { get; set; }

        public List<OptionSelect> Data { get; set; }
        public string Link { get; set; } //TODO sau này chuyển về select2 ajax
    }
    public class OptionSelect
    {
        public string Value { get; set; }
        public string Text { get; set; }
        //TODO Sẽ thêm các thuộc tính khác, vì phải chuyển select2 về Ajax
    }
    public class InputInfoCondition
    {
        public int isSearch { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; } = 10;
        public string Keyword { get; set; }
        public string IDParent { get; set; }
        public int Level { get; set; } = 0;
        public int IDTable { get; set; }
        public string View { get; set; }
        public string Title { get; set; }
        public int Status { get; set; } = -1;
        public int Sync { get; set; } = -1;
        public string OrderBy { get; set; }
        public string OrderType { get; set; }


        public InputInfoCondition()
        {
            PageIndex = 1;
            PageSize = 10;
        }
    }
}