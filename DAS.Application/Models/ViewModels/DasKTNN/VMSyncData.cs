﻿using DAS.Domain.Models.DASKTNN;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAS.Application.Models.ViewModels.DasKTNN
{
    public class VMSyncData
    {
        [Display(Name = "Bảng nhận dữ liệu", Prompt = "Bảng nhận dữ liệu")]
        [Required(ErrorMessage = "Vui lòng chọn bảng nhận dữ liệu")]
        public int? IDTable { get; set; }
        public int? IDColumnSync { get; set; }
        public int? IDColumnMap { get; set; }
        public int? IDTableMap { get; set; }
        public string TableName { get; set; }
        public string SchemaName { get; set; }
        public int? IDTableCurrent { get; set; }
        public IEnumerable<TableInfo> Tables { get; set; }
        public IEnumerable<VMSyncDataColumnMapper> ColumnMappers { get; set; }
        public List<ColumnTableInfo> ColumnTableSources { get; set; }
        public List<ColumnTableInfo> ColumnTableTargets { get; set; }
        public int[] IDRecords { get; set; }
        public int IDRecord { get; set; }
        public string Reason { get; set; }
        public List<string> ColumnNotMap { get; set; }
        public bool IsSuccess { get;  set; }
        public string Message { get;  set; }

        [Display(Name = "Prefix dữ liệu đồng bộ", Prompt = "Prefix dữ liệu đồng bộ")]
        public string Prefix { get;  set; }
    }

    public class VMSyncDataColumnMapper
    {
        public ColumnTableInfo ColumnTableTarget { get; set; }
        public int IDColumn { get; set; }
        public int ColumnID { get; set; }
        public string NameColumn { get; set; }
    }
}