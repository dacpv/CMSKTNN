using DAS.Domain.Models.DASKTNN;
using System.Collections.Generic;

namespace DAS.Application.Models.ViewModels.DasKTNN
{
    public class VMColumnTableGroup :VMBaseColumnTableGroup
    {
        public Dictionary<string, string> DictCode { get; set; } = new Dictionary<string, string>();
    }
    public class ColumnTableGroupCondition
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public string Keyword { get; set; }
        public string DBName { get; set; }
        public int IDSchema { get; set; }
        public int IDGroup { get; set; }

        public ColumnTableGroupCondition()
        {
            PageIndex = 1;
            PageSize = 10;
        }
    }
}