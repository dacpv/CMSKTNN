﻿using DAS.Domain.Models.DASKTNN;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAS.Application.Models.ViewModels.DasKTNN
{
    public class VMColumnPathInfo
    {
        public int? Index { get; set; }
        public int ID { get; set; }

        [Display(Name = "Tên CSDL", Prompt = "Tên CSDL")]
        [Required(ErrorMessage = "Tên trường không được để trống")]
        public int IDColumnTableInfo { get; set; }

        [Display(Name = "Tên CSDL", Prompt = "Tên CSDL")]
        [Required(ErrorMessage = "Tên trường không được để trống")]
        public int? IDColumnPath { get; set; }

        [Display(Name = "Tên bảng dữ liệu", Prompt = "Tên bảng dữ liệu")]
        [Required(ErrorMessage = "Tên trường không được để trống")]
        [MaxLength(50, ErrorMessage = "{0} không được quá {1} ký tự")]
        public string PathName { get; set; }

        public int Status { get; set; }
        public bool IsDetail { get; set; }
        public bool IsUpdate { get; set; }

        public IEnumerable<ColumnTableInfo> Columns { get; set; } = new List<ColumnTableInfo>();
    }
}