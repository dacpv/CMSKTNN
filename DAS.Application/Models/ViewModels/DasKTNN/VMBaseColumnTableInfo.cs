﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DAS.Application.Models.ViewModels.DasKTNN
{
    public class VMBaseColumnTableInfo
    {
        public int ID { get; set; }
        public int IDTable { get; set; }

        [Display(Name = "Tên cột", Prompt = "Tên cột")]
        [Required(ErrorMessage = "Tên trường không được để trống")]
        [MaxLength(200, ErrorMessage = "{0} không được quá {1} ký tự")]
        public string Name { get; set; }

        [Display(Name = "Tên cột trong CSDL", Prompt = "Tên cột trong CSDL")]
        [Required(ErrorMessage = "Tên trường không được để trống")]
        [MaxLength(100, ErrorMessage = "{0} không được quá {1} ký tự")]
        [RegularExpression(@"^[a-zA-Z_$][a-zA-Z_$0-9]*$", ErrorMessage = "{0} nhập vào không hợp lệ")]
        public string DbName { get; set; }

        [Display(Name = "Kiểu dữ liệu", Prompt = "Kiểu dữ liệu")]
        public int DataType { get; set; }

        [Display(Name = "Mô tả", Prompt = "Mô tả")] 
        [MaxLength(500, ErrorMessage = "{0} không được quá {1} ký tự")]
        public string Description { get; set; }

        [Display(Name = "Cho phép rỗng", Prompt = "Cho phép rỗng")]
        public int IsNullable { get; set; }

        [Display(Name = "Cho phép chọn nhiều", Prompt = "Cho phép chọn nhiều")]
        public int IsMultiple { get; set; }
        
        [Display(Name = "Cho phép tìm kiếm", Prompt = "Cho phép tìm kiếm")]
        public int IsSearchable { get; set; }
        
        [Display(Name = "Hiển thị trên lưới", Prompt = "Hiển thị trên lưới")]
        public int IsShowOnList { get; set; }
        
        [Display(Name = "Bắt buộc nhập", Prompt = "Bắt buộc nhập")]
        public int IsRequired { get; set; }

        [Display(Name = "Khóa chính", Prompt = "Khóa chính")]
        public int IsPrimaryKey { get; set; }

        [Display(Name = "Tự sinh mã", Prompt = "Tự sinh mã")]
        public int IsIdentity { get; set; }

        [Display(Name = "Giá trị tối thiểu", Prompt = "Giá trị tối thiểu")]
        public int? MinVal { get; set; }

        [Display(Name = "Giá trị tối đa", Prompt = "Giá trị tối đa")]
        public int? MaxVal { get; set; }

        [Display(Name = "Độ dài tối thiểu", Prompt = "Độ dài tối thiểu")]
        public int? MinLen { get; set; }

        [Display(Name = "Độ dài tối đa", Prompt = "Độ dài tối đa")]
        public int? MaxLen { get; set; }

        [Display(Name = "Giá trị mặc định", Prompt = "Giá trị mặc định")]
        [MaxLength(250, ErrorMessage = "{0} không được quá {1} ký tự")]
        public string DefaultValue { get; set; }

        [Display(Name = "Thứ tự", Prompt = "Thứ tự")]
        public int Weight { get; set; }

        [Display(Name = "Schema tham chiếu", Prompt = "Schema tham chiếu")]
        public int? IDSchemaRef { get; set; }

        [Display(Name = "Bảng dữ liệu tham chiếu", Prompt = "Bảng dữ liệu tham chiếu")]
        public int? IDTableRef { get; set; }

        [Display(Name = "Cột dữ liệu tham chiếu", Prompt = "Cột dữ liệu tham chiếu")]
        public int? IDColRef { get; set; }

        [Display(Name = "Cột dữ liệu tham chiếu hiển thị", Prompt = "Cột dữ liệu tham chiếu hiển thị")]
        public int? IDColTextRef { get; set; }

        [Display(Name = "Nhóm cột", Prompt = "Nhóm cột")]
        public int? IDColumnTableGroup { get; set; }

        public int IsSystem { get; set; }

        public int IsDefault { get; set; }
        public int IsSubColumn { get; set; }
        public int Status { get; set; }


        [Display(Name = "Tiền tố giá trị tự sinh", Prompt = "Tiền tố giá trị tự sinh")]
        public string IdentityPrefix { get; set; }

        [Display(Name = "Kiểu sắp xếp", Prompt = "Kiểu sắp xếp")]
        public string OrderType { get; set; }

        [Display(Name = "Sắp xếp mặc định", Prompt = "Sắp xếp mặc định")]
        public int IsOrder { get; set; }


        [Display(Name = "Dữ liệu gợi ý", Prompt = "Dữ liệu gợi ý (mỗi gợi ý trên một dòng)")]
        public string Suggestion { get; set; }


        [Display(Name = "Giá trị tự tăng từ", Prompt = "Giá trị tự tăng từ")]
        public int? IdentityIndex { get; set; } = 0;
    }
}