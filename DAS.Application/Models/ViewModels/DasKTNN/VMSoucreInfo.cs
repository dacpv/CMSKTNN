using DAS.Domain.Models.DASKTNN;
using System.Collections.Generic;

namespace DAS.Application.Models.ViewModels
{
    public class VMSoucreInfo
    {
        public bool IsUsed { get; set; }
        public int IsConfig { get; set; }
        public int? IsPrimaryKey { get; set; }
        public int Action { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public IEnumerable<SoucreInfo> soucreInfos { get; set; } = new List<SoucreInfo>();
        public Dictionary<int, string> dictableInfos { get; set; }
        public SoucreInfo soucreInfo { get; set; }
    }
    public class VMSourceInfoAPI
    {
        public string TypeNameSource { get; set; }
        public int TypeSource { get; set; }
        public List<VMSourceInfoAPIModel> SourceInfors { get; set; }

    }
    public class VMSourceInfoAPIModel
    {
        public string MaNguon { get; set; }
        public string TenNguon { get; set; }
        public string TenFile { get; set; }
        public string DuongDanFile { get; set; }
        public string IP { get; set; }
        public string DatabaseName { get; set; }
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public string MoTa { get; set; }
        public string ChuoiKetNoi { get; set; }
        public string Port { get; set; }
        public string TokenUrl { get; set; }
        public string Url { get; set; }
        public List<VMSourceInfoAPIModelMap> modelmaping { get; set; }
    }
    public class VMSourceInfoAPIModelMap
    {
        public string databasename { get; set; }
        public string tablename { get; set; }

    }
    public class SoucreInfoCondition
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public string Keyword { get; set; }
        public string DBName { get; set; }
        public int IDSchema { get; set; }
        public int Type { get; set; }

        public SoucreInfoCondition()
        {
            PageIndex = 1;
            PageSize = 10;
        }
    }
}