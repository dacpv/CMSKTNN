﻿using DAS.Domain.Models.DASKTNN;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;

namespace DAS.Application.Models.ViewModels.DasKTNN
{
    public class TabLefTree
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public int IDTable { get; set; }
    }
    public class TableLeftTree
    {
        public string ID { get; set; }
        public string TieuDe { get; set; }
        public int Level { get; set; }
        public int IDTable { get; set; }
        public int IDSchema { get; set; }
        public int IsChild { get; set; }
        public string View { get; set; }

    }
    public class LienKetNguoc
    {
        public string TableName { get; set; }
        public string TenDanhMuc { get; set; }
        public string MaDanhMuc { get; set; }
    }
    public class VMLienKetNguoc
    {
        public List<LienKetNguoc> LienKets { get; set; }
        public int PageIndex { get; set; }
        public bool IsLoadView { get; set; }
    }
    public class VMLeftTree 
    {
        public InputInfoCondition InputInfoCondition { get; set; } = new InputInfoCondition();
        public IEnumerable<ColumnTableInfo> columnTables { get; set; }
        public DataTable dataFirst { get; set; }
        public List<TableOptionSelect> DataParent_Refer { get; set; }
        public TableInfo TableInfo { get; set; }
        public SchemaInfo SchemaInfo { get; set; }
        public List<TableLeftTree> TableLeftTrees { get; set; }
        public List<TabLefTree> TabLefTrees { get; set; }
        public IEnumerable<SelectListItem> Schemas { get; set; } = new List<SelectListItem>();
        public IEnumerable<TableInfo> Tables { get; set; } = new List<TableInfo>();
        public IEnumerable<VMTableLink> tableLinks { get; set; } = new List<VMTableLink>();
        public bool IsLoadView { get; set; }
        public VMLienKetNguoc VMLienKetNguoc { get; set; }
    }
}