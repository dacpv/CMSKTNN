﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections;
using System.Collections.Generic;
using System.Data;

namespace DAS.Application.Models.ViewModels.DasKTNN
{
   public class QueryAPI
    {
        public string column { get; set; }
        public string operators { get; set; }
        public string value { get; set; }
        public string clause { get; set; }

    }
    public class VMParamAPI 
    {
        public string id { get; set; }
        public string apiid { get; set; }

        public string databaseName { get; set; }
        public string tableName { get; set; }
        public string appname { get; set; }
        public string urlcallback { get; set; }

        public int page { get; set; }
        public int pageSize { get; set; }
        public List<string> viewColumns { get; set; }
        public Hashtable properties { get; set; }   
        public List<string> orderBy { get; set; }
        public int rowLimit { get; set; }
        public List<QueryAPI> query { get; set; }
        public QueryAPI[] query1 { get; set; }

    }
    public class VMScriptParamAPI
    {
        public string id { get; set; }
        public string column { get; set; }
        public Hashtable values { get; set; }
    }
    public class VMResultAPISearch
    {
        public DataTable Result { get; set; }
        public DataTable LstTach { get; set; }
        public DataTable LsTGop { get; set; }

        public long TotalItems { get; set; }
        public string Message { get; set; }
        public bool IsSuccess { get; set; }
    }
}