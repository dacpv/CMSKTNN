using DAS.Application.Models.Param;
using DAS.Domain.Models.DASKTNN;
using System.Collections.Generic;

namespace DAS.Application.Models.ViewModels.DasKTNN
{
    public class VMAPIReport
    {
        public List<APIMangeReport> APIMangeReports { get; set; }
        public List<API> APIs { get; set; }
        public APIReportCondition Param { get; set; }
    }


}