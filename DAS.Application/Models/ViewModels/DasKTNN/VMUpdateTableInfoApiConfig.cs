﻿using DAS.Application.Models.CustomModels;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAS.Application.Models.ViewModels.DasKTNN
{
    public class VMUpdateTableInfoApiConfig :VMBaseTableInfoApiConfig
    {
        [Display(Name = "Script SQL", Prompt = "Script SQL")]
        public string ScriptSQL { get; set; } 

        public int IsConfig { get; set; } 
        public int IsShareAllApp { get; set; } 
        public int[] IDSharedApps { get; set; } 
         
        public IEnumerable<APIConfigConditionModel> Conditions { get; set; } 
    }
}