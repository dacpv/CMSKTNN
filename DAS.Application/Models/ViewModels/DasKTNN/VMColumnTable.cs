﻿using DAS.Domain.Models.DASKTNN;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DAS.Application.Models.ViewModels.DasKTNN
{
    public class VMColumnTableInfo : VMBaseColumnTableInfo
    {


        public int? Index { get; set; }
        public bool IsDelete { get; set; }
        public bool IsUpdate { get; set; }

        public bool IsDetail { get; set; }


        public IEnumerable<SelectListItem> DlSchemaRef { get; set; } = new List<SelectListItem>();
        public IEnumerable<SelectListItem> DlColGroups { get; set; } = new List<SelectListItem>();
        public IEnumerable<TableInfo> Tables { get; set; } = new List<TableInfo>();
        public IEnumerable<ColumnTableInfo> Columns { get; set; } = new List<ColumnTableInfo>();
        public IEnumerable<VMColumnPathInfo> ColumnPaths { get; set; } = new List<VMColumnPathInfo>();
        public IEnumerable<SelectListItem> DlDataType { get; set; } = new List<SelectListItem>();
        public IEnumerable<SelectListItem> DlTableInfo { get; set; } = new List<SelectListItem>();
        public IEnumerable<ColumnTableInfo> FiendColumns { get; set; }
        public VMColumnTableInfo ColumnRef { get; set; }
    }
}