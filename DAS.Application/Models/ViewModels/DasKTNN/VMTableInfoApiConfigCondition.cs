using DAS.Application.Models.CustomModels;
using DAS.Domain.Models.DASKTNN;
using System.Collections.Generic;

namespace DAS.Application.Models.ViewModels.DasKTNN
{
    public class VMTableInfoApiConfigConditionConfig
    {
        public int? Index { get; set; }
        public bool IsDetail { get; set; }
        public bool IsUpdate { get; set; }
        public TableInfoApiConfig TableInfoApiConfig { get; set; } = new TableInfoApiConfig();
        public APIConfigConditionModel Condition { get; set; } = new APIConfigConditionModel();
        public IEnumerable<ColumnTableInfo> Columns { get; set; } = new List<ColumnTableInfo>();
        public Dictionary<int, string> CondOperators { get; set; }
    }
}