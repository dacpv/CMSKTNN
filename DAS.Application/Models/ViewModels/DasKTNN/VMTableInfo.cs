using DAS.Domain.Models.DASKTNN;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAS.Application.Models.ViewModels.DasKTNN
{
    public class VMTableInfo : VMBaseTableInfo
    {
        public IEnumerable<VMColumnTableInfo> ColumnTables { get; set; } = new List<VMColumnTableInfo>();
        public bool IsUsed { get; set; }
        public int IsConfig { get; set; }
        public int? IsPrimaryKey { get; set; }
        public Dictionary<int, string> DictDataType { get; set; } = new Dictionary<int, string>();
        public Dictionary<int, string> DictSchema { get;  set; } = new Dictionary<int, string>();
        public IEnumerable<TableInfo> Tables { get; set; } = new List<TableInfo>();
        public IEnumerable<ColumnTableInfo> Columns { get; set; } = new List<ColumnTableInfo>();
        public SchemaInfo Schema { get; set; }
        public IEnumerable<ColumnTableGroup> ColGroups { get;  set; } = new List<ColumnTableGroup>();

        [Display(Name = "Tên bảng dữ liệu mới", Prompt = "Tên bảng dữ liệu mới")]
        [MaxLength(200, ErrorMessage = "{0} không được quá {1} ký tự")]
        public string CloneName { get; set; }

        [Display(Name = "Tên bảng trong CSDL mới", Prompt = "Tên bảng trong CSDL mới")]
        [MaxLength(200, ErrorMessage = "{0} không được quá {1} ký tự")]
        public string CloneDbName { get; set; }


        [Display(Name = "Giới hạn cấp độ dữ liệu", Prompt = "Không giới hạn, để trống")]
        public int? CloneLevel { get; set; }

        public IEnumerable<SoucreInfo> SourceInfos { get;  set; }
        public bool IsDanhMucDungChung { get;  set; }
        public bool IsReadOnly { get; set; }
    }
    public class TableInfoCondition
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public string Keyword { get; set; }
        public string DBName { get; set; }
        public int IDSchema { get; set; }
        public int IDGroup { get; set; }
        public int IDSource { get; set; }
        public int[] IDSources { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public TableInfoCondition()
        {
            PageIndex = 1;
            PageSize = 10;
        }
    }
}