using DAS.Domain.Models.DASKTNN;
using System.Collections.Generic;

namespace DAS.Application.Models.ViewModels
{
    public class VMGroupInfo
    {
        public bool IsUsed { get; set; }
        public int IsConfig { get; set; }
        public int? IsPrimaryKey { get; set; }
        public int Action { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public IEnumerable<GroupInfo> groupInfos { get; set; } = new List<GroupInfo>();
        public IEnumerable<TableInfo> tableInfos { get; set; } = new List<TableInfo>();
        public Dictionary<int, string> dictableInfos { get; set; }
        public IEnumerable<SchemaInfo> schemaInfos { get; set; } = new List<SchemaInfo>();
        public IEnumerable<GroupTableInfo> groupTables { get; set; } = new List<GroupTableInfo>();
        public GroupInfo groupInfo { get; set; }
    }
    public class GroupInfoCondition
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public string Keyword { get; set; }
        public string DBName { get; set; }
        public int IDSchema { get; set; }

        public GroupInfoCondition()
        {
            PageIndex = 1;
            PageSize = 10;
        }
    }
}