﻿using System.ComponentModel.DataAnnotations;

namespace DAS.Application.Models.ViewModels.DasKTNN
{
    public class VMBaseColumnTableGroup
    {
        public int ID { get; set; }

   
        [Display(Name = "Tên nhóm", Prompt = "Tên nhóm")]
        [Required(ErrorMessage = "Tên trường không được để trống")]
        [MaxLength(250, ErrorMessage = "{0} không được quá {1} ký tự")]
        public string Name { get; set; }

        
        [Display(Name = "Loại nhóm", Prompt = "Loại nhóm")]
        [MaxLength(50, ErrorMessage = "{0} không được quá {1} ký tự")]
        public string Code { get; set; }
    }
}