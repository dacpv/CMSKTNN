using DAS.Application.Models.CustomModels;
using DAS.Domain.Models.DASKTNN;
using DAS.Utility;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAS.Application.Models.ViewModels.DasKTNN
{
    public class VMTableInfoApiConfig : VMBaseTableInfoApiConfig
    {
        [Display(Name = "Script SQL", Prompt = "Script SQL")]
        public string ScriptSQL { get; set; }

        public bool IsDetail { get; set; }
        public bool IsUpdate { get; set; }
        public IEnumerable<TableInfo> Tables { get; set; } = new List<TableInfo>();
        public IEnumerable<TableInfo> TempTables { get; set; } = new List<TableInfo>();
        public SchemaInfo Schema { get; set; } = new SchemaInfo();
        public IEnumerable<TableInfoApiConfig> TableInfoApiConfigs { get; set; } = new List<TableInfoApiConfig>();
        public IEnumerable<ColumnTableInfo> Columns { get; set; } = new List<ColumnTableInfo>();
        public Dictionary<int, string> CondOperators { get; set; }

        public int[] ArrViewColumn
        {
            get
            {
                return ViewColumn.IsEmpty() ? new int[0] : Utils.ParserInts(ViewColumn, ',');
            }
        }

        public List<APIConfigConditionModel> Conditions
        {
            get
            {
                return Condition.IsEmpty() ? new List<APIConfigConditionModel>() : Utils.Deserialize<List<APIConfigConditionModel>>(Condition);
            }
        }

        public Dictionary<int, string> ApiTypes { get; set; }
        public IEnumerable<ColumnTableGroup> ColGroups { get; set; } = new List<ColumnTableGroup>();
        public IEnumerable<SharedApp> SharedApps { get;  set; } = new List<SharedApp>();
        public IEnumerable<TableInfoApiConfigSharedApp> SharedAppConfigs { get;  set; } = new List<TableInfoApiConfigSharedApp>();
    }
    public class TableInfoApiConfigCondition
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public string Keyword { get; set; }
        public string DBName { get; set; }
        public int IDSchema { get; set; }
        public int IDTable { get; set; }

        public TableInfoApiConfigCondition()
        {
            PageIndex = 1;
            PageSize = 10;
        }
    }
}