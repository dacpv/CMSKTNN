﻿using System.Collections.Generic;

namespace DAS.Application.Models.ViewModels.DasKTNN
{
    public class VMNotificationConfig
    {
        public NotificationConfigCondition Condition { get; set; }
        public PaginatedList<NotificationConfig> NotificationConfigs { get; set; }
        public NotificationConfig NotificationConfig { get; set; } = new NotificationConfig();
        public Dictionary<int, string> lstNotificationConfig { get; set; }
        public int Status { get; set; }
    }

    public class NotificationConfig
    {
        public int ID { get; set; }
        public int IDNotification { get; set; }
        public string Name { get; set; }
        public int ActiveNotification { get; set; }
        public int ActiveEmail { get; set; }
        public string DescriptionForNotification { get; set; }
        public int Action { get; set; }
        public string Url { get; set; }
    }

    public class NotificationConfigCondition
    {
        public NotificationConfigCondition()
        {
            PageIndex = 1;
            PageSize = 10;
        }
        public string Keyword { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}
