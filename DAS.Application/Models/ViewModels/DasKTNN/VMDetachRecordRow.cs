﻿using DAS.Domain.Models.DASKTNN;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Data;

namespace DAS.Application.Models.ViewModels.DasKTNN
{
    public class VMDetachRecordRow
    {
        public int? Index { get; set; }
        public TableInfo TableInfo { get; set; }
        public DataTable Record { get; set; }
        public IEnumerable<ColumnTableInfo> ColumnTables { get; set; }
        public IEnumerable<TableInfo> Tables { get; set; } = new List<TableInfo>();
        public IEnumerable<SelectListItem> DlSchemaRef { get; set; } = new List<SelectListItem>();
    }
}