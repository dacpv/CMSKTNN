﻿using DAS.Domain.Models.Abstractions;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAS.Application.Models.ViewModels
{
    public class HomeCondition
    {
        public int Type { get; set; }
    }
}
