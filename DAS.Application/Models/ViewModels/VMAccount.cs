﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;
using DAS.Domain.Models.Abstractions;

namespace DAS.Application.Models.ViewModels
{
    public class VMAccount
    {
        public int userID { get; set; } = 0;
        public string AccountName { get; set; }
        public string UserName { get; set; }
        [Required(ErrorMessage = "Mật khẩu không được để trống")]
        [DisplayName("Mật khẩu hiện tại")]
        [MinLength(6, ErrorMessage = "Mật khẩu phải có ít nhất 6 ký tự")]
        [MaxLength(255, ErrorMessage = "Mật khẩu có tối đa 255 ký tự")]
        public string OldPassword { get; set; }
        [Required(ErrorMessage = "Mật khẩu mới không được để trống")]
        [DisplayName("Mật khẩu mới")]
        [MinLength(6, ErrorMessage = "Mật khẩu phải có ít nhất 6 ký tự")]
        [MaxLength(255, ErrorMessage = "Mật khẩu có tối đa 255 ký tự")]
        public string Password { get; set; }
        [Required(ErrorMessage = "Xác nhận mật khẩu mới không được để trống")]
        [DisplayName("Xác nhận mật khẩu mới")]
        [MinLength(6, ErrorMessage = "Mật khẩu phải có ít nhất 6 ký tự")]
        [Compare(nameof(Password), ErrorMessage = "Nhập lại mật khẩu không chính xác")]
        [MaxLength(255, ErrorMessage = "Mật khẩu có tối đa 255 ký tự")]
        public string ConfirmPassword { get; set; }
        public string Token { get; set; }
        public string Url { get; set; }
        public string Email { get; set; }
    }
    public class Rootobject
    {
        public Envelope Envelope { get; set; }
    }

    public class Envelope
    {
        public HeaderXML Header { get; set; }
        public Body Body { get; set; }
    }

    public class HeaderXML
    {
        public Reference Reference { get; set; }
        public From From { get; set; }
        public string To { get; set; }
        public Subject Subject { get; set; }
    }

    public class Reference
    {
        public string version { get; set; }
        public string messageId { get; set; }
        public string requestType { get; set; }
        public string numberOfReciver { get; set; }
    }

    public class From
    {
        public string name { get; set; }
        public string identity { get; set; }
        public string appServicesUrl { get; set; }
    }

    public class Subject
    {
        public string identity { get; set; }
        public string reference { get; set; }
        public DateTime created { get; set; }
        public string actionCode { get; set; }
        public DateTime sendDate { get; set; }
    }

    public class Body
    {
        public Content Content { get; set; }
    }

    public class Content
    {
        public appResult appResult { get; set; }
    }
    public class appResult
    {
        public Datarows dataRows { get; set; }
        public string appCode { get; set; }
        public string appName { get; set; }
        public string resultCode { get; set; }
    }
    public class Datarows
    {
        public List<RowDataXml> row { get; set; }
    }
    public class RowDataXml
    {
        public string MaNguoiDung { get; set; }
        public string MaDonVi { get; set; }
        public string HoVaTen { get; set; }
        public string SoThe { get; set; }
        public string SoCMTND { get; set; }
        public string MaChucVuHanhChinh { get; set; }
        public string TenChucVuHanhChinh { get; set; }
        public string DienThoai { get; set; }
        public string Email { get; set; }
        public int TrangThai { get; set; }
        public string GhiChu { get; set; }
        public string TenDangNhap { get; set; }
        public string NgayNghiHuu { get; set; }
        public string NgayThoiViec { get; set; }
        public string NgayRaKhoiNganh { get; set; }
        public string NgayChamDutHD { get; set; }
        public string MaPhongBan { get; set; }
    }
}
