﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;
using DAS.Domain.Models.Abstractions;
using DAS.Domain.Models.DasCongViec;
using DAS.Utility;

namespace DAS.Application.Models.ViewModels
{
    public class NhatKyCongViecModel
    {
        public PaginatedList<VMNhatKyCongViec> NhatKyCongViecs { get; set; }
        public List<VMCongViec> CongViecs { get; set; }
        public VMNhatKyCongViec NhatKyCongViec { get; set; }
        public List<VMChiTietNhatKyCongViec> ChiTietNhatKyCongViecs { get; set; }
        public List<VMUser> VMUsers { get; set; }

    }
    public class VMNhatKyCongViec : NhatKyCongViec
    {
        public List<VMChiTietNhatKyCongViec> VMChiTietNhatKyCongViecs { get; set; } = new List<VMChiTietNhatKyCongViec>();
        public List<VMCongViec> CongViecAll { get; set; }
        public List<VMUser> VMUserAll { get; set; }
        public CongViecTonModel CongViecTonModel { get; set; }
        public bool IsUpdate { get; set; }
        public bool IsDetail { get; set; }
        public bool IsDelete { get; set; }
        public bool IsSendApprove { get; set; }
        public bool IsApprove { get; set; }
    }
    public class VMChiTietNhatKyCongViec : ChiTietNhatKy
    {
        public VMCongViec VMCongViec { get; set; }
        public List<VMChiTietNhatKyNguoiXL> VMChiTietNhatKyNguoiXL { get; set; }
        public List<VMCongViec> VMCongViecAll { get; set; }// danh sách All tất cả công việc
        public List<VMUser> VMUserAll { get; set; }//
        //Liên quan đến view
        public bool IsUpdate { get; set; }
        public bool IsDetail { get; set; }
        public bool IsDelete { get; set; }
        public string IDUser { get; set; }
        public List<int> IDUsers
        {
            get
            {
                var temp = 0;
                if (int.TryParse(IDUser, out temp))
                {
                    return new List<int> { temp };
                }
                return Utils.Deserialize<List<int>>(this.IDUser);
            }
        }
    }
    public class VMCongViec
    {
        public int ID { get; set; }
        public int Index { get; set; }
        public string Ten { get; set; }
        public string LoaiCongViec { get; set; }
        public float DiemSo { get; set; }
        public string KieuCongViec { get; set; }
        public int IDLoaiCongViec { get; set; }
        public int IDKieuCongViec { get; set; }
    }
    public class VMChiTietNhatKyNguoiXL
    {
        public int ID { get; set; }
        public int IDNhatKyCongViec { get; set; }
        public int IDChiTietNhatKy { get; set; }
        public int IDUser { get; set; }
        public string TenNguoiXL { get; set; }

    }
    public class CongViecTonModel
    {
        public DateTime Ngay { get; set; }
        public int IDNhatKyCongViec { get; set; }
        public string TenCa { get; set; }
        /// <summary>
        /// Tên người lập công việc tồn
        /// </summary>
        public string TenNguoi { get; set; }
        public List<VMCongViecTon> VMCongViecTons { get; set; }
    }
    public class VMCongViecTon:ChiTietNhatKy
    {
        public string TenCongViec { get; set; }

    }
    public class VMBaoCaoChiTiet 
    {
        public int ID { get; set; }
        public int IDNhatKyCongViec { get; set; }
        public int IDCongViec { get; set; }
        public string TenCongViec { get; set; }
        public int IDLoaiCongViec { get; set; }
        public int IDKieuCongViec { get; set; }
        public string TenLoaiCongViec { get; set; }
        public string TenKieuCongViec { get; set; }
        public float Diem { get; set; }
        public DateTime Ngay { get; set; }
        public int Ca { get; set; }
        public string TenCa { get; set; }
        public int SoGio { get; set; }//Số giờ làm việc trong 1 ca
        public int IDUser { get; set; }
        public string TenUser { get; set; }
        public string NoiDungTacDong { get; set; }
        public int ApprovedBy { get; set; }
        public string ApprovedByName { get; set; }
        public string ApprovedStatusName { get; set; }
        public int ApprovedStatus { get; set; }
        public string ApprovedNote { get; set; }
        public DateTime? Approved { get; set; }
        public int Status { get; set; }
    }
    public class VMBaoCaoTongHop
    {
        public float TongDiemCR { get; set; }
        public float TongDiemTT1 { get; set; }
        public float TongDiemTT2 { get; set; }
        public float TongDiemTT3 { get; set; }
        public float TongDiemTT4 { get; set; }
        public int TongCongViec { get; set; }
        public float PTTheoGio { get; set; }
        public float PTTheoCongViec{ get; set; }
        public float PTTheoDiemCongViec { get; set; }
        public float TongDiem { get; set; }
        public int TongSoGio { get; set; }//Số giờ làm việc trong 1 ca
        public int TongSoGioCa1 { get; set; }//Số giờ làm việc trong ca 1
        public int TongSoGioCa2 { get; set; }//Số giờ làm việc trong ca 1
        public int IDUser { get; set; }
        public string TenUser { get; set; }
    }
}
