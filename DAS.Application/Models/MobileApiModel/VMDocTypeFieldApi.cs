﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAS.Application.Models.MobileApiModel
{
    public class VMDocTypeFieldApi
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int InputType { get; set; }

    }
}
