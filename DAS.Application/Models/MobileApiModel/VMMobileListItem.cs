﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAS.Application.Models.MobileApiModel
{
    public class VMMobileListItem
    {
        public string Value { get; set; }
        public string Text { get; set; }
    }
}
