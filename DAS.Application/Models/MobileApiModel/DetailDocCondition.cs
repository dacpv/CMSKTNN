﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAS.Application.Models.MobileApiModel
{
    public class DetailDocCondition
    {
        public int IdDoc { get; set; }
        public List<int> ListCartIdDoc { get; set; }
    }
}
