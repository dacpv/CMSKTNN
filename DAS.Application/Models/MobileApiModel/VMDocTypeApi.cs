﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAS.Application.Models.MobileApiModel
{
    public class VMDocTypeApi
    {
        public int Type { get; set; }
        public string Name { get; set; }
    }
}
