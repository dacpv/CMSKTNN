﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAS.Application.Enums
{
    public static class EnumProfileCategory
    {
        public enum NodeType
        {
            Category = 0,
            Doc = 1,
            Profile = 2,
        }
    }
}
