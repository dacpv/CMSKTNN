﻿using System.ComponentModel;

namespace DAS.Domain.Enums
{
    public static class EnumCommon
    {
        public enum Status
        {
            [Description("Không hiệu lực")]
            InActive = 0,
            [Description("Hiệu lực")]
            Active = 1,
        }

        public enum IsShow
        {
            [Description("Hiển thị")]
            Show = 1,
            [Description("Ẩn")]
            Hidden = 0,
        }
        public enum TypeAccount
        {
            [Description("General")]
            General = 0,
            [Description("SSO")]
            SSO = 1,
        }
    }
}