﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DAS.Application.Enums
{
    public static class EnumModule
    {
        public enum Code
        {
            #region Quản trị hệ thống
            [Description("Quản lý cơ quan")]
            M20010 =1,
            [Description("Quản lý tài khoản quản trị cơ quan")]
            S9023 =2,
            [Description("Nhật ký người dùng")]
            NKND =3,
            [Description("Nhật ký hệ thống")]
            NKHT =4,
            [Description("Cấu hình tham số cơ quan")]
            CHTS =5,
            
            [Description("Quản lý menu")]
            QLMENU = 48, //chưa sử dụng

            [Description("Quản lý đơn vị")]
            M20020 =6,
            [Description("Quản lý nhóm quyền")]
            S9030 =7,
            [Description("Quản lý nhóm người dùng")]
            S9010 =8,
            [Description("Quản lý người dùng")]
            S9020 =9,
            [Description("Quản lý phân quyền")]
            QLPQ =10,//Chưa sử dụng
           
            #endregion
           
            #region Quản trị danh mục chung
            [Description("Chức vụ")]
            M20030 =14,
            #endregion
            
            
            #region csdl
            [Description("Cơ sở dữ liệu")]
            SCHEMA = 50, 
            [Description("Quản lý bảng dữ liệu")]
            TABLEINFO = 51, 
            [Description("Quản lý nhập liệu")]
            INPUTINFO = 52,
            [Description("Quản lý phê duyệt dữ liệu")]
            INPUTINFOTEMP = 53,
            [Description("Báo cáo Api")]
            REPORTAPI = 54,
            [Description("Quản lý mã nguồn")]
            SOUCREINFO = 55,
            [Description("Quản lý ứng dụng chia sẻ")]
            SHAREDAPP = 58,
            [Description("Báo cáo thống kê dữ liệu")]
            TABLERECORDSTATS = 56,
            [Description("Báo cáo thống kê về lỗi hệ thống")]
            SYSTEMLOGERROL = 57,

            [Description("Báo cáo tần xuất ứng dụng chia sẻ")]
            SHAREDAPPFREQUENCYSTATS = 59,
            #endregion
        }
    }
}
