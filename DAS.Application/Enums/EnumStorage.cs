﻿using System.ComponentModel;

namespace DAS.Domain.Enums
{
    public static class EnumStorage
    {
        public enum SearchProfileType
        {
            [Description("Hồ sơ")]
            Profile = 0,
            [Description("Thành phần hồ sơ")]
            ProfileElement = 1,
        }
    }
}