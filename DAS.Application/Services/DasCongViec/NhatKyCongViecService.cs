﻿using AutoMapper;
using DAS.Application.Interfaces;
using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels;
using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Models.DAS;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using NLog.Config;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using DAS.Application.Constants;
using Newtonsoft.Json;
using DAS.Utility;
using DAS.Utility.CacheUtils;
using DAS.Domain.Enums;
using Microsoft.AspNetCore.Http;
using DAS.Application.Enums;
using DAS.Domain.Interfaces.DASNotify;
using DAS.Application.Interfaces.DasCongViec;
using System.Collections;
using DAS.Application.Models.Param;
using Microsoft.VisualBasic;
using DAS.Domain.Models.DasCongViec;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using DocumentFormat.OpenXml.Drawing.Diagrams;
using DAS.Infrastructure.ContextAccessors;
using Microsoft.Data.SqlClient;
using System.Data;
using DocumentFormat.OpenXml.Office.CustomUI;
using DocumentFormat.OpenXml.Spreadsheet;

namespace DAS.Application.Services.DasCongViec
{
    public class NhatKyCongViecService : INhatKyCongViecService
    {
        private readonly IDasCongViecRepositoryWrapper _dasCongViecRepo;
        private readonly IDasRepositoryWrapper _dasRepo;
        private readonly IMapper _mapper;
        private readonly IDistributedCache _cache;
        private readonly ICategoryServices _categoryServices;
        private readonly IUserService _userService;
        private readonly ICacheManagementServices _cacheManagementServices;
        private readonly IUserPrincipalService _userPrincipalService;


        public NhatKyCongViecService(IDasRepositoryWrapper dasRepository, IDasCongViecRepositoryWrapper dasCongViecRepository
            , IMapper mapper, ICategoryServices categoryServices, ICacheManagementServices cacheManagementServices
            , IDistributedCache cache, IUserService userService, IUserPrincipalService iUserPrincipalService)
        {
            _dasRepo = dasRepository;
            _dasCongViecRepo = dasCongViecRepository;
            _mapper = mapper;
            _cache = cache;
            _categoryServices = categoryServices;
            _userService = userService;
            _cacheManagementServices = cacheManagementServices;
            _userPrincipalService = iUserPrincipalService;
        }

        public async Task<ServiceResult> Approve(VMNhatKyCongViec nhatkycongviec)
        {
            try
            {
                var nhatky = await _dasCongViecRepo.NhatKyCongViec.GetAsync(nhatkycongviec.ID);
                if (nhatky == null || nhatky.Status != (int)EnumCongViec.Status.ChoDuyet)
                    return new ServiceResultError("Nhật ký công việc không được phép duyệt");
                var chiTietNhatKys = await _dasCongViecRepo.ChiTietNhatKy.GetAllListAsync(t => t.IDNhatKyCongViec == nhatky.ID);
                nhatky.Approved = DateTime.Now;
                nhatky.ApprovedNote = nhatkycongviec.ApprovedNote;
                nhatky.Status = (int)EnumCongViec.Status.DaDuyet;
                await _dasCongViecRepo.NhatKyCongViec.UpdateAsync(nhatky);
                await _dasCongViecRepo.SaveAync();
                // xóa chi tiết công việc cũ
                // var chiTietNhatKys = _mapper.Map<List<ChiTietNhatKy>>(nhatkycongviec.VMChiTietNhatKyCongViecs);
                foreach (var item in chiTietNhatKys)
                {
                    var chitiet = nhatkycongviec.VMChiTietNhatKyCongViecs.FirstOrDefault(t => t.ID == item.ID);
                    if (Utils.IsNotEmpty(item))
                    {
                        item.ApprovedStatus = chitiet.ApprovedStatus;
                        item.Note = chitiet.Note;
                    }
                }
                await _dasCongViecRepo.ChiTietNhatKy.UpdateAsync(chiTietNhatKys);
                await _dasCongViecRepo.SaveAync();
                return new ServiceResultSuccess("Duyệt nhật ký công việc thành công");
            }
            catch (Exception ex)
            {
                //  _logger.LogError(ex);
                return new ServiceResultError(ex.Message);
            }
            throw new NotImplementedException();
        }

        public async Task<List<VMBaoCaoChiTiet>> BaoCaoChiTiet(NhatKyCongViecCondition condition)
        {
           

            var startdate = condition.StartDate ?? DateTime.Now.AddYears(-50);
            var enddate = condition.EndDate ?? DateTime.Now.AddYears(50);
            object[] parameters =
            {
                new SqlParameter(){  ParameterName = "@keyword",   SqlDbType = SqlDbType.NVarChar,   Value = condition.Keyword??"" },
                 new SqlParameter(){  ParameterName = "@ngaybd",   SqlDbType = SqlDbType.DateTime,   Value = startdate },
                 new SqlParameter(){  ParameterName = "@ngaykt",   SqlDbType = SqlDbType.DateTime,   Value = enddate },
                 new SqlParameter(){  ParameterName = "@iduser",   SqlDbType = SqlDbType.Int,   Value = condition.IDUser },
                 new SqlParameter(){  ParameterName = "@status",   SqlDbType = SqlDbType.Int,   Value = condition.Status },
             };
            var result = await _dasCongViecRepo.StaticBaoCao.ExecuteStoreProc("EXECUTE [dbo].[SP_BaoCaoTongHopCV] @keyword, @ngaybd,@ngaykt,@iduser,@status", parameters).Select(n => new VMBaoCaoChiTiet
            {
                Ngay = n.Ngay,
                Ca = n.Ca,
                IDCongViec = n.IDCongViec,
                NoiDungTacDong = n.NoiDungTacDong,
                IDUser = n.IDUser,
                Approved = n.Approved,
                ApprovedBy = n.ApprovedBy,
                ApprovedStatus = n.ApprovedStatus,
                Status = n.Status
            }).ToListAsync();
            if (Utils.IsNotEmpty(result))
            {
                var congviecs = await GetListAllCongViec();
                var users = await _userService.GetListAll();

                foreach (var item in result)
                {
                    var approvedby = users.FirstOrNewObj(t => t.ID == item.ApprovedBy);
                    var user = users.FirstOrNewObj(t => t.ID == item.IDUser);
                    var congviec = congviecs.FirstOrNewObj(t => t.ID == item.IDCongViec);
                    item.TenCongViec = congviec.Ten;
                    item.TenKieuCongViec = congviec.KieuCongViec;
                    item.TenLoaiCongViec = congviec.LoaiCongViec;
                    item.IDKieuCongViec = congviec.IDKieuCongViec;
                    item.IDLoaiCongViec = congviec.IDLoaiCongViec;
                    item.Diem = congviec.DiemSo;
                    item.TenCa = Utils.GetDescriptionEnumByKey<EnumCongViec.Ca>(item.Ca);
                    item.SoGio = item.Ca == (int)EnumCongViec.Ca.Ngay ? 10 : 14;
                    item.ApprovedByName = approvedby.Name;
                    item.TenUser = user.Name;
                    item.ApprovedStatusName = Utils.GetDescriptionEnumByKey<EnumCongViec.KetQua>(item.ApprovedStatus);
                }
            }
            return result;
        }
        public async Task<List<VMBaoCaoTongHop>> BaoCaoTongHop(NhatKyCongViecCondition condition)
        {
            var tt1 = "Thường trình 1";
            var tt2 = "Thường trình 2";
            var tt3 = "Thường trình 3";
            var cr = "CR";
            var results = new List<VMBaoCaoTongHop>();
            var chitiets = await BaoCaoChiTiet(condition);
            var countnv = 0;
            if (Utils.IsNotEmpty(chitiets))
            {
                var groups = chitiets.GroupBy(t => t.IDUser).Select(x => x);
                foreach (var group in groups)
                {
                    var result = new VMBaoCaoTongHop();
                    countnv++;
                    var items = group.ToList();
                    var grngay = items.GroupBy(x => new
                    {
                        x.Ngay,
                        x.Ca
                    }).Select(x => x);
                    foreach (var item in grngay)
                    {
                        if(item.Key.Ca==(int)EnumCongViec.Ca.Ngay)
                        {
                            result.TongSoGioCa1 += 10;
                        }
                        else if (item.Key.Ca == (int)EnumCongViec.Ca.Dem)
                        {
                            result.TongSoGioCa2 += 14;
                        }
                    }
                    result.TenUser = items.FirstOrDefault().TenUser;
                    result.TongDiemCR = items.Where(t => t.TenKieuCongViec == cr).Sum(t => t.Diem);
                    result.TongDiemTT1 = items.Where(t => t.TenKieuCongViec == tt1).Sum(t => t.Diem);
                    result.TongDiemTT2 = items.Where(t => t.TenKieuCongViec == tt2).Sum(t => t.Diem);
                    result.TongDiemTT3 = items.Where(t => t.TenKieuCongViec == tt3).Sum(t => t.Diem);
                    result.TongDiem = items.Sum(t => t.Diem);
                    result.TongSoGio = result.TongSoGioCa1 + result.TongSoGioCa2;
                    result.TongCongViec = items.Count;
                    results.Add(result);
                }
                float tbCongViec = (float)results.Sum(t=>t.TongCongViec)/countnv;
                float tbGio = (float)results.Sum(t => t.TongSoGio) / countnv;
                float tbDiemCongViec = (float)results.Sum(t => t.TongDiem) / countnv;
                foreach (var result in results)
                {
                    result.PTTheoCongViec = (float)Math.Round(result.TongCongViec / tbCongViec * 100,2);
                    result.PTTheoGio = (float)Math.Round(result.TongSoGio / tbGio * 100,2);
                    result.PTTheoDiemCongViec = (float)Math.Round(result.TongDiem / tbDiemCongViec * 100,2);
                }
            }
            return results;
        }
        public async Task<ServiceResult> Create(VMNhatKyCongViec nhatkycongviec)
        {
            try
            {
                var nhatKyCongViec = _mapper.Map<NhatKyCongViec>(nhatkycongviec);
                nhatKyCongViec.Status = (int)EnumCongViec.Status.Nhap;
                await _dasCongViecRepo.NhatKyCongViec.InsertAsync(nhatkycongviec);
                await _dasCongViecRepo.SaveAync();
                var chiTietNhatKys = new List<ChiTietNhatKy>();//_mapper.Map<List<ChiTietNhatKy>>(nhatkycongviec.VMChiTietNhatKyCongViecs);
                var dicUsers = new Dictionary<int, List<int>>();
                var index = 0;
                nhatkycongviec.VMChiTietNhatKyCongViecs.ForEach(t =>
                {
                    var chitiet = new ChiTietNhatKy();
                    chitiet = _mapper.Map<ChiTietNhatKy>(t);
                    chitiet.Index = index;
                    chitiet.IDNhatKyCongViec = nhatkycongviec.ID;
                    dicUsers.Add(index, t.IDUsers);
                    index++;
                    chiTietNhatKys.Add(chitiet);
                });
                await _dasCongViecRepo.ChiTietNhatKy.InsertAsync(chiTietNhatKys);
                await _dasCongViecRepo.SaveAync();
                var nxl = new List<ChiTietNhatKyNguoiXL>();
                chiTietNhatKys.ForEach(t =>
                {
                    nxl.AddRange(dicUsers[t.Index].Select(xl => new ChiTietNhatKyNguoiXL
                    {
                        IDNhatKyCongViec = t.IDNhatKyCongViec,
                        IDChiTietNhatKy = t.ID,
                        IDUser = xl
                    }));
                });
                await _dasCongViecRepo.ChiTietNhatKyNguoiXL.InsertAsync(nxl);
                await _dasCongViecRepo.SaveAync();
                return new ServiceResultSuccess("Thêm mới nhật ký công việc thành công");
            }
            catch (Exception ex)
            {
                //  _logger.LogError(ex);
                return new ServiceResultError(ex.Message);
            }
            throw new NotImplementedException();
        }

        public async Task<ServiceResult> Delete(int id)
        {
            var nhatky = await _dasCongViecRepo.NhatKyCongViec.GetAsync(id);
            if (nhatky == null || nhatky.Status != (int)EnumCongViec.Status.Nhap)
                return new ServiceResultError("Nhật ký công việc không được phép xóa khi đang ở thái " + Utils.GetDescriptionEnumByKey<EnumCongViec.Status>(nhatky.Status));
            await _dasCongViecRepo.NhatKyCongViec.DeleteAsync(t => t.ID == id);
            await _dasCongViecRepo.ChiTietNhatKy.DeleteAsync(t => t.ID == id);
            await _dasCongViecRepo.ChiTietNhatKyNguoiXL.DeleteAsync(t => t.ID == id);
            await _dasCongViecRepo.SaveAync();
            return new ServiceResultSuccess("Xóa thành công");
            throw new NotImplementedException();
        }
        public async Task<VMNhatKyCongViec> GetDetail(int id)
        {
            var result = await (from nhatky in _dasCongViecRepo.NhatKyCongViec.GetAll()
                                where nhatky.ID == id
                                select new VMNhatKyCongViec
                                {
                                    ID = nhatky.ID,
                                    Ca = nhatky.Ca,
                                    IDCaTruoc = nhatky.IDCaTruoc,
                                    Ngay = nhatky.Ngay,
                                    NoteTon = nhatky.NoteTon,
                                    Status = nhatky.Status,
                                    CreateDate = nhatky.CreateDate,
                                    CreatedBy = nhatky.CreatedBy,
                                    Approved = nhatky.Approved,
                                    ApprovedBy = nhatky.ApprovedBy
                                }).FirstOrDefaultAsync();
            if (Utils.IsEmpty(result))
            {
                return null;
            }
            result.IsApprove = _userPrincipalService.UserId == result.ApprovedBy && (result.Status == (int)EnumCongViec.Status.ChoDuyet || result.Status == (int)EnumCongViec.Status.DaDuyet);
            result.IsDelete = result.IsUpdate = _userPrincipalService.UserId == result.CreatedBy && result.Status == (int)EnumCongViec.Status.Nhap;
            result.VMChiTietNhatKyCongViecs = new List<VMChiTietNhatKyCongViec>();
            var queryDetail = from detail in _dasCongViecRepo.ChiTietNhatKy.GetAll()
                              where detail.IDNhatKyCongViec == id
                              select new VMChiTietNhatKyCongViec
                              {
                                  ID = detail.ID,
                                  IDNhatKyCongViec = detail.IDNhatKyCongViec,
                                  IDCongViec = detail.IDCongViec,
                                  ApprovedStatus = detail.ApprovedStatus,
                                  KetQua = detail.KetQua,
                                  NoiDung = detail.NoiDung,
                                  CreateDate = detail.CreateDate,
                                  CreatedBy = detail.CreatedBy
                              };
            var totaldetail = await queryDetail.LongCountAsync();
            if (totaldetail > 0)
            {
                result.VMChiTietNhatKyCongViecs = await queryDetail.ToListAsync();
            }
            var test = from nxlchitiet in _dasCongViecRepo.ChiTietNhatKyNguoiXL.GetAll()
                       where nxlchitiet.IDNhatKyCongViec == id
                       select nxlchitiet;
            var check = await test.ToListAsync();
            var queryDetailUser = from nxlchitiet in _dasCongViecRepo.ChiTietNhatKyNguoiXL.GetAll()
                                  where nxlchitiet.IDNhatKyCongViec == id
                                  select new VMChiTietNhatKyNguoiXL
                                  {
                                      ID = nxlchitiet.ID,
                                      IDNhatKyCongViec = nxlchitiet.IDNhatKyCongViec,
                                      IDChiTietNhatKy = nxlchitiet.IDChiTietNhatKy,
                                      IDUser = nxlchitiet.IDUser
                                  };
            var nxl = await queryDetailUser.ToListAsync();
            if (Utils.IsNotEmpty(nxl))
            {
                foreach (var item in result.VMChiTietNhatKyCongViecs)
                {
                    item.IsUpdate = result.Status == (int)EnumCongViec.Status.Nhap;
                    item.IsDelete = result.Status == (int)EnumCongViec.Status.Nhap;
                    item.VMChiTietNhatKyNguoiXL = nxl.Where(t => t.IDChiTietNhatKy == item.ID).ToList() ?? new List<VMChiTietNhatKyNguoiXL>();
                }
            }
            result.CongViecAll = await GetListAllCongViec();
            result.VMUserAll = await _userService.GetListAll();
            if (result.IDCaTruoc > 0)
            {
                var catruoc = await _dasCongViecRepo.NhatKyCongViec.GetAsync(result.IDCaTruoc);
                result.CongViecTonModel = await GetTon(catruoc);
            }
            else
                result.CongViecTonModel = await GetTon(result.Ngay, result.Ca);
            return result;
            throw new NotImplementedException();
        }

        public async Task<List<VMCongViec>> GetListAllCongViec()
        {
            var result = new List<VMCongViec>();
            var categorycondition = new CategoryCondition
            {
                CodeType = EnumCategoryType.Code.DM_CongViec.ToString(),
                PageIndex = 0,
                PageSize = 1000,
                IDParentCategory = 0,
            };
            var dataHas = new Hashtable();
            var vmindexCategory = await _categoryServices.SearchByConditionPagging(categorycondition, dataHas);
            if (Utils.IsNotEmpty(vmindexCategory.VMCategorys))
            {
                var gridFields = vmindexCategory.VMCategoryTypeFields.Where(n => n.IsShowGrid).OrderBy(n => n.Priority);
                foreach (var cate in vmindexCategory.VMCategorys)
                {
                    var item = new VMCongViec();
                    item.ID = cate.ID;
                    foreach (var field in gridFields)
                    {
                        var data = cate.VMCategoryFields.FirstOrNewObj(n => n.IDCategoryTypeField == field.ID);
                        if (field.Code == "Name")
                        {
                            item.Ten = data.DisplayVal;
                        }
                        else if (field.Code == "LoaiCongViec")
                        {
                            item.LoaiCongViec = data.DisplayVal;
                        }
                        else if (field.Code == "DiemSo")
                        {
                            item.DiemSo = data.FloatVal.HasValue? data.FloatVal.Value:9;
                        }
                        else if (field.Code == "KieuCongViec")
                        {
                            item.KieuCongViec = data.DisplayVal;
                        }
                    }
                    result.Add(item);
                }
            }
            return result;
            throw new NotImplementedException();
        }

        public async Task<CongViecTonModel> GetTon(NhatKyCongViec item)
        {
            var temp = from cv in _dasCongViecRepo.ChiTietNhatKy.GetAll()
                       where cv.KetQua == (int)EnumCongViec.KetQua.Ton && cv.IDNhatKyCongViec == item.ID
                       select new VMCongViecTon
                       {
                           ID = cv.ID,
                           IDCongViec = cv.IDCongViec,
                           NoiDung=cv.NoiDung
                       };
            var congviectons = new List<VMCongViecTon>();
            var total = await temp.LongCountAsync();
            if (total > 0)
            {
                var congviecs = await GetListAllCongViec();
                congviectons = await temp.ToListAsync();
                if (Utils.IsNotEmpty(congviectons))
                {
                    congviectons.ForEach(c =>
                    {
                        var cv = congviecs.FirstOrDefault(t => t.ID == c.IDCongViec) ?? new VMCongViec();
                        c.TenCongViec = cv.Ten;
                    });
                }
            }
            return new CongViecTonModel
            {
                IDNhatKyCongViec = item.ID,
                TenCa = Utils.GetDescriptionEnumByKey<EnumCongViec.Ca>(item.Ca),
                Ngay = item.Ngay,
                TenNguoi = (_dasRepo.User.FirstOrDefault(t => t.ID == item.CreatedBy) ?? new User()).Name,
                VMCongViecTons = congviectons
            };
        }

        public async Task<CongViecTonModel> GetTon(DateTime ngay, int ca)
        {
            ngay = new DateTime(ngay.Year, ngay.Month, ngay.Day, 23, 59, 59);
            var temp = from cv in _dasCongViecRepo.NhatKyCongViec.GetAll()
                       where cv.Ngay <= ngay //&& cv.Ca !=ca
                       orderby cv.Ngay descending
                       select cv;
            //if(ca==(int)EnumCongViec.Ca.Dem)
            //{
            //    temp.Where(t => t.Ngay <= ngay && t.Ca != ca);
            //}
            //else
            //{
            //    temp.Where(t => t.Ngay < ngay && t.Ca != ca);
            //}
            var nhatky = temp.FirstOrDefault();
            if (Utils.IsEmpty(nhatky))
                return new CongViecTonModel();
            return await GetTon(nhatky);
            throw new NotImplementedException();
        }

        public async Task<PaginatedList<VMNhatKyCongViec>> SearchListConditionPagging(NhatKyCongViecCondition condition)
        {

            var temp = (from cv in _dasCongViecRepo.NhatKyCongViec.GetAll()
                        select new VMNhatKyCongViec
                        {
                            ID = cv.ID,
                            Ca = cv.Ca,
                            Ngay = cv.Ngay,
                            Approved = cv.Approved,
                            ApprovedBy = cv.ApprovedBy,
                            ApprovedNote = cv.ApprovedNote,
                            NoteTon = cv.NoteTon,
                            CreateDate = cv.CreateDate,
                            CreatedBy = cv.CreatedBy,
                            Status = cv.Status
                        });
            if (condition.StartDate.HasValue)
            {
                var startDate = new DateTime(condition.StartDate.Value.Year, condition.StartDate.Value.Month, condition.StartDate.Value.Day, 0, 0, 0);
                temp = temp.Where(t => t.Ngay >= startDate);
            }
            if (condition.EndDate.HasValue)
            {
                var enddate = new DateTime(condition.EndDate.Value.Year, condition.EndDate.Value.Month, condition.EndDate.Value.Day, 23, 59, 59);
                temp = temp.Where(t => t.Ngay <= enddate);
            }
            if (condition.Status != 0)
            {
                temp = temp.Where(t => t.Status == condition.Status);
            }
            if (condition.ApprovedBy != 0)
            {
                temp = temp.Where(t => t.ApprovedBy == condition.ApprovedBy);
            }
            var total = await temp.LongCountAsync();
            if (total <= 0)
                return null;

            int totalPage = (int)Math.Ceiling(total / (double)condition.PageSize);
            if (totalPage < condition.PageIndex)
                condition.PageIndex = 1;
            var result = await temp.Skip((condition.PageIndex - 1) * condition.PageSize).Take(condition.PageSize).ToListAsync();
            result.ForEach(t =>
            {
                t.IsUpdate = t.IsDelete = (int)EnumCongViec.Status.Nhap == t.Status;
                t.IsSendApprove = (int)EnumCongViec.Status.Nhap == t.Status || (int)EnumCongViec.Status.Huy == t.Status;
                t.IsApprove = (int)EnumCongViec.Status.ChoDuyet == t.Status;
            });
            return new PaginatedList<VMNhatKyCongViec>(result.ToList(), (int)total, condition.PageIndex, condition.PageSize);
            throw new NotImplementedException();
        }

        public async Task<ServiceResult> SendApprove(int id, int iduser)
        {
            try
            {
                var nhatkycongviec = _dasCongViecRepo.NhatKyCongViec.FirstOrDefault(t => t.ID == id);
                if (Utils.IsEmpty(nhatkycongviec))
                    return new ServiceResultError("Nhật ký không việc không tồn tại");
                nhatkycongviec.Status = (int)EnumCongViec.Status.ChoDuyet;
                nhatkycongviec.ApprovedBy = iduser;
                await _dasCongViecRepo.NhatKyCongViec.UpdateAsync(nhatkycongviec);
                await _dasCongViecRepo.SaveAync();
                return new ServiceResultSuccess("Gửi duyệt thành công");
            }
            catch (Exception ex)
            {
                return new ServiceResultError("Gửi duyệt không thành công");
            }
        }

        public async Task<ServiceResult> Update(VMNhatKyCongViec nhatkycongviec)
        {
            try
            {
                var nhatky = await _dasCongViecRepo.NhatKyCongViec.GetAsync(nhatkycongviec.ID);
                if (nhatky == null || nhatky.Status != (int)EnumCongViec.Status.Nhap)
                    return new ServiceResultError("Nhật ký công việc không được phép chỉnh sửa");
                nhatky.IDCaTruoc = nhatkycongviec.IDCaTruoc;
                nhatky.Ngay = nhatkycongviec.Ngay;
                nhatky.Ca = nhatkycongviec.Ca;
                nhatky.NoteTon = nhatkycongviec.NoteTon;
                await _dasCongViecRepo.NhatKyCongViec.UpdateAsync(nhatky);
                await _dasCongViecRepo.SaveAync();
                // xóa chi tiết công việc cũ
                await _dasCongViecRepo.ChiTietNhatKy.DeleteAsync(t => t.IDNhatKyCongViec == nhatkycongviec.ID);
                await _dasCongViecRepo.ChiTietNhatKyNguoiXL.DeleteAsync(t => t.IDNhatKyCongViec == nhatkycongviec.ID);
                var chiTietNhatKys = new List<ChiTietNhatKy>();// _mapper.Map<List<ChiTietNhatKy>>(nhatkycongviec.VMChiTietNhatKyCongViecs);
                var dicUsers = new Dictionary<int, List<int>>();
                var index = 0;
                nhatkycongviec.VMChiTietNhatKyCongViecs.ForEach(t =>
                {
                    var chitiet = new ChiTietNhatKy();
                    chitiet = _mapper.Map<ChiTietNhatKy>(t);
                    chitiet.Index = index;
                    chitiet.IDNhatKyCongViec = nhatkycongviec.ID;
                    dicUsers.Add(index, t.IDUsers);
                    index++;
                    chiTietNhatKys.Add(chitiet);
                });
                await _dasCongViecRepo.ChiTietNhatKy.InsertAsync(chiTietNhatKys);
                await _dasCongViecRepo.SaveAync();
                var nxl = new List<ChiTietNhatKyNguoiXL>();
                chiTietNhatKys.ForEach(t =>
                {
                    nxl.AddRange(dicUsers[t.Index].Select(xl => new ChiTietNhatKyNguoiXL
                    {
                        IDNhatKyCongViec = t.IDNhatKyCongViec,
                        IDChiTietNhatKy = t.ID,
                        IDUser = xl
                    }));
                });
                await _dasCongViecRepo.ChiTietNhatKyNguoiXL.InsertAsync(nxl);
                await _dasCongViecRepo.SaveAync();
                return new ServiceResultSuccess("Cập nhật mới nhật ký công việc thành công");
            }
            catch (Exception ex)
            {
                //  _logger.LogError(ex);
                return new ServiceResultError(ex.Message);
            }
            throw new NotImplementedException();
        }
    }
}
