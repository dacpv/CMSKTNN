﻿using DAS.Application.Interfaces;
using DAS.Application.Interfaces.DasCongViec;
using DAS.Application.Interfaces.DasKTNN;
using DAS.Application.Services.DasCongViec;
using DAS.Application.Services.DasKTNN;
using DAS.Infrastructure.HttpClientAccessors.Implementations;
using DAS.Infrastructure.HttpClientAccessors.Interfaces;
using DAS.Infrastructure.Notifications;
using Microsoft.Extensions.DependencyInjection;

namespace DAS.Application.Services
{
    public static class DIServiceWrapper
    {
        public static void DependencyInjectionService(this IServiceCollection services)
        {
            services.AddSingleton<ILogHttpClient, LogHttpClient>();
            // HttpClientService
            services.AddScoped<IHttpClientService, HttpClientService>();
            services.AddSingleton<IBaseHttpClientFactory, BaseHttpClientFactory>();
            services.AddScoped<IStgFileClientService, StgFileClientService>();
            services.AddScoped<IDefaultDataService, DefaultDataService>();

            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IAgencyServices, AgencyService>();
            //services.AddScoped<IArchiveManagementService, ArchiveManagementService>();
            services.AddScoped<IAuthorizeService, AuthorizeService>();
            services.AddScoped<ICatalogingProfileService, CatalogingProfileService>();
            services.AddScoped<ICategoryServices, CategoryService>();
            services.AddScoped<ICategoryTypeServices, CategoryTypeService>();
            services.AddScoped<IDefaultDataService, DefaultDataService>();
            //services.AddScoped<IDeliveryRecordServices, DeliveryRecordServices>();
           // services.AddScoped<IDocBorrowServices, DocBorrowService>();
           // services.AddScoped<IDocTypeServices, DocTypeService>();
            services.AddScoped<IExcelServices, ExcelService>();
           // services.AddScoped<IExpiryDateServices, ExpiryDateService>();
            services.AddScoped<IGroupPermissionService, GroupPermissionService>();
            services.AddScoped<IHomeServices, HomeService>();
            services.AddScoped<IIPAddressClientServices, IPAddressClientService>();
            services.AddScoped<ILanguageServices, LanguageService>();
            services.AddScoped<IModuleService, ModuleService>();
            services.AddScoped<IOrganConfigServices, OrganConfigService>();
            services.AddScoped<IOrganServices, OrganService>();
            services.AddScoped<IPermissionService, PermissionService>();
           // services.AddScoped<IPlanDocServices, PlanDocService>();
          //  services.AddScoped<IPlanServices, PlanService>();
            services.AddScoped<IPositionServices, PositionService>();
        //    services.AddScoped<IProfileListService, ProfileListService>();
        //    services.AddScoped<IProfileService, ProfileService>();
        //    services.AddScoped<IProfileTemplateServices, ProfileTemplateService>();
            services.AddScoped<IReaderServices, ReaderService>();
       //     services.AddScoped<IReportArchiveServices, ReportArchiveService>();
           services.AddScoped<IResetPasswordService, ResetPasswordService>();
            services.AddScoped<IRoleServices, RoleService>();
            services.AddScoped<ISercureLevelServices, SercureLevelService>();
            services.AddScoped<IStgFileService, StgFileService>();
           // services.AddScoped<IStorageServices, StorageService>();
            services.AddScoped<ISystemConfigServices, SystemConfigService>();
            services.AddScoped<ITeamService, TeamService>();
            services.AddScoped<ITemplateServices, TemplateServices>();
            services.AddScoped<IUserLogServices, UserLogService>();
            services.AddScoped<IUserService, UserService>();
         //   services.AddScoped<IProfileCategoryServices, ProfileCategoryService>();
            services.AddScoped<IUserBookMarkServices, UserBookMarkService>();
          //  services.AddScoped<IProfileDestructionServices, ProfileDestructionService>();
            services.AddScoped<ICacheManagementServices, CacheManagementService>();
            services.AddScoped<IDataApiServices, DataApiServices>();
            // Công việc
            services.AddScoped<INhatKyCongViecService, NhatKyCongViecService>();
            services.AddScoped<ITableTestService, TableTestService>();
            services.AddScoped<IGroupInfoService, GroupInfoService>();
            services.AddScoped<ISoucreInfoService, SoucreInfoService>();
            services.AddScoped<ISharedAppService, SharedAppService>();
            services.AddScoped<ITableInfoServices, TableInfoService>();
            services.AddScoped<ISchemaInfoService, SchemaInforService>();
            services.AddScoped<IDynamicDBService, DynamicDBService>();
            services.AddScoped<IInputInfoServices, InputInfoService>();
            services.AddScoped<IImportDataServices, ImportDataService>();
            //
            //  services.AddScoped<IHubNotificationHelper, HubNotificationHelper>();
            
            services.AddScoped<IAPIManageService, APIManageService>();
            services.AddScoped<ITableInfoApiConfigServices, TableInfoApiConfigService>();
            services.AddScoped<IColumnTableGroupServices, ColumnTableGroupService>();
            services.AddScoped<ISendNotificationServices, SendNotificationService>();
            services.AddScoped<ITanXuatTruyCapApiService, TanXuatTruyCapApiService>();
            services.AddScoped<INotificationConfigService, NotificationConfigService>();



            
            services.AddScoped<ICollectionReportService, CollectionReportService>();


        }
    }
}
