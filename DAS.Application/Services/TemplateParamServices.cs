﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DAS.Application.Interfaces;
using DAS.Application.Models.CustomModels;
using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Models.DAS;

namespace DAS.Application.Services
{
    public class TemplateParamServices : BaseMasterService, ITemplateParamServices
    {
        #region Ctor
        public TemplateParamServices(IDasRepositoryWrapper dasRepository) : base(dasRepository)
        {

        }
        #endregion
        public Task<ServiceResult> Create(TemplateParam model)
        {
            throw new NotImplementedException();
        }

        public Task<ServiceResult> Delete(object id)
        {
            throw new NotImplementedException();
        }

        public Task<TemplateParam> Get(object id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<TemplateParam>> Gets()
        {
            throw new NotImplementedException();
        }

        public Task<ServiceResult> Update(TemplateParam model)
        {
            throw new NotImplementedException();
        }
    }
}
