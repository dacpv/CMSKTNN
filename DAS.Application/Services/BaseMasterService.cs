﻿using DAS.Domain.Interfaces;
using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Interfaces.DASNotify;
using DAS.Infrastructure.Repositories.DASKTNN;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace DAS.Application.Services
{
    public class BaseMasterService
    {
        protected IDasRepositoryWrapper _dasRepo;
        protected IDasNotifyRepositoryWrapper _dasNotifyRepo;
        protected IDasKTNNRepositoryWrapper _dasKTNNRepo;
        protected IDasDataDapperRepo _dasDapperRepo;
        protected readonly IDbConnection _dasDataDapperConn;
        protected readonly IDbConnection _dasDynamicDbDapperConn;
        protected readonly IDbConnection _apiManageDbDapperConn;

        public BaseMasterService(IDasRepositoryWrapper dasRepository)
        {
            _dasRepo = dasRepository;
        }
        public BaseMasterService(IDasRepositoryWrapper dasRepository, IDasDataDapperRepo dasdataDapperRepo)
        {
            _dasRepo = dasRepository;
            _dasDataDapperConn = dasdataDapperRepo.idbConnection;
        }
        public BaseMasterService(IDasRepositoryWrapper dasRepository, IDasDataDapperRepo dasdataDapperRepo, IDynamicDBDapperRepo dasdynaicDapperRepo)
        {
            _dasRepo = dasRepository;
            _dasDataDapperConn = dasdataDapperRepo.idbConnection;
            _dasDynamicDbDapperConn = dasdynaicDapperRepo.idbConnection;
        }
        public BaseMasterService(IDasRepositoryWrapper dasRepository, IAPIManageDBDapperRepo apiMangageDBDapperRepo)
        {
            _dasRepo = dasRepository;
            _apiManageDbDapperConn = apiMangageDBDapperRepo.idbConnection;
        }

        public BaseMasterService(IDasRepositoryWrapper dasRepository, IDasNotifyRepositoryWrapper dasNotifyRepository)
        {
            _dasRepo = dasRepository;
            _dasNotifyRepo = dasNotifyRepository;
        }
        public BaseMasterService(IDasRepositoryWrapper dasRepository, IDasNotifyRepositoryWrapper dasNotifyRepository, IDasDataDapperRepo dasdataDapperRepo, IDynamicDBDapperRepo dasdynaicDapperRepo)
        {
            _dasRepo = dasRepository;
            _dasNotifyRepo = dasNotifyRepository;
            _dasDataDapperConn = dasdataDapperRepo.idbConnection;
            _dasDynamicDbDapperConn = dasdynaicDapperRepo.idbConnection;
        }
        public BaseMasterService(IDasNotifyRepositoryWrapper dasNotifyRepository)
        {
            _dasNotifyRepo = dasNotifyRepository;
        }
        public BaseMasterService(IDasNotifyRepositoryWrapper dasNotifyRepository, IDasDataDapperRepo dasdataDapperRepo)
        {
            _dasNotifyRepo = dasNotifyRepository;
            _dasDataDapperConn = dasdataDapperRepo.idbConnection;
        }
        public BaseMasterService(IDasRepositoryWrapper dasRepository, IDasKTNNRepositoryWrapper dasKTNNRepository)
        {
            _dasRepo = dasRepository;
            _dasKTNNRepo = dasKTNNRepository;
        }

        public BaseMasterService(IDasRepositoryWrapper dasRepository, IDasDataDapperRepo dasDapperRepo, IDasKTNNRepositoryWrapper dasKTNNRepository)
        {
            _dasRepo = dasRepository;
            _dasDataDapperConn = dasDapperRepo.idbConnection;
            _dasKTNNRepo = dasKTNNRepository;
        }
        public BaseMasterService(IDasRepositoryWrapper dasRepository, IDasDataDapperRepo dasdataDapperRepo, IDasKTNNRepositoryWrapper dasKTNNRepositoryWrapper, IDynamicDBDapperRepo dasdynaicDapperRepo)
        {
            _dasRepo = dasRepository;
            _dasKTNNRepo = dasKTNNRepositoryWrapper;
            _dasDataDapperConn = dasdataDapperRepo.idbConnection;
            _dasDynamicDbDapperConn = dasdynaicDapperRepo.idbConnection;
        }
    }
}