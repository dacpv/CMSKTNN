﻿using AutoMapper;
using Dapper;
using DAS.Application.Constants;
using DAS.Application.Enums;
using DAS.Application.Enums.DasKTNN;
using DAS.Application.Interfaces;
using DAS.Application.Interfaces.DasKTNN;
using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels;
using DAS.Application.Models.ViewModels.DasKTNN;
using DAS.Domain.Enums;
using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Interfaces.DASNotify;
using DAS.Domain.Models.DASKTNN;
using DAS.Infrastructure.ContextAccessors;
using DAS.Infrastructure.Repositories.DASKTNN;
using DAS.Utility;
using DAS.Utility.CustomClass;
using DAS.Utility.LogUtils;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAS.Application.Services.DasKTNN
{
    public class GroupInfoService : BaseMasterService, IGroupInfoService
    {
        #region Properties
        private readonly IMapper _mapper;
        private readonly ILoggerManager _logger;
        private readonly IModuleService _module;
        private readonly IDynamicDBService _dynamicDBService;
        private readonly IDefaultDataService _defaultDataService;
        private readonly IUserPrincipalService _userPrincipalService;
        private ICacheManagementServices _cacheManagementServices;

        #endregion

        #region Ctor
        public GroupInfoService(IDasRepositoryWrapper dasRepository, IDasKTNNRepositoryWrapper dasKTNN
            , IMapper mapper
            , ILoggerManager logger
            , IModuleService module
            , IDynamicDBService dynamicDBService
            , IDefaultDataService defaultDataService
            , IUserPrincipalService userPrincipalService
            , ICacheManagementServices cacheManagementServices, IDasDataDapperRepo dasDapperRepo) : base(dasRepository, dasDapperRepo, dasKTNN)
        {
            _mapper = mapper;
            _logger = logger;
            _module = module;
            _userPrincipalService = userPrincipalService;
            _dynamicDBService = dynamicDBService;
            _cacheManagementServices = cacheManagementServices;
            _defaultDataService = defaultDataService;
        }

        #endregion

        #region Gets  


        public async Task<IEnumerable<GroupInfo>> GetsList()
        {
            var temp = from ct in _dasKTNNRepo.GroupInfo.GetAll()
                       orderby ct.Name
                       select ct;
            return await temp.ToListAsync();
        }
        public async Task<IEnumerable<GroupTableInfo>> GetsListTable()
        {
            var temp = from ct in _dasKTNNRepo.GroupTableInfo.GetAll()
                       orderby ct.IDTable
                       select ct;
            return await temp.ToListAsync();
        }

        public async Task<GroupInfo> Get(int id)
        {
            return await _dasKTNNRepo.GroupInfo.FirstOrDefaultAsync(n => n.ID == id);
        }

        public async Task<PaginatedList<GroupInfo>> SearchByConditionPagging(GroupInfoCondition condition)
        {
            UserData userData = await _cacheManagementServices.GetUserDataAndSetCache();

            var temp = from ct in _dasKTNNRepo.GroupInfo.GetAll()
                       where (condition.Keyword.IsEmpty() || ct.Name.Contains(condition.Keyword))

                       orderby ct.UpdatedDate ?? ct.CreateDate descending
                       select ct;
            var total = await temp.LongCountAsync();
            int totalPage = (int)Math.Ceiling(total / (double)condition.PageSize);
            if (totalPage < condition.PageIndex)
            {
                condition.PageIndex = 1;
            }
            var result = await temp.Skip((condition.PageIndex - 1) * condition.PageSize).Take(condition.PageSize).ToListAsync();
            return new PaginatedList<GroupInfo>(result, (int)total, condition.PageIndex, condition.PageSize);
        }

        #endregion

        #region Create

        public async Task<VMGroupInfo> Create()
        {
            var model = new VMGroupInfo()
            {
                groupInfo = new GroupInfo(),
                tableInfos = _dasKTNNRepo.TableInfo.GetAll().Where(n=>n.Status== (int)EnumCommon.Status.Active).ToList(),
                schemaInfos = _dasKTNNRepo.SchemaInfo.GetAll(),
                groupTables= Enumerable.Empty<GroupTableInfo>(),
        };
            model.dictableInfos = new Dictionary<int, string>();
            foreach (var item in model.tableInfos.OrderBy(n=>n.IDSchema).OrderBy(n=>n.ID).ToList())
            {
                var schemal = model.schemaInfos.FirstOrDefault(n => n.ID == item.IDSchema) ?? new SchemaInfo();
                model.dictableInfos.Add(item.ID, $"{item.Name} - {schemal.Name}");
            }
            return model;
        }

        public async Task<VMGroupInfo> Create(Hashtable data)
        {
            var model = new VMGroupInfo();
            try
            {
                var item = Utils.Bind<GroupInfo>(data);
                var tables = Utils.GetBigInts(data, "IDTables");
                if (Utils.IsEmpty(item.Name))
                {
                    model.Message = "Tên nhóm không để trống";
                    model.IsSuccess = false;
                    return model;
                }
                if (Utils.IsEmpty(tables) || tables.Length <= 0)
                {
                    model.Message = "Bảng dữ liệu không để trống";
                    model.IsSuccess = false;
                    return model;
                }
                UserData userData = await _cacheManagementServices.GetUserDataAndSetCache();
                item.CreateDate = DateTime.Now;
                item.CreatedBy = _userPrincipalService.UserId;
                await _dasKTNNRepo.GroupInfo.InsertAsync(item);
                await _dasKTNNRepo.SaveAync();
                if (item.ID > 0)
                {
                    await SaveGroupTable(item, tables);
                    await _dasKTNNRepo.SaveAync();
                    model.Message = "Thêm dữ liệu thành công";
                    model.IsSuccess = true;
                    return model;
                }
                else
                {
                    model.Message = "Thêm dữ liệu không thành công";
                    model.IsSuccess = true;
                    return model;
                }


            }
            catch (LogicException ex)
            {
                model.Message = ex.Message;
                model.IsSuccess = false;
                return model;
            }
            catch (Exception ex)
            {
                model.Message = "Có lỗi khi thêm mới bảng dữ liệu";
                model.IsSuccess = false;
                return model;
            }
        }
        private async Task SaveGroupTable(GroupInfo groupInfo, long[] ids)
        {
            await _dasKTNNRepo.GroupTableInfo.DeleteAsync(n=>n.IDGroup== groupInfo.ID);
            if (ids.IsNotEmpty() && groupInfo.ID > 0)
            {
                foreach (var item in ids)
                {
                    var detail = new GroupTableInfo
                    {
                        IDGroup = groupInfo.ID,
                        IDTable = (int)item,
                        CreateDate = DateTime.Now,
                        CreatedBy = _userPrincipalService.UserId
                    };
                    await _dasKTNNRepo.GroupTableInfo.InsertAsync(detail);
                }
            }
        }
        #endregion

        #region Update
        public async Task<VMGroupInfo> Update(int? id)
        {
            var group = await Get(id ?? 0);
            if (group == null || group.ID == 0)
            {
                throw new LogicException("Bảng dữ liệu không còn tồn tại");
            }
            var tableInfos = await _dasKTNNRepo.GroupTableInfo.GetAllListAsync(n => n.IDGroup== id);
            var model = new VMGroupInfo
            {
                groupInfo= group,
                tableInfos = _dasKTNNRepo.TableInfo.GetAll().Where(n => n.Status == (int)EnumCommon.Status.Active).ToList(),
                schemaInfos = _dasKTNNRepo.SchemaInfo.GetAll(),
                groupTables= tableInfos,
            };
            model.dictableInfos = new Dictionary<int, string>();
            foreach (var item in model.tableInfos.OrderBy(n => n.IDSchema).OrderBy(n => n.ID).ToList())
            {
                var schemal = model.schemaInfos.FirstOrDefault(n => n.ID == item.IDSchema) ?? new SchemaInfo();
                model.dictableInfos.Add(item.ID, $"{item.Name} - {schemal.Name}");
            }
            return model;
        }

        public async Task<VMGroupInfo> Update(Hashtable data)
        {
            var model = new VMGroupInfo();
            try
            {
                var ID=  Utils.GetInt(data, "ID");
                var info = _dasKTNNRepo.GroupInfo.Get(ID);
                if (Utils.IsEmpty(info))
                {
                    model.Message = "Nhóm bảng này không tồn tại";
                    model.IsSuccess = false;
                    return model;
                }
                var item = Utils.Bind<GroupInfo>(info,data);
                var tables = Utils.GetBigInts(data, "IDTables");
                if (Utils.IsEmpty(item.Name))
                {
                    model.Message = "Tên nhóm không để trống";
                    model.IsSuccess = false;
                    return model;
                }
                if (Utils.IsEmpty(tables) || tables.Length <= 0)
                {
                    model.Message = "Bảng dữ liệu không để trống";
                    model.IsSuccess = false;
                    return model;
                }
                item.UpdatedDate = DateTime.Now;
                item.UpdatedBy = _userPrincipalService.UserId;
                await _dasKTNNRepo.GroupInfo.UpdateAsync(item);
                await _dasKTNNRepo.SaveAync();
                if (item.ID > 0)
                {
                    await SaveGroupTable(item, tables);
                    await _dasKTNNRepo.SaveAync();
                    model.Message = "Cập nhật dữ liệu thành công";
                    model.IsSuccess = true;
                    return model;
                }
                else
                {
                    model.Message = "Cập nhật dữ liệu không thành công";
                    model.IsSuccess = true;
                    return model;
                }

            }
            catch (LogicException ex)
            {
                model.Message = ex.Message;
                model.IsSuccess = false;
                return model;
            }
            catch (Exception ex)
            {
                model.Message = "Có lỗi khi thêm mới bảng dữ liệu";
                model.IsSuccess = false;
                return model;
            }
        }



        #endregion

        #region Delete
        public async Task<ServiceResult> Delete(int id)
        {
            try
            {
                var tableInfo = await _dasKTNNRepo.GroupInfo.GetAsync(id);
                if (tableInfo == null)
                    return new ServiceResultError("Nhóm bảng dữ liệu này hiện không tồn tại hoặc đã bị xóa");

                await _dasKTNNRepo.GroupInfo.DeleteAsync(tableInfo);
                await _dasKTNNRepo.GroupTableInfo.DeleteAsync(n=>n.IDGroup== tableInfo.ID);
                await _dasKTNNRepo.SaveAync();
                //Update Module
                return new ServiceResultSuccess("Xóa dữ liệu thành công");
            }
            catch (LogicException ex)
            {
                return new ServiceResultError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return new ServiceResultError(ex.Message);

            }
        }

        public async Task<ServiceResult> Delete(IEnumerable<int> ids)
        {
            try
            {
                var tableInfoDeletes = await _dasKTNNRepo.GroupInfo.GetAllListAsync(n => ids.Contains(n.ID));
                if (tableInfoDeletes == null || tableInfoDeletes.Count() == 0)
                    return new ServiceResultError("Nhóm bảng dữ liệu đã chọn hiện không tồn tại hoặc đã bị xóa");
                foreach (var item in tableInfoDeletes)
                {
                    await _dasKTNNRepo.GroupInfo.DeleteAsync(item);
                    await _dasKTNNRepo.GroupTableInfo.DeleteAsync(n => n.IDGroup == item.ID);
                }
                await _dasKTNNRepo.SaveAync();


                //Update Module
                return new ServiceResultSuccess("Xóa dữ liệu thành công");
            }
            catch (LogicException ex)
            {
                return new ServiceResultError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return new ServiceResultError(ex.Message);
            }
        }

        #endregion


    }
}