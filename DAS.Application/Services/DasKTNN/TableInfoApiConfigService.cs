﻿using AutoMapper;
using DAS.Application.Enums.DasKTNN;
using DAS.Application.Interfaces;
using DAS.Application.Interfaces.DasKTNN;
using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels;
using DAS.Application.Models.ViewModels.DasKTNN;
using DAS.Domain.Enums;
using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Interfaces.DASNotify;
using DAS.Domain.Models.DASKTNN;
using DAS.Infrastructure.Repositories.DASKTNN;
using DAS.Utility;
using DAS.Utility.BuildCondition;
using DAS.Utility.CustomClass;
using DAS.Utility.LogUtils;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static DAS.Application.Enums.DasKTNN.EnumTableInfo;

namespace DAS.Application.Services.DasKTNN
{
    public class TableInfoApiConfigService : BaseMasterService, ITableInfoApiConfigServices
    {
        #region Properties
        private readonly IMapper _mapper;
        private readonly ILoggerManager _logger;
        private readonly IModuleService _module;
        private readonly IDynamicDBService _dynamicDBService;
        private readonly IDefaultDataService _defaultDataService;

        private readonly IHostApplicationLifetime _host;
        private ICacheManagementServices _cacheManagementServices;
        private IWebHostEnvironment _env;

        #endregion

        #region Ctor
        public TableInfoApiConfigService(IDasRepositoryWrapper dasRepository, IDasKTNNRepositoryWrapper dasKTNN
            , IMapper mapper
            , ILoggerManager logger
            , IModuleService module
            , IDynamicDBService dynamicDBService
            , IDefaultDataService defaultDataService,
            IHostApplicationLifetime host
            , ICacheManagementServices cacheManagementServices, IDasDataDapperRepo dasDapperRepo,
            IWebHostEnvironment env) : base(dasRepository, dasDapperRepo, dasKTNN)
        {
            _mapper = mapper;
            _logger = logger;
            _module = module;
            _dynamicDBService = dynamicDBService;
            _cacheManagementServices = cacheManagementServices;
            _defaultDataService = defaultDataService;
            _host = host;
            _env = env;
        }

        #endregion

        #region Gets  

        public async Task<IEnumerable<TableInfoApiConfig>> GetsList()
        {
            var temp = from ct in _dasKTNNRepo.TableInfoApiConfig.GetAll()
                       orderby ct.ID descending
                       select ct;
            return await temp.ToListAsync();
        }
        public async Task<TableInfoApiConfig> Get(int id)
        {
            return await _dasKTNNRepo.TableInfoApiConfig.FirstOrDefaultAsync(n => n.ID == id);
        }

        public async Task<VMIndexTableInfoApiConfig> SearchByConditionPagging(TableInfoApiConfigCondition condition)
        {
            var rs = new VMIndexTableInfoApiConfig
            {
                SearchParam = condition
            };

            UserData userData = await _cacheManagementServices.GetUserDataAndSetCache();

            var temp = from tb in _dasKTNNRepo.TableInfoApiConfig.GetAll()

                       where (condition.IDTable == 0 || tb.IDTable == condition.IDTable)
                      && (condition.Keyword.IsEmpty() || tb.Name.Contains(condition.Keyword))

                       orderby tb.UpdatedDate ?? tb.CreateDate descending
                       select _mapper.Map<VMTableInfoApiConfig>(tb);

            var total = await temp.LongCountAsync();
            int totalPage = (int)Math.Ceiling(total / (double)condition.PageSize);
            if (totalPage < condition.PageIndex)
            {
                condition.PageIndex = 1;
            }
            var result = await temp.Skip((condition.PageIndex - 1) * condition.PageSize).Take(condition.PageSize).ToListAsync();
            rs.Configs = new PaginatedList<VMTableInfoApiConfig>(result, (int)total, condition.PageIndex, condition.PageSize);
            rs.TableInfos = await GetsTableList();
            rs.ApiTypes = Utils.EnumToDic<TableInfoApiConfigType>();

            rs.DynamicAPIs = ConfigUtils.GetAppSetting<DynamicAPIModel[]>("DynamicAPIs");

            return rs;
        }


        /// <summary>
        /// Lấy danh sách cột theo id bảng
        /// </summary>
        /// <param name="idTables">ID bảng</param>
        /// <returns></returns>
        public async Task<VMTableInfoApiConfig> GetColumnConfig(Hashtable data)
        {
            var rs = new VMTableInfoApiConfig();
            rs.Type = Utils.GetInt(data, "Type");
            rs.IDTable = Utils.GetInt(data, "IDTable");
            rs.IDTempTable = Utils.GetInt(data, "IDTempTable");
            rs.Columns = await GetColumnByIDTable(rs.IDTable ?? 0);
            rs.ColGroups = await GetsColumnTableGroupList();
            rs.CondOperators = GetCondOperator();
            rs.TempTables = await GetsTableList(true);
            rs.SharedApps = await GetActiveSharedApps();
            return rs;
        }
        #endregion

        #region Create

        public async Task<VMTableInfoApiConfig> Create()
        {
            var tables = await GetsTableList(false);
            var sharedApps = await GetActiveSharedApps();
            var model = new VMTableInfoApiConfig()
            {
                Tables = tables,
                ApiTypes = Utils.EnumToDic<TableInfoApiConfigType>(),
                SharedApps = sharedApps,
                Status = (int)EnumCommon.Status.Active
            };
            model.TableInfoApiConfigs = await GetsList();

            return model;
        }

        public async Task<ServiceResult> Save(VMUpdateTableInfoApiConfig data)
        {
            try
            {
                UserData userData = await _cacheManagementServices.GetUserDataAndSetCache();
                var tableInfoApiConfig = _mapper.Map<TableInfoApiConfig>(data);

                var table = await _dasKTNNRepo.TableInfo.GetAsync(data.IDTable);
                if (table == null || table.Status == (int)EnumCommon.Status.InActive)
                {
                    if (tableInfoApiConfig.Type != (int)TableInfoApiConfigType.RunScriptSQL)
                        throw new LogicException("Bảng không tồn tại hoặc đã bị xóa");

                    table = new TableInfo();
                }
                ValidateData(data);

                tableInfoApiConfig.IDSchema = table.IDSchema;
                tableInfoApiConfig.Condition = GetStrCondition(data);
                tableInfoApiConfig.StartDate = Utils.GetDatetime(data.StrStartDate, "dd/MM/yyyy");
                tableInfoApiConfig.EndDate = Utils.GetDatetime(data.StrEndDate, "dd/MM/yyyy");

                if (tableInfoApiConfig.Type == (int)TableInfoApiConfigType.RunScriptSQL)
                {
                    tableInfoApiConfig.Description = data.ScriptSQL;
                }

                await _dasKTNNRepo.TableInfoApiConfig.InsertAsync(tableInfoApiConfig);
                await _dasKTNNRepo.SaveAync();
                if (data.IsShareAllApp > 0)
                {
                    var sharedApps = await GetActiveSharedApps();
                    await InsertTableInfoApiConfigSharedApps(tableInfoApiConfig, sharedApps.Select(n => n.ID));
                }
                else
                {
                    if (data.IDSharedApps.IsNotEmpty())
                    {
                        await InsertTableInfoApiConfigSharedApps(tableInfoApiConfig, data.IDSharedApps);
                    }
                }
                //Upate lại  ParamJson
                tableInfoApiConfig.ParamJson = GetParamJson(tableInfoApiConfig);
                await _dasKTNNRepo.TableInfoApiConfig.UpdateAsync(tableInfoApiConfig);
                await _dasKTNNRepo.SaveAync();
                return new ServiceResultSuccess("Thêm cấu hình api thành công");

            }
            catch (LogicException ex)
            {
                return new ServiceResultError(ex.Message);
            }
            catch (Exception ex)
            {
                Guid.NewGuid().ToString();
                _logger.LogError(ex);
                return new ServiceResultError("Có lỗi khi thêm mới cấu hình api");
            }
        }

        private async Task InsertTableInfoApiConfigSharedApps(TableInfoApiConfig tableInfoApiConfig, IEnumerable<int> iDSharedApps)
        {
            await _dasKTNNRepo.TableInfoApiConfigSharedApp.DeleteAsync(n => n.IDTableInfoApiConfig == tableInfoApiConfig.ID);
            if (iDSharedApps.IsNotEmpty())
            {
                var sharedAppConfigs = iDSharedApps.Select(n => new TableInfoApiConfigSharedApp
                {
                    IDSharedApp = n,
                    IDTableInfoApiConfig = tableInfoApiConfig.ID,
                });
                await _dasKTNNRepo.TableInfoApiConfigSharedApp.InsertAsync(sharedAppConfigs);
            }
        }


        #endregion

        #region Update
        public async Task<VMTableInfoApiConfig> Update(int? id)
        {
            var tableInfoApiConfig = await Get(id ?? 0);
            if (tableInfoApiConfig == null || tableInfoApiConfig.ID == 0)
            {
                throw new LogicException("Cấu hình api không còn tồn tại");
            }
            var model = _mapper.Map<VMTableInfoApiConfig>(tableInfoApiConfig);
            var tables = await GetsTableList();
            model.ScriptSQL = model.Description;
            model.Tables = tables.Where(n => !n.IsTempTable);
            model.TempTables = tables.Where(n => n.IsTempTable);
            model.Columns = await GetColumnByIDTable(tableInfoApiConfig.IDTable);
            model.CondOperators = GetCondOperator();
            model.ApiTypes = Utils.EnumToDic<TableInfoApiConfigType>();
            model.ColGroups = await GetsColumnTableGroupList();
            model.SharedApps = await GetActiveSharedApps();
            model.SharedAppConfigs = await _dasKTNNRepo.TableInfoApiConfigSharedApp.GetAllListAsync(n => n.IDTableInfoApiConfig == tableInfoApiConfig.ID);
            return model;
        }

        public async Task<ServiceResult> Change(VMUpdateTableInfoApiConfig data)
        {
            try
            {
                UserData userData = await _cacheManagementServices.GetUserDataAndSetCache();

                var tableInfoApiConfig = await _dasKTNNRepo.TableInfoApiConfig.GetAsync(data.ID);
                if (tableInfoApiConfig == null)
                    return new ServiceResultError("Cấu hình api này hiện không tồn tại hoặc đã bị xóa");
                var table = await _dasKTNNRepo.TableInfo.GetAsync(data.IDTable);
                if (table == null || table.Status == (int)EnumCommon.Status.InActive)
                {
                    if (tableInfoApiConfig.Type != (int)TableInfoApiConfigType.RunScriptSQL)
                        throw new LogicException("Bảng không tồn tại hoặc đã bị xóa");

                    table = new TableInfo();
                }
                ValidateData(data);
                tableInfoApiConfig.Bind(data.KeyValue());
                tableInfoApiConfig.IDSchema = table.IDSchema;
                tableInfoApiConfig.Condition =
                tableInfoApiConfig.Condition = GetStrCondition(data);
                tableInfoApiConfig.StartDate = Utils.GetDatetime(data.StrStartDate, "dd/MM/yyyy");
                tableInfoApiConfig.EndDate = Utils.GetDatetime(data.StrEndDate, "dd/MM/yyyy");
                if (tableInfoApiConfig.Type == (int)TableInfoApiConfigType.RunScriptSQL)
                {
                    tableInfoApiConfig.Description = data.ScriptSQL;
                }

                tableInfoApiConfig.ParamJson = GetParamJson(tableInfoApiConfig);
                await _dasKTNNRepo.TableInfoApiConfig.UpdateAsync(tableInfoApiConfig);
                if (data.IsShareAllApp > 0)
                {
                    var sharedApps = await GetActiveSharedApps();
                    await InsertTableInfoApiConfigSharedApps(tableInfoApiConfig, sharedApps.Select(n => n.ID));
                }
                else
                {
                    if (data.IDSharedApps.IsNotEmpty())
                    {
                        await InsertTableInfoApiConfigSharedApps(tableInfoApiConfig, data.IDSharedApps);
                    }
                }
                await _dasKTNNRepo.SaveAync();
                return new ServiceResultSuccess("Cập nhật cấu hình api thành công");
            }
            catch (LogicException ex)
            {
                return new ServiceResultError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return new ServiceResultError(ex.Message);
            }
        }
        #endregion


        #region Delete
        public async Task<ServiceResult> Delete(int id)
        {
            try
            {
                var tableInfoApiConfig = await _dasKTNNRepo.TableInfoApiConfig.GetAsync(id);
                if (tableInfoApiConfig == null)
                    return new ServiceResultError("Cấu hình api này hiện không tồn tại hoặc đã bị xóa");

                await _dasKTNNRepo.TableInfoApiConfig.DeleteAsync(tableInfoApiConfig);
                await _dasKTNNRepo.SaveAync();

                return new ServiceResultSuccess("Xóa cấu hình api thành công");
            }
            catch (LogicException ex)
            {
                return new ServiceResultError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return new ServiceResultError(ex.Message);

            }
        }
        public async Task<ServiceResult> Delete(IEnumerable<int> ids)
        {
            try
            {
                var tableInfoApiConfigDeletes = await _dasKTNNRepo.TableInfoApiConfig.GetAllListAsync(n => ids.Contains(n.ID));
                if (tableInfoApiConfigDeletes == null || tableInfoApiConfigDeletes.Count() == 0)
                    return new ServiceResultError("Cấu hình api đã chọn hiện không tồn tại hoặc đã bị xóa");

                await _dasKTNNRepo.TableInfoApiConfig.DeleteAsync(tableInfoApiConfigDeletes);
                await _dasKTNNRepo.SaveAync();
                return new ServiceResultSuccess("Xóa cấu hình api thành công");
            }
            catch (LogicException ex)
            {
                return new ServiceResultError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return new ServiceResultError(ex.Message);
            }
        }
        #endregion


        #region Validate
        private void ValidateData(VMUpdateTableInfoApiConfig data)
        {
            //if (vmTableInfoApiConfig.Type ==(int)TableInfoApiConfigType.GetTableRecord && vmTableInfoApiConfig.ViewColumn.IsEmpty())
            //{
            //    throw new LogicException("Vui lòng chọn Cột hiển thị");
            //}

            if (data.Type == (int)TableInfoApiConfigType.RunScriptSQL && data.ScriptSQL.IsEmpty())
            {
                throw new LogicException("Vui lòng nhập Script SQL");
            }
        }
        #endregion

        #region Funtions
        private async Task<IEnumerable<TableInfo>> GetsTableList(bool? isGetTempTable = null)
        {
            var temp = from t in _dasKTNNRepo.TableInfo.GetAll()
                       where t.Status == (int)EnumCommon.Status.Active
                       && (isGetTempTable == null || t.IsTempTable == isGetTempTable)
                       orderby t.Name
                       select t;
            return await temp.ToListAsync();
        }
        private async Task<IEnumerable<ColumnTableGroup>> GetsColumnTableGroupList()
        {
            var temp = from t in _dasKTNNRepo.ColumnTableGroup.GetAll()
                           //  where ids.IsEmpty() || ids.Contains(t.ID)
                       orderby t.Name
                       select t;
            return await temp.ToListAsync();
        }
        private async Task<IEnumerable<SchemaInfo>> GetsSchemaList(params int[] ids)
        {
            var temp = from t in _dasKTNNRepo.SchemaInfo.GetAll()
                       where ids == null || ids.Contains(t.ID)
                       orderby t.Name
                       select t;
            return await temp.ToListAsync();
        }
        private async Task<IEnumerable<SharedApp>> GetActiveSharedApps()
        {
            var temp = from t in _dasKTNNRepo.SharedApp.GetAll()
                       where t.Status == (int)EnumCommon.Status.Active
                       orderby t.Name
                       select t;
            return await temp.ToListAsync();
        }
        private static Dictionary<int, string> GetCondOperator()
        {
            var condOperators = Utils.EnumToDic<CondOperator>();
            condOperators.Remove((int)CondOperator.Or);
            return condOperators;
        }

        private static string GetStrCondition(VMUpdateTableInfoApiConfig data)
        {
            return data.Conditions.IsEmpty() ? string.Empty : Utils.Serialize(data.Conditions);
        }
        /// <summary>
        /// Get json param
        /// </summary>
        /// <param name="config"></param>
        /// <returns></returns>
        private string GetParamJson(TableInfoApiConfig config)
        {
            if (config.Type == (int)TableInfoApiConfigType.RunScriptSQL)
            {
                return GetParamJsonByScriptSQL(config.Description);
            }
            else
            {
                //{
                //  "apiid":"286",
                //  "databaseName":"DasCategory",
                //  "tableName":"Test",
                //  "viewColumns":["TenDanhMuc","ID","NguoiTao"],
                //  "query":[{ "column":"ID","operators":">","value":"1","clause":""}],
                //  "rowLimit":10,
                //  "orderBy":["ID desc"],
                //  "pageSize":1,
                //  "page":1
                //}

                var json = "{";
                var dictParam = new Dictionary<string, string>()
                {
                    // {"apiid", config.ID.ToString() }
                };

                if (config.Type == (int)TableInfoApiConfigType.GetTableRecord)
                {
                    if (config.ViewColumn.IsEmpty())
                    {
                        dictParam.Add("viewColumns", "[\"TenDanhMuc\", \"ID\", \"NguoiTao\"]");
                    }
                    if (config.Condition.IsEmpty())
                    {
                        dictParam.Add("query", "[{\"column\":\"ID\",\"operators\":\">\",\"value\":\"1\",\"clause\":\"\"}]");
                    }
                    if (config.Limit <= 0)
                    {
                        dictParam.Add("rowLimit", "10");
                        dictParam.Add("pageSize", "1");
                        dictParam.Add("page", "1");
                    }
                    if (config.OrderBy.IsEmpty())
                    {
                        dictParam.Add("orderBy", "ID desc");
                    }
                }

                var numberKeys = new[] { "query", "rowLimit", "pageSize", "page" };
                var index = 0;
                foreach (var item in dictParam)
                {
                    if (index > 0)
                    {
                        json += ",";
                    }
                    index++;
                    if (numberKeys.Contains(item.Key))
                    {
                        json += $"\"{item.Key}\":{item.Value}";
                    }
                    else if (item.Key == "orderBy")
                    {
                        json += $"\"{item.Key}\":[\"{item.Value}\"]";
                    }
                    else
                    {
                        json += $"\"{item.Key}\":\"{item.Value}\"";
                    }
                }
                json += "}";
                return json;
            }

        }

        private string GetParamJsonByScriptSQL(string sqlQuery)
        {
            string pattern = @"\{(\w+)\}";
            Regex regex = new Regex(pattern);
            MatchCollection matches = regex.Matches(sqlQuery);
            var props = new List<string>();

            var json = "{ ";
            var prs = new List<string>();
            foreach (Match match in matches)
            {
                var key = match.Groups[1].Value;
                if (key.IndexOf("columns") > -1)
                {
                    props.Add($"\"column\": \"{key}\"");
                }
                else
                    prs.Add($"\"{key}\":\"value_{key}\"");
            }

            if (prs.IsNotEmpty())
            {
                props.Add("\"values\": {" + string.Join(", ", prs) + "}");
            }
            if (props.IsNotEmpty())
                json += string.Join(", ", props);

            json += "}";
            return json;
        }

        /// <summary>
        /// Lấy danh sách cột theo id bảng
        /// </summary>
        /// <param name="idTables">ID bảng</param>
        /// <returns></returns>
        private async Task<IEnumerable<ColumnTableInfo>> GetColumnByIDTable(params int[] idTables)
        {
            var temp = from col in _dasKTNNRepo.ColumnTableInfo.GetAll()
                       where idTables.IsNotEmpty() && idTables.Contains(col.IDTable)
                       && col.DbName != SystemField.ID.ToString()
               //&& (!col.IsSystem || col.IsSubColumn) && !col.DbName.Contains("_DataLookUp")
                       orderby col.Weight
                       select col;
            return await temp.ToListAsync();
        }
        #endregion
    }
}