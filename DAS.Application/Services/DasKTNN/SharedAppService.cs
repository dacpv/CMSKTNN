﻿using AutoMapper;
using Dapper;
using DAS.Application.Constants;
using DAS.Application.Enums;
using DAS.Application.Enums.DasKTNN;
using DAS.Application.Interfaces;
using DAS.Application.Interfaces.DasKTNN;
using DAS.Application.Models.CustomModels;
using DAS.Application.Models.MobileApiModel;
using DAS.Application.Models.ViewModels;
using DAS.Application.Models.ViewModels.DasKTNN;
using DAS.Domain.Enums;
using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Interfaces.DASNotify;
using DAS.Domain.Models.DASKTNN;
using DAS.Infrastructure.ContextAccessors;
using DAS.Infrastructure.Repositories.DASKTNN;
using DAS.Utility;
using DAS.Utility.CustomClass;
using DAS.Utility.LogUtils;
using DocumentFormat.OpenXml.Office2010.Excel;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAS.Application.Services.DasKTNN
{
    public class SharedAppService : BaseMasterService, ISharedAppService
    {
        #region Properties
        private readonly IMapper _mapper;
        private readonly ILoggerManager _logger;
        private readonly IModuleService _module;
        private readonly IDynamicDBService _dynamicDBService;
        private readonly IDefaultDataService _defaultDataService;
        private readonly IUserPrincipalService _userPrincipalService;
        private ICacheManagementServices _cacheManagementServices;

        #endregion

        #region Ctor
        public SharedAppService(IDasRepositoryWrapper dasRepository, IDasKTNNRepositoryWrapper dasKTNN
            , IMapper mapper
            , ILoggerManager logger
            , IModuleService module
            , IDynamicDBService dynamicDBService
            , IDefaultDataService defaultDataService
            , IUserPrincipalService userPrincipalService
            , ICacheManagementServices cacheManagementServices, IDasDataDapperRepo dasDapperRepo) : base(dasRepository, dasDapperRepo, dasKTNN)
        {
            _mapper = mapper;
            _logger = logger;
            _module = module;
            _userPrincipalService = userPrincipalService;
            _dynamicDBService = dynamicDBService;
            _cacheManagementServices = cacheManagementServices;
            _defaultDataService = defaultDataService;
        }

        #endregion

        #region Gets  


        public async Task<IEnumerable<SharedApp>> GetsList()
        {
            var temp = from ct in _dasKTNNRepo.SharedApp.GetAll()
                       orderby ct.Name
                       select ct;
            return await temp.ToListAsync();
        }

        public async Task<SharedApp> Get(int id)
        {
            return await _dasKTNNRepo.SharedApp.FirstOrDefaultAsync(n => n.ID == id);
        }

        public async Task<PaginatedList<SharedApp>> SearchByConditionPagging(SharedAppCondition condition)
        {
            UserData userData = await _cacheManagementServices.GetUserDataAndSetCache();

            var temp = from ct in _dasKTNNRepo.SharedApp.GetAll()
                       where (condition.Keyword.IsEmpty() || ct.Name.ToLower().Contains(condition.Keyword.ToLower()))

                       orderby ct.UpdatedDate ?? ct.CreateDate descending
                       select ct;
            var total = await temp.LongCountAsync();
            int totalPage = (int)Math.Ceiling(total / (double)condition.PageSize);
            if (totalPage < condition.PageIndex)
            {
                condition.PageIndex = 1;
            }
            var result = await temp.Skip((condition.PageIndex - 1) * condition.PageSize).Take(condition.PageSize).ToListAsync();
            return new PaginatedList<SharedApp>(result, (int)total, condition.PageIndex, condition.PageSize);
        }

        #endregion

        #region Create

        public async Task<VMSharedApp> Create()
        {
            var model = new VMSharedApp()
            {
                sharedApp = new SharedApp(),
            };

            return model;
        }

        public async Task<VMSharedApp> Create(Hashtable data)
        {
            var model = new VMSharedApp();
            try
            {
                var item = Utils.Bind<SharedApp>(data);
                if (Utils.IsEmpty(item.Code))
                {
                    model.Message = "Mã ứng dụng không để trống";
                    model.IsSuccess = false;
                    return model;
                }
                if (Utils.IsEmpty(item.Name))
                {
                    model.Message = "Tên ứng dụng không để trống";
                    model.IsSuccess = false;
                    return model;
                }

                if (await _dasKTNNRepo.SharedApp.IsCodeExist(item.Code) == true)
                {
                    model.Message = $"Mã ứng dụng {item.Code} đã tồn tại trong hệ thống";
                    model.IsSuccess = false;
                    return model;
                }

                if (await _dasKTNNRepo.SharedApp.IsNameExist(item.Name) == true)
                {
                    model.Message = $"Tên ứng dụng {item.Name} đã tồn tại trong hệ thống";
                    model.IsSuccess = false;
                    return model;
                }
                if (Utils.IsEmpty(item.Username))
                {
                    model.Message = "Username không để trống";
                    model.IsSuccess = false;
                    return model;
                }
                else if (await _dasKTNNRepo.SharedApp.IsUserNameExist(item.Username) == true)
                {
                    model.Message = "Username đã tồn tại, vui lòng nhập Username khác";
                    model.IsSuccess = false;
                    return model;
                }
                if (Utils.IsEmpty(item.Password))
                {
                    model.Message = "Password không để trống";
                    model.IsSuccess = false;
                    return model;
                }
                if (Utils.IsNotEmpty(item.Password))
                {
                    item.Password = StringUltils.Md5Encryption(item.Password);
                }
                UserData userData = await _cacheManagementServices.GetUserDataAndSetCache();
                item.CreateDate = DateTime.Now;
                item.CreatedBy = _userPrincipalService.UserId;
                await _dasKTNNRepo.SharedApp.InsertAsync(item);
                await _dasKTNNRepo.SaveAync();
                if (item.ID > 0)
                {
                    model.Message = "Thêm dữ liệu thành công";
                    model.IsSuccess = true;
                    return model;
                }
                else
                {
                    model.Message = "Thêm dữ liệu không thành công";
                    model.IsSuccess = true;
                    return model;
                }


            }
            catch (LogicException ex)
            {
                model.Message = ex.Message;
                model.IsSuccess = false;
                return model;
            }
            catch (Exception ex)
            {
                model.Message = "Có lỗi khi thêm mới bảng dữ liệu";
                model.IsSuccess = false;
                return model;
            }
        }
        #endregion

        #region Update
        public async Task<VMSharedApp> Update(int? id)
        {
            var group = await Get(id ?? 0);
            if (group == null || group.ID == 0)
            {
                throw new LogicException("Bảng dữ liệu không còn tồn tại");
            }
            var tableInfos = await _dasKTNNRepo.GroupTableInfo.GetAllListAsync(n => n.IDGroup == id);
            var model = new VMSharedApp
            {
                sharedApp = group,
            };
            return model;
        }

        public async Task<VMSharedApp> Update(Hashtable data)
        {
            var model = new VMSharedApp();
            try
            {
                var ID = Utils.GetInt(data, "ID");
                var info = _dasKTNNRepo.SharedApp.Get(ID);
                if (Utils.IsEmpty(info))
                {
                    model.Message = "Bảng ứng dụng này không tồn tại";
                    model.IsSuccess = false;
                    return model;
                }
                var item = Utils.Bind<SharedApp>(info, data);
                if (Utils.IsEmpty(item.Code))
                {
                    model.Message = "Mã ứng dụng không để trống";
                    model.IsSuccess = false;
                    return model;
                }
                if (Utils.IsEmpty(item.Name))
                {
                    model.Message = "Tên ứng dụng không để trống";
                    model.IsSuccess = false;
                    return model;
                }
                if (Utils.IsEmpty(item.Username))
                {
                    model.Message = "Username không để trống";
                    model.IsSuccess = false;
                    return model;
                }
                else if (await _dasKTNNRepo.SharedApp.IsUserNameExist(item.Username, item.ID) == true)
                {
                    model.Message = "Username đã tồn tại, vui lòng nhập Username khác";
                    model.IsSuccess = false;
                    return model;
                }
                if (await _dasKTNNRepo.SharedApp.IsCodeExist(item.Code, item.ID) == true)
                {
                    model.Message = $"Mã ứng dụng {item.Code} đã tồn tại trong hệ thống";
                    model.IsSuccess = false;
                    return model;
                }
                if (await _dasKTNNRepo.SharedApp.IsNameExist(item.Name, item.ID) == true)
                {
                    model.Message = $"Tên ứng dụng {item.Name} đã tồn tại trong hệ thống";
                    model.IsSuccess = false;
                    return model;
                }
                if (Utils.IsNotEmpty(item.Password))
                {
                    item.Password = StringUltils.Md5Encryption(item.Password);
                }
                else
                {
                    item.Password = info.Password;
                }
                item.UpdatedDate = DateTime.Now;
                item.UpdatedBy = _userPrincipalService.UserId;
                await _dasKTNNRepo.SharedApp.UpdateAsync(item);
                await _dasKTNNRepo.SaveAync();
                if (item.ID > 0)
                {
                    model.Message = "Cập nhật dữ liệu thành công";
                    model.IsSuccess = true;
                    return model;
                }
                else
                {
                    model.Message = "Cập nhật dữ liệu không thành công";
                    model.IsSuccess = true;
                    return model;
                }

            }
            catch (LogicException ex)
            {
                model.Message = ex.Message;
                model.IsSuccess = false;
                return model;
            }
            catch (Exception ex)
            {
                model.Message = "Có lỗi khi thêm mới bảng dữ liệu";
                model.IsSuccess = false;
                return model;
            }
        }



        #endregion

        #region Delete
        public async Task<ServiceResult> Delete(int id)
        {
            try
            {
                var tableInfo = await _dasKTNNRepo.SharedApp.GetAsync(id);
                if (tableInfo == null)
                    return new ServiceResultError("Bảng ứng dụng này hiện không tồn tại hoặc đã bị xóa");

                await _dasKTNNRepo.SharedApp.DeleteAsync(tableInfo);
                await _dasKTNNRepo.SaveAync();
                //Update Module
                return new ServiceResultSuccess("Xóa dữ liệu thành công");
            }
            catch (LogicException ex)
            {
                return new ServiceResultError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return new ServiceResultError(ex.Message);

            }
        }

        public async Task<ServiceResult> Delete(IEnumerable<int> ids)
        {
            try
            {
                var tableInfoDeletes = await _dasKTNNRepo.SharedApp.GetAllListAsync(n => ids.Contains(n.ID));
                if (tableInfoDeletes == null || tableInfoDeletes.Count() == 0)
                    return new ServiceResultError("Bảng ứng dụng đã chọn hiện không tồn tại hoặc đã bị xóa");
                foreach (var item in tableInfoDeletes)
                {
                    await _dasKTNNRepo.SharedApp.DeleteAsync(item);
                }
                await _dasKTNNRepo.SaveAync();
                return new ServiceResultSuccess("Xóa dữ liệu thành công");
            }
            catch (LogicException ex)
            {
                return new ServiceResultError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return new ServiceResultError(ex.Message);
            }
        }
        #endregion

        #region Authencation
        public async Task<ServiceResult> Authenticate(VMMobileReaderLogin loginModel)
        {
            try
            {
                var reader = await _dasKTNNRepo.SharedApp.SingleOrDefaultAsync(x => x.Username == loginModel.UserName && x.Password == StringUltils.Md5Encryption(loginModel.Password));
                if (reader == null)
                {
                    return new ServiceResultError("Thông tin tài khoản không chính xác");
                }
                return new ServiceResultSuccess(reader);
            }
            catch (Exception ex)
            {
                _logger.LogError("Đăng nhập" + ex.Message);
                return new ServiceResultError("Thông tin tài khoản không chính xác");
            }

        }
        #endregion

        #region Thống kê

        public async Task<VMSharedAppStatisticIndex> AppByApiStats(SharedAppStatsCondition condition)
        {
            var model = new VMSharedAppStatisticIndex();
            var tempApps = from ct in _dasKTNNRepo.SharedApp.GetAll()
                           where (condition.Keyword.IsEmpty() || ct.Name.ToLower().Contains(condition.Keyword.ToLower()))
                            && (condition.IDSharedApp == 0 || condition.IDSharedApp == ct.ID)
                           orderby ct.UpdatedDate ?? ct.CreateDate descending
                           select ct;
            var total = await tempApps.LongCountAsync();
            int totalPage = (int)Math.Ceiling(total / (double)condition.PageSize);
            if (totalPage < condition.PageIndex)
            {
                condition.PageIndex = 1;
            }
            var resultApps = await tempApps.Skip((condition.PageIndex - 1) * condition.PageSize).Take(condition.PageSize).ToListAsync();
            model.SharedAppPagings = new PaginatedList<SharedApp>(resultApps, (int)total, condition.PageIndex, condition.PageSize);

            var idApps = model.SharedAppPagings.Select(n => n.ID).ToArray();

            var temp = from n in _dasKTNNRepo.TanXuatTruyCapAPI.GetAll()
                       where (!condition.StartDate.HasValue || n.CreateDate >= condition.StartDate.Value.Date)
                      && (idApps.IsNotEmpty() && idApps.Contains(n.IDSharedApp))
                      && (!condition.EndDate.HasValue || n.CreateDate <= condition.EndDate.Value.AddDays(1).AddMinutes(-1))
                       orderby n.APIName, n.NoiDung
                       group n by new
                       {
                           n.APIName,
                           n.IDSharedApp,
                       } into g
                       select new VMSharedAppStatistic
                       {
                           APIName = g.Key.APIName,
                           IDSharedApp = g.Key.IDSharedApp,
                       };

            var sharedAppStats = await temp.ToListAsync();
            sharedAppStats = sharedAppStats.GroupBy(n => new { n.IDSharedApp }).Select(g => new VMSharedAppStatistic
            {
                IDSharedApp = g.Key.IDSharedApp,
                Count = g.Count(),
            }).OrderByDescending(n => n.Count).ToList();

            model.SharedAppStatistics = new PaginatedList<VMSharedAppStatistic>(sharedAppStats);
            model.Condition = condition;
            return model;
        }

        public async Task<VMSharedAppStatisticIndex> SharedAppFrequencyStats(SharedAppStatsCondition condition)
        {
            var model = new VMSharedAppStatisticIndex();
            var tempApps = from ct in _dasKTNNRepo.SharedApp.GetAll()
                           where (condition.Keyword.IsEmpty() || ct.Name.ToLower().Contains(condition.Keyword.ToLower()))
                            && (condition.IDSharedApp == 0 || condition.IDSharedApp == ct.ID)
                           orderby ct.UpdatedDate ?? ct.CreateDate descending
                           select ct;
            var total = await tempApps.LongCountAsync();
            int totalPage = (int)Math.Ceiling(total / (double)condition.PageSize);
            if (totalPage < condition.PageIndex)
            {
                condition.PageIndex = 1;
            }
            var resultApps = await tempApps.Skip((condition.PageIndex - 1) * condition.PageSize).Take(condition.PageSize).ToListAsync();
            model.SharedAppPagings = new PaginatedList<SharedApp>(resultApps, (int)total, condition.PageIndex, condition.PageSize);

            var idApps = model.SharedAppPagings.Select(n => n.ID).ToArray();

            var temp = from n in _dasKTNNRepo.TanXuatTruyCapAPI.GetAll()
                       where (!condition.StartDate.HasValue || n.CreateDate >= condition.StartDate.Value.Date)
                      && (idApps.IsNotEmpty() && idApps.Contains(n.IDSharedApp))
                      && (!condition.EndDate.HasValue || n.CreateDate <= condition.EndDate.Value.AddDays(1).AddMinutes(-1))
                       orderby n.APIName, n.NoiDung
                       group n by n.IDSharedApp into g
                       select new VMSharedAppStatistic
                       {
                           IDSharedApp = g.Key,
                           Count = g.Count()
                       };

            var sharedAppStats = await temp.ToListAsync();
            model.SharedAppStatistics = new PaginatedList<VMSharedAppStatistic>(sharedAppStats);
            model.Condition = condition;
            return model;
        }

        public async Task<VMSharedAppStatisticIndex> SharedAppStats(SharedAppStatsCondition condition)
        {
            var model = new VMSharedAppStatisticIndex();

            var temp = from n in _dasKTNNRepo.TanXuatTruyCapAPI.GetAll()
                       where (condition.Keyword.IsEmpty()
                       || n.APIName.ToLower().Contains(condition.Keyword.ToLower()) || n.NoiDung.ToLower().Contains(condition.Keyword.ToLower()))
                      && (!condition.StartDate.HasValue || n.CreateDate >= condition.StartDate.Value.Date)
                      && (condition.IDSharedApp == 0 || condition.IDSharedApp == n.IDSharedApp)
                      && (!condition.EndDate.HasValue || n.CreateDate <= condition.EndDate.Value.AddDays(1).AddMinutes(-1))
                       orderby n.APIName, n.NoiDung
                       group n by new
                       {
                           n.APIName,
                           n.NoiDung,
                           n.IDSharedApp
                       } into g

                       select new VMSharedAppStatistic
                       {
                           APIName = g.Key.APIName,
                           NoiDung = g.Key.NoiDung,
                           IDSharedApp = g.Key.IDSharedApp,
                           Count = g.Count()
                       };
            var total = await temp.LongCountAsync();
            int totalPage = (int)Math.Ceiling(total / (double)condition.PageSize);
            if (totalPage < condition.PageIndex)
            {
                condition.PageIndex = 1;
            }
            var sharedAppStats = await temp.ToListAsync();

            var order = sharedAppStats.OrderByDescending(n => n.Count).Select(n => n.ID).ToList();

            //Gộp theo api name
            var chartTableByAPIName = sharedAppStats.Where(n => n.Count > 0).GroupBy(n => n.APIName).Select(n => new VMSharedAppStatistic
            {
                APIName = n.Key,
                Count = n.Sum(x => x.Count)
            }).OrderBy(n => n.APIName).ToList();
            var chartData = new
            {
                labels = chartTableByAPIName.Select(n => n.APIName).ToArray(),
                data = chartTableByAPIName.Select(n => n.Count).ToArray(),
            };
            sharedAppStats = sharedAppStats.OrderBy(n => order.IndexOf(n.ID)).Skip((condition.PageIndex - 1) * condition.PageSize).Take(condition.PageSize).ToList();

            model.SharedAppStatistics = new PaginatedList<VMSharedAppStatistic>(sharedAppStats, (int)total, condition.PageIndex, condition.PageSize);
            model.ChartData = chartData;
            model.SharedApps = await _dasKTNNRepo.SharedApp.GetAllListAsync();
            model.Condition = condition;
            return model;
        }
        #endregion
    }
}