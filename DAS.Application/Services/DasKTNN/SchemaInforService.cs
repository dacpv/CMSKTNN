﻿using AutoMapper;
using DAS.Application.Interfaces;
using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels;
using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Models.DAS;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using NLog.Config;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using DAS.Application.Constants;
using Newtonsoft.Json;
using DAS.Utility;
using DAS.Domain.Interfaces.DASNotify;
using DAS.Infrastructure.ContextAccessors;
using DAS.Domain.Models.DASKTNN;
using DAS.Infrastructure.Repositories.DASKTNN;
using DAS.Application.Interfaces.DasKTNN;
using DocumentFormat.OpenXml.Presentation;
using DAS.Application.Enums;
using DAS.Utility.LogUtils;
using DocumentFormat.OpenXml.Office2010.Excel;
using Dapper;

namespace DAS.Application.Services.DasKTNN
{
    public class SchemaInforService : BaseMasterService, ISchemaInfoService
    {
        #region Properties
        private readonly IMapper _mapper;
        private readonly ILoggerManager _logger;
        private readonly IUserService _userService;
        private readonly ICacheManagementServices _cacheManagementServices;
        private readonly IUserPrincipalService _userPrincipalService;
        private readonly IDynamicDBService _dynamicDBService;
        private readonly ITableInfoServices _tableInfoServices;
        #endregion

        #region Ctor
        public SchemaInforService(IDasRepositoryWrapper dasRepository, ILoggerManager logger
            , IMapper mapper, ICacheManagementServices cacheManagementServices
            , IUserService userService, IUserPrincipalService iUserPrincipalService, ITableInfoServices tableInfoServices,
            IDasKTNNRepositoryWrapper dasKTNNRepositoryWrapper, IDasDataDapperRepo dasDapperRepo, IDynamicDBService dynamicDBService) : base(dasRepository, dasDapperRepo, dasKTNNRepositoryWrapper)
        {
            _mapper = mapper;
            _logger = logger;
            _userService = userService;
            _tableInfoServices = tableInfoServices;
            _dasKTNNRepo = dasKTNNRepositoryWrapper;
            _cacheManagementServices = cacheManagementServices;
            _userPrincipalService = iUserPrincipalService;
            _dynamicDBService = dynamicDBService;
        }
        #endregion

        #region Get
        public async Task<SchemaInfo> Get(object id)
        {
            return await _dasKTNNRepo.SchemaInfo.GetAsync(id);
        }

        public async Task<VMSchemaInfo> GetByID(int? id)
        {
            var schemaInfo = await Get(id.Value);
            var model = _mapper.Map<VMSchemaInfo>(schemaInfo);
            return model;
        }

        public async Task<PaginatedList<VMSchemaInfo>> SearchByConditionPagging(SchemaInfoCondition condition)
        {
            var temp = from ct in _dasKTNNRepo.SchemaInfo.GetAll()
                       where (condition.Keyword.IsEmpty() || ct.Name.Contains(condition.Keyword))
                       orderby ct.UpdatedDate ?? ct.CreateDate descending
                       select _mapper.Map<VMSchemaInfo>(ct);


            var total = await temp.LongCountAsync();
            int totalPage = (int)Math.Ceiling(total / (double)condition.PageSize);
            if (totalPage < condition.PageIndex)
            {
                condition.PageIndex = 1;
            }
            var result = await temp.Skip((condition.PageIndex - 1) * condition.PageSize).Take(condition.PageSize).ToListAsync();
            return new PaginatedList<VMSchemaInfo>(result, (int)total, condition.PageIndex, condition.PageSize);
        }

        #endregion

        #region Create
        public async Task<ServiceResult> Save(VMSchemaInfo vmSchemaInfo)
        {
            try
            {
                var schemaInfo = _mapper.Map<SchemaInfo>(vmSchemaInfo);
                if (await _dasKTNNRepo.SchemaInfo.IsCodeExist(schemaInfo.Code, schemaInfo.ID))
                    return new ServiceResultError("Tên schema đã tồn tại");

                var valCheck = _dynamicDBService.IsCheckSchema(schemaInfo.Name);
                if (valCheck == 0)
                {
                    if (_dynamicDBService.CreateSchema(schemaInfo.Code, schemaInfo.Password))
                    {
                        schemaInfo.CreatedBy = _userPrincipalService.UserId;
                        schemaInfo.CreateDate = DateTime.Now;
                        await _dasKTNNRepo.SchemaInfo.InsertAsync(schemaInfo);
                        await _dasKTNNRepo.SaveAync();
                        return new ServiceResultSuccess("Thêm mới schema thành công");
                    }
                    else
                        return new ServiceResultError("Thêm mới schema không thành công");
                }    
                else if(valCheck == 1)
                    return new ServiceResultError("Đã tồn tại schema trong cơ sở dữ liệu");
                else
                    return new ServiceResultError("Thêm mới schema không thành công");
            }
            catch (Exception ex)
            {
                Guid.NewGuid().ToString();
                _logger.LogError(ex);
                return new ServiceResultError(ex.Message);
            }
        }

        #endregion

        #region Update
        public async Task<ServiceResult> Change(VMSchemaInfo vmSchemaInfo)
        {
            try
            {
                var schemaInfo = await _dasKTNNRepo.SchemaInfo.GetAsync(vmSchemaInfo.ID);
                var newSchemaInfo = _mapper.Map(vmSchemaInfo, schemaInfo);
                if (schemaInfo == null)
                    return new ServiceResultError("Schema này hiện không tồn tại hoặc đã bị xóa");
                if (await _dasKTNNRepo.SchemaInfo.IsCodeExist(schemaInfo.Code, schemaInfo.ID))
                    return new ServiceResultError("Tên schema đã tồn tại");

                schemaInfo.Bind(vmSchemaInfo.KeyValue());
                schemaInfo.UpdatedDate = DateTime.Now;
                schemaInfo.UpdatedBy = _userPrincipalService.UserId;
                await _dasKTNNRepo.SchemaInfo.UpdateAsync(schemaInfo);
                await _dasKTNNRepo.SaveAync();
                return new ServiceResultSuccess("Cập nhật thông tin schema thành công");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return new ServiceResultError(ex.Message);
            }
        }

        #endregion

        #region Delete
        public async Task<ServiceResult> Delete(int id)
        {
            try
            {
                var schemaInfo = await _dasKTNNRepo.SchemaInfo.GetAsync(id);
                if (schemaInfo == null)
                    return new ServiceResultError("Schema này hiện không tồn tại hoặc đã bị xóa");

                await _dasKTNNRepo.SchemaInfo.DeleteAsync(schemaInfo);
                await _tableInfoServices.DeleteBySchema(schemaInfo.ID);
                await _dasKTNNRepo.SaveAync();
                _dynamicDBService.DeleteSchema(schemaInfo.Name);
                return new ServiceResultSuccess("Xóa schema thành công");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return new ServiceResultError(ex.Message);
            }
        }

        public async Task<ServiceResult> Delete(IEnumerable<int> ids)
        {
            try
            {
                //Dac
                var schemaInfos = await _dasKTNNRepo.SchemaInfo.GetAllListAsync(n => ids.Contains(n.ID));
                if (schemaInfos == null || schemaInfos.Count() == 0)
                    return new ServiceResultError("Schema đã chọn hiện không tồn tại hoặc đã bị xóa");

                var idSchemas = schemaInfos.Select(x => x.ID).ToArray(); 
                var nameSchemas = schemaInfos.Select(x => x.Name).ToArray();
                await _dasKTNNRepo.SchemaInfo.DeleteAsync(schemaInfos);
                await _tableInfoServices.DeleteBySchema(idSchemas);
                await _dasKTNNRepo.SaveAync();

                _dynamicDBService.DeleteSchema(nameSchemas);
                return new ServiceResultSuccess("Xóa schema thành công");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return new ServiceResultError(ex.Message);
            }
        }

        public Task<List<SchemaInfo>> GetAllList()
        {
           return  _dasKTNNRepo.SchemaInfo.GetAll().ToListAsync();

        }
        #endregion
    }
}
