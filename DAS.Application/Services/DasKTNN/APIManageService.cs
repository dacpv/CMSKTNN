﻿using AutoMapper;
using DAS.Application.Interfaces;
using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels;
using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Models.DAS;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using NLog.Config;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using DAS.Application.Constants;
using Newtonsoft.Json;
using DAS.Utility;
using DAS.Utility.CacheUtils;
using DAS.Domain.Enums;
using Microsoft.AspNetCore.Http;
using DAS.Application.Enums;
using DAS.Domain.Interfaces.DASNotify;
using DAS.Application.Interfaces.DasCongViec;
using System.Collections;
using DAS.Application.Models.Param;
using Microsoft.VisualBasic;
using DAS.Domain.Models.DasCongViec;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using DocumentFormat.OpenXml.Drawing.Diagrams;
using DAS.Infrastructure.ContextAccessors;
using Microsoft.Data.SqlClient;
using System.Data;
using DocumentFormat.OpenXml.Office.CustomUI;
using DocumentFormat.OpenXml.Spreadsheet;
using DAS.Application.Interfaces.DasKTNN;
using DAS.Domain.Models.DASKTNN;
using DAS.Infrastructure.Repositories.DASKTNN;
using Dapper;
using DAS.Application.Models.ViewModels.DasKTNN;

namespace DAS.Application.Services.DasKTNN
{
    public class APIManageService : BaseMasterService, IAPIManageService
    {
        private readonly IAPIManageDBDapperRepo _apiMangeDBDapperRepo;
        private readonly IDasRepositoryWrapper _dasRepo;
        private readonly IMapper _mapper;
        private readonly IDistributedCache _cache;
        private readonly ICategoryServices _categoryServices;
        private readonly IUserService _userService;
        private readonly ICacheManagementServices _cacheManagementServices;
        private readonly IUserPrincipalService _userPrincipalService;

        public APIManageService(IDasRepositoryWrapper dasRepository
            , IMapper mapper, ICategoryServices categoryServices, ICacheManagementServices cacheManagementServices
            , IDistributedCache cache, IUserService userService, IUserPrincipalService iUserPrincipalService,
            IAPIManageDBDapperRepo apiMangeDBDapperRepo) : base(dasRepository, apiMangeDBDapperRepo)
        {
            _dasRepo = dasRepository;
            _mapper = mapper;
            _cache = cache;
            _categoryServices = categoryServices;
            _userService = userService;
            _cacheManagementServices = cacheManagementServices;
            _userPrincipalService = iUserPrincipalService;
            _apiMangeDBDapperRepo = apiMangeDBDapperRepo;
        }

        public VMAPIReport Search(APIReportCondition condition)
        {
            var result = new VMAPIReport();
            var sql = "exec sp_GET_API_REPORT @apiId";
            var values = new { apiId = condition.apiId};
            result.APIMangeReports = _apiManageDbDapperConn.Query<APIMangeReport>(sql, values).ToList();
            result.APIs = _apiManageDbDapperConn.Query<API>("exec sp_GET_LIST_API").ToList();
            result.Param = condition;
            return result;
        }
    }
}
