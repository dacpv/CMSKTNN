﻿using AutoMapper;
using Dapper;
using DAS.Application.Constants;
using DAS.Application.Enums.DasKTNN;
using DAS.Application.Helper;
using DAS.Application.Interfaces;
using DAS.Application.Interfaces.DasKTNN;
using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels;
using DAS.Application.Models.ViewModels.DasKTNN;
using DAS.Domain.Enums;
using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Interfaces.DASNotify;
using DAS.Domain.Models.DAS;
using DAS.Domain.Models.DASKTNN;
using DAS.Infrastructure.ContextAccessors;
using DAS.Infrastructure.Repositories.DASKTNN;
using DAS.Utility;
using DAS.Utility.BuildCondition;
using DAS.Utility.CustomClass;
using DAS.Utility.LogUtils;
using DocumentFormat.OpenXml.Office2010.Excel;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.
    Data;
using System.Linq;
using System.Threading.Tasks;

namespace DAS.Application.Services.DasKTNN
{
    public class DynamicDBService : BaseMasterService, IDynamicDBService
    {
        #region Properties
        private readonly IMapper _mapper;
        private readonly ILoggerManager _logger;
        private readonly IDistributedCache _cache;
        private readonly ICategoryServices _categoryServices;
        private readonly IUserService _userService;
        private readonly ICacheManagementServices _cacheManagementServices;
        private readonly IUserPrincipalService _userPrincipalService;
        // private readonly InputInfoService _inputInfoService;
        #endregion

        #region Ctor
        public DynamicDBService(IDasRepositoryWrapper dasRepository, IDasCongViecRepositoryWrapper dasCongViecRepository, ILoggerManager logger
        , IMapper mapper, ICategoryServices categoryServices, ICacheManagementServices cacheManagementServices
        , IDistributedCache cache, IUserService userService, IUserPrincipalService iUserPrincipalService
        , IDasKTNNRepositoryWrapper dasKTNNRepositoryWrapper, IDasDataDapperRepo dasDapperRepo
        , IDynamicDBDapperRepo dasdynamicDapperRepo//, InputInfoService inputInfoService
            ) : base(dasRepository, dasDapperRepo, dasKTNNRepositoryWrapper, dasdynamicDapperRepo)
        {
            _dasRepo = dasRepository;
            _mapper = mapper;
            _cache = cache;
            _logger = logger;
            _categoryServices = categoryServices;
            _userService = userService;
            _cacheManagementServices = cacheManagementServices;
            _userPrincipalService = iUserPrincipalService;
            _dasKTNNRepo = dasKTNNRepositoryWrapper;
            //_inputInfoService = inputInfoService;
        }
        #endregion

        #region Get
        public IEnumerable<TableTest> GetAll()
        {
            //
            GetDataTable();
            return null;
        }
        /// <summary>
        /// Lấy dữ liệu với đạng dữ liệu động , lấy về dạng datatable
        /// </summary>
        private void GetDataTable()
        {
            DataTable _dt = new DataTable();
            _dt.Load(_dasDynamicDbDapperConn.ExecuteReader("select * from SYS.TableTest"));
            /// var testcolumn = _dt.Rows[0]["ID"]; // Cách thức lấy dữ liệu
        }
        public DataTable GetListShowTable(string Schema, string tablename, string columns = "*")
        {
            DataTable _dt = new DataTable();
            if (columns == "*")
                _dt.Load(_dasDynamicDbDapperConn.ExecuteReader($"select {columns} from {Schema}.{tablename}")); //TODO tamj fi
            else
                _dt.Load(_dasDynamicDbDapperConn.ExecuteReader($"select ID,{columns} from {Schema}.{tablename}"));
            return _dt;
        }

        public DataTable GetFirstByIds(string Schema, string tablename, object Id)
        {
            DataTable _dt = new DataTable();
            var sql = $"select * from {Schema}.{tablename} where ID in ({string.Join(",", Id)})";
            _dt.Load(_dasDynamicDbDapperConn.ExecuteReader(sql));
            return _dt;
        }

        public DataTable GetFirstById(string Schema, string tablename, object Id)
        {
            try
            {
                DataTable _dt = new DataTable();
                _dt.Load(_dasDynamicDbDapperConn.ExecuteReader($"select * from {Schema}.{tablename} where ID={Id} and rownum = 1"));
                return _dt;
            }
            catch (Exception)
            {
                return null;
            }

        }
        public int GetCount(string Schema, string tablename, params CondParam[] condparams)
        {
            var where = string.Empty;
            if (Utils.IsNotEmpty(condparams))
            {
                where = "Where ( " + condparams.ToList().BuildWhere() + " )";
            }
            return _dasDynamicDbDapperConn.ExecuteScalar<int>($"select count(1) from {Schema}.{tablename} {where}");
        }
        public DataTable GetListByField(string Schema, string tablename, string columnName, object value)
        {
            DataTable _dt = new DataTable();
            _dt.Load(_dasDynamicDbDapperConn.ExecuteReader($"select * from {Schema}.{tablename} where {columnName}='{value}' "));
            return _dt;
        }
        public DataTable GetFirstByField(string Schema, string tablename, string columnName, object value, bool ignoreCase = false)
        {
            if (ignoreCase)
            {
                columnName = $"LOWER({columnName})";
                value = value != null ? value.ToString().ToLower() : string.Empty;
            }
            DataTable _dt = new DataTable();
            _dt.Load(_dasDynamicDbDapperConn.ExecuteReader($"select * from {Schema}.{tablename} where {columnName}='{value}' and rownum = 1"));
            return _dt;
        }

        public DataTable GetFirst(string Schema, string tablename, string orderBy = "", params CondParam[] condparams)
        {
            var where = string.Empty;
            if (Utils.IsNotEmpty(condparams))
            {
                where = "Where ( " + condparams.ToList().BuildWhere() + " )";
            }
            if (orderBy.IsNotEmpty())
            {
                orderBy = $"Order by {orderBy}";
            }
            DataTable _dt = new DataTable();

            _dt.Load(_dasDynamicDbDapperConn.ExecuteReader($"select * from {Schema}.{tablename} {where} {orderBy} fetch first 1 row only"));
            return _dt;
        }

        public DataTable ExecuteSql(string sql)
        {
            DataTable _dt = new DataTable();
            _dt.Load(_dasDynamicDbDapperConn.ExecuteReader(sql));
            return _dt;
        }

        public bool IsExists(string Schema, string tablename, params CondParam[] condparams)
        {
            var _dt = GetFirst(Schema, tablename, "", condparams);
            return _dt != null && _dt.Rows.Count > 0;
        }
        public DataRow GetFirstRowByField(string Schema, string tablename, string columnName, object value)
        {
            try
            {
                DataTable _dt = new DataTable();
                _dt.Load(_dasDynamicDbDapperConn.ExecuteReader($"select * from {Schema}.{tablename} " +
                    $"where {columnName}='{value}' and rownum = 1"));
                return _dt.Rows[0];
            }
            catch (Exception ex)
            {
                DataTable table = new DataTable();
                DataRow row = table.NewRow();
                return row;
            }
        }
        #endregion

        #region Schema
        public int IsCheckSchema(string nameSchema)
        {
            try
            {
                var rs = _dasDynamicDbDapperConn.ExecuteScalar<int>("SELECT CHECK_DATA('" + nameSchema + "','') FROM DUAL");
                return rs;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return -1;
            }
        }

        public bool CreateSchema(string nameSchema, string password)
        {
            try
            {
                _dasDynamicDbDapperConn.Execute("CALL prc_job_create_schema('" + nameSchema + "','" + password + "',NULL)");
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return false;
            }
        }

        public bool DeleteSchema(string nameSchema)
        {
            try
            {
                _dasDynamicDbDapperConn.Execute("CALL prc_job_drop_schema('" + nameSchema + "')");
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return false;
            }
        }

        public bool DeleteSchema(string[] nameSchemas)
        {
            try
            {
                if (nameSchemas.IsNotEmpty())
                {
                    foreach (var item in nameSchemas)
                    {
                        _dasDynamicDbDapperConn.Execute("CALL prc_job_drop_schema('" + item + "')");
                    }
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return false;
            }
        }
        #endregion

        #region Column
        public bool CreateColumn(string schemaName, TableInfo table, IEnumerable<ColumnPathInfo> columnPaths, params VMColumnTableInfo[] columns)
        {
            if (columns.IsEmpty() || table == null || table.DbName.IsEmpty() || schemaName.IsEmpty())
                return false;

            var strColumnName = DynamicDBHelper.BuildStringColumns(columns, table, columnPaths, '|');

            var query = $"CALL PRC_JOB_ALTER_TABLE_COLUMS('{schemaName}','{table.DbName}', '{strColumnName}','ADDNEW')";
            var result = _dasDynamicDbDapperConn.Execute(query);
            return true;
        }

        public bool DropColumn(string schemaName, TableInfo table, IEnumerable<ColumnPathInfo> columnPaths, params ColumnTableInfo[] columns)
        {
            if (columns.IsEmpty() || table == null || table.DbName.IsEmpty() || schemaName.IsEmpty())
                return false;

            //CALL PRC_JOB_ALTER_TABLE_COLUMS('DASADMIN','TABLE_TEST','CO1,CO2,CO3','DELETE');
            var strColumnName = DynamicDBHelper.BuildStringDelColumns(columns, columnPaths);
            var query = $"CALL PRC_JOB_ALTER_TABLE_COLUMS('{schemaName}','{table.DbName}', '{string.Join(",", strColumnName)}','DELETE')";
            var result = _dasDynamicDbDapperConn.Execute(query);
            return true;
        }

        public bool AddColumnIdentityFormat(string schemaName, TableInfo table, ColumnTableInfo column)
        {
            if (table == null || table.DbName.IsEmpty() || schemaName.IsEmpty() || !column.IsIdentity)
                return false;

            //call prc_job_generated_colum ('schema','table','column','HX'); 

            var query = $"CALL PRC_JOB_GENERATED_COLUM('{schemaName}','{table.DbName}', '{column.DbName}','{column.IdentityPrefix ?? ""}')";
            var result = _dasDynamicDbDapperConn.Execute(query);
            return true;
        }
        public bool UpdateIdentityColumns(string schemaName, TableInfo table, IEnumerable<ColumnTableInfo> columns)
        {
            if (table == null || table.DbName.IsEmpty() || schemaName.IsEmpty() || columns.IsEmpty())
                return false;

            var idetityCols = columns.Where(n => n.IsIdentity);
            if (idetityCols.IsEmpty())
                return false;

            //prc_job_generated_colums('schema','table','column1|Prefix1;column|2Prefix2;...);

            var cols = string.Join(";", idetityCols.Select(n => $"{n.DbName}|{n.IdentityPrefix}"));

            var query = $"CALL PRC_JOB_GENERATED_COLUMS('{schemaName}','{table.DbName}', '{cols}')";
            var result = _dasDynamicDbDapperConn.Execute(query);
            return true;
        }
        public bool AlertColumn()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Table
        public bool CreateTable(string schemaName, TableInfo table, IEnumerable<ColumnPathInfo> columnPaths, params VMColumnTableInfo[] columns)
        {
            if (columns.IsEmpty() || table == null || table.DbName.IsEmpty() || schemaName.IsEmpty())
                return false;
            var strColumnName = DynamicDBHelper.BuildStringColumns(columns, table, columnPaths, ',');
            var query = $"CALL prc_job_create_table('{schemaName}','{table.DbName}', '{strColumnName}')";
            var result = _dasDynamicDbDapperConn.Execute(query);
            return true;
        }

        public bool CreateTable(string schemaName, string tableName, params string[] columns)
        {
            if (columns.IsEmpty() || tableName.IsEmpty() || schemaName.IsEmpty())
                return false;

            var strColumnName = string.Join(",", columns);
            var query = $"CALL prc_job_create_table('{schemaName}','{tableName}', '{strColumnName}')";
            var result = _dasDynamicDbDapperConn.Execute(query);
            return true;
        }

        public bool DropTable(string schemaName, string tableName)
        {
            if (tableName.IsEmpty() || schemaName.IsEmpty())
                return false;
            var query = $"CALL prc_job_drop_table('{schemaName}','{tableName}')";
            _dasDynamicDbDapperConn.Execute(query);
            return true;
        }

        public bool AlertTable(string schemaName, TableInfo table, ColumnTableInfo[] oldColumns, ColumnTableInfo[] columns)
        {
            //var query = $"CALL prc_job_alter_table('{schemaName}','{tableName}', '{strColumnName}')";
            //var result = _dasDynamicDbDapperConn.Execute(query);
            return true;
        }
        public bool RenameTable(string schemaName, string oldName, string newNew)
        {
            var query = $"CALL PRC_JOB_RENAME_TABLE('{schemaName}','{oldName}', '{newNew}')";
            var result = _dasDynamicDbDapperConn.Execute(query);
            return true;
        }
        public bool CloneTable(string schemaName, string tableName, string newSchemaName, string newTableName)
        {
            var query = $"CALL PRC_JOB_CLONER_TABLE('{schemaName}','{tableName}','{newSchemaName}','{newTableName}')";
            var result = _dasDynamicDbDapperConn.Execute(query);
            return true;
        }
        public int CheckTableExists(string schemaName, string tableName)
        {
            try
            {
                var rs = _dasDynamicDbDapperConn.ExecuteScalar<int>($"SELECT CHECK_DATA('{schemaName}','{tableName}') FROM DUAL");
                return rs;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return -1;
            }
        }

        public bool CloneTableCustom(string schemaName, string tableName, string newSchemaName, string newTableName, bool isGetAll, int[] idRecords, int level)
        {
            if (isGetAll)
            {
                return CloneTable(schemaName, tableName, newSchemaName, newTableName);
            }
            else
            {
                level = level - 1; //Cấp user truyền vào từ 1, trên db từ 0 

                //"CALL PRC_JOB_CLONER_TABLE_LEVER(schemacu, bảng cũ, schemamoi, bảng mới, islaytatca, ids, level )";
                //islaytatca truyền là true hoặc false,ids: truyền id cột dạng id1 | id2 | id3 | id4 level truyền số: 0 - 1 - 2 - 3......,
                var query = $"CALL PRC_JOB_CLONER_TABLE_LEVER('{schemaName}','{tableName}','{newSchemaName}','{newTableName}', 'false', '{string.Join("|", idRecords)}',{level} )";
                var result = _dasDynamicDbDapperConn.Execute(query);
                return true;
            }
        }

        #endregion

        #region Get Data
        public int Count(List<CondParam> condparams, string Schema, string tablename, string columns = "*", string orderby = "", int limit = 0)
        {
            DataTable _dt = new DataTable();
            var where = "";
            if (Utils.IsNotEmpty(condparams))
            {
                where = " Where ( " + condparams.BuildWhere() + " )";
            }
            return _dasDynamicDbDapperConn.ExecuteScalar<int>($"select count(1) from {Schema}.{tablename} {where}");
        }
        public DataTable SearchPage(List<CondParam> condparams, string Schema, string tablename, int page, int pageSize, ref Int64 TotalRow, bool checkApproveData = false, string columns = "*", string orderby = "ID asc")
        {
            DataTable _dt = new DataTable();
            var where = "";
            if (Utils.IsNotEmpty(condparams))
            {
                where = " Where " + condparams.BuildWhere();
            }
            var selectColumns = "*";
            if (columns != "*")
                selectColumns = $"ID,{columns}";

            var query = string.Format(@"WITH TempResult as (
                                            select
                                                *
                                            from(
                                                SELECT 
                                                    {5}
                                                FROM {0} tableTemp 
                                            )tableTemp
                                            {1}
                                        ),
                                        TempCount as (
                                            select
                                                COUNT(*) As TotalRow
                                            from TempResult
                                        )
                                        select
                                            *
                                        from TempResult , TempCount
                                        Order by {2}
                                        OFFSET (({3} - 1)* {4}) ROW FETCH NEXT {4} ROW ONLY
                                        ",
                                      /*0*/"" + Schema + "." + tablename,
                                      /*1*/where,
                                      /*2*/orderby,
                                      /*3*/page,
                                      /*4*/pageSize,
                                      /*5*/selectColumns
                                  );

            //var rows = _dasDynamicDbDapperConn.Query(query, null).ToList();
            //if (rows.Count > 0)
            //{
            //    TotalRow = Convert.ToInt64( rows[0].TOTALROW);
            //}
            _dt.Load(_dasDynamicDbDapperConn.ExecuteReader(query));
            if (_dt.Rows.Count > 0)
            {
                TotalRow = Convert.ToInt64(_dt.Rows[0]["TotalRow"]);
            }
            return _dt;
        }
        public DataTable Search(List<CondParam> condparams, string Schema, string tablename, string columns = "*", string orderby = "", Pagination page = null)
        {
            if (page == null)
                page = new Pagination
                {
                    PageIndex = 1,
                    PageSize = 10000000,
                };
            DataTable _dt = new DataTable();
            var where = "";
            if (Utils.IsNotEmpty(condparams))
            {
                where = " Where ( " + condparams.BuildWhere() + " )";
            }
            if (string.IsNullOrEmpty(orderby))
                orderby = "ID";
            if (columns != "*")
                columns = "ID , " + columns;
            var query = string.Format(@"WITH TempResult as (
                                            select
                                                *
                                            from(
                                                SELECT 
                                                    {5}
                                                FROM {0} tableTemp 
                                            )tableTemp
                                            {1}
                                        ),
                                        TempCount as (
                                            select
                                                COUNT(*) As TotalRow
                                            from TempResult
                                        )
                                        select
                                            *
                                        from TempResult , TempCount
                                        Order by {2}
                                        OFFSET (({3} - 1)* {4}) ROW FETCH NEXT {4} ROW ONLY
                                        ",
                                      /*0*/"" + Schema + "." + tablename,
                                      /*1*/where,
                                      /*2*/orderby,
                                      /*3*/page.PageIndex,
                                      /*4*/page.PageSize,
                                      /*5*/columns
                                  );
            _dt.Load(_dasDynamicDbDapperConn.ExecuteReader(query));
            if (_dt.Rows.Count > 0)
            {
                page.TotalItem = Convert.ToInt32(_dt.Rows[0]["TotalRow"]);
                page.TotalPage = (int)Math.Ceiling((float)page.TotalItem / page.PageSize);
            }
            return _dt;
        }

        #endregion

        #region Data
        public bool InsertData(string sql, DynamicParameters param, ref int Idinserted)
        {
            try
            {
                var result = _dasDynamicDbDapperConn.Execute(sql, param);
                Idinserted = param.Get<int>("Id");
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return false;
            }
        }
        public bool InsertData(string sql, object param)
        {
            try
            {
                var result = _dasDynamicDbDapperConn.Execute(sql, param);
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return false;
            }
        }
        public bool UpdateData(string sql, object param)
        {
            try
            {
                _dasDynamicDbDapperConn.Execute(sql, param);
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return false;
            }
        }

        public bool DeleteData(string sql, object param)
        {
            try
            {
                _dasDynamicDbDapperConn.Execute(sql, param);
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return false;
            }
        }
        public bool DeleteDataByIDRecordLink(string schemaName, string tableName, params int[] idRecord)
        {
            try
            {
                if (!idRecord.IsNotEmpty())
                    return false;
                var query = $"DELETE FROM {schemaName}.{tableName} where IDRecord in ({string.Join(",", idRecord)})";
                _dasDynamicDbDapperConn.Execute(query);
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return false;
            }
        }

        public bool TruncateTable(string schemaName, string tableName)
        {
            try
            {
                if (tableName.IsEmpty() || schemaName.IsEmpty())
                    return false;
                var query = $"TRUNCATE TABLE {schemaName}.{tableName}";
                _dasDynamicDbDapperConn.Execute(query);
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return false;
            }
        }
        #endregion

        #region Get DataAPI
        public async Task<VMResultAPISearch> Getitems(VMParamAPI input)
        {
            // mặc định nếu Rowlit có giá trị thì sẽ chỉ số lượng bản ghi theo rowLimit
            var paging = new Pagination
            {
                PageIndex = input.page == 0 ? 1 : input.page,
                PageSize = input.page == 0 ? input.rowLimit == 0 ? 10000000 : input.rowLimit : input.pageSize,
            };
            paging.PageSize = input.rowLimit == 0 ? paging.PageSize : input.rowLimit;
            var resultAPI = new VMResultAPISearch();
            var tableInfo = new TableInfo();
            //Check Table tin đầu vào có chính xác hay không
            // check Table và Schema
            if (!CheckTableExists(input.tableName, input.databaseName, out tableInfo))
            {
                resultAPI.IsSuccess = false;
                resultAPI.Message = "Tên bảng không hợp lệ";
                return resultAPI;
            }
            if (Utils.IsNotEmpty(input.viewColumns) && !CheckColumnExistColumns(tableInfo.ID, input.viewColumns))
            {
                resultAPI.IsSuccess = false;
                resultAPI.Message = "Tồn tại tên cột không tồn tại trên bảng";
                return resultAPI;
            }
            // View lại dữ liệu hiển thị
            // CHuyển dữ liệu datatable về dạng dữ liệu show
            var model = new VMInputInfo();
            UserData userData = await _cacheManagementServices.GetUserDataAndSetCache();
            model.tableInfo = tableInfo;
            model.schemaInfo = _dasKTNNRepo.SchemaInfo.Get(model.tableInfo.IDSchema) ?? new SchemaInfo();
            model.columnTables = await GetColumnTables(model.tableInfo.ID) ?? new List<ColumnTableInfo>();
            model.Users = await _dasRepo.User.GetAllListAsync() ?? new List<User>();
            //Tính toán điều kiện đầu vào
            var lstCondParam = new List<CondParam>();
            if (Utils.IsNotEmpty(input.query))
            {
                // values.Add($"TO_DATE('{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}', 'yyyy-MM-dd hh24:mi:ss')");
                //Check điều kiện về hiệu lực, nếu không nhập ngày tháng thì mặc định lấy còn hiệu lực
                if (model.columnTables.Select(t => t.DbName.ToUpper()).Contains("HIEULUCTU"))
                {
                    var lstcls = input.query.Select(t => t.column.ToUpper());
                    if (!lstcls.Contains("HIEULUCTU") && !lstcls.Contains("HIEULUCDEN"))
                    {
                        lstCondParam.Add(new CondParam
                        {
                            Sql = $"(HIEULUCDEN >= TO_DATE('{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}', 'yyyy-MM-dd hh24:mi:ss') OR HIEULUCDEN Is Null)"
                        });
                    }
                }
                foreach (var item in input.query)
                {
                    // Fix tam code cho kiem toan 2/3/2022- neu DanhMucCha thi chuyen ve ID
                    if (item.column.ToUpper() == "DANHMUCCHA")
                    {
                        var rs = GetFirstByField(input.databaseName, input.tableName, "MADANHMUC",item.value);
                        if (rs != null && rs.Rows.Count > 0)
                        {
                            item.value = rs.Rows[0]["ID"].ToString();
                        }
                    }
                    var condParam = new CondParam();
                    condParam.FieldName = item.column;
                    condParam.Value = item.value;
                    condParam.Clause = item.clause;
                    switch (item.operators.Trim().ToUpper())
                    {
                        case "=":
                            condParam.Operator = CondOperator.Equal;
                            break;
                        case "!=":
                            condParam.Operator = CondOperator.NotEqual;
                            break;
                        case "NOT IN":
                            condParam.Operator = CondOperator.NotIn;
                            break;
                        case "IN":
                            condParam.Operator = CondOperator.NotIn;
                            break;
                        case ">=":
                            condParam.Operator = CondOperator.GreaterOrEqual;
                            break;
                        case ">":
                            condParam.Operator = CondOperator.Greater;
                            break;
                        case "<=":
                            condParam.Operator = CondOperator.LowerOrEqual;
                            break;
                        case "<":
                            condParam.Operator = CondOperator.Lower;
                            break;
                        case "NOT LIKE":
                            condParam.Operator = CondOperator.NotLike;
                            break;
                        case "LIKE":
                            condParam.Operator = CondOperator.Like;
                            break;
                        case "IS NULL":
                        case "ISNULL":
                            condParam.Operator = CondOperator.IsNull;
                            break;
                        case "IS NOT NULL":
                            condParam.Operator = CondOperator.IsNotNull;
                            break;
                        default:
                            break;
                    }
                    lstCondParam.Add(condParam);
                }
            }
            // Tính toán view columns
            var columns = "*";
            if (Utils.IsNotEmpty(input.viewColumns))
            {
                columns = string.Join(',', input.viewColumns);
            }
            //Tinh toán orderby
            var orderBy = "";
            if (Utils.IsNotEmpty(input.orderBy))
            {
                orderBy = string.Join(',', input.orderBy);
            }

            var data = Search(lstCondParam, input.databaseName, input.tableName, "*", orderBy, paging);// string.Join(",", model.columnTables.Select(n => n.DbName).ToList()));
            var idTables = model.columnTables.Where(t => t.IDTableRef > 0).Select(t => t.IDTableRef).ToArray();
            var idColumns = model.columnTables.Where(t => t.IDColRef > 0).Select(t => t.IDColRef);
            var tableRefers = await GetAllTablesByIDs(idTables);
            var ColumnRefers = await GetAllColumnTablesByIDTable(idTables);
            var result = new DataTable();
            var dicColumns = new Dictionary<string, string>();
            var isContainID = false;
            //result.Columns.Add("parentidtree_view", typeof(string));
            //result.Columns.Add("DANHMUCCHA", typeof(string));
            //result.Columns.Add("DANHMUCCHA_ID", typeof(string));
            foreach (var item in model.columnTables)
            {
                //chỉ lấy  các cột thuộc view
                if (Utils.IsNotEmpty(input.viewColumns) && !input.viewColumns.Exists(t => t == item.DbName))
                    continue;
                if (item.DbName == "ID")
                {
                    isContainID = true;
                }
                //result.Columns.Add(item.Name, typeof(string));// Để sau DacPV TODO
                result.Columns.Add(item.DbName + "", typeof(string));//_ValueShow
                switch (item.DataType)
                {
                    case (int)EnumTableInfo.ColumnDataType.String:
                        dicColumns.Add(item.DbName, item.DbName);
                        break;
                    case (int)EnumTableInfo.ColumnDataType.Integer:
                        dicColumns.Add(item.DbName, item.DbName);
                        break;
                    case (int)EnumTableInfo.ColumnDataType.Decimal:
                        dicColumns.Add(item.DbName, item.DbName);
                        break;
                    case (int)EnumTableInfo.ColumnDataType.DateTime:
                        dicColumns.Add(item.DbName, item.DbName);
                        break;
                    case (int)EnumTableInfo.ColumnDataType.Boolean:
                        dicColumns.Add(item.DbName, item.DbName);
                        break;
                    case (int)EnumTableInfo.ColumnDataType.TableRef:
                        var nameColumns = (ColumnRefers.First(t => t.ID == item.IDColRef) ?? new ColumnTableInfo()).DbName;
                        var nameColumnText = (ColumnRefers.First(t => t.ID == item.IDColTextRef) ?? new ColumnTableInfo()).DbName;
                        dicColumns.Add(item.DbName, nameColumns);
                        dicColumns.Add(item.DbName + "_Text", nameColumnText);
                        result.Columns.Add(item.DbName + "_Text", typeof(string));
                        break;
                    case (int)EnumTableInfo.ColumnDataType.TableUser:
                        dicColumns.Add(item.DbName, item.DbName);
                        break;
                    case (int)EnumTableInfo.ColumnDataType.Parent:
                        var nameColumnParent = (ColumnRefers.First(t => t.ID == item.IDColRef) ?? new ColumnTableInfo()).DbName;
                        dicColumns.Add(item.DbName, item.DbName);
                        dicColumns.Add(item.DbName + "_Text", nameColumnParent);
                        result.Columns.Add(item.DbName + "_Text", typeof(string));
                        break;
                    default:
                        break;
                }
            }
            // Check trường hợp bảng hiện thị có hiển thị ID hay không
            if (!isContainID)
            {
                result.Columns.Add("ID", typeof(string));
                dicColumns.Add("ID", "ID");
            }
            for (int i = 0; i < data.Rows.Count; i++)
            {
                var row = result.NewRow();
                var datarow = data.Rows[i];
                if (!isContainID)
                {
                    row["ID"] = datarow["ID"].ToString();
                }
                foreach (var item in model.columnTables)
                {
                    if (Utils.IsNotEmpty(input.viewColumns) && !input.viewColumns.Exists(t => t == item.DbName))
                        continue;
                    var value = (datarow[item.DbName] ?? "").ToString();
                    switch (item.DataType)
                    {
                        case (int)EnumTableInfo.ColumnDataType.String:
                            value = datarow[item.DbName].ToString();
                            break;
                        case (int)EnumTableInfo.ColumnDataType.Integer:
                            value = datarow[item.DbName].ToString();
                            break;
                        case (int)EnumTableInfo.ColumnDataType.Decimal:
                            value = datarow[item.DbName].ToString();
                            break;
                        case (int)EnumTableInfo.ColumnDataType.DateTime:
                            var valdt = datarow[item.DbName];
                            value = Utils.DateToString(valdt == DBNull.Value ? null : (DateTime?)valdt, "dd/MM/yyyy HH:mm");
                            break;
                        case (int)EnumTableInfo.ColumnDataType.Boolean:
                            value = (datarow[item.DbName] ?? true).ToString();
                            break;
                        case (int)EnumTableInfo.ColumnDataType.TableRef:
                            var KEY = datarow[item.DbName + "_DataLookUp"].ToString();//.Replace(":null,",@":\""\,");
                            var column = dicColumns[item.DbName + "_Text"];
                            row[item.DbName + "_Text"] = GetValueDataLookup(KEY, column);
                            break;
                        case (int)EnumTableInfo.ColumnDataType.TableUser:
                            value = (model.Users.FirstOrDefault(n => n.ID.ToString() == value) ?? new DAS.Domain.Models.DAS.User()).Name;
                            break;
                        case (int)EnumTableInfo.ColumnDataType.Parent:
                            var columnPr = dicColumns[item.DbName + "_Text"];
                            row[item.DbName + "_Text"] = GetValueDataLookup(datarow[item.DbName + "_DataLookUp"].ToString(), columnPr);
                            break;
                        default:
                            break;
                    }

                    row[item.DbName + ""] = value;//_ValueShow
                }
                //cột mặc định
                if (data.Columns.Contains("DANHMUCCHA"))
                {
                    // fix tam
                    var rs = GetFirstById(input.databaseName, input.tableName, datarow["DANHMUCCHA"].ToString());
                    if (rs != null && rs.Rows.Count > 0 && rs.Columns.Contains("MADANHMUC"))
                    {
                        row["DANHMUCCHA"] = rs.Rows[0]["MADANHMUC"].ToString();
                    }
                    else if(datarow["DANHMUCCHA"].ToString()=="0")
                    {
                        row["DANHMUCCHA"] = "";
                    }
                }
                //-----
                result.Rows.Add(row);
            }
            resultAPI.IsSuccess = true;
            resultAPI.Result = result;
            resultAPI.TotalItems = paging.TotalItem;
            return resultAPI;
        }
        public async Task<VMResultAPISearch> Getitem(VMParamAPI input)
        {
            input.pageSize = 1;
            input.page = 1;
            var result = await Getitems(input);
            var tableinfor = new TableInfo();
            CheckTableExists(input.tableName, input.databaseName, out tableinfor);
            var allcolumns = await GetAllColumnTablesByIDTable(tableinfor.ID);
            if (Utils.IsNotEmpty(result) && allcolumns.Select(t => t.DbName.ToUpper()).Contains("IDGOP")
                && allcolumns.Select(t => t.DbName.ToUpper()).Contains("IDTACH"))
            {
                var idRoot = result.Result.Rows[0]["ID"];
                var lstGop = Search(new List<CondParam> {
                        new CondParam
                        {
                            FieldName="IDGop",
                            Operator=CondOperator.Equal,
                            Value=idRoot
                        }
                }, input.databaseName, input.tableName);
                var lstTach = Search(new List<CondParam> {
                        new CondParam
                        {
                            FieldName="IDTach",
                            Operator=CondOperator.Equal,
                            Value=idRoot
                        }
                }, input.databaseName, input.tableName);
                result.LsTGop = lstGop;
                result.LstTach = lstTach;
            }
            return result;
        }
        /// <summary>
        /// Check tồn tại của Table
        /// </summary>
        /// <param name="table">Tên table</param>
        /// <param name="schema">Tên schema</param>
        /// <param name="tableInfo">đầu ra tableInfo</param>
        /// <returns></returns>
        private bool CheckTableExists(string table, string schema, out TableInfo tableInfo)
        {
            tableInfo = new TableInfo();
            var schemaInfo = _dasKTNNRepo.SchemaInfo.FirstOrDefault(t => t.Code == schema);
            if (Utils.IsEmpty(schemaInfo))
                return false;
            tableInfo = _dasKTNNRepo.TableInfo.FirstOrDefault(t => t.DbName == table && t.IDSchema == schemaInfo.ID && t.Status == 1);
            if (Utils.IsEmpty(tableInfo))
                return false;
            return true;
        }

        private bool CheckColumnExistColumns(int table, List<string> columns)
        {
            var columnTableInfos = (_dasKTNNRepo.ColumnTableInfo.GetAllListAsync(t => t.IDTable == table)).Result.ToList();
            foreach (var item in columns)
            {
                if (!columnTableInfos.Exists(t => t.DbName.ToUpper() == item.ToUpper()))
                {
                    return false;
                }
            }
            return true;
        }
        private async Task<List<ColumnTableInfo>> GetColumnTables(params int[] idTables)
        {
            return await (from col in _dasKTNNRepo.ColumnTableInfo.GetAll()
                          where idTables.Contains(col.IDTable) && col.Status == (int)EnumCommon.Status.Active
                          orderby col.Weight
                          select col).ToListAsync();
        }
        private async Task<List<TableInfo>> GetAllTablesByIDs(params int[] idTables)
        {
            if (Utils.IsEmpty(idTables))
                return new List<TableInfo>();
            return await (from col in _dasKTNNRepo.TableInfo.GetAll()
                          where idTables.Contains(col.ID) && col.Status == (int)EnumCommon.Status.Active
                          select col).ToListAsync();
        }
        private async Task<List<ColumnTableInfo>> GetAllColumnTablesByIDTable(params int[] idTables)
        {
            return await (from col in _dasKTNNRepo.ColumnTableInfo.GetAll()
                          where idTables.Contains(col.IDTable) && col.Status == (int)EnumCommon.Status.Active
                          orderby col.Weight
                          select col).ToListAsync();
        }
        private string GetValueDataLookup(string inputLookUp, string column)
        {
            try
            {
                var json = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(inputLookUp);
                if (Utils.IsNotEmpty(json) && json.FirstOrDefault().ContainsKey(column.ToUpper()))
                {
                    return json.FirstOrDefault()[column.ToUpper()].ToString();
                }
                return "";
            }
            catch
            {
                return "";
            }
        }

        public bool InsertTableLinkData(string schema, string dbName, LinkTableModel tableLinkData)
        {
            try
            {
                var query = $"INSERT INTO {schema}.{schema}{KTNNConst.TableLinkNameExt} " +
                    $"(" +
                    $" {nameof(LinkTableModel.IDSchema)}," +
                    $" {nameof(LinkTableModel.SchemaName)}," +
                    $" {nameof(LinkTableModel.IDTable)}," +
                    $" {nameof(LinkTableModel.TableName)}," +
                    $" {nameof(LinkTableModel.IDRecord)}," +
                    $" {nameof(LinkTableModel.DataLookUp)}," +

                    $" {nameof(LinkTableModel.IDSchemaRef)}," +
                    $" {nameof(LinkTableModel.SchemaNameRef)}," +
                    $" {nameof(LinkTableModel.IDTableRef)}," +
                    $" {nameof(LinkTableModel.TableNameRef)}," +
                    $" {nameof(LinkTableModel.IDRecordRef)}," +
                    $" {nameof(LinkTableModel.DataLookUpRef)}" +
                    $")" +
                    $" VALUES " +
                        $"(" +
                        $"'{tableLinkData.IDSchema}'," +
                        $"'{tableLinkData.SchemaName}'," +
                        $"'{tableLinkData.IDTable}'," +
                        $"'{tableLinkData.TableName}'," +
                        $"'{tableLinkData.IDRecord}'," +
                        $"{tableLinkData.DataLookUp}," +

                        $"'{tableLinkData.IDSchemaRef}'," +
                        $"'{tableLinkData.SchemaNameRef}'," +
                        $"'{tableLinkData.IDTableRef}'," +
                        $"'{tableLinkData.TableNameRef}'," +
                        $"'{tableLinkData.IDRecordRef}'," +
                        $"{tableLinkData.DataLookUpRef}" +
                        $")";
                _logger.LogError("Insert" + query);
                try
                {
                    var result = _dasDynamicDbDapperConn.Execute(query);
                    return true;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex);
                    return false;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return false;
            }
        }

        public VMResultAPISearch GetInfoTable(VMParamAPI data)
        {
            var result = new VMParamAPI();
            var resultAPI = new VMResultAPISearch
            {
                IsSuccess = true
            };
            var tableInfo = new TableInfo();
            if (!CheckTableExists(data.tableName, data.databaseName, out tableInfo))
            {
                resultAPI.IsSuccess = false;
                resultAPI.Message = "Tên bảng không hợp lệ";
                return resultAPI;
            }
            var columnInfors = GetAllColumnTablesByIDTable(tableInfo.ID).Result;
            var columnsView = columnInfors.Where(t => !t.IsSubColumn);
            var datahas = new Hashtable();
            if (Utils.IsNotEmpty(columnsView))
            {
                foreach (var item in columnsView)
                {
                    if (Utils.IsNotEmpty(data.viewColumns) &&
                       !data.viewColumns.Select(t => t.ToUpper()).Contains(item.DbName.ToUpper()))
                    {
                        continue;
                    }
                    var kieu = "";
                    switch (item.DataType)
                    {
                        case (int)EnumTableInfo.ColumnDataType.String:
                            kieu = "(kiểu chuỗi)";
                            break;
                        case (int)EnumTableInfo.ColumnDataType.Integer:
                            kieu = "(kiểu số nguyên)";
                            break;
                        case (int)EnumTableInfo.ColumnDataType.Decimal:
                            kieu = "(kiểu số số thực)";
                            break;
                        case (int)EnumTableInfo.ColumnDataType.DateTime:
                            kieu = "(kiểu số ngày tháng dd/MM/yyyy HH:ss)";
                            break;
                        case (int)EnumTableInfo.ColumnDataType.Boolean:
                            kieu = "(kiểu Boolean)";
                            break;
                        case (int)EnumTableInfo.ColumnDataType.TableRef:
                            kieu = "(Kiểu tham chiếu qua 1 bảng)";
                            break;
                        case (int)EnumTableInfo.ColumnDataType.TableUser:
                            kieu = "(Kiểu tham tham chiếu qua bảng account)";
                            break;
                        case (int)EnumTableInfo.ColumnDataType.Parent:
                            kieu = "(Kiểu cha con)";
                            break;
                        default:
                            break;
                    }
                    datahas.Add(item.DbName, $"{item.Name} {kieu}");
                }
                data.properties = datahas;
            }
            return resultAPI;
        }
        #endregion------------
    }
}
