﻿using AutoMapper;
using DAS.Application.Enums.DasKTNN;
using DAS.Application.Interfaces;
using DAS.Application.Interfaces.DasKTNN;
using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels;
using DAS.Domain.Enums;
using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Interfaces.DASNotify;
using DAS.Domain.Models.DASKTNN;
using DAS.Infrastructure.ContextAccessors;
using DAS.Infrastructure.Repositories.DASKTNN;
using DAS.Utility;
using DAS.Utility.CustomClass;
using DAS.Utility.LogUtils;
using DocumentFormat.OpenXml.Bibliography;
using DocumentFormat.OpenXml.EMMA;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace DAS.Application.Services.DasKTNN
{
    public class SoucreInfoService : BaseMasterService, ISoucreInfoService
    {
        #region Properties
        private readonly IMapper _mapper;
        private readonly ILoggerManager _logger;
        private readonly IModuleService _module;
        private readonly IDynamicDBService _dynamicDBService;
        private readonly IDefaultDataService _defaultDataService;
        private readonly IUserPrincipalService _userPrincipalService;
        private ICacheManagementServices _cacheManagementServices;

        #endregion

        #region Ctor
        public SoucreInfoService(IDasRepositoryWrapper dasRepository, IDasKTNNRepositoryWrapper dasKTNN
            , IMapper mapper
            , ILoggerManager logger
            , IModuleService module
            , IDynamicDBService dynamicDBService
            , IDefaultDataService defaultDataService
            , IUserPrincipalService userPrincipalService
            , ICacheManagementServices cacheManagementServices, IDasDataDapperRepo dasDapperRepo) : base(dasRepository, dasDapperRepo, dasKTNN)
        {
            _mapper = mapper;
            _logger = logger;
            _module = module;
            _userPrincipalService = userPrincipalService;
            _dynamicDBService = dynamicDBService;
            _cacheManagementServices = cacheManagementServices;
            _defaultDataService = defaultDataService;
        }

        #endregion

        #region Gets  


        public async Task<IEnumerable<SoucreInfo>> GetsList()
        {
            var temp = from ct in _dasKTNNRepo.SoucreInfo.GetAll()
                       orderby ct.Name
                       select ct;
            return await temp.ToListAsync();
        }

        public async Task<SoucreInfo> Get(int id)
        {
            return await _dasKTNNRepo.SoucreInfo.FirstOrDefaultAsync(n => n.ID == id);
        }

        public async Task<PaginatedList<SoucreInfo>> SearchByConditionPagging(SoucreInfoCondition condition)
        {
            var temp = from ct in _dasKTNNRepo.SoucreInfo.GetAll()
                       where (condition.Keyword.IsEmpty() || ct.Name.Contains(condition.Keyword))
                       && (ct.Type == condition.Type || condition.Type == 0)
                       orderby ct.UpdatedDate ?? ct.CreateDate descending
                       select ct;
            var total = await temp.LongCountAsync();
            int totalPage = (int)Math.Ceiling(total / (double)condition.PageSize);
            if (totalPage < condition.PageIndex)
            {
                condition.PageIndex = 1;
            }
            var result = await temp.Skip((condition.PageIndex - 1) * condition.PageSize).Take(condition.PageSize).ToListAsync();
            return new PaginatedList<SoucreInfo>(result, (int)total, condition.PageIndex, condition.PageSize);
        }

        #endregion

        #region Create

        public VMSoucreInfo Create(int type)
        {
            var model = new VMSoucreInfo()
            {
                soucreInfo = new SoucreInfo() { Type = type },
            };

            return model;
        }

        public async Task<VMSoucreInfo> Create(Hashtable data)
        {
            var model = new VMSoucreInfo();
            try
            {
                var item = Utils.Bind<SoucreInfo>(data);

                await ValidateDataAsync(item);

                BindData(item, null);

                UserData userData = await _cacheManagementServices.GetUserDataAndSetCache();
                item.CreateDate = DateTime.Now;
                item.CreatedBy = _userPrincipalService.UserId;
                await _dasKTNNRepo.SoucreInfo.InsertAsync(item);
                await _dasKTNNRepo.SaveAync();
                if (item.ID > 0)
                {
                    model.Message = "Thêm dữ liệu thành công";
                    model.IsSuccess = true;
                    return model;
                }
                else
                {
                    model.Message = "Thêm dữ liệu không thành công";
                    model.IsSuccess = true;
                    return model;
                }
            }
            catch (LogicException ex)
            {
                model.Message = ex.Message;
                model.IsSuccess = false;
                return model;
            }
            catch (Exception ex)
            {
                model.Message = "Có lỗi khi thêm mới bảng dữ liệu";
                model.IsSuccess = false;
                return model;
            }
        }

        #endregion

        #region Update
        public async Task<VMSoucreInfo> Update(int? id)
        {
            var group = await Get(id ?? 0);
            if (group == null || group.ID == 0)
            {
                throw new LogicException("Bảng dữ liệu không còn tồn tại");
            }
            var tableInfos = await _dasKTNNRepo.GroupTableInfo.GetAllListAsync(n => n.IDGroup == id);
            var model = new VMSoucreInfo
            {
                soucreInfo = group,
            };
            return model;
        }

        public async Task<VMSoucreInfo> Update(Hashtable data)
        {
            var model = new VMSoucreInfo();
            try
            {
                var ID = Utils.GetInt(data, "ID");
                var info = _dasKTNNRepo.SoucreInfo.Get(ID);
                if (Utils.IsEmpty(info))
                {
                    model.Message = "Bảng nguồn này không tồn tại";
                    model.IsSuccess = false;
                    return model;
                }
                var item = Utils.Bind(info, data);

                await ValidateDataAsync(item);
                BindData(item, info);

                item.UpdatedDate = DateTime.Now;
                item.UpdatedBy = _userPrincipalService.UserId;
                await _dasKTNNRepo.SoucreInfo.UpdateAsync(item);
                await _dasKTNNRepo.SaveAync();
                if (item.ID > 0)
                {
                    model.Message = "Cập nhật dữ liệu thành công";
                    model.IsSuccess = true;
                    return model;
                }
                else
                {
                    model.Message = "Cập nhật dữ liệu không thành công";
                    model.IsSuccess = true;
                    return model;
                }

            }
            catch (LogicException ex)
            {
                model.Message = ex.Message;
                model.IsSuccess = false;
                return model;
            }
            catch (Exception ex)
            {
                model.Message = "Có lỗi khi thêm mới bảng dữ liệu";
                model.IsSuccess = false;
                return model;
            }
        }

        #endregion

        #region Delete
        public async Task<ServiceResult> Delete(int id)
        {
            try
            {
                var tableInfo = await _dasKTNNRepo.SoucreInfo.GetAsync(id);
                if (tableInfo == null)
                    return new ServiceResultError("Bảng nguồn dữ liệu này hiện không tồn tại hoặc đã bị xóa");

                await _dasKTNNRepo.SoucreInfo.DeleteAsync(tableInfo);
                await _dasKTNNRepo.SaveAync();
                //Update Module
                return new ServiceResultSuccess("Xóa dữ liệu thành công");
            }
            catch (LogicException ex)
            {
                return new ServiceResultError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return new ServiceResultError(ex.Message);

            }
        }

        public async Task<ServiceResult> Delete(IEnumerable<int> ids)
        {
            try
            {
                var tableInfoDeletes = await _dasKTNNRepo.SoucreInfo.GetAllListAsync(n => ids.Contains(n.ID));
                if (tableInfoDeletes == null || tableInfoDeletes.Count() == 0)
                    return new ServiceResultError("Bảng nguồn dữ liệu đã chọn hiện không tồn tại hoặc đã bị xóa");
                foreach (var item in tableInfoDeletes)
                {
                    await _dasKTNNRepo.SoucreInfo.DeleteAsync(item);
                }
                await _dasKTNNRepo.SaveAync();
                return new ServiceResultSuccess("Xóa dữ liệu thành công");
            }
            catch (LogicException ex)
            {
                return new ServiceResultError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return new ServiceResultError(ex.Message);
            }
        }

        #endregion

        #region Functions
        private async Task ValidateDataAsync(SoucreInfo item)
        {
            if (Utils.IsEmpty(item.Code))
            {
                throw new LogicException("Mã nguồn không được để trống");
            }
            if (Utils.IsEmpty(item.Name))
            {
                throw new LogicException("Tên nguồn không được để trống");
            }
            if (Utils.IsEmpty(item.Type) || item.Type <= 0)
            {
                throw new LogicException("Loại nguồn không được để trống");
            }

            var isSQL = item.Type == (int)EnumSoucreInfo.TypeSoucre.SQLServer
                || item.Type == (int)EnumSoucreInfo.TypeSoucre.Oracle
                || item.Type == (int)EnumSoucreInfo.TypeSoucre.MySQL
                || item.Type == (int)EnumSoucreInfo.TypeSoucre.Cassanda;

            var isAPI = item.Type == (int)EnumSoucreInfo.TypeSoucre.API;
            var isFile = item.Type == (int)EnumSoucreInfo.TypeSoucre.FileXML
                || item.Type == (int)EnumSoucreInfo.TypeSoucre.FileCSV;

            if (isFile)
            {
                if (Utils.IsEmpty(item.FileName))
                {
                    throw new LogicException("Tên file không được để trống");
                }
                if (Utils.IsEmpty(item.FileLocation))
                {
                    throw new LogicException("Đường dẫn file không được để trống");
                }
            }

            if (isSQL || isAPI)
            {
                if (isSQL && Utils.IsEmpty(item.IP))
                {
                    throw new LogicException("IP không được để trống");
                }
                if (isAPI && Utils.IsEmpty(item.Url))
                {
                    throw new LogicException("Url không được để trống");
                }
                if (isAPI && Utils.IsEmpty(item.TokenUrl))
                {
                    throw new LogicException("Token Url không được để trống");
                }
                if (isSQL && Utils.IsEmpty(item.Port))
                {
                    throw new LogicException("Port không được để trống");
                }
                if (isSQL && Utils.IsEmpty(item.DatabaseName))
                {
                    throw new LogicException("DatabaseName không được để trống");
                }
                if (Utils.IsEmpty(item.Username))
                {
                    throw new LogicException("Username không được để trống");
                }
                if (Utils.IsEmpty(item.Password))
                {
                    throw new LogicException("Password không được để trống");
                }
            }
            if (await _dasKTNNRepo.SoucreInfo.IsCodeExist(item.Code, item.ID))
            {
                throw new LogicException($"Mã nguồn {item.Code} đã tồn tại trong hệ thống");
            }
        }
        private void BindData(SoucreInfo item, SoucreInfo info)
        {
            //if (Utils.IsNotEmpty(item.Password))
            //{
            //    item.Password = StringUltils.Md5Encryption(item.Password);
            //}
            //else if (info != null)
            //{
            //    item.Password = info.Password;
            //}

            var isSQL = item.Type == (int)EnumSoucreInfo.TypeSoucre.SQLServer
               || item.Type == (int)EnumSoucreInfo.TypeSoucre.Oracle
               || item.Type == (int)EnumSoucreInfo.TypeSoucre.MySQL
               || item.Type == (int)EnumSoucreInfo.TypeSoucre.Cassanda;
            if (isSQL)
            {
                var dbTypeName = "";
                if (item.Type == (int)EnumSoucreInfo.TypeSoucre.SQLServer)
                {
                    dbTypeName = "sqlserver";
                }
                else if (item.Type == (int)EnumSoucreInfo.TypeSoucre.Oracle)
                {
                    dbTypeName = "oracle";
                }
                else if (item.Type == (int)EnumSoucreInfo.TypeSoucre.Cassanda)
                {
                    dbTypeName = "cassandra";
                }
                var host = item.IP;
                if (item.Port.IsNotEmpty())
                    host += $":{item.Port}";

                item.DBConnection = $"jdbc:{dbTypeName}://{host};encrypt=true;databaseName={item.DatabaseName};user={item.Username};password={item.Password};";
            }

        }

        public async Task<IEnumerable<VMSourceInfoAPI>> GetListSourceAPI()
        {
            var tableInfors = await (from ct in _dasKTNNRepo.TableInfo.GetAll()
                                     where ct.Status == (int)EnumCommon.Status.Active
                                     select ct).ToListAsync();
            var schemaInfor = await (from ct in _dasKTNNRepo.SchemaInfo.GetAll()
                                     select ct).ToListAsync();
            var sourceInforAlls = await (from ct in _dasKTNNRepo.SoucreInfo.GetAll()
                                         where ct.Status == (int)EnumSoucreInfo.TypeStatus.HoatDong
                                         select ct).ToListAsync();
            var result = new List<VMSourceInfoAPI>();
            var enumtypes = Utils.GetDescribes<EnumSoucreInfo.TypeSoucre>();
            foreach (var type in enumtypes)
            {
                var sourceInfors = sourceInforAlls.Where(t => t.Type == type.Key).ToList();
                var srs = new List<VMSourceInfoAPIModel>();
                if (Utils.IsNotEmpty(sourceInfors))
                {
                    foreach (var item in sourceInfors)
                    {
                        var sr = new VMSourceInfoAPIModel
                        {
                            MaNguon = item.Code,
                            TenNguon = item.Name,
                            TenFile = item.FileName,
                            DuongDanFile = item.FileLocation,
                            IP = item.IP,
                            DatabaseName = item.DatabaseName,
                            UserName = item.Username,
                            PassWord = item.Password,
                            MoTa = item.Describe,
                            ChuoiKetNoi = item.DBConnection,
                            Port = item.Port,
                            TokenUrl = item.TokenUrl,
                            Url = item.Url,
                            modelmaping = new List<VMSourceInfoAPIModelMap>()
                        };
                        var itresults = new List<VMSourceInfoAPIModelMap>();
                        var tables = tableInfors.Where(t => t.IDSourceInfo == item.ID).ToList();
                        if (Utils.IsNotEmpty(tables))
                        {
                            foreach (var tb in tables)
                            {
                                var schema = schemaInfor.FirstOrDefault(t => t.ID == tb.IDSchema);
                                if (Utils.IsNotEmpty(schema))
                                {
                                    itresults.Add(new VMSourceInfoAPIModelMap
                                    {
                                        databasename = schema.Code,
                                        tablename = tb.DbName
                                    });
                                }
                            }
                        }
                        sr.modelmaping = itresults;
                        srs.Add(sr);
                    }
                    result.Add(new VMSourceInfoAPI
                    {
                        TypeSource = type.Key,
                        SourceInfors = srs
                    });
                }
                else
                {
                    result.Add(new VMSourceInfoAPI
                    {
                        TypeSource = type.Key,
                        SourceInfors = null
                    });
                }

            }
            return result;
        }
        #endregion
    }
}