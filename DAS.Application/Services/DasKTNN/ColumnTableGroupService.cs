﻿using AutoMapper;
using DAS.Application.Enums.DasKTNN;
using DAS.Application.Interfaces;
using DAS.Application.Interfaces.DasKTNN;
using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels;
using DAS.Application.Models.ViewModels.DasKTNN;
using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Interfaces.DASNotify;
using DAS.Domain.Models.DASKTNN;
using DAS.Infrastructure.Repositories.DASKTNN;
using DAS.Utility;
using DAS.Utility.CustomClass;
using DAS.Utility.LogUtils;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using static DAS.Application.Enums.DasKTNN.EnumTableInfo;

namespace DAS.Application.Services.DasKTNN
{
    public class ColumnTableGroupService : BaseMasterService, IColumnTableGroupServices
    {
        #region Properties
        private readonly IMapper _mapper;
        private readonly ILoggerManager _logger;
        private readonly IModuleService _module;
        private readonly IDynamicDBService _dynamicDBService;
        private readonly IDefaultDataService _defaultDataService;

        private readonly IHostApplicationLifetime _host;
        private ICacheManagementServices _cacheManagementServices;
        private IWebHostEnvironment _env;

        #endregion

        #region Ctor
        public ColumnTableGroupService(IDasRepositoryWrapper dasRepository, IDasKTNNRepositoryWrapper dasKTNN
            , IMapper mapper
            , ILoggerManager logger
            , IModuleService module
            , IDynamicDBService dynamicDBService
            , IDefaultDataService defaultDataService,
            IHostApplicationLifetime host
            , ICacheManagementServices cacheManagementServices, IDasDataDapperRepo dasDapperRepo,
            IWebHostEnvironment env) : base(dasRepository, dasDapperRepo, dasKTNN)
        {
            _mapper = mapper;
            _logger = logger;
            _module = module;
            _dynamicDBService = dynamicDBService;
            _cacheManagementServices = cacheManagementServices;
            _defaultDataService = defaultDataService;
            _host = host;
            _env = env;
        }

        #endregion

        #region Gets  

        public async Task<IEnumerable<ColumnTableGroup>> GetsList()
        {
            var temp = from ct in _dasKTNNRepo.ColumnTableGroup.GetAll()
                       orderby ct.ID descending
                       select ct;
            return await temp.ToListAsync();
        }
        public async Task<ColumnTableGroup> Get(int id)
        {
            return await _dasKTNNRepo.ColumnTableGroup.FirstOrDefaultAsync(n => n.ID == id);
        }

        public async Task<VMIndexColumnTableGroup> SearchByConditionPagging(ColumnTableGroupCondition condition)
        {
            var rs = new VMIndexColumnTableGroup
            {
                SearchParam = condition
            };
            UserData userData = await _cacheManagementServices.GetUserDataAndSetCache();

            var temp = from tb in _dasKTNNRepo.ColumnTableGroup.GetAll()
                       where (condition.Keyword.IsEmpty() || tb.Name.Contains(condition.Keyword))
                       orderby tb.UpdatedDate ?? tb.CreateDate descending
                       select _mapper.Map<VMColumnTableGroup>(tb);

            var total = await temp.LongCountAsync();
            int totalPage = (int)Math.Ceiling(total / (double)condition.PageSize);
            if (totalPage < condition.PageIndex)
            {
                condition.PageIndex = 1;
            }
            var result = await temp.Skip((condition.PageIndex - 1) * condition.PageSize).Take(condition.PageSize).ToListAsync();
            rs.ColumnTableGroups = new PaginatedList<VMColumnTableGroup>(result, (int)total, condition.PageIndex, condition.PageSize);
            return rs;
        }

        #endregion

        #region Create

        public async Task<VMColumnTableGroup> Create()
        {
            var model = new VMColumnTableGroup()
            {
                DictCode = Utils.EnumToStringDic<ColumnTableGroupCode>(),
            };
            return model;
        }
        
    

        public async Task<ServiceResult> Save(VMUpdateColumnTableGroup data)
        {
            try
            {
                UserData userData = await _cacheManagementServices.GetUserDataAndSetCache();
                var ColumnTableGroup = _mapper.Map<ColumnTableGroup>(data);
                await ValidateData(data);
                await _dasKTNNRepo.ColumnTableGroup.InsertAsync(ColumnTableGroup);
                await _dasKTNNRepo.SaveAync();
                return new ServiceResultSuccess("Thêm nhóm cột thành công");

            }
            catch (LogicException ex)
            {
                return new ServiceResultError(ex.Message);
            }
            catch (Exception ex)
            {
                Guid.NewGuid().ToString();
                _logger.LogError(ex);
                return new ServiceResultError("Có lỗi khi thêm mới nhóm cột");
            }
        }

        #endregion

        #region Update
        public async Task<VMColumnTableGroup> Update(int? id)
        {
            var ColumnTableGroup = await Get(id ?? 0);
            if (ColumnTableGroup == null || ColumnTableGroup.ID == 0)
            {
                throw new LogicException("Nhóm cột không còn tồn tại");
            }
            var model = _mapper.Map<VMColumnTableGroup>(ColumnTableGroup);
            model.DictCode = Utils.EnumToStringDic<ColumnTableGroupCode>();
            return model;
        }

        public async Task<ServiceResult> Change(VMUpdateColumnTableGroup vmColumnTableGroup)
        {
            try
            {
                UserData userData = await _cacheManagementServices.GetUserDataAndSetCache();


                var ColumnTableGroup = await _dasKTNNRepo.ColumnTableGroup.GetAsync(vmColumnTableGroup.ID);
                if (ColumnTableGroup == null)
                    return new ServiceResultError("Nhóm cột này hiện không tồn tại hoặc đã bị xóa");
                //   var oldParent = vmColumnTableGroup.ParentPath;


                await ValidateData(vmColumnTableGroup);

                await _dasKTNNRepo.ColumnTableGroup.UpdateAsync(ColumnTableGroup);
                await _dasKTNNRepo.SaveAync();
                return new ServiceResultSuccess("Cập nhật nhóm cột thành công");
            }
            catch (LogicException ex)
            {
                return new ServiceResultError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return new ServiceResultError(ex.Message);
            }
        }
        #endregion


        #region Delete
        public async Task<ServiceResult> Delete(int id)
        {
            try
            {
                var ColumnTableGroup = await _dasKTNNRepo.ColumnTableGroup.GetAsync(id);
                if (ColumnTableGroup == null)
                    return new ServiceResultError("Nhóm cột này hiện không tồn tại hoặc đã bị xóa");

              
                await _dasKTNNRepo.ColumnTableGroup.DeleteAsync(ColumnTableGroup);

                await _dasKTNNRepo.SaveAync();

                return new ServiceResultSuccess("Xóa nhóm cột thành công");
            }
            catch (LogicException ex)
            {
                return new ServiceResultError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return new ServiceResultError(ex.Message);

            }
        }
        public async Task<ServiceResult> Delete(IEnumerable<int> ids)
        {
            try
            {
                var ColumnTableGroupDeletes = await _dasKTNNRepo.ColumnTableGroup.GetAllListAsync(n => ids.Contains(n.ID));
                if (ColumnTableGroupDeletes == null || ColumnTableGroupDeletes.Count() == 0)
                    return new ServiceResultError("Nhóm cột đã chọn hiện không tồn tại hoặc đã bị xóa");

                await _dasKTNNRepo.ColumnTableGroup.DeleteAsync(ColumnTableGroupDeletes);
                await _dasKTNNRepo.SaveAync();

                return new ServiceResultSuccess("Xóa nhóm cột thành công");
            }
            catch (LogicException ex)
            {
                return new ServiceResultError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return new ServiceResultError(ex.Message);
            }
        }
        #endregion


        #region Validate
        private async Task ValidateData(VMUpdateColumnTableGroup vmColumnTableGroup)
        {

            //if (await _dasKTNNRepo.ColumnTableGroup.IsNameExist(vmColumnTableGroup.Name, (int)EnumCommon.Status.InActive, vmColumnTableGroup.ID))
            //{
            //    throw new LogicException("Tên nhóm cột đã tồn tại");
            //}

        }
        #endregion

        #region Funtions


        #endregion
    }
}