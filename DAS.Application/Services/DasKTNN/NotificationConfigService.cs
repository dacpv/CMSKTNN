﻿using AutoMapper;
using DAS.Application.Interfaces;
using DAS.Application.Interfaces.DasKTNN;
using DAS.Application.Models.ViewModels;
using DAS.Application.Models.ViewModels.DasKTNN;
using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Interfaces.DASNotify;
using DAS.Infrastructure.ContextAccessors;
using DAS.Infrastructure.Repositories.DASKTNN;
using DAS.Utility.LogUtils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using DAS.Domain.Enums;
using DAS.Utility;
using DAS.Application.Models.CustomModels;
using System.Collections;
using System.Data;
using System.Text;
using DAS.Domain.Models.DAS;
using DocumentFormat.OpenXml.Office2010.Excel;

namespace DAS.Application.Services.DasKTNN
{
    public class NotificationConfigService : BaseMasterService, INotificationConfigService
    {
        #region Properties
        private readonly IMapper _mapper;
        private readonly ILoggerManager _logger;
        private readonly IUserService _userService;
        private readonly ICacheManagementServices _cacheManagementServices;
        private readonly IUserPrincipalService _userPrincipalService;
        private readonly IDynamicDBService _dynamicDBService;
        private readonly ITableInfoServices _tableInfoServices;
        #endregion

        #region Ctor
        public NotificationConfigService(IDasRepositoryWrapper dasRepository
            , ILoggerManager logger
            , IMapper mapper, ICacheManagementServices cacheManagementServices
            , IUserService userService
            , IUserPrincipalService iUserPrincipalService
            , ITableInfoServices tableInfoServices
            , IDasKTNNRepositoryWrapper dasKTNNRepositoryWrapper
            , IDasDataDapperRepo dasDapperRepo
            , IDynamicDBService dynamicDBService) : base(dasRepository, dasDapperRepo, dasKTNNRepositoryWrapper)
        {
            _mapper = mapper;
            _logger = logger;
            _userService = userService;
            _tableInfoServices = tableInfoServices;
            _dasKTNNRepo = dasKTNNRepositoryWrapper;
            _cacheManagementServices = cacheManagementServices;
            _userPrincipalService = iUserPrincipalService;
            _dynamicDBService = dynamicDBService;
        }
        #endregion

        #region Gets
        public async Task<VMNotificationConfig> SearchByCondition(NotificationConfigCondition condition)
        {
            var model = new VMNotificationConfig();
            model.Condition = condition;
            var temp = from gp in _dasRepo.GroupPermission.GetAll()
                       where (condition.Keyword.IsEmpty() || gp.Name.Contains(condition.Keyword)) && gp.Status == (int)EnumGroupPermission.Status.Active && gp.IsShow == 1
                       select new NotificationConfig
                       {
                           ID = gp.ID,
                           Name = gp.Name,
                           ActiveNotification = gp.ActiveNotification,
                           DescriptionForNotification = gp.DescriptionForNotification,
                       };

            var total = await temp.LongCountAsync();
            int totalPage = (int)Math.Ceiling(total / (double)condition.PageSize);
            if (totalPage < condition.PageIndex)
                condition.PageIndex = 1;

            var result = await temp.Skip((condition.PageIndex - 1) * condition.PageSize).Take(condition.PageSize).ToListAsync();
            model.NotificationConfigs = new PaginatedList<NotificationConfig>(result.ToList(), (int)total, condition.PageIndex, condition.PageSize);
            return model;
        }

        public async Task<ServiceResult> EnableNotification(Hashtable Data)
        {
            try
            {
                var id = Utils.GetInt(Data, "ID");
                var activeNotification = Utils.GetInt(Data, "ActiveNotification");
                var groupPermissionUpdate = await _dasRepo.GroupPermission.GetAsync(id);
                if (groupPermissionUpdate == null)
                    return new ServiceResultError("Nhóm quyền này hiện không tồn tại hoặc đã bị xóa");

                groupPermissionUpdate.ActiveNotification = activeNotification;
                await _dasRepo.GroupPermission.UpdateAsync(groupPermissionUpdate);
                await _dasRepo.SaveAync();
                return new ServiceResultSuccess("Cập nhật trạng thái thành công");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return new ServiceResultError(ex.Message);
            }
        }

        public async Task<NotificationConfig> GetByID(object id)
        {
            try
            {
                var model = new NotificationConfig();
                var groupPermistion = await _dasRepo.GroupPermission.GetAsync(id);
                model.ID = groupPermistion.ID;
                model.DescriptionForNotification = groupPermistion.DescriptionForNotification;
                model.IDNotification = groupPermistion.ID;
                model.ActiveEmail = groupPermistion.ActiveEmail;
                return model;
            }
            catch (Exception)
            {
                return new NotificationConfig();
            }
        }

        public async Task<Dictionary<int, string>> GetList()
        {
            try
            {
                var dicGroupPermission = new Dictionary<int, string>();
                var groupPermistion = await _dasRepo.GroupPermission.GetAllListAsync();
                var newGroupPermisstion = groupPermistion.Where(x => x.Status == (int)EnumGroupPermission.Status.Active).ToList() ?? new List<Domain.Models.DAS.GroupPermission>();
                dicGroupPermission = newGroupPermisstion.ToDictionary(x => x.ID, x => x.Name);
                return dicGroupPermission;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return new Dictionary<int, string>();
            }
        }

        public async Task<ServiceResult> Save(NotificationConfig model)
        {
            try
            {
                if (model.IDNotification <= 0)
                    return new ServiceResultError("Nhóm quyền không được để trống");

                var check = await _dasRepo.GroupPermission.AnyAsync(n => n.ID == model.IDNotification && n.IsShow == 1);
                if (check)
                    return new ServiceResultError("Cấu hình thông báo đã tồn tại");

                var groupPermisstion = await _dasRepo.GroupPermission.GetAsync(model.IDNotification);
                if (groupPermisstion == null)
                    return new ServiceResultError("Nhóm quyền không tồn tại hoặc đã bị xóa");
                groupPermisstion.IsShow = 1;
                groupPermisstion.ActiveEmail = model.ActiveEmail;
                await _dasRepo.GroupPermission.UpdateAsync(groupPermisstion);
                await _dasRepo.SaveAync();
                return new ServiceResultSuccess("Thêm cấu hình thông báo thành công");
            }
            catch (Exception ex)
            {
                Guid.NewGuid().ToString();
                _logger.LogError(ex);
                return new ServiceResultError(ex.Message);
            }
        }

        public async Task<ServiceResult> Update(NotificationConfig model)
        {
            try
            {
                if (model.IDNotification <= 0)
                    return new ServiceResultError("Nhóm quyền không được để trống");

                var groupPermisstion = await _dasRepo.GroupPermission.GetAsync(model.ID);
                if (groupPermisstion == null)
                    return new ServiceResultError("Cấu hình thông báo không tồn tại hoặc đã bị xóa");

                if (await IsExist(model))
                    return new ServiceResultError("Cấu hình thông báo đã tồn tại");

                groupPermisstion.DescriptionForNotification = model.DescriptionForNotification;
                groupPermisstion.ActiveEmail = model.ActiveEmail;
                await _dasRepo.GroupPermission.UpdateAsync(groupPermisstion);
                await _dasRepo.SaveAync();
                return new ServiceResultSuccess("Chỉnh sửa cấu hình thành công");
            }
            catch (Exception ex)
            {
                Guid.NewGuid().ToString();
                _logger.LogError(ex);
                return new ServiceResultError(ex.Message);
            }
        }

        public async Task<ServiceResult> Delete(int id)
        {
            try
            {
                var data = await _dasRepo.GroupPermission.GetAll().FirstOrDefaultAsync(x => x.ID == id);
                if (data == null)
                    return new ServiceResultError("Cấu hình thông báo không tồn tại hoặc đã bị xóa");

                data.IsShow = 0;
                data.ActiveEmail = 0;
                await _dasRepo.GroupPermission.UpdateAsync(data);
                await _dasRepo.SaveAync();
                return new ServiceResultSuccess("Xóa cấu hình thành công");
            }
            catch (Exception ex)
            {
                Guid.NewGuid().ToString();
                _logger.LogError(ex);
                return new ServiceResultError(ex.Message);
            }
        }

        public async Task<ServiceResult> Deletes(int[] ids)
        {
            try
            {
                var results = _dasRepo.GroupPermission.GetAllList(n => ids.Contains(n.ID)).ToList();
                if (results == null || results.Count() == 0)
                    return new ServiceResultError("Cấu hình thông báo đã chọn hiện không tồn tại hoặc đã bị xóa");

                foreach (var result in results)
                {
                    result.IsShow = 0;
                    result.ActiveEmail = 0;
                }    

                await _dasRepo.GroupPermission.UpdateAsync(results);
                await _dasRepo.SaveAync();
                return new ServiceResultSuccess("Xóa cấu hình thành công");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return new ServiceResultError(ex.Message);
            }
        }

        public async Task<bool> IsExist(NotificationConfig model)
        {
            var groupPermisstion = await _dasRepo.GroupPermission.AnyAsync(n => n.ID != model.ID && n.ID == model.IDNotification && n.IsShow == 1);
            return groupPermisstion;
        }
        #endregion
    }
}
