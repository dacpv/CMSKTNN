﻿using AutoMapper;
using DAS.Application.Interfaces;
using DAS.Application.Interfaces.DasKTNN;
using DAS.Application.Models.ViewModels;
using DAS.Application.Models.ViewModels.DasKTNN;
using DAS.Domain.Enums;
using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Interfaces.DASNotify;
using DAS.Domain.Models.DASKTNN;
using DAS.Infrastructure.ContextAccessors;
using DAS.Utility;
using DAS.Utility.LogUtils;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static DAS.Application.Enums.DasKTNN.EnumTableInfo;

namespace DAS.Application.Services.DasKTNN
{
    public class CollectionReportService : BaseMasterService, ICollectionReportService
    {
        #region Properties
        private readonly IMapper _mapper;
        private readonly ILoggerManager _logger;
        private readonly IDistributedCache _cache;
        private readonly ICategoryServices _categoryServices;
        private readonly IUserService _userService;
        private readonly ICacheManagementServices _cacheManagementServices;
        private readonly IUserPrincipalService _userPrincipalService;
        private readonly IDynamicDBService _dynamicDBService;
        #endregion

        #region Ctor
        public CollectionReportService(IDasRepositoryWrapper dasRepository, IDasKTNNRepositoryWrapper dasKTNN
            , IMapper mapper
            , IDynamicDBService dynamicDBService
            , ILoggerManager logger
            , ICategoryServices categoryServices
            , ICacheManagementServices cacheManagementServices
            , IDistributedCache cache
            , IUserService userService
            , IUserPrincipalService iUserPrincipalService) : base(dasRepository, dasKTNN)
        {
            _dynamicDBService = dynamicDBService;
            _dasRepo = dasRepository;
            _mapper = mapper;
            _logger = logger;
            _cache = cache;
            _categoryServices = categoryServices;
            _userService = userService;
            _cacheManagementServices = cacheManagementServices;
            _userPrincipalService = iUserPrincipalService;
        }
        #endregion

        public async Task<VMMainCollectionReport> SearchByCondition(CollectionReportCondition condition)
        {
            condition.Month = condition.Month > 0 ? condition.Month : DateTime.Now.Month;
            condition.Year = condition.Year > 0 ? condition.Year : DateTime.Now.Year;
            var model = new VMMainCollectionReport();
            model.Condition = condition;
            model.Years = Enumerable.Range(1999, (DateTime.Now.Year - 1999) + 1).OrderByDescending(n => n).ToList();
            model.Months = Enumerable.Range(1, 12).OrderBy(n => n).ToList();

            var temp = from s in _dasKTNNRepo.CollectionReport.GetAll().AsNoTracking()
                       where (string.IsNullOrEmpty(condition.Keyword) || s.DataSource.Contains(condition.Keyword.Trim())) && s.Month == condition.Month && s.Year == condition.Year
                       select s;

            var total = await temp.LongCountAsync();
            int totalPage = (int)Math.Ceiling(total / (double)condition.PageSize);
            if (totalPage < condition.PageIndex)
                condition.PageIndex = 1;

            var res = await temp.Skip((condition.PageIndex - 1) * condition.PageSize).Take(condition.PageSize).ToListAsync();
            model.CollectionReports = new PaginatedList<CollectionReport>(res, (int)total, condition.PageIndex, condition.PageSize);
            model.DataChart = JsonConvert.SerializeObject(temp);
            return model;
        }
        public async Task<VMMainCollectionReport> StatisticBySoucre(CollectionReportCondition condition)
        {
            condition.Month = condition.Month > 0 ? condition.Month : DateTime.Now.Month;
            condition.Year = condition.Year > 0 ? condition.Year : DateTime.Now.Year;
            var model = new VMMainCollectionReport();
            model.Condition = condition;
            model.Years = Enumerable.Range(1999, (DateTime.Now.Year - 1999) + 1).OrderByDescending(n => n).ToList();
            model.Months = Enumerable.Range(1, 12).OrderBy(n => n).ToList();
            var schemas = await GetSchemas();

            var temp = from tb in _dasKTNNRepo.SoucreInfo.GetAll()
                       where (condition.Keyword.IsEmpty() || tb.Name.ToLower().Contains(condition.Keyword.ToLower()))
                       && tb.Status == (int)EnumCommon.Status.Active
                       orderby tb.UpdatedDate ?? tb.CreateDate descending
                       select tb;


            var total = await temp.LongCountAsync();
            int totalPage = (int)Math.Ceiling(total / (double)condition.PageSize);
            if (totalPage < condition.PageIndex)
                condition.PageIndex = 1;

            var soucrePaging = await temp.ToListAsync();

            //var allTables = await GetTables();

            var vMTables = new List<VMSoucreInfoStatistic>();
            if (soucrePaging.IsNotEmpty())
            {
                var soucreids = soucrePaging.Select(n => n.ID).ToList();
                var where = "";
                var tables = from tb in _dasKTNNRepo.TableInfo.GetAll()
                             where soucreids.Contains(tb.IDSourceInfo) && tb.Status == 1
                             select tb;

                where = $"Where EXTRACT(MONTH FROM {SystemField.NgayTao}) = {condition.Month}  AND Extract(YEAR FROM {SystemField.NgayTao}) = {condition.Year}";
                foreach (var item in soucrePaging)
                {
                    var vmsoucre = Utils.Bind<VMSoucreInfoStatistic>(item.KeyValue());
                    vmsoucre.RecordCount = 0;
                    vmsoucre.Month = condition.Month;
                    vmsoucre.Year = condition.Year;
                    foreach (var table in tables.Where(n => n.IDSourceInfo == item.ID).ToList())
                    {
                        var selectors = new List<string>();
                        var schema = schemas.FirstOrNewObj(n => n.ID == table.IDSchema);
                        if (schema.Code.IsNotEmpty() && table.DbName.IsNotEmpty())
                            selectors.Add($"(select count(1) from {schema.Code}.{table.DbName} {where} ) as {table.DbName}");

                        var query = $"select {string.Join(",", selectors)} FROM dual";
                        try
                        {
                            var _dt = _dynamicDBService.ExecuteSql(query);
                            if (_dt != null && _dt.Rows.Count > 0)
                            {
                                var vmTable = Utils.Bind<VMTableItemStatistic>(table.KeyValue());
                                vmsoucre.RecordCount += Utils.GetInt(_dt, 0, table.DbName);
                            }
                        }
                        catch(Exception ex)
                        {
                            _logger.LogError(ex.ToString());
                        }
                    }
                    vMTables.Add(vmsoucre);
                }
            }
            model.TableStatistics = vMTables;
            var chartTables = vMTables;
            var order = vMTables.OrderByDescending(n => n.RecordCount).Select(n => n.ID).ToList();
            var chartData = new
            {
                labels = chartTables.Select(n => n.Name).ToArray(),
                data = chartTables.Select(n => n.RecordCount).ToArray(),
            };
            soucrePaging = soucrePaging.OrderBy(n => order.IndexOf(n.ID)).Skip((condition.PageIndex - 1) * condition.PageSize).Take(condition.PageSize).ToList();
            model.Tables = new PaginatedList<VMSoucreInfoStatistic>(vMTables, (int)total, condition.PageIndex, condition.PageSize);
            model.ChartData = chartData;
            return model;
        }
        private async Task<List<SchemaInfo>> GetSchemas(params int[] ids)
        {
            return await (from schema in _dasKTNNRepo.SchemaInfo.GetAll()
                          where ids.IsEmpty() || (ids.IsNotEmpty() && ids.Contains(schema.ID))
                          select schema).ToListAsync();
        }
    }
}
