﻿using AutoMapper;
using ClosedXML.Excel;
using Dapper;
using DAS.Application.Constants;
using DAS.Application.Helper;
using DAS.Application.Interfaces;
using DAS.Application.Interfaces.DasKTNN;
using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels.DasKTNN;
using DAS.Domain.Enums;
using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Interfaces.DASNotify;
using DAS.Domain.Models.DASKTNN;
using DAS.Infrastructure.ContextAccessors;
using DAS.Infrastructure.Repositories.DASKTNN;
using DAS.Utility;
using DAS.Utility.BuildCondition;
using DAS.Utility.CustomClass;
using DAS.Utility.LogUtils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static DAS.Application.Enums.DasKTNN.EnumTableInfo;

namespace DAS.Application.Services.DasKTNN
{
    public class ImportDataService : BaseMasterService, IImportDataServices
    {
        #region Properties
        private readonly IMapper _mapper;
        private readonly ILoggerManager _logger;
        private readonly ITableInfoServices _tableInfoServices;
        private readonly IUserPrincipalService _userPrincipalService;
        private readonly IDynamicDBService _dynamicDBService;
        private IExcelServices _excelService;

        #endregion

        #region Ctor
        public ImportDataService(IDasRepositoryWrapper dasRepository, IDasKTNNRepositoryWrapper dasKTNN
            , IMapper mapper
            , ILoggerManager logger
            , ITableInfoServices tableInfoServices
            , IUserPrincipalService userPrincipalService
            , IDynamicDBService dynamicDBService
            , IExcelServices excelService
            , IDasDataDapperRepo dasDapperRepo) : base(dasRepository, dasDapperRepo, dasKTNN)
        {
            _mapper = mapper;
            _logger = logger;
            _tableInfoServices = tableInfoServices;
            _tableInfoServices = tableInfoServices;
            _userPrincipalService = userPrincipalService;
            _dynamicDBService = dynamicDBService;
            _excelService = excelService;
        }
        #endregion

        public async Task<VMImportData> Index(int? idTable)
        {
            var model = new VMImportData();
            var tables = await _tableInfoServices.GetTables();
            model.Tables = tables;
            return model;
        }

        public async Task<VMSyncData> Sync(VMSyncData data)
        {
            var model = new VMSyncData();
            var tables = await _tableInfoServices.GetTables();
            model.Tables = tables;

            if (data.IDTable > 0)
            {
                var columns = await _tableInfoServices.GetColumnByIDTable(data.IDTable.Value);
                model.ColumnTableSources = columns.ToList();
            }
            return model;
        }
        public async Task<VMSyncData> SaveSync(VMSyncData data)
        {
            try
            {
                var model = new VMSyncData();
                var tableInfo = _dasKTNNRepo.TableInfo.Get(data.IDTable) ?? new TableInfo() { Name = "Bảng chưa tạo" };
                var tableInfoMap = _dasKTNNRepo.TableInfo.Get(data.IDTableMap) ?? new TableInfo() { Name = "Bảng chưa tạo" };

                var schemaInfo = _dasKTNNRepo.SchemaInfo.Get(tableInfo.IDSchema) ?? new SchemaInfo();
                var schemaInfoMap = _dasKTNNRepo.SchemaInfo.Get(tableInfoMap.IDSchema) ?? new SchemaInfo();

                var col = _dasKTNNRepo.ColumnTableInfo.Get(data.IDColumnSync) ?? new ColumnTableInfo();

                var colMap = _dasKTNNRepo.ColumnTableInfo.Get(data.IDColumnMap) ?? new ColumnTableInfo(); //Cột map

                var dlValueName = "ID";

                if (col.IDColRef > 0)
                {
                    var columnValueRef = _dasKTNNRepo.ColumnTableInfo.Get(col.IDColRef) ?? new ColumnTableInfo();
                    if (columnValueRef.DbName.IsNotEmpty())
                    {
                        //Cột giá trị
                        dlValueName = columnValueRef.DbName.ToUpper();
                    }
                }

                var count = 0;
                var fail = 0;
                string json;
                var datas = _dynamicDBService.Search(new List<CondParam>
            {
                new CondParam
                        {
                            Sql=  $"{col.DbName} is NULL"
                        }
            }, schemaInfo.Code, tableInfo.DbName);

                if (datas != null && datas.Rows.Count > 0)
                {
                    for (int i = 0; i < datas.Rows.Count; i++)
                    {
                        try
                        {
                            var cRecord = datas.Rows[i];
                            var valUpdates = new List<string>();

                            var idUpdate = cRecord["ID"]?.ToString();
                            var valueUpdate = cRecord[col.DbName]?.ToString();
                            var valueMap = cRecord[colMap.DbName]?.ToString();
                            if (string.IsNullOrEmpty(valueUpdate) && valueMap.IsNotEmpty() && idUpdate.IsNotEmpty())
                            {
                                //Update trường chưa có
                                var valRef = data.Prefix.IsNotEmpty() ? $"{data.Prefix} {valueMap}" : valueMap;
                                var dtRef = _dynamicDBService.Search(new List<CondParam> {
                                new CondParam
                                {
                                    FieldName= $"{colMap.DbName}",
                                    Operator=CondOperator.Equal,
                                    Value=  $"{valRef.Replace("'","''")}",
                                }
                                }, schemaInfoMap.Code, tableInfoMap.DbName, page: new Pagination
                                {
                                    PageIndex = 1,
                                    PageSize = 1,
                                });

                                if (dtRef != null && dtRef.Rows.Count > 0)
                                {
                                    var idRef = dtRef.Rows.Count > 0 ? dtRef.Rows[0]["ID"].ToString() : "0";
                                    var _valRef = dtRef.Rows.Count > 0 ? dtRef.Rows[0][dlValueName].ToString() : "0";


                                    // Giá trị tham chiếu
                                    valUpdates.Add($"{col.DbName} = N'{_valRef}'");
                                    // giá trị ID
                                    valUpdates.Add($"{col.DbName + "_ID"} = N'{idRef}'");
                                    //Lookup
                                    json = Utils.ConvertDataTabletoString(dtRef);
                                    valUpdates.Add($"{col.DbName + "_DATALOOKUP"} = '{json}'");

                                    var comlumvalues = string.Join(",", valUpdates);
                                    var sql = $"UPDATE  {schemaInfo.Code}.{tableInfo.DbName} SET  {comlumvalues} where ID={idUpdate}";
                                    //_logger.LogInfo(sql);
                                    if (_dynamicDBService.UpdateData(sql, null))
                                    {
                                        count++;
                                    }
                                    else
                                    {
                                        _logger.LogError(sql);
                                        fail++;
                                    }
                                }
                                else
                                {
                                    // Giá trị tham chiếu
                                    valUpdates.Add($"{col.DbName} = '0'");

                                    var comlumvalues = string.Join(",", valUpdates);
                                    var sql = $"UPDATE  {schemaInfo.Code}.{tableInfo.DbName} SET  {comlumvalues} where ID={idUpdate}";
                                    //_logger.LogInfo(sql);
                                    if (_dynamicDBService.UpdateData(sql, null))
                                    {
                                        count++;
                                    }
                                    else
                                    {
                                        _logger.LogError(sql);
                                        fail++;
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            fail++;
                        }
                    }
                    model.IsSuccess = true;
                    model.Message = $"Cập nhật {count}/{datas.Rows.Count} bản ghi thành công.";
                    if (fail > 0)
                        model.Message += $"Thất bại {fail}";
                    return model;
                }
                else
                {
                    model.Message = $"Không có dữ liệu cập nhật";
                    return model;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(Utils.Serialize(ex));
                throw ex;
            }
        }
        public async Task<VMImportData> GetHeader(VMImportData importData)
        {
            if (!importData.IDTable.HasValue)
                throw new LogicException("Vui lòng chọn bảng dữ liệu cần import");

            var tableInfo = await _dasKTNNRepo.TableInfo.GetAsync(importData.IDTable ?? 0);
            if (tableInfo == null || tableInfo.Status == (int)EnumCommon.Status.InActive)
                throw new LogicException("Bảng dữ liệu này hiện không tồn tại hoặc đã bị xóa");

            var tableColumns = await _tableInfoServices.GetColumnByIDTable(importData.IDTable.Value);
            var model = new VMImportData()
            {
                IsQuickImport = importData.IsQuickImport
            };

            if (!importData.Sheet.HasValue)
                importData.Sheet = 1;

            var sheet = importData.Sheet > 0 ? importData.Sheet.Value : 1;
            var isQuickImport = importData.IsQuickImport > 0; //import từ ds các bảng
            model.Sheet = sheet;
            var headers = new Dictionary<int, string>();
            var dicSheet = new Dictionary<int, string>();
            var headerMappers = new List<VMImportDataHeaderMapper>();
            var file = importData.FileImport;
            if (file != null && file.Length > 0)
            {
                using (var ms = new MemoryStream())
                {
                    file.CopyTo(ms);
                    var workBook = new XLWorkbook(ms);
                    var sheetCount = workBook.Worksheets.Count;
                    for (int i = 1; i <= sheetCount; i++)
                    {
                        var _ws = workBook.Worksheet(i);
                        dicSheet.Add(i, _ws.Name);
                    }
                    var ws = workBook.Worksheet(sheet);

                    CheckTemplate(ws, importData, tableColumns, out headers);

                    model.Headers = headers;
                }


                var tableRefIds = tableColumns.Where(n => n.IDTableRef > 0).Select(n => n.IDTableRef).ToList();


                var tableRefs = await GetAllTablesByIDs(tableRefIds.ToArray());
                var columnRefs = await GetAllColumnTables(tableRefIds.ToArray());
                var tabelLinkColRef = GetLinkTableRefCol();

                foreach (var tableColumn in tableColumns)
                {
                    if (tableColumn.IsSystem)
                        continue;
                    var listColRef = new List<ColumnTableInfo>();
                    if (tableColumn.IDTableRef > 0)
                    {
                        var colRefs = columnRefs.Where(n => n.IDTable == tableColumn.IDTableRef);
                        if (colRefs.Any())
                            listColRef.AddRange(colRefs);
                    }
                    else if (tableColumn.DataType == (int)ColumnDataType.TableLink)
                    {
                        listColRef.AddRange(tabelLinkColRef);
                    }

                    var mapper = new VMImportDataHeaderMapper
                    {
                        ColumnTableInfo = tableColumn,
                        ColumnRefs = listColRef,
                        IsUnique = importData.IsQuickImport > 0 ? (byte)0 : (tableColumn.DbName == CategoryDefaultField.TenDanhMuc.ToString()
                        || tableColumn.DbName == DefaultField.Ten.ToString()) ? (byte)1 : (byte)0 //Tự động check cho trường Name
                    };
                    KeyValuePair<int, string> header;
                    if (isQuickImport)
                    {
                        //import từ ds các bảng thì check đúng tuyệt đối
                        header = headers.FirstOrDefault(n => IsStringEqual(n.Value, tableColumn.Name));
                    }
                    else
                    {
                        if (importData.IsMapCol > 0)
                        {
                            header = headers.FirstOrDefault(n => n.Value != null && (
                                IsStringEqual(n.Value, tableColumn.DbName) || IsStringContain(n.Value, tableColumn.DbName)
                                || IsStringContain(tableColumn.DbName, n.Value.Substring(0, n.Value.LastIndexOf("(")))));
                        }
                        else
                        {
                            header = headers.FirstOrDefault(n => n.Value != null &&
                               (IsStringEqual(n.Value, tableColumn.DbName) || IsStringContain(n.Value, tableColumn.DbName) || IsStringContain(tableColumn.DbName, n.Value.Substring(0, n.Value.LastIndexOf("(")))
                            || IsStringEqual(n.Value, tableColumn.Name) || IsStringContain(n.Value, tableColumn.Name) || IsStringContain(tableColumn.Name, n.Value.Substring(0, n.Value.LastIndexOf("(")))));
                        }
                    }
                    if (header.Key.IsNotEmpty())
                    {
                        mapper.HeaderTitle = header.Value;
                        mapper.HeaderIndex = header.Key;
                    }
                    headerMappers.Add(mapper);
                }
                model.TableRefs = tableRefs;
            }
            model.HeaderMappers = headerMappers;
            model.DicSheet = dicSheet;
            return model;
        }

        public class DataImportSort
        {

            public string Ma { get; set; }
            public string Cha { get; set; }
            public int Level { get; set; }
            public Hashtable Data { get; set; }
        }


        public async Task<ServiceResult> Import(VMImportData data)
        {
            try
            {
                var tableInfo = await _dasKTNNRepo.TableInfo.GetAsync(data.IDTable ?? 0);
                if (tableInfo == null || tableInfo.Status == (int)EnumCommon.Status.InActive)
                    throw new LogicException("Bảng dữ liệu này hiện không tồn tại hoặc đã bị xóa");
                var schema = await _dasKTNNRepo.SchemaInfo.GetAsync(tableInfo.IDSchema);
                if (schema == null)
                {
                    throw new LogicException("CSDL không tồn tại");
                }
                var tableColumns = await _tableInfoServices.GetColumnByIDTable(data.IDTable.Value);

                var importDatas = new List<Hashtable>();//Chứa ds bản ghi đã thêm
                var sortItems = new List<DataImportSort>();//Chứa ds bản ghi sap xep
                //var records = new List<Hashtable>(); //Chứa ds bản ghi trên db
                var headers = new Dictionary<string, int>();
                var headerMappers = new List<VMImportDataHeaderMapper>();
                var file = data.FileImport;
                var errCount = 0;
                var ssCount = 0;
                var sqls = new List<string>();

                if (!data.Sheet.HasValue)
                    data.Sheet = 1;

                var sheet = data.Sheet > 0 ? data.Sheet.Value : 1;
                if (file != null && file.Length > 0)
                {
                    using (var ms = new MemoryStream())
                    {
                        file.CopyTo(ms);
                        var workBook = new XLWorkbook(ms);
                        var ws = workBook.Worksheet(sheet);

                        CheckTemplate(ws, data, tableColumns, out _);

                        int rowused = ws.LastRowUsed() != null ? ws.LastRowUsed().RowNumber() : 0;

                        //lấy danh sách column refer và column path
                        var idColumnRefer = tableColumns.Select(t => t.IDColRef).Distinct().ToList() ?? new List<int>();
                        var idColumnPath = tableColumns.Select(t => t.ID).Distinct().ToArray();
                        var columnsPaths = await GetAllColumnPathByIDColumnRefer(idColumnPath) ?? new List<ColumnPathInfo>();
                        if (Utils.IsNotEmpty(columnsPaths))
                        {
                            idColumnRefer.AddRange(columnsPaths.Select(t => t.IDColumnPath));
                        }
                        var idColumnReferMapper = data.HeaderMappers.Where(t => t.IDColumnRef > 0).Select(t => t.IDColumnRef).ToList();
                        if (Utils.IsNotEmpty(idColumnReferMapper))
                        {
                            idColumnRefer.AddRange(idColumnReferMapper);
                        }
                        var columnsRefers = await GetAllColumnByIDs(idColumnRefer.ToArray()) ?? new List<ColumnTableInfo>();

                        var idchemas = tableColumns.Select(t => t.IDSchemaRef).Distinct().ToArray();
                        var idtables = tableColumns.Select(t => t.IDTableRef).Distinct().ToArray();
                        var tablseRefer = await GetAllTablesByIDs(idtables) ?? new List<TableInfo>();
                        var schemasRefer = await GetSchemas(idchemas) ?? new List<SchemaInfo>();
                        //-------------
                        var totalItem = 0;
                        var startDataRow = data.DataRow > 0 ? data.DataRow.Value : 1;
                        if (data.MaxRow.HasValue && data.MaxRow.Value > 0)
                        {
                            totalItem = startDataRow + data.MaxRow.Value - 1;
                            if (totalItem > rowused)
                            {
                                totalItem = rowused;
                            }
                        }
                        else
                        {
                            totalItem = rowused;
                        }

                        //Đọc từ file excel
                        for (int rowIndex = startDataRow; rowIndex <= totalItem; rowIndex++)
                        {
                            var dataItem = new Hashtable();
                            var isExists = false;
                            var maDm = string.Empty;
                            var dmCha = string.Empty;

                            var parentCol = tableColumns.FirstOrNewObj(n => n.DbName == DefaultField.DanhMucCha.ToString());
                            var parentColMapper = data.HeaderMappers.FirstOrNewObj(n => n.ColumnID == parentCol.ID); //Cấu hình map cột dm cha & cột excel
                            var columnMapperRefer = columnsRefers.FirstOrNewObj(t => t.ID == parentColMapper.IDColumnRef);

                            foreach (var tableColumn in tableColumns)
                            {
                                if (tableColumn.IsSubColumn)
                                    continue;

                                if (tableColumn.IsSystem && tableColumn.IsIdentity)
                                    continue;

                                var colName = tableColumn.DbName;
                                var colMapper = data.HeaderMappers.FirstOrNewObj(n => n.ColumnID == tableColumn.ID); //Cấu hình map cột db & cột excel

                                var isCheckColAllowIdentity = true;
                                var tempData = new Hashtable();
                                if (colMapper != null && colMapper.HeaderIndex > 0)
                                {
                                    var cell = ws.Cell(rowIndex, colMapper.HeaderIndex);
                                    string cellVal = string.Empty;
                                    cellVal = cell.Value?.ToString();
                                    if (tableColumn.IsIdentity && colName == CategoryDefaultField.MaDanhMuc.ToString() && cellVal.IsEmpty())
                                    {
                                        cellVal = KTNNConst.IdentityValue;
                                        if (isCheckColAllowIdentity)
                                        {
                                            isCheckColAllowIdentity = false; //CChi check 1 lan
                                            var isAllowIdentity = CheckedColAllowIdentity(schema, tableInfo, tableColumn);
                                            if (!isAllowIdentity)
                                            {
                                                throw new LogicException($"Bảng dữ liệu không thể tự động sinh mã, vui lòng nhập '{tableColumn.Name}'");
                                            }
                                        }
                                    }

                                    else if (tableColumn.IsIdentity && !tableColumn.IsDefault && cellVal.IsEmpty())
                                    {
                                        cellVal = DynamicDBHelper.GetIdentityValue(tableColumn);
                                    }
                                    else if(colName == DefaultField.KieuPhatSinh.ToString() && cellVal.IsEmpty())
                                    {
                                        cellVal= CreateTypeConst.MacDinh.ToString();
                                    }

                                    if (tableColumn.DataType == (int)ColumnDataType.DateTime)
                                    {
                                        if (Utils.IsObjDate(cell.Value, out DateTime? dt))
                                        {
                                            tempData.Add(colName, dt.Value.ToString("dd/MM/yyyy HH:mm"));
                                        }
                                        else
                                            tempData.Add(colName, cellVal);
                                    }
                                    else
                                        tempData.Add(colName, cellVal);


                                    if (colName == columnMapperRefer.DbName)
                                    {
                                        maDm = cellVal;
                                    }

                                    else if (colName == DefaultField.DanhMucCha.ToString())
                                    {
                                        dmCha = cellVal.IsEmpty() ? "0" : cellVal;
                                    }

                                }

                                var value = GetColumnValue(tableColumn, tempData);

                                if (!dataItem.ContainsKey(colName))
                                {
                                    dataItem.Add(colName, value);
                                }
                                if (colMapper.IsUnique > 0
                                    || (data.IsUniqueName > 0 && (tableColumn.Name == DefaultField.Ten.ToString() || tableColumn.Name == CategoryDefaultField.TenDanhMuc.ToString())))
                                {
                                    isExists = importDatas.IsNotEmpty() && importDatas.Any(n => Utils.GetString(n, colName) == value); //Check trùng trên file excel
                                }
                                if (isExists)
                                    break;
                            }

                            if (isExists)
                                continue;


                            sortItems.Add(new DataImportSort
                            {
                                Ma = maDm.Trim(),
                                Cha = dmCha.Trim(),
                                Data = dataItem
                            });

                            importDatas.Add(dataItem);
                        }
                        //Convert hastable to data
                        if (importDatas.IsEmpty())
                            throw new LogicException("File import không có dữ liệu");

                        var index = 0;
                        var colLinkTable = tableColumns.FirstOrDefault(n => n.DataType == (int)ColumnDataType.TableLink);
                        var nameTableLienKet = $"{schema.Code}{KTNNConst.TableLinkNameExt}";
                        var allSchemas = await GetSchemas();
                        var allTables = await GetTables();

                        var tableLinkRefCols = GetLinkTableRefCol();


                        var sorted = new List<DataImportSort>();

                        var chas = sortItems.Select(n => n.Ma).Distinct().ToList();

                        var otherParents = sortItems.Where(n => !chas.Exists(t => t == n.Cha) || n.Ma == n.Cha).ToList();
                        //var otherParents = sortItems.Where(n => n.Cha == "0" || n.Cha == "-1").ToList();
                        foreach (var item in otherParents)
                        {
                            item.Level = 0;
                            sorted.Add(item);
                            var _sorted = GetChildItemImport(sortItems, item.Ma, 1);
                            if (_sorted.IsNotEmpty())
                                sorted.AddRange(_sorted);
                        }
                        //var sortedId = sorted.Select(n => n.Ma);
                        //var otherChilds = sortItems.Where(n => !sortedId.Contains(n.Ma));
                        //if (otherChilds.IsNotEmpty())
                        //{
                        //    sorted.AddRange(otherChilds);
                        //}

                        foreach (var importData in sorted.Select(n => n.Data))
                        {
                           
                            index++;
                            var isExists = false;
                            var query = GetInsertQuery(importData, tableColumns, data, schema, tableInfo, tablseRefer, schemasRefer, columnsRefers, columnsPaths, ref isExists);
                            var param = new DynamicParameters();
                            param.Add(name: "Id", dbType: DbType.Int32, direction: ParameterDirection.Output);
                            var Idinserted = 0;

                            if (_dynamicDBService.InsertData(query, param, ref Idinserted))
                            {
                                if (colLinkTable != null && colLinkTable.ID > 0)
                                {
                                    var colLinkTableVal = Utils.GetString(importData, colLinkTable.DbName);
                                    await SaveTableLinkDataAsync(data, allSchemas, allTables, schema, tableInfo, tableLinkRefCols, colLinkTable, colLinkTableVal, Idinserted);
                                }
                                //Todo
                                ssCount++;
                            }
                            else
                            {
                                errCount++;
                            }
                        }
                    }

                    //var query = string.Join(" ", sqls);
                    //if (_dynamicDBService.InsertData(query, null))
                    //{
                    if (errCount == 0 && ssCount > 0)
                        return new ServiceResultSuccess($"Import dữ liệu thành công {ssCount}/{importDatas.Count}");

                    if (errCount > 0 && ssCount > 0)
                        return new ServiceResultSuccess($"Import dữ liệu thành công {ssCount}/{importDatas.Count}, lỗi {errCount}");
                    //}
                }
                return new ServiceResultError("Import dữ liệu không thành công");
            }
            catch (LogicException ex)
            {
                return new ServiceResultError(ex.Message);
            }
            catch (Exception ex)
            {
                Guid.NewGuid().ToString();
                _logger.LogError(ex);
                return new ServiceResultError(ex.Message);
            }

        }

        private List<DataImportSort> GetChildItemImport(List<DataImportSort> sortItems, string parent, int lv = 0)
        {

            var rs = new List<DataImportSort>();

            if (lv == 100)
                return rs;

            var items = sortItems.Where(n => n.Cha == parent && n.Cha != n.Ma).ToList();
            if (items.IsNotEmpty())
            {
                foreach (var item in items)
                {
                    item.Level = lv;
                    rs.Add(item);
                    var childs = GetChildItemImport(sortItems, item.Ma, lv + 1);
                    if (childs.Count > 0)
                        rs.AddRange(childs);
                }
            }
            return rs;
        }

        public async Task<ServiceResult> DownloadTemplate(int IDTable)
        {
            try
            {
                var tableInfo = _dasKTNNRepo.TableInfo.Get(IDTable) ?? new TableInfo() { };
                var schemaInfo = _dasKTNNRepo.SchemaInfo.Get(tableInfo.IDSchema) ?? new SchemaInfo();
                var tableColumns = await GetAllColumnTables(tableInfo.ID) ?? new List<ColumnTableInfo>();
                var hearsers = new List<Header>
                {
                    //new Header("STT", 8),
                };
                foreach (var tableColumn in tableColumns)
                {
                    if (tableColumn.IsSystem)
                        continue;

                    hearsers.Add(new Header(tableColumn.Name));
                }
                var export = new ExportExtend
                {
                    Data = new List<dynamic>(),
                    Cols = new List<Col>(),
                    Headers = hearsers
                };
                var rs = await _excelService.ExportExcel(export, tableInfo.DbName);
                return rs;
            }
            catch (LogicException ex)
            {
                return new ServiceResultError(ex.Message);
            }
            catch (Exception ex)
            {
                Guid.NewGuid().ToString();
                _logger.LogError(ex);
                return new ServiceResultError("Có lỗi khi tải file biểu mẫu import");
            }

        }

        #region Gets
        public async Task<List<TableInfo>> GetAllTablesByIDs(params int[] idTables)
        {
            if (Utils.IsEmpty(idTables))
                return new List<TableInfo>();
            return await (from col in _dasKTNNRepo.TableInfo.GetAll()
                          where idTables.Contains(col.ID) && col.Status == (int)EnumCommon.Status.Active
                          select col).ToListAsync();
        }
        public async Task<List<ColumnTableInfo>> GetAllColumnByIDTable(params int[] idTables)
        {
            return await (from col in _dasKTNNRepo.ColumnTableInfo.GetAll()
                          where idTables.Contains(col.IDTable) && col.Status == (int)EnumCommon.Status.Active
                          orderby col.Weight
                          select col).ToListAsync();
        }
        public async Task<List<ColumnTableInfo>> GetAllColumnByIDs(params int[] ids)
        {
            return await (from col in _dasKTNNRepo.ColumnTableInfo.GetAll()
                          where ids.Contains(col.ID) && col.Status == (int)EnumCommon.Status.Active
                          orderby col.Weight
                          select col).ToListAsync();
        }
        public async Task<List<ColumnPathInfo>> GetAllColumnPathByIDColumnRefer(params int[] idclrefers)
        {
            return await (from col in _dasKTNNRepo.ColumnPathInfo.GetAll()
                          where idclrefers.Contains(col.IDColumnTableInfo) && col.Status == (int)EnumCommon.Status.Active
                          select col).ToListAsync();
        }
        public async Task<List<ColumnTableInfo>> GetColumnTables(params int[] idTables)
        {
            return await (from col in _dasKTNNRepo.ColumnTableInfo.GetAll()
                          where idTables.Contains(col.IDTable) && col.IsShowOnList == true && col.Status == (int)EnumCommon.Status.Active
                          orderby col.Weight
                          select col).ToListAsync();
        }
        #endregion

        #region Funtions
        private async Task<List<ColumnTableInfo>> GetAllColumnTables(params int[] idTables)
        {
            return await (from col in _dasKTNNRepo.ColumnTableInfo.GetAll()
                          where idTables.Contains(col.IDTable) && col.Status == (int)EnumCommon.Status.Active
                          orderby col.Weight
                          select col).ToListAsync();
        }
        private async Task<List<SchemaInfo>> GetSchemas(params int[] ids)
        {
            return await (from schema in _dasKTNNRepo.SchemaInfo.GetAll()
                          where ids.IsEmpty() || ids.Contains(schema.ID)
                          select schema).ToListAsync();
        }
        private string GetInsertQuery(Hashtable importData, IEnumerable<ColumnTableInfo> tableColumns, VMImportData data, SchemaInfo schema, TableInfo tableInfo, List<TableInfo> tablseRefer, List<SchemaInfo> schemasRefer, List<ColumnTableInfo> columnsRefers, List<ColumnPathInfo> columnsPath, ref bool isExists)
        {
            var columns = new List<string>();
            var values = new List<string>();

            foreach (var tableColumn in tableColumns)
            {
                if (tableColumn.IsSubColumn)
                    continue;

                if (tableColumn.IsSystem && tableColumn.IsIdentity)
                    continue;


                var colName = tableColumn.DbName;
                var value = Utils.GetString(importData, colName);
                var colMapper = data.HeaderMappers.FirstOrNewObj(n => n.ColumnID == tableColumn.ID); //Cấu hình map cột db & cột excel

                //if (colMapper.IsUnique > 0)
                //{
                //    var dtExists = _dynamicDBService.Search(new List<CondParam> {
                //                    new CondParam
                //                    {
                //                        FieldName= tableColumn.DbName,
                //                        Operator=CondOperator.Equals,
                //                        Value=value
                //                    }
                //                    }, schema.Code, tableInfo.DbName);

                //    isExists = dtExists != null && dtExists.Rows.Count > 0;
                //    if (isExists)
                //        break;
                //}

                var columnMapperRefer = columnsRefers.FirstOrNewObj(t => t.ID == colMapper.IDColumnRef);
                var columnRefer = columnsRefers.FirstOrNewObj(t => t.ID == tableColumn.IDColRef);
                var json = "";
                if (tableColumn.DataType == (int)ColumnDataType.Parent && value.IsNotEmpty())
                {
                    if (value.IsEmpty())
                        value = "0";

                    var level = 0;
                    //TH cha con
                    var tableRefer = tablseRefer.FirstOrNewObj(t => t.ID == columnRefer.IDTable);
                    var schemaRefer = schemasRefer.FirstOrNewObj(t => t.ID == tableRefer.IDSchema);
                    DataTable dt = null;
                    try
                    {
                        value = value.TrimStart("'").TrimEnd("'");
                        dt = _dynamicDBService.GetFirstByField(schemaRefer.Code, tableRefer.DbName, columnMapperRefer.DbName, value, true);
                    }
                    catch (Exception ex)
                    {
                    }
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        var Parent = dt.Rows[0];
                        level = Utils.GetInt(Parent[tableColumn.DbName + "_LEVEL"]?.ToString()) + 1;
                        //PathParent = Parent[tableColumn.DbName + "_" + columnRefer.DbName].ToString() + "|" + Parent[columnRefer.DbName];
                        //PathParent = PathParent.TrimStart('|');
                        json = Utils.ConvertDataTabletoString(dt);
                        if (json.IsNotEmpty())
                        {
                            json = json.Replace("'", "''");
                        }
                        //Đường dẫn
                        var columnspathParent = columnsPath.Where(t => t.IDColumnTableInfo == tableColumn.ID).ToList();
                        if (Utils.IsNotEmpty(columnspathParent))
                        {
                            foreach (var dd in columnspathParent)
                            {
                                var columnPath = columnsRefers.FirstOrDefault(t => t.ID == dd.IDColumnPath);
                                if (columnPath != null)
                                {
                                    columns.Add(dd.PathName);
                                    //if (columnPath.IsIdentity)
                                    //{
                                    //    //TH tự tăng thì path là: Parents|ParentID
                                    //    var cPath = Parent[columnPath.DbName]?.ToString().TrimStart("N'").TrimEnd("'");
                                    //    var vlPath = (Parent[dd.PathName].ToString() + "|" + cPath).TrimStart('|');
                                    //    //Bỏ N' & '
                                    //    vlPath = vlPath.TrimStart("N'").TrimEnd("'");
                                    //    values.Add($"'{vlPath.Replace("'", "''")}'");
                                    //}
                                    //else
                                    //{
                                    var cPath = Utils.GetString(importData, columnPath.DbName).TrimStart("N'").TrimEnd("'");    //Bỏ N' & '
                                    var vlPath = (Parent[dd.PathName].ToString() + cPath ).TrimStart('|');
                                    vlPath = vlPath.TrimStart("N'").TrimEnd("'");  //Bỏ N' & '
                                    vlPath = $"|{vlPath}|";
                                    values.Add($"'{vlPath.Replace("'", "''")}'");
                                    //}
                                }
                            }
                        }
                        columns.Add(tableColumn.DbName + "_ID");
                        values.Add($"'{Parent["ID"]}'");

                        //Add data
                        columns.Add(colName);
                        values.Add($"'{Parent[columnRefer.DbName]}'");
                    }
                    else
                    {
                        //Đường dẫn
                        var columnspathParent = columnsPath.Where(t => t.IDColumnTableInfo == tableColumn.ID).ToList();
                        if (Utils.IsNotEmpty(columnspathParent))
                        {
                            foreach (var dd in columnspathParent)
                            {
                                var columnPath = columnsRefers.FirstOrDefault(t => t.ID == dd.IDColumnPath);
                                if (columnPath != null)
                                {
                                    columns.Add(dd.PathName);
                                    //if (columnPath.IsIdentity)
                                    //{
                                    //    if (columnPath.DataType == (int)ColumnDataType.String)
                                    //        values.Add($"''");
                                    //    else
                                    //        values.Add($"'0'");
                                    //}
                                    //else
                                    //{
                                    var vlPath = (Utils.GetString(importData, columnPath.DbName));
                                    vlPath = vlPath.TrimStart("N'").TrimEnd("'"); //Bỏ N' & '
                                    vlPath = $"|{vlPath}|";
                                    values.Add($"'{vlPath.Replace("'", "''")}'");
                                    //}
                                }
                            }
                        }

                        // Path
                        columns.Add(tableColumn.DbName + "_ID");
                        values.Add($"'0'");

                        //Thêm giá trị trực tiếp
                        columns.Add(colName);
                        values.Add($"'{(columnRefer.DataType == (int)ColumnDataType.String ? "" : "0")}'");

                    }
                    // Level
                    columns.Add(tableColumn.DbName + "_LEVEL");
                    values.Add($"'{level}'");

                    //Lookup
                    columns.Add(tableColumn.DbName + "_DATALOOKUP");
                    values.Add($"'{json}'");

                }
                else if (tableColumn.DataType == (int)ColumnDataType.TableRef)
                {
                    //TH tham chiếu

                    var tableRefer = tablseRefer.FirstOrNewObj(t => t.ID == columnRefer.IDTable);
                    var schemaRefer = schemasRefer.FirstOrNewObj(t => t.ID == tableRefer.IDSchema);
                    DataTable dt = null;
                    try
                    {
                        value = value.TrimStart("'").TrimEnd("'");
                        dt = _dynamicDBService.GetFirstByField(schemaRefer.Code, tableRefer.DbName, columnMapperRefer.DbName, value, true);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                    if (dt != null && dt.Rows.Count > 0)
                    {
                        var Parent = dt.Rows[0];
                        json = Utils.ConvertDataTabletoString(dt);
                        if (json.IsNotEmpty())
                        {
                            json = json.Replace("'", "''");
                        }
                        columns.Add(tableColumn.DbName + "_ID");
                        values.Add($"'{Parent["ID"]}'");

                        columns.Add(colName);
                        values.Add($"'{Parent[columnRefer.DbName]}'"); //Gán giá trị tham chiếu trực tiếp
                    }
                    //Lookup
                    columns.Add(tableColumn.DbName + "_DATALOOKUP");
                    values.Add($"'{json}'");
                }
                else if (tableColumn.DataType == (int)ColumnDataType.TableLink)
                {
                    //Dữ liệu thêm bảng _LienKet
                    columns.Add(colName);
                    values.Add("''");
                }
                else
                {
                    if (IsStringEqual(colName, SystemField.TrangThai.ToString()))
                    {
                        value = "1";
                    }
                    //TH thông thường
                    columns.Add(colName);
                    values.Add(value);
                }
            }

            if (!columns.Exists(t => t.ToUpper() == SystemField.TrangThai.ToString().ToUpper()))
            {
                columns.Add(SystemField.TrangThai.ToString().ToUpper());
                values.Add($"'1'");
            }
            var column = string.Join(",", columns); //string.Join(",", columnbyAlls.Select(n => n.DbName).ToList());
            var valuedata = string.Join(",", values);
            var query = $"INSERT INTO {schema.Code}.{tableInfo.DbName} ({column}) VALUES ({valuedata}) returning ID into :Id";
            _logger.LogDebug(query);
            return query;
        }
        private string GetColumnValue(ColumnTableInfo item, Hashtable data)
        {
            var val = Utils.GetString(data, item.DbName);
            if (val.IsNotEmpty())
            {
                val = val.Replace("'", "''");
            }
            switch (item.DataType)
            {
                case (int)ColumnDataType.DateTime:
                    if (item.IsSystem)
                    {
                        return DateTime.Now.ToOracleStringDatetime();
                    }
                    else
                    {
                        if (Utils.IsDate(Utils.GetDate(data, item.DbName)))
                        {
                            return Utils.GetDate(data, item.DbName).ToOracleStringDatetime();
                        }
                        else if (Utils.IsDate(Utils.GetDatetime(data, item.DbName)))
                        {
                            return Utils.GetDatetime(data, item.DbName).ToOracleStringDatetime();
                        }
                        else if (Utils.IsDate(Utils.GetDatetime(data, item.DbName, "dd/MM/yyyy HH:mm:ss")))
                        {
                            return Utils.GetDatetime(data, item.DbName, "dd/MM/yyyy HH:mm:ss").ToOracleStringDatetime();
                        }
                        else
                            return "NULL";// $"TO_DATE('NULL', 'yyyy-MM-dd hh24:mi:ss')";
                    }
                case (int)ColumnDataType.TableUser:
                    return _userPrincipalService.UserId.ToOracleStringNumber();
                case (int)ColumnDataType.Decimal:
                case (int)ColumnDataType.Integer:
                    return Utils.GetInt(data, item.DbName).ToOracleStringNumber();
                case (int)ColumnDataType.Boolean:
                    return Utils.GetInt(data, item.DbName).ToOracleStringNumber();
                case (int)ColumnDataType.TableRef:
                case (int)ColumnDataType.Parent:
                    return $"'{val}'";
                case (int)ColumnDataType.TableLink:
                    return val;
                default:
                    var str = val;
                    if (str.IsNotEmpty())
                        return $"N'{str}'";
                    return "''";
            }
        }
        private void CheckTemplate(IXLWorksheet ws, VMImportData data, IEnumerable<ColumnTableInfo> tableColumns, out Dictionary<int, string> headers)
        {
            headers = new Dictionary<int, string>();

            var isQuickImport = data.IsQuickImport > 0; //import từ ds các bảng

            var startRow = data.HeaderRow > 0 ? data.HeaderRow.Value : 1;
            var row = ws.Row(startRow); //Đọc header
            var iCol = 0;
            var cells = row.Cells();
            foreach (var cell in cells)
            {
                iCol++;
                headers.Add(iCol, isQuickImport ? cell.Value.ToString().Trim() : $"{cell.Value} ({cell.Address})");
            }

            //Check đúng bm ko
            if (isQuickImport)
            {
                var inputHeader = tableColumns.Where(n => !n.IsSystem);
                foreach (var item in inputHeader)
                {
                    if (headers.Count(n => n.Value.ToLower() == item.Name.ToLower().Trim()) == 0)
                        throw new LogicException("Biểu mẫu không đúng mẫu, vui lòng tải biểu mẫu về và thử lại");
                }
            }
        }
        private async Task<IEnumerable<TableInfo>> GetTables(params int[] idTables)
        {
            var temp = from tb in _dasKTNNRepo.TableInfo.GetAll()
                       where idTables.IsEmpty() || idTables.Contains(tb.ID) && tb.Status == (int)EnumCommon.Status.Active
                       orderby tb.Name
                       select tb;
            return await temp.ToListAsync();
        }
        private async Task<bool> TableIsCategory(int idTable)
        {
            return await _dasKTNNRepo.ColumnTableInfo.AnyAsync(n => n.DbName == CategoryDefaultField.TenDanhMuc.ToString() && n.IDTable == idTable);

        }
        private async Task<bool> SaveTableLinkDataAsync(VMImportData data, List<SchemaInfo> allSchemas, IEnumerable<TableInfo> allTables, SchemaInfo schema, TableInfo tableInfo, List<ColumnTableInfo> tableLinkRefCols, ColumnTableInfo tableColumn, string colLinkTableVal, int idinserted)
        {
            var colMapper = data.HeaderMappers.FirstOrNewObj(n => n.ColumnID == tableColumn.ID); //Cấu hình map cột db & cột excel


            var vlLinks = Utils.Deserialize<List<VMTableLinkValue>>(colLinkTableVal);
            if (Utils.IsNotEmpty(vlLinks))
            {
                string json;
                foreach (var item in vlLinks)
                {
                    var schemaRef = allSchemas.FirstOrDefault(n => n.Code.ToLower() == item.databaseName.ToLower()
                    || n.Name.ToLower() == item.databaseName.ToLower());
                    if (schemaRef == null)
                        continue;

                    var tableRef = allTables.FirstOrDefault(n => n.IDSchema == schemaRef.ID && (n.DbName.ToLower() == item.tableName.ToLower()
                    || n.Name.ToLower() == item.tableName.ToLower()));
                    if (tableRef == null)
                        continue;

                    var columnRefer = tableLinkRefCols.FirstOrNewObj(n => n.ID == colMapper.IDColumnRef);
                    if (columnRefer.DbName == DefaultField.Ten.ToString())
                    {
                        //Check bảng thuộc loại DM hay ko
                        columnRefer.DbName = await TableIsCategory(tableRef.ID)
                            ? CategoryDefaultField.TenDanhMuc.ToString()
                            : DefaultField.Ten.ToString();
                    }

                    DataTable dt = null;
                    try
                    {
                        var value = item.value ?? string.Empty;
                        if (value.IsNotEmpty())
                            value = value.Replace("'", "''");
                        dt = _dynamicDBService.GetFirstByField(schemaRef.Code, tableRef.DbName, columnRefer.DbName, value, true);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        var Parent = dt.Rows[0];
                        var currentData = _dynamicDBService.GetFirstById(schema.Code, tableInfo.DbName, idinserted);
                        var tableLinkData = new LinkTableModel()
                        {
                            IDSchema = tableInfo.IDSchema,
                            SchemaName = schema.Code,
                            IDTable = tableInfo.ID,
                            TableName = tableInfo.DbName,
                            IDRecord = idinserted,
                            DataLookUp = currentData.ToOracleDataLookup(new List<string> { "ID", "TenDanhmuc", "MaDanhMuc" }),

                            IDSchemaRef = schemaRef.ID,
                            SchemaNameRef = schemaRef.Code,
                            IDTableRef = tableRef.ID,
                            TableNameRef = tableRef.DbName,

                            IDRecordRef = Utils.GetInt(Parent["ID"]?.ToString()),
                            DataLookUpRef = dt.ToOracleDataLookup(new List<string> { "ID", "TenDanhmuc", "MaDanhMuc" }),
                        };
                        _dynamicDBService.InsertTableLinkData(schema.Code, tableInfo.DbName, tableLinkData);
                    }
                }
            }
            return true;
        }
        private List<ColumnTableInfo> GetLinkTableRefCol()
        {
            return new List<ColumnTableInfo>
            {
                new ColumnTableInfo
                {
                    ID =1,
                    Name = "ID",
                    DbName = SystemField.ID.ToString()
                },
                new ColumnTableInfo
                {
                    ID =2,
                    Name = "Tên",
                    DbName = DefaultField.Ten.ToString()
                },
                new ColumnTableInfo
                {
                    ID =3,
                    Name = "Mã",
                    DbName = "Ma"
                }
            };
        }

        private bool IsStringContain(string str1, string str2)
        {
            if (str1 == str2)
                return true;

            if (str1 == null || str2 == null)
                return false;

            if (str1.Trim().ToLower().IndexOf(str2.Trim().ToLower()) > -1)
                return true;

            return false;
        }
        private bool IsStringEqual(string str1, string str2)
        {
            if (str1 == str2)
                return true;

            if (str1 == null || str2 == null)
                return false;

            if (str1.Trim().ToLower() == str2.Trim().ToLower())
                return true;

            return false;
        }

        private bool CheckedColAllowIdentity(SchemaInfo schemaInfo, TableInfo tableInfo, ColumnTableInfo identityCol)
        {

            try
            {
                var lastRecord = _dynamicDBService.GetFirst(schemaInfo.Code, tableInfo.DbName, $"{identityCol.DbName} DESC");
                if (lastRecord == null || lastRecord.Rows.Count == 0)
                {
                    return true;
                };

                var colVal = lastRecord.Rows[0][identityCol.DbName]?.ToString();
                return long.TryParse(colVal, out _);

            }
            catch (Exception ex)
            {

                _logger.LogError(Utils.Serialize(ex));
            }
            return false;
        }
        #endregion
    }
}