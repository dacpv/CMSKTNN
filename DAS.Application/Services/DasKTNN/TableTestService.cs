﻿using AutoMapper;
using DAS.Application.Interfaces;
using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels;
using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Models.DAS;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using NLog.Config;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using DAS.Application.Constants;
using Newtonsoft.Json;
using DAS.Utility;
using DAS.Utility.CacheUtils;
using DAS.Domain.Enums;
using Microsoft.AspNetCore.Http;
using DAS.Application.Enums;
using DAS.Domain.Interfaces.DASNotify;
using DAS.Application.Interfaces.DasCongViec;
using System.Collections;
using DAS.Application.Models.Param;
using Microsoft.VisualBasic;
using DAS.Domain.Models.DasCongViec;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using DocumentFormat.OpenXml.Drawing.Diagrams;
using DAS.Infrastructure.ContextAccessors;
using Microsoft.Data.SqlClient;
using System.Data;
using DocumentFormat.OpenXml.Office.CustomUI;
using DocumentFormat.OpenXml.Spreadsheet;
using DAS.Application.Interfaces.DasKTNN;
using DAS.Domain.Models.DASKTNN;
using DAS.Infrastructure.Repositories.DASKTNN;
using Dapper;

namespace DAS.Application.Services.DasKTNN
{
    public class TableTestService : BaseMasterService, ITableTestService
    {
        private readonly IDasKTNNRepositoryWrapper _dasKTNNRepo;
        private readonly IDasRepositoryWrapper _dasRepo;
        private readonly IMapper _mapper;
        private readonly IDistributedCache _cache;
        private readonly ICategoryServices _categoryServices;
        private readonly IUserService _userService;
        private readonly ICacheManagementServices _cacheManagementServices;
        private readonly IUserPrincipalService _userPrincipalService;

        public TableTestService(IDasRepositoryWrapper dasRepository, IDasCongViecRepositoryWrapper dasCongViecRepository
            , IMapper mapper, ICategoryServices categoryServices, ICacheManagementServices cacheManagementServices
            , IDistributedCache cache, IUserService userService, IUserPrincipalService iUserPrincipalService,
            IDasKTNNRepositoryWrapper dasKTNNRepositoryWrapper, IDasDataDapperRepo dasDapperRepo) : base(dasRepository, dasDapperRepo)
        {
            _dasRepo = dasRepository;
            _mapper = mapper;
            _cache = cache;
            _categoryServices = categoryServices;
            _userService = userService;
            _cacheManagementServices = cacheManagementServices;
            _userPrincipalService = iUserPrincipalService;
            _dasKTNNRepo = dasKTNNRepositoryWrapper;
        }

        public IEnumerable<TableTest> GetAll()
        {
            //
            GetDataTable();
            CreateShema();
            return null;
        }
        /// <summary>
        /// Lấy dữ liệu với đạng dữ liệu động , lấy về dạng datatable
        /// </summary>
        private void GetDataTable()
        {
            DataTable _dt = new DataTable();
            _dt.Load(_dasDataDapperConn.ExecuteReader("select * from SYS.TableTest"));
            //  var testcolumn = _dt.Rows[0]["ID"]; // Cách thức lấy dữ liệu
        }
        /// <summary>
        /// Tạo Schema
        /// </summary>
        private void CreateShema()
        {
            var searchResult = _dasDataDapperConn.Execute("CALL prc_job_create_schema('TestSchema1','123456',NULL)");
        }
        private void CreateTable()
        {
           //
        }
    }
}
