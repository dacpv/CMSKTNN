﻿using AutoMapper;
using Dapper;
using DAS.Application.Constants;
using DAS.Application.Enums.DasKTNN;
using DAS.Application.Helper;
using DAS.Application.Interfaces;
using DAS.Application.Interfaces.DasKTNN;
using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels;
using DAS.Application.Models.ViewModels.DasKTNN;
using DAS.Domain.Enums;
using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Interfaces.DASNotify;
using DAS.Domain.Models.DAS;
using DAS.Domain.Models.DASKTNN;
using DAS.Infrastructure.ContextAccessors;
using DAS.Utility;
using DAS.Utility.BuildCondition;
using DAS.Utility.CustomClass;
using DAS.Utility.LogUtils;
using DocumentFormat.OpenXml.Office2010.Excel;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using static DAS.Application.Enums.DasKTNN.EnumTableInfo;

namespace DAS.Application.Services.DasKTNN
{
    public class Temp
    {
        public string IDRow { get; set; }
        public int ID { get; set; }
        public string Name { get; set; }
        public long ParentId { get; set; }
        public string ParentPath { get; set; }
    }
    public class InputInfoService : BaseMasterService, IInputInfoServices
    {
        #region Properties
        private readonly IMapper _mapper;
        private readonly ILoggerManager _logger;
        private readonly IUserPrincipalService _userPrincipalService;
        private readonly IDynamicDBService _dynamicDBService;
        private readonly IExcelServices _excelService;
        private readonly ITableInfoServices _tableInfoServices;
        private ICacheManagementServices _cacheManagementServices;

        #endregion

        #region Ctor
        public InputInfoService(IDasRepositoryWrapper dasRepository, IDasKTNNRepositoryWrapper dasKTNN
            , IMapper mapper
            , IExcelServices excel
            , ILoggerManager logger
            , IUserPrincipalService userPrincipalService
            , IDynamicDBService dynamicDBService
            , ICacheManagementServices cacheManagementServices, ITableInfoServices tableInfoServices) : base(dasRepository, dasKTNN)
        {
            _mapper = mapper;
            _logger = logger;
            _userPrincipalService = userPrincipalService;
            _dynamicDBService = dynamicDBService;
            _cacheManagementServices = cacheManagementServices;
            _excelService = excel;
            _tableInfoServices = tableInfoServices;
        }

        #endregion

        #region Gets  
        public async Task<VMInputInfo> Index(InputInfoCondition condition, int IDTable, Hashtable hashtable)
        {
            var orderby = "";
            if (!string.IsNullOrEmpty(condition.OrderBy))
            {
                orderby = $"{condition.OrderBy} {(string.IsNullOrEmpty(condition.OrderType) ? "ASC" : condition.OrderType)}";
            }
            else
                orderby = "ID DESC";
            var model = new VMInputInfo();
            UserData userData = await _cacheManagementServices.GetUserDataAndSetCache();
            model.tableInfo = _dasKTNNRepo.TableInfo.Get(IDTable) ?? new TableInfo() { Name = "Bảng chưa tạo" };
            model.schemaInfo = _dasKTNNRepo.SchemaInfo.Get(model.tableInfo.IDSchema) ?? new SchemaInfo();
            model.columnTables = await GetColumnTables(model.tableInfo.ID) ?? new List<ColumnTableInfo>();
            model.columnTableAlls = await GetAllColumnTables(model.tableInfo.ID) ?? new List<ColumnTableInfo>();
            model.Users = await _dasRepo.User.GetAllListAsync() ?? new List<User>();
            model.inputInfoCondition = condition;
            long total = 0;
            var lstCondParam = new List<CondParam>();
            var columnsearchs = model.columnTableAlls.Where(n => n.IsSearchable);
            foreach (var item in columnsearchs)
            {
                if (Utils.IsNotEmpty(Utils.GetString(hashtable, item.DbName)))
                {
                    switch (item.DataType)
                    {
                        case (int)ColumnDataType.DateTime:
                            lstCondParam.Add(new CondParam
                            {
                                FieldName = $"TO_DATE({item.DbName}, 'yyyy-MM-dd')",
                                Value = $"TO_DATE('{Utils.GetDate(hashtable, item.DbName).Value.ToString("yyyy-MM-dd")}', 'yyyy-MM-dd')",
                                Operator = CondOperator.EqualDate
                            });
                            break;
                        case (int)ColumnDataType.Decimal:
                        case (int)ColumnDataType.Integer:
                            lstCondParam.Add(new CondParam
                            {
                                FieldName = item.DbName,
                                Value = Utils.GetString(hashtable, item.DbName),
                                Operator = CondOperator.Equal
                            });
                            break;
                        case (int)ColumnDataType.Parent:
                        case (int)ColumnDataType.TableRef:
                        case (int)ColumnDataType.TableUser:
                            if (Utils.GetString(hashtable, item.DbName) != "0")
                            {
                                lstCondParam.Add(new CondParam
                                {
                                    FieldName = item.DbName,
                                    Value = Utils.GetString(hashtable, item.DbName),
                                    Operator = CondOperator.Equal
                                });
                            }
                            break;
                        case (int)ColumnDataType.String:
                            lstCondParam.Add(new CondParam
                            {
                                FieldName = item.DbName,
                                Value = Utils.GetString(hashtable, item.DbName),
                                Operator = CondOperator.Like
                            });
                            break;
                        default:
                            break;
                    }
                }

            }

            bool checkApproveData = model.tableInfo.IsRequireApprove;
            if (checkApproveData && condition.Status > -1)
            {
                lstCondParam.Add(new CondParam
                {
                    FieldName = "DADUYET",
                    Value = condition.Status,
                    Operator = CondOperator.Equal
                });
            }
            bool checkSyncData = model.tableInfo.IsTempTable;
            if (checkSyncData && condition.Sync > -1)
            {
                lstCondParam.Add(new CondParam
                {
                    FieldName = "DADONGBO",
                    Value = condition.Sync,
                    Operator = CondOperator.Equal
                });
            }
            var data = _dynamicDBService.SearchPage(lstCondParam, model.schemaInfo.Code, model.tableInfo.DbName, condition.PageIndex, condition.PageSize, ref total, checkApproveData, orderby: orderby);// string.Join(",", model.columnTables.Select(n => n.DbName).ToList()));
                                                                                                                                                                                                         // CHuyển dữ liệu datatable về dạng dữ liệu show
            var idTables = model.columnTables.Where(t => t.IDTableRef > 0).Select(t => t.IDTableRef).ToArray();
            var idColumns = model.columnTables.Where(t => t.IDColRef > 0).Select(t => t.IDColRef);
            var tableRefers = await GetAllTablesByIDs(idTables);
            var ColumnRefers = await GetAllColumnTablesByIDTable(idTables);
            var result = new DataTable();
            var dicColumns = new Dictionary<string, string>();
            var isContainID = false;
            result.Columns.Add("parentidtree_view", typeof(string));
            result.Columns.Add("DANHMUCCHA", typeof(string));
            result.Columns.Add("DANHMUCCHA_ID", typeof(string));
            result.Columns.Add("DADUYET", typeof(string));
            result.Columns.Add("DADONGBO", typeof(string));
            foreach (var item in model.columnTables)
            {
                if (item.DbName == "ID")
                {
                    isContainID = true;
                    result.Columns.Add(item.DbName, typeof(string));
                }

                //result.Columns.Add(item.Name, typeof(string));// Để sau DacPV TODO

                result.Columns.Add(item.DbName + "_ValueShow", typeof(string));


                switch (item.DataType)
                {
                    case (int)ColumnDataType.String:
                        dicColumns.Add(item.DbName, item.DbName);
                        break;
                    case (int)ColumnDataType.Integer:
                        dicColumns.Add(item.DbName, item.DbName);
                        break;
                    case (int)ColumnDataType.Decimal:
                        dicColumns.Add(item.DbName, item.DbName);
                        break;
                    case (int)ColumnDataType.DateTime:
                        dicColumns.Add(item.DbName, item.DbName);
                        break;
                    case (int)ColumnDataType.Boolean:
                        dicColumns.Add(item.DbName, item.DbName);
                        break;
                    case (int)ColumnDataType.TableRef:
                        var nameColumns = (ColumnRefers.First(t => t.ID == item.IDColRef) ?? new ColumnTableInfo()).DbName;
                        dicColumns.Add(item.DbName, nameColumns);
                        break;
                    case (int)ColumnDataType.TableUser:
                        dicColumns.Add(item.DbName, item.DbName);
                        break;
                    case (int)ColumnDataType.Parent:
                        var nameColumnParent = (ColumnRefers.First(t => t.ID == item.IDColRef) ?? new ColumnTableInfo()).DbName;
                        dicColumns.Add(item.DbName, item.DbName);
                        dicColumns.Add(item.DbName + "_Text", nameColumnParent);
                        result.Columns.Add(item.DbName + "_Text", typeof(string));
                        break;
                    default:
                        break;
                }
            }
            // Check trường hợp bảng hiện thị có hiển thị ID hay không
            if (!isContainID)
            {
                result.Columns.Add("ID", typeof(string));
                dicColumns.Add("ID", "ID");
            }
            if (checkApproveData)
            {
                dicColumns.Add("DADUYET", "DADUYET");
            }
            if (checkSyncData)
            {
                dicColumns.Add("DADONGBO", "DADONGBO");
            }
            for (int i = 0; i < data.Rows.Count; i++)
            {
                var row = result.NewRow();
                var datarow = data.Rows[i];
                if (!isContainID)
                {
                    row["ID"] = datarow["ID"].ToString();
                }

                if (checkApproveData)
                {
                    row[SystemField.DaDuyet.ToString().ToUpper()] = (datarow[SystemField.DaDuyet.ToString().ToUpper()] ?? true).ToString();
                }
                if (checkSyncData)
                {
                    row[CustomField.DaDongBo.ToString().ToUpper()] = (datarow[CustomField.DaDongBo.ToString().ToUpper()] ?? true).ToString();
                }
                var dictStatus = new Dictionary<int, string>
                {
                    { (int)EnumCommon.Status.Active, "Xuất bản" },
                    { (int)EnumCommon.Status.InActive, "Dừng xuất bản" }
                };

                foreach (var item in model.columnTables)
                {
                    if (item.DbName == "ID")
                    {
                        row["ID"] = datarow["ID"].ToString();
                    }
                    var value = (datarow[item.DbName] ?? "").ToString();
                    if (item.DbName == SystemField.TrangThai.ToString())
                    {
                        value = dictStatus.GetValueOrDefault(Utils.GetInt(datarow[item.DbName].ToString()));
                    }
                    else
                        switch (item.DataType)
                        {
                            case (int)ColumnDataType.String:
                                value = datarow[item.DbName].ToString();
                                break;
                            case (int)ColumnDataType.Integer:
                                value = datarow[item.DbName].ToString();
                                break;
                            case (int)ColumnDataType.Decimal:
                                value = datarow[item.DbName].ToString();
                                break;
                            case (int)ColumnDataType.DateTime:
                                var valdt = datarow[item.DbName];
                                value = Utils.DateToString(valdt == DBNull.Value ? null : (DateTime?)valdt, "dd/MM/yyyy HH:mm");
                                //Utils.DateToString((DateTime)datarow[item.DbName], "dd/MM/yyyy HH:mm");
                                break;
                            case (int)ColumnDataType.Boolean:
                                value = (datarow[item.DbName] ?? true).ToString();
                                break;
                            case (int)ColumnDataType.TableRef:
                                var column = dicColumns[item.DbName];
                                value = Utils.GetValueDataLookup(datarow[item.DbName + "_DataLookUp"].ToString(), column);
                                break;
                            case (int)ColumnDataType.TableUser:
                                value = (model.Users.FirstOrDefault(n => n.ID.ToString() == value) ?? new DAS.Domain.Models.DAS.User()).Name;
                                break;
                            case (int)ColumnDataType.Parent:
                                var columnPr = dicColumns[item.DbName + "_Text"];
                                value = Utils.GetValueDataLookup(datarow[item.DbName + "_DataLookUp"].ToString(), columnPr);
                                row[item.DbName + "_Text"] = value;
                                break;
                            default:
                                break;
                        }

                    row[item.DbName + "_ValueShow"] = value;
                }
                // cột mặc định
                if (data.Columns.Contains("DANHMUCCHA"))
                {
                    row["DANHMUCCHA"] = datarow["DANHMUCCHA"].ToString();
                    row["DANHMUCCHA_ID"] = datarow["DANHMUCCHA_ID"].ToString();
                }
                result.Rows.Add(row);
            }
            //_logger.LogError(Utils.Serialize(result));
            model.datas = result;
            model.datapage = new PaginatedList<DataTable>(result, (int)total, condition.PageIndex, condition.PageSize);
            var allcolumns = await GetAllColumnTables(model.tableInfo.ID) ?? new List<ColumnTableInfo>();
            model.IsTach = allcolumns.Count(n => n.DbName == DefaultField.HieuLucTu.ToString() || n.DbName == DefaultField.HieuLucDen.ToString()) == 2;

            return model;
        }

        public async Task<VMInputInfo> GetListParent(InputInfoCondition condition, int IDTable, int idRecord = 0)
        {
            var model = new VMInputInfo();
            model.tableInfo = _dasKTNNRepo.TableInfo.Get(IDTable) ?? new TableInfo() { Name = "Bảng chưa tạo" };
            model.schemaInfo = _dasKTNNRepo.SchemaInfo.Get(model.tableInfo.IDSchema) ?? new SchemaInfo();
            model.columnTables = await GetAllColumnTable(model.tableInfo.ID) ?? new List<ColumnTableInfo>();
            model.Users = await _dasRepo.User.GetAllListAsync() ?? new List<User>();
            var data = _dynamicDBService.GetListShowTable(model.schemaInfo.Code, model.tableInfo.DbName);
            var idTables = model.columnTables.Where(t => t.IDTableRef > 0).Select(t => t.IDTableRef).ToArray();
            var idColumns = model.columnTables.Where(t => t.IDColRef > 0).Select(t => t.IDColRef);
            var tableRefers = await GetAllTablesByIDs(idTables);
            var ColumnRefers = await GetAllColumnTablesByIDTable(idTables);
            var result = new DataTable();
            var dicColumns = new Dictionary<string, string>();
            var isContainID = false;
            foreach (var item in model.columnTables)
            {
                if (item.DbName == "ID")
                {
                    isContainID = true;
                    result.Columns.Add(item.DbName, typeof(string));
                }
                result.Columns.Add(item.DbName + "_ValueShow", typeof(string));
                switch (item.DataType)
                {
                    case (int)ColumnDataType.String:
                        dicColumns.Add(item.DbName, item.DbName);
                        break;
                    case (int)ColumnDataType.TableRef:
                        var nameColumns = (ColumnRefers.First(t => t.ID == item.IDColRef) ?? new ColumnTableInfo()).DbName;
                        dicColumns.Add(item.DbName, nameColumns);
                        break;
                    case (int)ColumnDataType.Parent:
                        dicColumns.Add(item.DbName, item.DbName);
                        break;
                    default:
                        break;
                }
            }
            if (!isContainID)
            {
                result.Columns.Add("ID", typeof(string));
                dicColumns.Add("ID", "ID");
            }
            for (int i = 0; i < data.Rows.Count; i++)
            {
                var row = result.NewRow();
                var datarow = data.Rows[i];
                if (!isContainID)
                {
                    row["ID"] = datarow["ID"].ToString();
                }
                row["Ten"] = datarow["Ten"].ToString();
                foreach (var item in model.columnTables)
                {
                    if (item.DbName == "ID")
                    {
                        row["ID"] = datarow["ID"].ToString();
                    }
                    var value = (datarow[item.DbName] ?? "").ToString();
                    switch (item.DataType)
                    {
                        case (int)ColumnDataType.Parent:
                            dicColumns.Add(item.DbName, item.DbName);
                            break;
                        default:
                            break;
                    }

                    row[item.DbName + "_ValueShow"] = value;
                }
                result.Rows.Add(row);
            }
            model.datas = result;
            return model;
        }

        private DataTable RenderByTree(DataTable dt)
        {
            //parentidtree_view

            var result = new DataTable();
            //TODO xử lý tạm
            if (dt.Columns.Contains("DANHMUCCHA"))
            {
                DataTable rs = dt.Copy();
                rs.Clear();
                var temps = new List<Temp>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    temps.Add(new Temp
                    {
                        IDRow = dt.Rows[i]["ID"].ToString(),
                        ID = Utils.GetInt(dt.Rows[i]["ID"].ToString()),
                        Name = "",
                        ParentId = Utils.GetInt(string.IsNullOrEmpty(dt.Rows[i]["DANHMUCCHA"].ToString()) ? "0" : dt.Rows[i]["DANHMUCCHA"].ToString()),
                        ParentPath = (dt.Rows[i]["DANHMUCCHA_ID"].ToString())
                    });
                }
                var treeModels = Utils.RenderTree(temps.Select(n => new TreeModel<Temp>
                {
                    ID = n.ID,
                    Name = n.Name,
                    Parent = n.ParentId,
                    ParentPath = n.ParentPath,
                    Item = n
                }).ToList(), null);
                var order = treeModels.Select(t => t.Item);
                foreach (var item in order)
                {
                    //var results = from myRow in dt.AsEnumerable()
                    //              where myRow.Field<int>("ID") == item.ID
                    //              select myRow;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        var id = dt.Rows[i]["ID"].ToString();
                        if (id == item.ID.ToString())
                        {
                            DataRow drNew = rs.NewRow();
                            drNew.ItemArray = dt.Rows[i].ItemArray;
                            drNew["parentidtree_view"] = item.ParentId;
                            rs.Rows.Add(drNew);
                        }
                    }
                }
                return rs;
            }
            else
                return dt;
        }
        #endregion

        #region Create  
        public async Task<VMInputInfo> Create(int IDTable)
        {
            var model = new VMInputInfo();
            model.Action = 1;
            var schemas = await GetDictSchema();
            model.DlSchemaRef = schemas.Select(n => new SelectListItem
            {
                Value = n.Key.ToString(),
                Text = n.Value,
            });
            var tables = await GetTables();
            model.Tables = tables.Where(n => n.ID != IDTable);
            model.tableInfo = _dasKTNNRepo.TableInfo.Get(IDTable) ?? new TableInfo() { Name = "Bảng chưa tạo" };
            model.schemaInfo = _dasKTNNRepo.SchemaInfo.Get(model.tableInfo.IDSchema) ?? new SchemaInfo();
            model.columnTables = await GetAllColumnTables(model.tableInfo.ID) ?? new List<ColumnTableInfo>();
            model.columnTables = model.columnTables.Where(n => !n.IsSystem).ToList();
            //var dataDropdows = _dynamicDBService.GetListShowTable(model.schemaInfo.Code, model.tableInfo.DbName);
            //model.DictTable = new Dictionary<int, string>();
            //for (int i = 0; i < dataDropdows.Rows.Count; i++)
            //{
            //    var datarow = dataDropdows.Rows[i];
            //    var id = (int)dataDropdows.Rows[i]["ID"];
            //    var name = dataDropdows.Rows[i]["ID"].ToString();
            //    if (datarow.Table.Columns.Contains("Name"))
            //    {
            //        name = dataDropdows.Rows[i]["Name"].ToString();
            //    }
            //    model.DictTable.Add(id, name);
            //}


            //NamNP: đã call dùng Select2
            //Lấy thông tin cho các trường tham chiếu và trường parent
            //var datasparent_refer = new List<TableOptionSelect>();
            //var colsparent_refer = model.columnTables.Where(t => t.DataType == (int)ColumnDataType.Parent || t.DataType == (int)ColumnDataType.TableRef).ToList() ?? new List<ColumnTableInfo>();
            //if (Utils.IsNotEmpty(colsparent_refer))
            //{
            //    var idchemas = colsparent_refer.Select(t => t.IDSchemaRef).Distinct().ToArray();
            //    var idtables = colsparent_refer.Select(t => t.IDTableRef).Distinct().ToArray();
            //    var tableRefer = await GetAllTablesByIDs(idtables) ?? new List<TableInfo>();
            //    var schemaRefer = await GetSchemas(idchemas) ?? new List<SchemaInfo>();
            //    foreach (var item in colsparent_refer)
            //    {
            //        var nameSchemaRefer = (schemaRefer.FirstOrDefault(t => t.ID == item.IDSchemaRef) ?? new SchemaInfo()).Code;
            //        var nameTableRefer = (tableRefer.FirstOrDefault(t => t.ID == item.IDTableRef) ?? new TableInfo()).DbName;
            //        var dataparent_refer = await GetDataTableRefer_Parent(nameSchemaRefer, nameTableRefer, item);
            //        if (Utils.IsNotEmpty(dataparent_refer))
            //            datasparent_refer.Add(dataparent_refer);
            //    }
            //}
            //model.DataParent_Refer = datasparent_refer;


            model.dataFirst = new DataTable();
            return model;
        }

        public async Task<IEnumerable<TableInfo>> GetTables(params int[] ids)
        {
            return await _dasKTNNRepo.TableInfo.GetAllListAsync(n => (ids.IsEmpty() || ids.Contains(n.ID)) && n.Status == (int)EnumCommon.Status.Active);
        }

        private async Task<Dictionary<int, string>> GetDictSchema()
        {
            var rs = await _dasKTNNRepo.SchemaInfo.GetAllListAsync();
            return rs.OrderBy(n => n.Name).ToDictionary(x => x.ID, x => x.Name);
        }
        private async Task<TableOptionSelect> GetDataTableRefer_Parent(string Schema, string table, ColumnTableInfo clinfo)
        {
            var result = new TableOptionSelect();
            var datas = new List<OptionSelect>();
            //lấy ra dữ liệu của bảng danh mục tham chiếu
            var dataDropdows = _dynamicDBService.GetListShowTable(Schema, table);
            //lấy ra thông tin cột của bảng sẽ refer tới
            var tableRefers = await GetAllTablesByIDs(clinfo.IDTableRef) ?? new List<TableInfo>();
            var ColumnRefers = await GetAllColumnTablesByIDTable(clinfo.IDTableRef) ?? new List<ColumnTableInfo>();
            var columnreferValue = ColumnRefers.FirstOrDefault(t => t.ID == clinfo.IDColRef);
            var columnreferText = ColumnRefers.FirstOrDefault(t => t.ID == clinfo.IDColTextRef);
            if (Utils.IsEmpty(columnreferValue) || Utils.IsEmpty(columnreferText))
                return null;
            result.DBName = clinfo.DbName;
            result.NameColumnText = columnreferText.DbName;
            result.NameColumnValue = columnreferValue.DbName;
            if (dataDropdows != null && dataDropdows.Rows.Count > 0)
            {
                for (int i = 0; i < dataDropdows.Rows.Count; i++)
                {
                    var row = dataDropdows.Rows[i];
                    var ops = new OptionSelect();
                    ops.Value = (row[result.NameColumnValue] ?? "").ToString();
                    ops.Text = (row[result.NameColumnText] ?? "").ToString();
                    datas.Add(ops);
                }
            }
            result.Data = datas;
            return result;
        }

        public async Task<VMInputInfo> Save(Hashtable data)
        {
            try
            {
                return await SaveRecord(data);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                throw;
            }
        }

        private async Task<VMInputInfo> SaveRecord(Hashtable data)
        {
            if (Utils.GetString(data, DefaultField.KieuPhatSinh.ToString()).IsEmpty())
            {
                data[DefaultField.KieuPhatSinh.ToString()] = CreateTypeConst.MacDinh.ToString();
            }

            var qrColumnName = new List<string>();
            var ID = Utils.GetInt(data, "IDTable");
            var model = new VMInputInfo();
            model.tableInfo = _dasKTNNRepo.TableInfo.Get(ID) ?? new TableInfo() { Name = "Bảng chưa tạo" };
            if (model.tableInfo.ID <= 0)
            {
                model.IsSuccess = false;
                model.Message = "Bảng không tồn tại";
                return model;
            }
            model.schemaInfo = _dasKTNNRepo.SchemaInfo.Get(model.tableInfo.IDSchema) ?? new SchemaInfo();
            var columnbyAlls = await GetAllColumnTables(model.tableInfo.ID);
            //columnbyAlls = columnbyAlls.Where(n => n.IsIdentity == false).ToList();
            model.columnTables = columnbyAlls.Where(n => n.IsSystem == false).ToList();
            var dicValue = new Dictionary<string, string>();
            //lấy danh sách column refer và column path
            var idColumnRefer = columnbyAlls.Select(t => t.IDColRef).Distinct().ToList() ?? new List<int>();
            var idColumnPath = columnbyAlls.Select(t => t.ID).Distinct().ToArray();
            var ColumnsPath = await GetAllColumnPathByIDColumnRefer(idColumnPath) ?? new List<ColumnPathInfo>();
            if (Utils.IsNotEmpty(ColumnsPath))
            {
                idColumnRefer.AddRange(ColumnsPath.Select(t => t.IDColumnPath));
            }
            var ColumnsRefer = await GetAllColumnTableByID(idColumnRefer.ToArray()) ?? new List<ColumnTableInfo>();

            var idchemas = columnbyAlls.Select(t => t.IDSchemaRef).Distinct().ToArray();
            var idtables = columnbyAlls.Select(t => t.IDTableRef).Distinct().ToArray();
            var tablseRefer = await GetAllTablesByIDs(idtables) ?? new List<TableInfo>();
            var schemasRefer = await GetSchemas(idchemas) ?? new List<SchemaInfo>();

            //Set identity
            var identityCol = columnbyAlls.FirstOrDefault(n => n.IsIdentity && !n.IsSystem && n.IsDefault);
            if (identityCol != null)
            {
                var isIdentity = Utils.GetBool(data, $"IsIdentity{identityCol.ID}");
                if (isIdentity)
                {
                    var isAllowIdentity = CheckedColAllowIdentity(model.schemaInfo, model.tableInfo, identityCol);
                    if (!isAllowIdentity)
                    {
                        model.IsSuccess = false;
                        model.Message = $"Bảng dữ liệu không thể tự động sinh mã, vui lòng nhập '{identityCol.Name}'";
                        return model;
                    }
                    //Set là giá trị để trigger SQL nhận diện set tự tăng
                    data[identityCol.DbName] = KTNNConst.IdentityValue;
                }
            }

            //Set tự tăng cho các trường thêm mới
            var identityCols = columnbyAlls.Where(n => n.IsIdentity && !n.IsSystem && !n.IsDefault);
            if (identityCols.IsNotEmpty())
            {
                foreach (var item in identityCols)
                {
                    //Set là giá trị để trigger SQL nhận diện set tự tăng
                    data[item.DbName] = DynamicDBHelper.GetIdentityValue(item);
                }
            }

            var values = new List<string>();
            foreach (var item in model.columnTables)
            {
                var value = Utils.GetString(data, item.DbName);
                if (item.IsRequired && Utils.IsEmpty(value))
                {
                    model.IsSuccess = false;
                    model.Message = $"{item.Name} không được trống";
                    return model;
                }
            }


            //Check trùng khóa chinh (ko phai ID)
            var uniqueCols = columnbyAlls.Where(n => n.IsPrimaryKey && n.DbName != SystemField.ID.ToString());
            if (uniqueCols.IsNotEmpty())
            {
                foreach (var uniqueCol in uniqueCols)
                {
                    var isExist = _dynamicDBService.IsExists(model.schemaInfo.Code, model.tableInfo.DbName, new CondParam
                    {
                        FieldName = uniqueCol.DbName,
                        Value = Utils.GetString(data, uniqueCol.DbName)
                    });
                    if (isExist)
                    {
                        model.IsSuccess = false;
                        model.Message = $"{uniqueCol.Name} đã tồn tại trên hệ thống";
                        return model;
                    }
                }
            }


            foreach (var item in columnbyAlls)
            {
                if (item.IsSubColumn)
                    continue;

                if (item.IsSystem && item.IsIdentity)
                    continue;


                switch (item.DataType)
                {
                    case (int)ColumnDataType.DateTime:
                        if (item.IsSystem)
                        {
                            qrColumnName.Add(item.DbName);
                            values.Add(DateTime.Now.ToOracleStringDatetime());
                            dicValue.Add(item.DbName, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                        }
                        else
                        {
                            qrColumnName.Add(item.DbName);
                            if (Utils.IsDate(Utils.GetDate(data, item.DbName)))
                            {
                                dicValue.Add(item.DbName, Utils.GetDate(data, item.DbName).Value.ToString("yyyy-MM-dd HH:mm:ss"));
                                values.Add(Utils.GetDate(data, item.DbName).ToOracleStringDatetime());
                            }
                            else
                                //values.Add($"TO_DATE('NULL', 'yyyy-MM-dd hh24:mi:ss')");
                                values.Add("NULL");
                        }
                        break;
                    case (int)ColumnDataType.TableUser:
                        qrColumnName.Add(item.DbName);
                        values.Add($"'{_userPrincipalService.UserId}'");
                        dicValue.Add(item.DbName, _userPrincipalService.UserId.ToString());
                        break;
                    case (int)ColumnDataType.Decimal:
                    case (int)ColumnDataType.Integer:
                        qrColumnName.Add(item.DbName);
                        values.Add($"'{Utils.GetInt(data, item.DbName)}'");
                        dicValue.Add(item.DbName, Utils.GetString(data, item.DbName));
                        break;
                    case (int)ColumnDataType.Boolean:
                        qrColumnName.Add(item.DbName);
                        values.Add($"'{Utils.GetInt(data, item.DbName)}'");
                        dicValue.Add(item.DbName, Utils.GetString(data, item.DbName));
                        break;
                    case (int)ColumnDataType.Parent:
                        //lấy giá trị của bản ghi cha
                        var columnRefer = ColumnsRefer.FirstOrDefault(t => t.ID == item.IDColRef);
                        var tableRefer = tablseRefer.FirstOrDefault(t => t.ID == item.IDTableRef);
                        var schemaRefer = schemasRefer.FirstOrDefault(t => t.ID == item.IDSchemaRef);
                        var dt = _dynamicDBService.Search(new List<CondParam> {
                            new CondParam
                            {
                                FieldName=columnRefer.DbName,
                                Operator=CondOperator.Equal,
                                Value=Utils.GetString(data, item.DbName)
                            }
                            }, schemaRefer.Code, tableRefer.DbName);

                        //Giá trị của chính parent
                        qrColumnName.Add(item.DbName);
                        values.Add($"'{Utils.GetString(data, item.DbName)}'");
                        var level = 0;
                        var json = "";
                        var PathParent = "0";
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            var Parent = dt.Rows[0];
                            level = Utils.GetInt(Parent[item.DbName + "_LEVEL"]?.ToString()) + 1;
                            PathParent = Parent[item.DbName + "_" + columnRefer.DbName].ToString() + "|" + Parent[columnRefer.DbName];
                            PathParent = PathParent.TrimStart('|');
                            json = Utils.ConvertDataTabletoString(dt);
                            if (json.IsNotEmpty())
                            {
                                json = json.Replace("'", "''");
                            }
                            //Đường dẫn
                            var columnspathParent = ColumnsPath.Where(t => t.IDColumnTableInfo == item.ID).ToList();
                            if (Utils.IsNotEmpty(columnspathParent))
                            {
                                foreach (var dd in columnspathParent)
                                {
                                    var columnPath = ColumnsRefer.FirstOrDefault(t => t.ID == dd.IDColumnPath);
                                    if (columnPath != null)
                                    {
                                        qrColumnName.Add(dd.PathName);
                                        //if (columnPath.IsIdentity)
                                        //{
                                        //    //TH tự tăng thì path là: Parents|ParentID
                                        //    var cPath = Parent[columnPath.DbName]?.ToString().TrimStart("N'").TrimEnd("'");
                                        //    var vlPath = (Parent[dd.PathName].ToString() + "|" + cPath).TrimStart('|');
                                        //    //Bỏ N' & '
                                        //    vlPath = vlPath.TrimStart("N'").TrimEnd("'");
                                        //    values.Add($"'{vlPath}'");
                                        //}
                                        //else
                                        //{
                                        var cPath = Utils.GetString(data, columnPath.DbName).TrimStart("N'").TrimEnd("'");    //Bỏ N' & '
                                        var vlPath = (Parent[dd.PathName].ToString() + cPath).TrimStart('|');
                                        vlPath = vlPath.TrimStart("N'").TrimEnd("'");  //Bỏ N' & '
                                        vlPath = $"|{vlPath}|";
                                        values.Add($"'{vlPath.Replace("'", "''")}'");
                                        //}
                                    }
                                }
                            }
                            qrColumnName.Add(item.DbName + "_ID");
                            values.Add($"'{Parent["ID"]}'");
                        }
                        else
                        {
                            //Đường dẫn
                            var columnspathParent = ColumnsPath.Where(t => t.IDColumnTableInfo == item.ID).ToList();
                            if (Utils.IsNotEmpty(columnspathParent))
                            {
                                foreach (var dd in columnspathParent)
                                {
                                    var columnPath = ColumnsRefer.FirstOrDefault(t => t.ID == dd.IDColumnPath);
                                    qrColumnName.Add(dd.PathName);
                                    //if (columnPath.IsIdentity)
                                    //{
                                    //    if (columnPath.DataType == (int)ColumnDataType.String)
                                    //    {
                                    //        values.Add($"''");
                                    //    }
                                    //    else
                                    //    {
                                    //        values.Add($"'0'");
                                    //    }
                                    //}
                                    //else
                                    //{
                                    var vlPath = (Utils.GetString(data, columnPath.DbName)).TrimStart('|');
                                    vlPath = vlPath.TrimStart("N'").TrimEnd("'"); //Bỏ N' & '
                                    vlPath = $"|{vlPath}|";
                                    values.Add($"'{vlPath}'");
                                    //}
                                }
                            }

                            // Path
                            qrColumnName.Add(item.DbName + "_ID");
                            values.Add($"'0'");
                        }
                        // Level
                        qrColumnName.Add(item.DbName + "_LEVEL");
                        values.Add($"'{level}'");

                        //Lookup
                        qrColumnName.Add(item.DbName + "_DATALOOKUP");
                        values.Add($"'{json}'");
                        break;
                    case (int)ColumnDataType.TableRef:
                        //lấy giá trị của bảng tham chiếu
                        var columnRef = ColumnsRefer.FirstOrDefault(t => t.ID == item.IDColRef);
                        var tableRêf = tablseRefer.FirstOrDefault(t => t.ID == item.IDTableRef);
                        var schemaRef = schemasRefer.FirstOrDefault(t => t.ID == item.IDSchemaRef);
                        var dtRef = _dynamicDBService.Search(new List<CondParam> {
                            new CondParam
                            {
                                FieldName=columnRef.DbName,
                                Operator=CondOperator.Equal,
                                Value=Utils.GetString(data, item.DbName)
                            }
                            }, schemaRef.Code, tableRêf.DbName);
                        // Giá trị tham chiếu
                        qrColumnName.Add(item.DbName);
                        values.Add($"N'{Utils.GetString(data, item.DbName)}'");
                        // giá trị ID
                        var id = dtRef.Rows.Count > 0 ? dtRef.Rows[0]["ID"].ToString() : "0";
                        qrColumnName.Add(item.DbName + "_ID");
                        values.Add($"N'{id}'");
                        //Lookup
                        json = Utils.ConvertDataTabletoString(dtRef);
                        if (json.IsNotEmpty())
                        {
                            json = json.Replace("'", "''");
                        }
                        qrColumnName.Add(item.DbName + "_DATALOOKUP");
                        values.Add($"'{json}'");
                        break;
                    case (int)ColumnDataType.TableLink:
                        break;
                    default:
                        qrColumnName.Add(item.DbName);
                        values.Add($"N'{Utils.GetString(data, item.DbName)}'");
                        break;
                }
            }


            if (model.tableInfo.IsRequireApprove)
            {
                qrColumnName.Add("DADUYET");
                values.Add($"'{Status.DaDuyet}'");
                qrColumnName.Add("NGUOIDUYET");
                values.Add($"'{_userPrincipalService.UserId}'");
                qrColumnName.Add("NGAYDUYET");
                values.Add(DateTime.Now.ToOracleStringDatetime());
            }
            var column = string.Join(",", qrColumnName);
            var valuedata = string.Join(",", values);

            var recordData = new Dictionary<string, string>();
            var index = 0;
            foreach (var colName in qrColumnName)
            {
                if (!recordData.ContainsKey(colName))
                {
                    recordData.Add(colName, values.ElementAtOrDefault(index) ?? "''");
                }
                index++;
            }

            var idRecordsRef = Utils.GetInts(data, "IDRecordRef");

            if (idRecordsRef.IsNotEmpty() && idRecordsRef.Length != idRecordsRef.Distinct().Count())
            {
                model.IsSuccess = false;
                model.Message = $"Dữ liệu liên kết không được trùng nhau";
                return model;
            }

            var sql = $"INSERT INTO {model.schemaInfo.Code}.{model.tableInfo.DbName} ({column}) VALUES ({valuedata}) returning ID into :Id";


            //_logger.LogDebug("sql" + Utils.Serialize(sql));
            var param = new DynamicParameters();
            param.Add(name: "Id", dbType: DbType.Int32, direction: ParameterDirection.Output);
            var Idinserted = 0;
            if (_dynamicDBService.InsertData(sql, param, ref Idinserted))
            {
                var nameTableLienKet = $"{model.schemaInfo.Code}{KTNNConst.TableLinkNameExt}";
                SaveTableLink(data, model, Idinserted, nameTableLienKet);
                model.DicRecordData = recordData;
                model.IDInserted = Idinserted;
                model.IsSuccess = true;
                model.Message = $"Tạo mới dữ liệu vào bảng {model.tableInfo.Name} thành công";
                return model;
            }
            else
            {
                model.IsSuccess = false;
                model.Message = $"Tạo mới dữ liệu vào bảng {model.tableInfo.Name} không thành công";
                return model;
            }
        }
        public async Task<VMInputInfo> SaveMerge(Hashtable data)
        {
            try
            {
                var model = await SaveRecord(data);
                if (model.IsSuccess)
                    HopNhatDuLieu(data, model, model.DicRecordData, model.IDInserted);
                return model;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                throw;
            }
        }
        #endregion

        #region Update  
        public async Task<VMInputInfo> Update(int IDTable, int ID)
        {
            var model = new VMInputInfo();
            model.Action = 2;
            var schemas = await GetDictSchema();
            model.DlSchemaRef = schemas.Select(n => new SelectListItem
            {
                Value = n.Key.ToString(),
                Text = n.Value,
            });
            var tables = await GetTables();
            model.Tables = tables.Where(n => n.ID != IDTable);
            model.tableInfo = _dasKTNNRepo.TableInfo.Get(IDTable) ?? new TableInfo() { Name = "Bảng chưa tạo" };
            model.schemaInfo = _dasKTNNRepo.SchemaInfo.Get(model.tableInfo.IDSchema) ?? new SchemaInfo();
            model.columnTables = await GetAllColumnTables(model.tableInfo.ID) ?? new List<ColumnTableInfo>();
            model.columnTables = model.columnTables.Where(n => !n.IsSystem).ToList();
            model.dataFirst = _dynamicDBService.GetFirstById(model.schemaInfo.Code, model.tableInfo.DbName, ID);
            //var dataDropdows = _dynamicDBService.GetListShowTable(model.schemaInfo.Code, model.tableInfo.DbName);

            model.tableLinks = GetRecordLinkTable(model.schemaInfo, model.tableInfo, ID);

            //Lấy thông tin cho các trường tham chiếu và trường parent
            //var datasparent_refer = new List<TableOptionSelect>();
            //var colsparent_refer = model.columnTables.Where(t => t.DataType == (int)ColumnDataType.Parent || t.DataType == (int)ColumnDataType.TableRef).ToList() ?? new List<ColumnTableInfo>();
            //if (Utils.IsNotEmpty(colsparent_refer))
            //{
            //    var idchemas = colsparent_refer.Select(t => t.IDSchemaRef).Distinct().ToArray();
            //    var idtables = colsparent_refer.Select(t => t.IDTableRef).Distinct().ToArray();
            //    var tableRefer = await GetAllTablesByIDs(idtables) ?? new List<TableInfo>();
            //    var schemaRefer = await GetSchemas(idchemas) ?? new List<SchemaInfo>();
            //    foreach (var item in colsparent_refer)
            //    {
            //        var nameSchemaRefer = (schemaRefer.FirstOrDefault(t => t.ID == item.IDSchemaRef) ?? new SchemaInfo()).Code;
            //        var nameTableRefer = (tableRefer.FirstOrDefault(t => t.ID == item.IDTableRef) ?? new TableInfo()).DbName;
            //        var dataparent_refer = await GetDataTableRefer_Parent(nameSchemaRefer, nameTableRefer, item);
            //        if (Utils.IsNotEmpty(dataparent_refer))
            //            datasparent_refer.Add(dataparent_refer);
            //    }
            //}
            //model.DataParent_Refer = datasparent_refer;
            return model;
        }

        public async Task<VMInputInfo> ApproveData(Hashtable data)
        {
            var IDTable = Utils.GetInt(data, "IDTable");
            var ID = Utils.GetInt(data, "ID");
            var model = new VMInputInfo();
            model.tableInfo = _dasKTNNRepo.TableInfo.Get(IDTable) ?? new TableInfo() { Name = "Bảng chưa tạo" };
            if (model.tableInfo.ID <= 0)
            {
                model.IsSuccess = false;
                model.Message = "Bảng không tồn tại";
                return model;
            }
            model.schemaInfo = _dasKTNNRepo.SchemaInfo.Get(model.tableInfo.IDSchema) ?? new SchemaInfo();
            var values = new List<string>();
            if (model.tableInfo.IsRequireApprove)
            {
                values.Add($"DADUYET ='{(int)Status.DaDuyet}'");
                values.Add($"NGUOIDUYET ='{_userPrincipalService.UserId}'");
                values.Add($"NGAYDUYET =TO_DATE('{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}', 'yyyy-MM-dd hh24:mi:ss')");
            }

            var valuedata = string.Join(",", values);
            var sql = $"UPDATE  {model.schemaInfo.Code}.{model.tableInfo.DbName} SET {valuedata} where ID={ID}";
            if (_dynamicDBService.UpdateData(sql, null))
            {
                model.IsSuccess = true;
                model.Message = $"Duyệt dữ liệu vào bảng {model.tableInfo.Name} thành công";
                return model;
            }
            else
            {
                model.IsSuccess = false;
                model.Message = $"Cập nhật dữ liệu vào bảng {model.tableInfo.Name} không thành công";
                return model;
            }
        }

        public async Task<VMInputInfo> Update(Hashtable data)
        {
            var IDTable = Utils.GetInt(data, "IDTable");
            var qrColumnName = new List<string>();
            var ID = Utils.GetInt(data, "ID");
            var model = new VMInputInfo();
            model.tableInfo = _dasKTNNRepo.TableInfo.Get(IDTable) ?? new TableInfo() { Name = "Bảng chưa tạo" };
            if (model.tableInfo.ID <= 0)
            {
                model.IsSuccess = false;
                model.Message = "Bảng không tồn tại";
                return model;
            }
            model.schemaInfo = _dasKTNNRepo.SchemaInfo.Get(model.tableInfo.IDSchema) ?? new SchemaInfo();
            var columnbyAlls = await GetAllColumnTables(model.tableInfo.ID);
            //columnbyAlls = columnbyAlls.Where(n => n.IsIdentity == false).ToList();
            model.columnTables = columnbyAlls.Where(n => n.IsSystem == false).ToList();
            //lấy danh sách column refer và column path
            var idColumnRefer = columnbyAlls.Select(t => t.IDColRef).Distinct().ToList() ?? new List<int>();
            var idColumnPath = columnbyAlls.Select(t => t.ID).Distinct().ToArray();
            var ColumnsPath = await GetAllColumnPathByIDColumnRefer(idColumnPath) ?? new List<ColumnPathInfo>();
            if (Utils.IsNotEmpty(ColumnsPath))
            {
                idColumnRefer.AddRange(ColumnsPath.Select(t => t.IDColumnPath));
            }
            var ColumnsRefer = await GetAllColumnTableByID(idColumnRefer.ToArray()) ?? new List<ColumnTableInfo>();

            var idchemas = columnbyAlls.Select(t => t.IDSchemaRef).Distinct().ToArray();
            var idtables = columnbyAlls.Select(t => t.IDTableRef).Distinct().ToArray();
            var tablseRefer = await GetAllTablesByIDs(idtables) ?? new List<TableInfo>();
            var schemasRefer = await GetSchemas(idchemas) ?? new List<SchemaInfo>();
            var values = new List<string>();
            foreach (var item in model.columnTables)
            {
                var value = Utils.GetString(data, item.DbName);
                if (item.IsRequired && Utils.IsEmpty(value))
                {
                    model.IsSuccess = false;
                    model.Message = $"{item.Name} không được trống";
                    return model;
                }
            }

            //Check trùng khóa chinh (ko phai ID)
            var uniqueCols = columnbyAlls.Where(n => n.IsPrimaryKey && n.DbName != SystemField.ID.ToString());
            if (uniqueCols.IsNotEmpty())
            {
                foreach (var uniqueCol in uniqueCols)
                {
                    var isExist = _dynamicDBService.IsExists(model.schemaInfo.Code, model.tableInfo.DbName, new CondParam
                    {
                        FieldName = uniqueCol.DbName,
                        Value = Utils.GetString(data, uniqueCol.DbName)
                    },
                    new CondParam
                    {
                        FieldName = SystemField.ID.ToString(),
                        Operator = CondOperator.NotEqual,
                        Value = Utils.GetString(data, SystemField.ID.ToString())
                    });
                    if (isExist)
                    {
                        model.IsSuccess = false;
                        model.Message = $"{uniqueCol.Name} đã tồn tại trên hệ thống";
                        return model;
                    }
                }
            }
            foreach (var item in columnbyAlls)
            {
                if (item.IsSystem && item.IsIdentity)
                    continue;

                if (item.IsSubColumn)
                    continue;

                switch (item.DataType)
                {
                    case (int)ColumnDataType.DateTime:
                        if (item.IsSystem)
                        {
                            qrColumnName.Add(item.DbName);
                            values.Add($"{item.DbName} =TO_DATE('{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}', 'yyyy-MM-dd hh24:mi:ss')");
                        }
                        else
                        {
                            qrColumnName.Add(item.DbName);
                            if (Utils.IsDate(Utils.GetDate(data, item.DbName)))
                            {
                                values.Add($"{item.DbName} =TO_DATE('{Utils.GetDate(data, item.DbName).Value.ToString("yyyy-MM-dd HH:mm:ss")}', 'yyyy-MM-dd hh24:mi:ss')");
                            }
                            else
                                //values.Add($"{item.DbName} = TO_DATE('NULL', 'yyyy-MM-dd hh24:mi:ss')");
                                values.Add($"{item.DbName} = NULL");
                        }
                        break;
                    case (int)ColumnDataType.TableUser:
                        qrColumnName.Add(item.DbName);
                        values.Add($"{item.DbName} ='{_userPrincipalService.UserId}'");
                        break;
                    case (int)ColumnDataType.Decimal:
                    case (int)ColumnDataType.Integer:
                        qrColumnName.Add(item.DbName);
                        values.Add($"{item.DbName} ='{Utils.GetInt(data, item.DbName)}'");
                        break;
                    case (int)ColumnDataType.Boolean:
                        qrColumnName.Add(item.DbName);
                        values.Add($"{item.DbName} ='{Utils.GetInt(data, item.DbName)}'");
                        break;
                    case (int)ColumnDataType.Parent:
                        //lấy giá trị của bản ghi cha
                        var columnRefer = ColumnsRefer.FirstOrDefault(t => t.ID == item.IDColRef);
                        var tableRefer = tablseRefer.FirstOrDefault(t => t.ID == item.IDTableRef);
                        var schemaRefer = schemasRefer.FirstOrDefault(t => t.ID == item.IDSchemaRef);
                        var dt = _dynamicDBService.Search(new List<CondParam> {
                            new CondParam
                            {
                                FieldName=columnRefer.DbName,
                                Operator=CondOperator.Equal,
                                Value=Utils.GetString(data, item.DbName)
                            }
                            }, schemaRefer.Code, tableRefer.DbName);

                        //Giá trị của chính parent
                        qrColumnName.Add(item.DbName);
                        values.Add($"{item.DbName}='{Utils.GetString(data, item.DbName)}'");
                        var level = 0;
                        var json = "";
                        var PathParent = "0";
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            var Parent = dt.Rows[0];
                            level = Utils.GetInt(Parent[item.DbName + "_LEVEL"]?.ToString()) + 1;
                            PathParent = Parent[item.DbName + "_" + columnRefer.DbName].ToString() + "|" + Parent[columnRefer.DbName];
                            PathParent = PathParent.TrimStart('|');
                            json = Utils.ConvertDataTabletoString(dt);
                            if (json.IsNotEmpty())
                            {
                                json = json.Replace("'", "''");
                            }
                            //Đường dẫn
                            var columnspathParent = ColumnsPath.Where(t => t.IDColumnTableInfo == item.ID).ToList();
                            if (Utils.IsNotEmpty(columnspathParent))
                            {
                                foreach (var dd in columnspathParent)
                                {
                                    var columnPath = ColumnsRefer.FirstOrDefault(t => t.ID == dd.IDColumnPath);
                                    if (columnPath != null)
                                    {
                                        qrColumnName.Add(dd.PathName);
                                        //if (columnPath.IsIdentity)
                                        //{
                                        //    //TH tự tăng thì path là: Parents|ParentID
                                        //    var cPath = Parent[columnPath.DbName]?.ToString().TrimStart("N'").TrimEnd("'");
                                        //    var vlPath = (Parent[dd.PathName].ToString() + "|" + cPath).TrimStart('|');
                                        //    //Bỏ N' & '
                                        //    vlPath = vlPath.TrimStart("N'").TrimEnd("'");
                                        //    values.Add($"{dd.PathName} ='{vlPath}'");
                                        //}
                                        //else
                                        //{
                                        var cPath = Utils.GetString(data, columnPath.DbName).TrimStart("N'").TrimEnd("'");    //Bỏ N' & '
                                        var vlPath = (Parent[dd.PathName].ToString() + cPath).TrimStart('|');
                                        vlPath = vlPath.TrimStart("N'").TrimEnd("'");  //Bỏ N' & '
                                        vlPath = $"|{vlPath}|";
                                        values.Add($"{dd.PathName} ='{vlPath}'");
                                        //}
                                    }
                                }
                            }
                            qrColumnName.Add(item.DbName + "_ID");
                            values.Add($"{item.DbName + "_ID"} ='{Parent["ID"]}'");
                        }
                        else
                        {
                            //Đường dẫn
                            var columnspathParent = ColumnsPath.Where(t => t.IDColumnTableInfo == item.ID).ToList();
                            if (Utils.IsNotEmpty(columnspathParent))
                            {
                                foreach (var dd in columnspathParent)
                                {
                                    var columnPath = ColumnsRefer.FirstOrDefault(t => t.ID == dd.IDColumnPath);
                                    qrColumnName.Add(dd.PathName);
                                    //if (columnPath.IsIdentity)
                                    //{
                                    //    if (columnPath.DataType == (int)ColumnDataType.String)
                                    //    {
                                    //        values.Add($"{dd.PathName}=''");
                                    //    }
                                    //    else
                                    //    {
                                    //        values.Add($"{dd.PathName}='0'");
                                    //    }
                                    //}
                                    //else
                                    //{
                                    var vlPath = Utils.GetString(data, columnPath.DbName).TrimStart('|');
                                    vlPath = vlPath.TrimStart("N'").TrimEnd("'");  //Bỏ N' & '
                                    vlPath = $"|{vlPath}|";
                                    values.Add($"{dd.PathName} ='{vlPath}'");
                                    //}
                                }
                            }

                            // Path
                            qrColumnName.Add(item.DbName + "_ID");
                            values.Add($"{item.DbName + "_ID"}='0'");
                        }
                        // Level
                        qrColumnName.Add(item.DbName + "_LEVEL");
                        values.Add($"{item.DbName + "_LEVEL"}='{level}'");

                        //Lookup
                        qrColumnName.Add(item.DbName + "_DATALOOKUP");
                        values.Add($"{item.DbName + "_DATALOOKUP"}='{json}'");
                        break;
                    case (int)ColumnDataType.TableRef:
                        //lấy giá trị của bảng tham chiếu
                        var columnRef = ColumnsRefer.FirstOrDefault(t => t.ID == item.IDColRef);
                        var tableRêf = tablseRefer.FirstOrDefault(t => t.ID == item.IDTableRef);
                        var schemaRef = schemasRefer.FirstOrDefault(t => t.ID == item.IDSchemaRef);
                        var dtRef = _dynamicDBService.Search(new List<CondParam> {
                        new CondParam
                        {
                            FieldName=columnRef.DbName,
                            Operator=CondOperator.Equal,
                            Value=Utils.GetString(data, item.DbName)
                        }
                        }, schemaRef.Code, tableRêf.DbName);
                        // Giá trị tham chiếu
                        qrColumnName.Add(item.DbName);
                        values.Add($" {item.DbName}= N'{Utils.GetString(data, item.DbName)}'");
                        // giá trị ID
                        var id = dtRef.Rows.Count > 0 ? dtRef.Rows[0]["ID"].ToString() : "0";
                        qrColumnName.Add(item.DbName + "_ID");
                        values.Add($"{item.DbName + "_ID"} = N'{id}'");
                        //Lookup
                        json = Utils.ConvertDataTabletoString(dtRef);
                        qrColumnName.Add(item.DbName + "_DATALOOKUP");
                        values.Add($"{item.DbName + "_DATALOOKUP"} = '{json}'");
                        break;
                    default:
                        qrColumnName.Add(item.DbName);
                        values.Add($"{item.DbName} = N'{Utils.GetString(data, item.DbName)}'");
                        break;
                }
            }

            // check dữ liệu liên kết
            var idTablesRefs = Utils.GetInts(data, "IDTableRef");
            var idRecordsRefs = Utils.GetInts(data, "IDRecordRef");
            var idTableRefCheck = idTablesRefs.IsNotEmpty() ? idTablesRefs.Where(x => x == 0) : new int[] { };
            if (idTableRefCheck.Count() > 0)
            {
                model.IsSuccess = false;
                model.Message = "Bảng liên kết không được để trống";
                return model;
            }
            else if (idTablesRefs.IsNotEmpty() && idRecordsRefs == null)
            {
                model.IsSuccess = false;
                model.Message = "Dữ liệu liên kết không được để trống";
                return model;
            }
            //_logger.LogDebug("idRecordsRefs" + Utils.Serialize(idRecordsRefs));

            if (idRecordsRefs.IsNotEmpty() && idRecordsRefs.Count() != idRecordsRefs.Distinct().Count())
            {
                model.IsSuccess = false;
                model.Message = "Dữ liệu liên kết không được trùng nhau";
                return model;
            }

            var comlumvalues = string.Join(",", values);
            var sql = $"UPDATE  {model.schemaInfo.Code}.{model.tableInfo.DbName} SET  {comlumvalues} where ID={ID}";
            //_logger.LogInfo(sql);
            if (_dynamicDBService.UpdateData(sql, null))
            {
                var nameTableLienKet = $"{model.schemaInfo.Code}{KTNNConst.TableLinkNameExt}";
                SaveTableLink(data, model, ID, nameTableLienKet, 1);
                model.IsSuccess = true;
                model.Message = $"Cập nhật dữ liệu vào bảng {model.tableInfo.Name} thành công";
                return model;
            }
            else
            {
                model.IsSuccess = false;
                model.Message = $"Cập nhật dữ liệu vào bảng {model.tableInfo.Name} không thành công";
                return model;
            }
        }
        public async Task<VMInputInfo> SaveDetach(Hashtable data)
        {
            var idTable = Utils.GetInt(data, "IDTable");
            var isSingle = Utils.GetInt(data, "IsSingle");
            var id = Utils.GetInt(data, "ID");
            var model = new VMInputInfo();
            model.tableInfo = _dasKTNNRepo.TableInfo.Get(idTable) ?? new TableInfo() { Name = "Bảng chưa tạo" };
            if (model.tableInfo.ID <= 0)
            {
                model.IsSuccess = false;
                model.Message = "Bảng không tồn tại";
                return model;
            }
            model.schemaInfo = _dasKTNNRepo.SchemaInfo.Get(model.tableInfo.IDSchema) ?? new SchemaInfo();
            var actionName = isSingle > 0 ? "Đổi tên danh mục" : "Phân tách dữ liệu";
            model.Message = $"{actionName} thất bại";
            if (isSingle > 0)
            {
                data["IDTable"] = idTable;
                data[CustomField.IDTach.ToString()] = id;
                var rs = await SaveRecord(data);
                if (rs.IsSuccess)
                {
                    var dataTach = _dynamicDBService.GetListByField(model.schemaInfo.Code, model.tableInfo.DbName, CustomField.IDTach.ToString(), id);
                    var recordUpdate = new Dictionary<string, string>()
                {
                    {CustomField.DataTach.ToString(), dataTach.ToOracleDataLookup() }
                };
                    //Cap nhat lai ngay hieu luc ban ghi tach
                    UpdateRecord(model.schemaInfo.Code, model.tableInfo.DbName, recordUpdate, id);
                    model.IsSuccess = true;
                    model.Message = $"{actionName} thành công";
                    return model;
                }
                model.Message = rs.Message;
                return model;
            }
            else
            {
                var ngayHieuLuc = Utils.GetDate(data, "NgayHieuLuc");
                if (ngayHieuLuc != null)
                {
                    var recordCount = Utils.GetStrings(data, "RecordLine").Length;
                    for (int i = 0; i < recordCount; i++)
                    {
                        var recordData = new Hashtable()
                    {
                        {"IDTable",idTable },
                        { CustomField.IDTach.ToString(),id },
                        { DefaultField.HieuLucTu.ToString(), Utils.DateToString(ngayHieuLuc) },
                    };
                        foreach (DictionaryEntry item in data)
                        {
                            var prefix = $"Records[{i}].";
                            if (item.Key.ToString().IndexOf(prefix) == 0)
                            {
                                var k = item.Key.ToString().Substring(prefix.Length);
                                if (!recordData.ContainsKey(k))
                                    recordData.Add(k, item.Value);
                            }
                        }

                        var rs = await SaveRecord(recordData);
                        if (!rs.IsSuccess)
                        {
                            model.Message = rs.Message;
                            return model;
                        }
                    }
                    var dataTach = _dynamicDBService.GetListByField(model.schemaInfo.Code, model.tableInfo.DbName, CustomField.IDTach.ToString(), id);

                    var recordUpdate = new Dictionary<string, string>()
                {
                    {DefaultField.HieuLucDen.ToString(), ngayHieuLuc.ToOracleStringDatetime() },
                    {CustomField.DataTach.ToString(), dataTach.ToOracleDataLookup() }
                };

                    //Cap nhat lai ngay hieu luc ban ghi tach
                    UpdateRecord(model.schemaInfo.Code, model.tableInfo.DbName, recordUpdate, id);

                    model.IsSuccess = true;
                    model.Message = $"{actionName} thành công";
                    return model;
                }
            }
            return model;
        }
        #endregion

        #region Delete  

        public async Task<VMInputInfo> Delete(Hashtable data)
        {
            var IDTable = Utils.GetInt(data, "IDTable");
            var ID = Utils.GetInt(data, "id");
            var model = new VMInputInfo();
            model.tableInfo = _dasKTNNRepo.TableInfo.Get(IDTable) ?? new TableInfo() { Name = "Bảng chưa tạo" };
            if (model.tableInfo.ID <= 0)
            {
                model.IsSuccess = false;
                model.Message = "Bảng không tồn tại";
                return model;
            }
            model.schemaInfo = _dasKTNNRepo.SchemaInfo.Get(model.tableInfo.IDSchema) ?? new SchemaInfo();
            var sql = $"DELETE   {model.schemaInfo.Code}.{model.tableInfo.DbName}  where ID = {ID}";
            var datas = _dynamicDBService.DeleteData(sql, null);
            if (datas)
                _dynamicDBService.DeleteDataByIDRecordLink(model.schemaInfo.Code, $"{model.schemaInfo.Code}{KTNNConst.TableLinkNameExt}", ID);

            model.IsSuccess = true;
            model.Message = $"Xóa dữ liệu vào bảng {model.tableInfo.Name} thành công";
            return model;
        }
        public async Task<VMInputInfo> Deletes(Hashtable data, int[] ids)
        {
            var IDTable = Utils.GetInt(data, "IDTable");
            var model = new VMInputInfo();
            model.tableInfo = _dasKTNNRepo.TableInfo.Get(IDTable) ?? new TableInfo() { Name = "Bảng chưa tạo" };
            if (model.tableInfo.ID <= 0)
            {
                model.IsSuccess = false;
                model.Message = "Bảng không tồn tại";
                return model;
            }
            model.schemaInfo = _dasKTNNRepo.SchemaInfo.Get(model.tableInfo.IDSchema) ?? new SchemaInfo();
            var sql = $"DELETE   {model.schemaInfo.Code}.{model.tableInfo.DbName}  where ID in ({string.Join(",", ids)})";
            if (_dynamicDBService.DeleteData(sql, null))
            {
                _dynamicDBService.DeleteDataByIDRecordLink(model.schemaInfo.Code, $"{model.schemaInfo.Code}{KTNNConst.TableLinkNameExt}", ids);
                model.IsSuccess = true;
                model.Message = $"Xóa dữ liệu vào bảng {model.tableInfo.Name} thành công";
                return model;
            }
            else
            {
                model.IsSuccess = false;
                model.Message = $"Xóa dữ liệu vào bảng {model.tableInfo.Name} không thành công";
                return model;
            }

        }
        #endregion

        #region Detail
        public async Task<VMLeftTree> GetInfoDataDetail(InputInfoCondition TableInfoCondition)
        {
            try
            {
                var model = new VMLeftTree();
                var tableInfo = _dasKTNNRepo.TableInfo.Get(TableInfoCondition.IDTable) ?? new TableInfo() { Name = "Bảng chưa tạo" };
                var schemaInfo = _dasKTNNRepo.SchemaInfo.Get(tableInfo.IDSchema) ?? new SchemaInfo();
                model.TableInfo = tableInfo;
                model.SchemaInfo = schemaInfo;
                var schemas = await GetDictSchema();
                model.Schemas = schemas.Select(n => new SelectListItem
                {
                    Value = n.Key.ToString(),
                    Text = n.Value,
                });
                var tables = await GetTables();
                model.Tables = tables.Where(n => n.ID != TableInfoCondition.IDTable);
                model.columnTables = await GetAllColumnTables(tableInfo.ID) ?? new List<ColumnTableInfo>();
                model.columnTables = model.columnTables.Where(n => !n.IsSystem).ToList();
                model.dataFirst = _dynamicDBService.GetFirstById(schemaInfo.Code, tableInfo.DbName, TableInfoCondition.IDParent);
                model.tableLinks = GetRecordLinkTable(schemaInfo, tableInfo, TableInfoCondition.IDParent.IsNotEmpty() ? int.Parse(TableInfoCondition.IDParent) : 0);
                //var datasparent_refer = new List<TableOptionSelect>();
                //var colsparent_refer = model.columnTables.Where(t => t.DataType == (int)ColumnDataType.Parent || t.DataType == (int)ColumnDataType.TableRef).ToList() ?? new List<ColumnTableInfo>();
                //if (Utils.IsNotEmpty(colsparent_refer))
                //{
                //    var idchemas = colsparent_refer.Select(t => t.IDSchemaRef).Distinct().ToArray();
                //    var idtables = colsparent_refer.Select(t => t.IDTableRef).Distinct().ToArray();
                //    var tableRefer = await GetAllTablesByIDs(idtables) ?? new List<TableInfo>();
                //    var schemaRefer = await GetSchemas(idchemas) ?? new List<SchemaInfo>();
                //    foreach (var item in colsparent_refer)
                //    {
                //        var nameSchemaRefer = (schemaRefer.FirstOrDefault(t => t.ID == item.IDSchemaRef) ?? new SchemaInfo()).Code;
                //        var nameTableRefer = (tableRefer.FirstOrDefault(t => t.ID == item.IDTableRef) ?? new TableInfo()).DbName;
                //        var dataparent_refer = await GetDataTableRefer_Parent(nameSchemaRefer, nameTableRefer, item);
                //        if (Utils.IsNotEmpty(dataparent_refer))
                //            datasparent_refer.Add(dataparent_refer);
                //    }
                //}
                //TableInfoCondition.Title = "Thông tin chi tiết";
                //model.InputInfoCondition = TableInfoCondition;
                //model.DataParent_Refer = datasparent_refer;
                //Get Table nhưng item liên quan
                var allTables = _dasKTNNRepo.TableInfo.GetAllList(); // Get Từ cache
                var allSchemas = _dasKTNNRepo.SchemaInfo.GetAllList();// Get Từ cache
                var allColumns = _dasKTNNRepo.ColumnTableInfo.GetAllList(n => n.Status == (int)EnumCommon.Status.Active);// Get Từ cache
                var lstTabTree = new List<TabLefTree>();
                // DacPV Bo Refer B Qua A, chi tham chieu den chinh tham chieu cua ban hien tai
                // var columnsRefer = allColumns.Where(t => t.IDTableRef == TableInfoCondition.IDTable && t.DataType == (int)ColumnDataType.TableRef);
                if (true)//lấy các bản ghi được tham chiếu bản ghi hiện tại (tham chiếu xuôi A-B)
                {
                    var columnsRefer = allColumns.Where(t => t.IDTable == TableInfoCondition.IDTable && t.DataType == (int)ColumnDataType.TableRef);
                    if (Utils.IsNotEmpty(columnsRefer))
                    {
                        var idTablesRefer = columnsRefer.Select(t => t.IDTableRef).Distinct();
                        if (Utils.IsNotEmpty(idTablesRefer))
                        {
                            // foreach (var idtable in idTablesRefer)
                            foreach (var columnRefer in columnsRefer)
                            {
                                var idtable = columnRefer.IDTableRef;
                                string nameColumnTD = "";
                                var tableRefer = allTables.First(t => t.ID == idtable);
                                // var columnRefer = columnsRefer.FirstOrDefault(t => t.IDTable == idtable);
                                var schemaRefer = allSchemas.FirstOrDefault(t => t.ID == tableRefer.IDSchema);
                                var colDBName = allColumns.FirstOrDefault(t => t.ID == columnRefer.IDColRef);
                                if (Utils.IsEmpty(colDBName))
                                    continue;
                                var lstConditionRefer = new List<CondParam> {
                            new CondParam{
                                FieldName=colDBName.DbName,
                                Operator=CondOperator.Equal,
                                Value=model.dataFirst == null ?"0":model.dataFirst.Rows[0][columnRefer.DbName].ToString()
                            }
                            };
                                var dataRefer = _dynamicDBService.Search(lstConditionRefer, schemaRefer.Code, tableRefer.DbName);// string.Join(",", model.columnTables.Select(n => n.DbName).ToList()));
                                if (dataRefer != null && dataRefer.Rows.Count > 0)
                                {
                                    if (dataRefer.Columns.Contains("TenDanhMuc"))
                                    {
                                        nameColumnTD = "TenDanhMuc";
                                    }
                                    else if (dataRefer.Columns.Contains("Ten"))
                                    {
                                        nameColumnTD = "Ten";
                                    }
                                    for (int i = 0; i < dataRefer.Rows.Count; i++)
                                    {
                                        lstTabTree.Add(new TabLefTree
                                        {
                                            ID = dataRefer.Rows[i]["ID"].ToString(),
                                            Name = dataRefer.Rows[i][nameColumnTD].ToString(),
                                            IDTable = tableRefer.ID
                                        });
                                    }
                                }

                            }
                        }
                    }

                }

                if (true)//lấy các bản ghi đã tham chiếu bản ghi hiện tại (tham chiếu ngược B-A)
                {
                    var columnsRefer = allColumns.Where(t => t.IDTableRef == TableInfoCondition.IDTable && t.DataType == (int)ColumnDataType.TableRef);
                    if (Utils.IsNotEmpty(columnsRefer))
                    {
                        var idTablesRefer = columnsRefer.Select(t => t.IDTable).Distinct();
                        if (Utils.IsNotEmpty(idTablesRefer))
                        {
                            // foreach (var idtable in idTablesRefer)
                            foreach (var columnRefer in columnsRefer)
                            {
                                var idtable = columnRefer.IDTable;
                                string nameColumnTD = "";
                                var tableRefer = allTables.First(t => t.ID == idtable);
                                // var columnRefer = columnsRefer.FirstOrDefault(t => t.IDTable == idtable);
                                var schemaRefer = allSchemas.FirstOrDefault(t => t.ID == tableRefer.IDSchema);
                                var colDBName = allColumns.FirstOrDefault(t => t.ID == columnRefer.IDColRef);
                                if (Utils.IsEmpty(colDBName))
                                    continue;
                                var lstConditionRefer = new List<CondParam> {
                            new CondParam{
                                FieldName=columnRefer.DbName,
                                Operator=CondOperator.Equal,
                                Value=model.dataFirst == null ?"0":model.dataFirst.Rows[0][colDBName.DbName].ToString()
                            }
                            };
                                var dataRefer = _dynamicDBService.Search(lstConditionRefer, schemaRefer.Code, tableRefer.DbName);// string.Join(",", model.columnTables.Select(n => n.DbName).ToList()));
                                if (dataRefer != null && dataRefer.Rows.Count > 0)
                                {
                                    if (dataRefer.Columns.Contains("TenDanhMuc"))
                                    {
                                        nameColumnTD = "TenDanhMuc";
                                    }
                                    else if (dataRefer.Columns.Contains("Ten"))
                                    {
                                        nameColumnTD = "Ten";
                                    }
                                    for (int i = 0; i < dataRefer.Rows.Count; i++)
                                    {
                                        lstTabTree.Add(new TabLefTree
                                        {
                                            ID = dataRefer.Rows[i]["ID"].ToString(),
                                            Name = dataRefer.Rows[i][nameColumnTD].ToString(),
                                            IDTable = tableRefer.ID
                                        });
                                    }
                                }

                            }
                        }
                    }

                }
                // Layas ra truong hop tham chieu nguoc
                if (allColumns.ToList().Exists(t => t.DataType == (int)ColumnDataType.TableLink))
                {
                    var pageLink = new Pagination();

                }
                model.VMLienKetNguoc = GetLienKetNguoc(1, schemaInfo, tableInfo, TableInfoCondition.IDParent);
                model.TabLefTrees = lstTabTree;
                //_logger.LogDebug(Utils.Serialize(model.VMLienKetNguoc));
                return model;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return new VMLeftTree();
            }

        }

        #endregion

        #region Excel
        public async Task<ServiceResult> DownloadTemplate(int IDTable)
        {
            var tableInfo = _dasKTNNRepo.TableInfo.Get(IDTable) ?? new TableInfo() { Name = "Bảng chưa tạo" };
            var schemaInfo = _dasKTNNRepo.SchemaInfo.Get(tableInfo.IDSchema) ?? new SchemaInfo();
            var columnTables = await GetAllColumnTables(tableInfo.ID) ?? new List<ColumnTableInfo>();
            var hearsers = new List<Header>
                {
                    new Header("STT", 8),
                };
            foreach (var item in columnTables)
            {
                hearsers.Add(new Header(item.Name));
            }
            var export = new ExportExtend
            {
                Data = new List<dynamic>(),
                Cols = new List<Col>(),
                Headers = hearsers
            };
            var rs = await _excelService.ExportExcel(export, tableInfo.DbName);
            return rs;
        }
        #endregion

        #region Funtions
        public async Task<List<TableInfo>> GetAllTablesByIDs(params int[] idTables)
        {
            if (Utils.IsEmpty(idTables))
                return new List<TableInfo>();
            return await (from col in _dasKTNNRepo.TableInfo.GetAll()
                          where idTables.Contains(col.ID) && col.Status == (int)EnumCommon.Status.Active
                          select col).ToListAsync();
        }
        public async Task<List<ColumnTableInfo>> GetAllColumnTablesByIDTable(params int[] idTables)
        {
            return await (from col in _dasKTNNRepo.ColumnTableInfo.GetAll()
                          where idTables.Contains(col.IDTable) && col.Status == (int)EnumCommon.Status.Active
                          orderby col.Weight
                          select col).ToListAsync();
        }
        public async Task<List<ColumnTableInfo>> GetAllColumnTableByID(params int[] ids)
        {
            return await (from col in _dasKTNNRepo.ColumnTableInfo.GetAll()
                          where ids.Contains(col.ID) && col.Status == (int)EnumCommon.Status.Active
                          orderby col.Weight
                          select col).ToListAsync();
        }
        public async Task<List<ColumnPathInfo>> GetAllColumnPathByIDColumnRefer(params int[] idclrefers)
        {
            return await (from col in _dasKTNNRepo.ColumnPathInfo.GetAll()
                          where idclrefers.Contains(col.IDColumnTableInfo) && col.Status == (int)EnumCommon.Status.Active
                          select col).ToListAsync();
        }
        public async Task<List<ColumnTableInfo>> GetColumnTables(params int[] idTables)
        {
            return await (from col in _dasKTNNRepo.ColumnTableInfo.GetAll()
                          where idTables.Contains(col.IDTable) && col.IsShowOnList == true && col.Status == (int)EnumCommon.Status.Active
                          orderby col.Weight
                          select col).ToListAsync();
        }
        private async Task<List<ColumnTableInfo>> GetAllColumnTables(params int[] idTables)
        {
            return await (from col in _dasKTNNRepo.ColumnTableInfo.GetAll()
                          where idTables.Contains(col.IDTable) && col.Status == (int)EnumCommon.Status.Active
                          orderby col.Weight
                          select col).ToListAsync();
        }

        public async Task<List<ColumnTableInfo>> GetAllColumnTable(params int[] idTables)
        {
            return await (from col in _dasKTNNRepo.ColumnTableInfo.GetAll()
                          orderby col.Weight
                          select col).ToListAsync();
        }

        public async Task<List<SchemaInfo>> GetSchemas(params int[] ids)
        {
            return await (from schema in _dasKTNNRepo.SchemaInfo.GetAll()
                          where ids.Contains(schema.ID)
                          select schema).ToListAsync();
        }
        public async Task<VMSyncData> GetDataTemp(int idTable, int idRecord)
        {
            var model = new VMSyncData();
            var tableInfo = await _dasKTNNRepo.TableInfo.GetAsync(idTable) ?? new TableInfo() { Name = "Bảng chưa tạo" };
            var schemaInfo = await _dasKTNNRepo.SchemaInfo.GetAsync(tableInfo.IDSchema) ?? new SchemaInfo();

            var data = _dynamicDBService.GetFirstById(schemaInfo.Code, tableInfo.DbName, idRecord);

            var reason = "";
            if (data.Columns.Contains("REASON"))
                reason = data.Rows[0]["REASON"].ToString();

            model.IDTable = tableInfo.ID;
            model.IDRecord = idRecord;
            model.Reason = reason;

            return model;
        }


        /// <summary>
        /// Lấy dl bảng liên kết
        /// </summary>
        /// <param name="schema"></param>
        /// <param name="tableInfo"></param>
        /// <param name="idRecord"></param>
        /// <returns></returns>
        public List<VMTableLink> GetRecordLinkTable(SchemaInfo schema, TableInfo tableInfo, int idRecord)
        {
            try
            {
                var data = _dynamicDBService.GetListByField(schema.Code, $"{schema.Code}{KTNNConst.TableLinkNameExt}", nameof(VMTableLink.IDRecord), idRecord);
                if (data != null && data.Rows.Count > 0)
                {
                    return data.AsEnumerable().Select(m => new VMTableLink()
                    {
                        ID = m.Field<int>(nameof(VMTableLink.ID)),

                        IDSchema = m.Field<int>(nameof(VMTableLink.IDSchema)),
                        IDTable = m.Field<int>(nameof(VMTableLink.IDTable)),
                        IDRecord = m.Field<int>(nameof(VMTableLink.IDRecord)),

                        IDSchemaRef = m.Field<int>(nameof(VMTableLink.IDSchemaRef)),
                        IDTableRef = m.Field<int>(nameof(VMTableLink.IDTableRef)),
                        IDRecordRef = m.Field<int>(nameof(VMTableLink.IDRecordRef)),
                        DataLookUp = m.Field<string>(nameof(VMTableLink.DataLookUp)),
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                //Ignore
            }
            return new List<VMTableLink>();

        }
        #endregion

        #region Lấy dữ liệu dạng tree
        public async Task<VMLeftTree> GetTableLeftTree(InputInfoCondition TableInfoCondition)
        {
            var IsViewLoagPage = false;
            var page1 = new Pagination
            {
                PageSize = TableInfoCondition.PageSize,
                PageIndex = TableInfoCondition.PageIndex == 0 ? 1 : TableInfoCondition.PageIndex
            };
            var page2 = new Pagination
            {
                PageSize = TableInfoCondition.PageSize,
                PageIndex = TableInfoCondition.PageIndex == 0 ? 1 : TableInfoCondition.PageIndex
            };
            bool isviewdc = TableInfoCondition.View?.ToUpper() == "DC";
            try
            {
                var nameColumnTD = "ID";
                TableInfoCondition.Title = "Thông tin chi tiết";
                var level = TableInfoCondition.Level == 0 ? 2 : TableInfoCondition.Level == 2 ? 12 : TableInfoCondition.Level + 12;
                var result = new VMLeftTree();
                if (TableInfoCondition.IDTable <= 0)
                    return result;

                result.columnTables = new List<ColumnTableInfo>();
                result.dataFirst = new DataTable();
                result.DataParent_Refer = new List<TableOptionSelect>();
                result.InputInfoCondition = TableInfoCondition;
                result.TableLeftTrees = new List<TableLeftTree>();
                var model = new VMInputInfo();
                var allTables = _dasKTNNRepo.TableInfo.GetAllList();
                var allSchemas = _dasKTNNRepo.SchemaInfo.GetAllList();
                var allColumns = _dasKTNNRepo.ColumnTableInfo.GetAllList();
                //UserData userData = await _cacheManagementServices.GetUserDataAndSetCache();
                model.tableInfo = allTables.FirstOrDefault(t => t.ID == TableInfoCondition.IDTable) ?? new TableInfo();
                model.schemaInfo = allSchemas.FirstOrDefault(t => t.ID == model.tableInfo.IDSchema);
                model.columnTables = allColumns.Where(t => t.IDTable == TableInfoCondition.IDTable);
                model.Users = await _dasRepo.User.GetAllListAsync() ?? new List<User>();
                result.TableInfo = model.tableInfo;
                // tính toán điều kiện lấy dữ liệu lấy parent
                var lstCondition = new List<CondParam>();
                if (model.columnTables.ToList().Exists(t => t.DataType == (int)ColumnDataType.Parent))
                {
                    var columnParent = model.columnTables.FirstOrDefault(t => t.DataType == (int)ColumnDataType.Parent);
                    lstCondition.Add(new CondParam
                    {
                        FieldName = $"NVL({columnParent.DbName}_ID,'0')",
                        Operator = CondOperator.Equal,
                        Value = string.IsNullOrEmpty(TableInfoCondition.IDParent) ? "0" : TableInfoCondition.IDParent
                    });
                }
                else if (!string.IsNullOrEmpty(TableInfoCondition.IDParent) && TableInfoCondition.IDParent != "0")
                {
                    // không hiển thị
                    lstCondition.Add(new CondParam
                    {
                        FieldName = "ID",
                        Operator = CondOperator.Equal,
                        Value = "-100"
                    });
                }

                var dataChild = _dynamicDBService.Search(lstCondition, model.schemaInfo.Code, model.tableInfo.DbName, page: page1);// string.Join(",", model.columnTables.Select(n => n.DbName).ToList()));
                if (dataChild != null && dataChild.Rows.Count > 0)
                {
                    if (page1.PageIndex < page1.TotalPage)
                        IsViewLoagPage = IsViewLoagPage || true;

                    if (dataChild.Columns.Contains("TenDanhMuc"))
                    {
                        nameColumnTD = "TenDanhMuc";
                    }
                    else if (dataChild.Columns.Contains("Ten"))
                    {
                        nameColumnTD = "Ten";
                    }
                    for (int i = 0; i < dataChild.Rows.Count; i++)
                    {
                        result.TableLeftTrees.Add(new TableLeftTree
                        {
                            ID = dataChild.Rows[i]["ID"].ToString(),
                            IDSchema = model.schemaInfo.ID,
                            IDTable = model.tableInfo.ID,
                            Level = level,

                            TieuDe = dataChild.Rows[i][nameColumnTD].ToString(),
                            IsChild = (isviewdc ? (TotalRefer(allTables, allColumns, allSchemas, model.tableInfo.ID, dataChild.Rows[i]["ID"].ToString()) + TotalChild(allTables, allColumns, allSchemas, model.tableInfo.ID, dataChild.Rows[i]["ID"].ToString())) :
                                        TotalChild(allTables, allColumns, allSchemas, model.tableInfo.ID, dataChild.Rows[i]["ID"].ToString())) == 0 ? 0 : 1,
                            View = TableInfoCondition.View
                        });
                    }
                }
                if (!isviewdc)
                {
                    result.IsLoadView = IsViewLoagPage;
                    return result;
                }
                // Lấy về các phần từ tham chiếu
                //   -- lấy các bảng tham chiếu đến
                var columnsRefer = allColumns.Where(t => t.IDTableRef == model.tableInfo.ID && t.DataType == (int)ColumnDataType.TableRef);
                if (Utils.IsNotEmpty(columnsRefer))
                {
                    var idTablesRefer = columnsRefer.Select(t => t.IDTable).Distinct();
                    if (Utils.IsNotEmpty(idTablesRefer))
                    {
                        foreach (var idtable in idTablesRefer)
                        {
                            var tableRefer = allTables.First(t => t.ID == idtable);
                            var columnRefer = columnsRefer.FirstOrDefault(t => t.IDTable == idtable);
                            var schemaRefer = allSchemas.FirstOrDefault(t => t.ID == tableRefer.IDSchema);
                            var lstConditionRefer = new List<CondParam> {
                            new CondParam{
                                FieldName=columnRefer.DbName,
                                Operator=CondOperator.Equal,
                                Value=TableInfoCondition.IDParent??"0"
                            }
                            };
                            var dataRefer = _dynamicDBService.Search(lstConditionRefer, schemaRefer.Code, tableRefer.DbName, page: page2);// string.Join(",", model.columnTables.Select(n => n.DbName).ToList()));
                            if (dataRefer != null && dataRefer.Rows.Count > 0)
                            {
                                if (page2.PageIndex < page2.TotalPage)
                                    IsViewLoagPage = IsViewLoagPage || true;

                                if (dataRefer.Columns.Contains("TenDanhMuc"))
                                {
                                    nameColumnTD = "TenDanhMuc";
                                }
                                else if (dataRefer.Columns.Contains("Ten"))
                                {
                                    nameColumnTD = "Ten";
                                }
                                for (int i = 0; i < dataRefer.Rows.Count; i++)
                                {
                                    result.TableLeftTrees.Add(new TableLeftTree
                                    {
                                        ID = dataRefer.Rows[i]["ID"].ToString(),
                                        IDSchema = schemaRefer.ID,
                                        IDTable = tableRefer.ID,
                                        Level = level,
                                        TieuDe = dataRefer.Rows[i][nameColumnTD].ToString(),
                                        IsChild = (isviewdc ? (TotalRefer(allTables, allColumns, allSchemas, tableRefer.ID, dataRefer.Rows[i]["ID"].ToString()) + TotalChild(allTables, allColumns, allSchemas, tableRefer.ID, dataRefer.Rows[i]["ID"].ToString())) :
                                        TotalChild(allTables, allColumns, allSchemas, tableRefer.ID, dataRefer.Rows[i]["ID"].ToString())) == 0 ? 0 : 1,
                                        View = TableInfoCondition.View
                                    });
                                }
                            }
                        }
                    }
                }
                result.IsLoadView = IsViewLoagPage;
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return new VMLeftTree();
            }
        }

        public async Task<VMLeftTree> SearchByCondition(InputInfoCondition TableInfoCondition)
        {
            try
            {
                var IsViewLoagPage = false;
                var page1 = new Pagination
                {
                    PageSize = TableInfoCondition.PageSize,
                    PageIndex = TableInfoCondition.PageIndex == 0 ? 1 : TableInfoCondition.PageIndex
                };

                var nameColumnTD = "ID";
                var result = new VMLeftTree();
                if (TableInfoCondition.IDTable <= 0)
                    return result;

                result.columnTables = new List<ColumnTableInfo>();
                result.dataFirst = new DataTable();
                result.DataParent_Refer = new List<TableOptionSelect>();
                result.InputInfoCondition = TableInfoCondition;
                result.TableLeftTrees = new List<TableLeftTree>();
                var model = new VMInputInfo();
                var allTables = _dasKTNNRepo.TableInfo.GetAllList();
                var allSchemas = _dasKTNNRepo.SchemaInfo.GetAllList();
                var allColumns = _dasKTNNRepo.ColumnTableInfo.GetAllList();
                model.tableInfo = allTables.FirstOrDefault(t => t.ID == TableInfoCondition.IDTable) ?? new TableInfo();
                model.schemaInfo = allSchemas.FirstOrDefault(t => t.ID == model.tableInfo.IDSchema);
                model.columnTables = allColumns.Where(t => t.IDTable == TableInfoCondition.IDTable);
                var columnTablesSearch = model.columnTables.Where(x => x.IsSearchable);
                result.TableInfo = model.tableInfo;

                var lstCondition = new List<CondParam>();
                if (!string.IsNullOrEmpty(TableInfoCondition.Keyword))
                {
                    foreach (var item in columnTablesSearch)
                    {
                        if (item.DbName.Contains("TenDanhMuc") || item.DbName.Contains("Ten"))
                        {
                            lstCondition.Add(new CondParam
                            {
                                FieldName = item.DbName,
                                Operator = CondOperator.Like,
                                Value = TableInfoCondition.Keyword.Trim()
                            });
                        }
                    }
                }

                var dataChild = _dynamicDBService.Search(lstCondition, model.schemaInfo.Code, model.tableInfo.DbName, page: page1);
                if (dataChild != null && dataChild.Rows.Count > 0)
                {
                    if (page1.PageIndex < page1.TotalPage)
                        IsViewLoagPage = IsViewLoagPage || true;

                    if (dataChild.Columns.Contains("TenDanhMuc"))
                    {
                        nameColumnTD = "TenDanhMuc";
                    }
                    else if (dataChild.Columns.Contains("Ten"))
                    {
                        nameColumnTD = "Ten";
                    }
                    for (int i = 0; i < dataChild.Rows.Count; i++)
                    {
                        result.TableLeftTrees.Add(new TableLeftTree
                        {
                            ID = dataChild.Rows[i]["ID"].ToString(),
                            IDSchema = model.schemaInfo.ID,
                            IDTable = model.tableInfo.ID,
                            TieuDe = dataChild.Rows[i][nameColumnTD].ToString(),
                        }); ;
                    }
                }
                result.IsLoadView = IsViewLoagPage;
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return new VMLeftTree();
            }
        }
        private int TotalRefer(IEnumerable<TableInfo> allTables, IEnumerable<ColumnTableInfo> allColumns
            , IEnumerable<SchemaInfo> allSchemas, int idtableref, string value)
        {
            var result = 0;
            var columnsRefer = allColumns.Where(t => t.IDTableRef == idtableref && t.DataType == (int)ColumnDataType.TableRef);
            if (Utils.IsNotEmpty(columnsRefer))
            {
                var idTablesRefer = columnsRefer.Select(t => t.IDTable).Distinct();
                if (Utils.IsNotEmpty(idTablesRefer))
                {
                    foreach (var idtable in idTablesRefer)
                    {
                        var tableRefer = allTables.First(t => t.ID == idtable);
                        var columnRefer = columnsRefer.FirstOrDefault(t => t.IDTable == idtable);
                        var schemaRefer = allSchemas.FirstOrDefault(t => t.ID == tableRefer.IDSchema);
                        var lstConditionRefer = new List<CondParam> {
                            new CondParam{
                                FieldName=columnRefer.DbName,
                                Operator=CondOperator.Equal,
                                Value=value??"0"
                            }
                            };
                        result = _dynamicDBService.Count(lstConditionRefer, schemaRefer.Code, tableRefer.DbName);// string.Join(",", model.columnTables.Select(n => n.DbName).ToList()));
                    }
                }
            }
            return result;
        }
        private int TotalChild(IEnumerable<TableInfo> allTables, IEnumerable<ColumnTableInfo> allColumns
          , IEnumerable<SchemaInfo> allSchemas, int idtable, string value)
        {
            var result = 0;
            var lstCondition = new List<CondParam>();
            var columnParent = allColumns.FirstOrDefault(t => t.DataType == (int)ColumnDataType.Parent && t.IDTable == idtable);
            if (Utils.IsNotEmpty(columnParent))
            {
                var tableRefer = allTables.First(t => t.ID == idtable);
                var schemaRefer = allSchemas.FirstOrDefault(t => t.ID == tableRefer.IDSchema);
                lstCondition.Add(new CondParam
                {
                    FieldName = $"{columnParent.DbName}_ID",
                    Operator = CondOperator.Equal,
                    Value = value
                });
                result = _dynamicDBService.Count(lstCondition, schemaRefer.Code, tableRefer.DbName);// string.Join(",", model.columnTables.Select(n => n.DbName).ToList()));
            }
            return result;
        }
        #endregion

        #region Fuction For InFormationLevel
        public async Task<VMLeftTree> CreateInInformationLevel(int IDTable)
        {
            var model = new VMLeftTree();
            try
            {
                var schemas = await GetDictSchema();
                model.Schemas = schemas.Select(n => new SelectListItem
                {
                    Value = n.Key.ToString(),
                    Text = n.Value,
                });
                var tables = await GetTables();
                model.Tables = tables.Where(n => n.ID != IDTable);
                model.TableInfo = _dasKTNNRepo.TableInfo.Get(IDTable) ?? new TableInfo() { Name = "Bảng chưa tạo" };
                model.SchemaInfo = _dasKTNNRepo.SchemaInfo.Get(model.TableInfo.IDSchema) ?? new SchemaInfo();
                model.columnTables = await GetAllColumnTables(model.TableInfo.ID) ?? new List<ColumnTableInfo>();
                model.columnTables = model.columnTables.Where(n => !n.IsSystem).ToList();
                //var dataDropdows = _dynamicDBService.GetListShowTable(model.SchemaInfo.Code, model.TableInfo.DbName);
                //var datasparent_refer = new List<TableOptionSelect>();
                //var colsparent_refer = model.columnTables.Where(t => t.DataType == (int)ColumnDataType.Parent || t.DataType == (int)ColumnDataType.TableRef).ToList() ?? new List<ColumnTableInfo>();
                //if (Utils.IsNotEmpty(colsparent_refer))
                //{
                //    var idchemas = colsparent_refer.Select(t => t.IDSchemaRef).Distinct().ToArray();
                //    var idtables = colsparent_refer.Select(t => t.IDTableRef).Distinct().ToArray();
                //    var tableRefer = await GetAllTablesByIDs(idtables) ?? new List<TableInfo>();
                //    var schemaRefer = await GetSchemas(idchemas) ?? new List<SchemaInfo>();
                //    foreach (var item in colsparent_refer)
                //    {
                //        var nameSchemaRefer = (schemaRefer.FirstOrDefault(t => t.ID == item.IDSchemaRef) ?? new SchemaInfo()).Code;
                //        var nameTableRefer = (tableRefer.FirstOrDefault(t => t.ID == item.IDTableRef) ?? new TableInfo()).DbName;
                //        var dataparent_refer = await GetDataTableRefer_Parent(nameSchemaRefer, nameTableRefer, item);
                //        if (Utils.IsNotEmpty(dataparent_refer))
                //            datasparent_refer.Add(dataparent_refer);
                //    }
                //}
                //model.DataParent_Refer = datasparent_refer;
                model.dataFirst = new DataTable();
                return model;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return model;
            }

        }
        #endregion

        #region Phan Tach du lieu 
        private void HopNhatDuLieu(Hashtable data, VMInputInfo model, Dictionary<string, string> recordData, int idRecord)
        {
            try
            {
                var recordUpdate = new Dictionary<string, string>();
                var ngayHieuLuc = Utils.GetDate(data, "NgayHieuLuc");
                if (ngayHieuLuc != null)
                {
                    //Update ban ghi goc
                    //recordUpdate.Add(DefaultField.HieuLucTu.ToString(), ngayHieuLuc.ToOracleStringDatetime());

                    //Update ban ghi dc gop

                    var idHopNhats = Utils.GetInts(data, "IDHopNhat");
                    var dicUpdate = new Dictionary<string, string>();
                    if (idHopNhats.IsNotEmpty())
                    {
                        dicUpdate.Add(CustomField.IDGop.ToString(), idRecord.ToOracleStringNumber());
                        dicUpdate.Add(DefaultField.HieuLucDen.ToString(), ngayHieuLuc.ToOracleStringDatetime());
                        //dicUpdate.Add(DefaultField.HieuLucTu.ToString(), recordData.GetValueOrDefault(DefaultField.HieuLucTu.ToString()));
                        foreach (var idHopNhat in idHopNhats)
                        {
                            UpdateRecord(model.schemaInfo.Code, model.tableInfo.DbName, dicUpdate, idHopNhat);
                        }
                        var dataGop = _dynamicDBService.GetListByField(model.schemaInfo.Code, model.tableInfo.DbName, CustomField.IDGop.ToString(), idRecord);
                        if (dataGop != null && dataGop.Rows.Count > 0)
                            recordUpdate.Add(CustomField.DataGop.ToString(), dataGop.ToOracleDataLookup());
                    }
                    UpdateRecord(model.schemaInfo.Code, model.tableInfo.DbName, recordUpdate, idRecord);
                }
            }
            catch
            {

            }
        }
        private bool InsertRecord(string schema, string table, Dictionary<string, string> clone)
        {
            var cols = new List<string>();
            var values = new List<string>();

            foreach (var item in clone)
            {
                cols.Add(item.Key);
                values.Add(item.Value);
            }

            var sql = $"INSERT INTO {schema}.{table} ({string.Join(",", cols)}) VALUES ({string.Join(",", values)}) ";
            return _dynamicDBService.InsertData(sql, null);
        }
        private bool UpdateRecord(string schema, string table, Dictionary<string, string> recordUpdate, int idRecord)
        {
            if (recordUpdate.IsEmpty() || idRecord == 0)
                return false;

            var fieldUpdate = new List<string>();
            foreach (var item in recordUpdate)
            {
                fieldUpdate.Add($"{item.Key} = {item.Value}");
            }
            var strUpdate = string.Join(",", fieldUpdate);

            var sql = $"UPDATE {schema}.{table} SET  {strUpdate} where ID={idRecord}";
            return _dynamicDBService.UpdateData(sql, null);
        }
        #endregion

        private async void SaveTableLink(Hashtable data, VMInputInfo model, int Idinserted, string nameTableLienKet, int action = 0)
        {
            try
            {
                var idTablesRef = Utils.GetInts(data, "IDTableRef");
                var idRecordsRef = Utils.GetInts(data, "IDRecordRef");
                if (action > 0)
                    _dynamicDBService.DeleteDataByIDRecordLink(model.schemaInfo.Code, nameTableLienKet, Idinserted);

                if (idTablesRef.IsNotEmpty() && idRecordsRef.IsNotEmpty())
                {
                    int i = 0;
                    var allSchemas = await _dasKTNNRepo.SchemaInfo.GetAllListAsync();
                    var allTables = await _dasKTNNRepo.TableInfo.GetAllListAsync(n => n.Status == (int)EnumCommon.Status.Active);
                    var tableInfo = model.tableInfo;
                    foreach (var itemTable in idTablesRef)
                    {
                        var idRecordRef = idRecordsRef[i];
                        if (idRecordRef <= 0 || itemTable <= 0)
                            continue;

                        var tableRef = allTables.FirstOrNewObj(n => n.ID == itemTable);
                        var schemaRef = allSchemas.FirstOrNewObj(n => n.ID == tableRef.IDSchema);

                        var dataRef = _dynamicDBService.GetFirstById(schemaRef.Code, tableRef.DbName, idRecordRef);
                        if (dataRef != null && dataRef.Rows.Count > 0)
                        {
                            var Parent = dataRef.Rows[0];
                            var currentData = _dynamicDBService.GetFirstById(schemaRef.Code, model.tableInfo.DbName, Idinserted);
                            var tableLinkData = new LinkTableModel()
                            {
                                IDSchema = schemaRef.ID,
                                SchemaName = schemaRef.Code,
                                IDTable = tableInfo.ID,
                                TableName = tableInfo.DbName,
                                IDRecord = Idinserted,
                                DataLookUp = currentData.ToOracleDataLookup(new List<string> { "ID", "TenDanhmuc", "MaDanhMuc" }),

                                IDSchemaRef = schemaRef.ID,
                                SchemaNameRef = schemaRef.Code,
                                IDTableRef = tableRef.ID,
                                TableNameRef = tableRef.DbName,

                                IDRecordRef = Utils.GetInt(Parent["ID"]?.ToString()),
                                DataLookUpRef = dataRef.ToOracleDataLookup(new List<string> { "ID", "TenDanhmuc", "MaDanhMuc" })
                            };
                            _dynamicDBService.InsertTableLinkData(schemaRef.Code, tableInfo.DbName, tableLinkData);
                        }

                        //var query = $"INSERT INTO {model.schemaInfo.Code}.{nameTableLienKet}" +
                        //    $" (IDSchema, IDTable, IDRecord, IDSchemaRef, IDTableRef, IDRecordRef, DataLookUp) VALUES (:IDSchema, :IDTable, :IDRecord, :IDSchemaRef, :IDTableRef, :IDRecordRef, :DataLookUp)";
                        //var param = new DynamicParameters();
                        //param.Add("IDSchema", model.tableInfo.IDSchema);
                        //param.Add("IDTable", model.tableInfo.ID);
                        //param.Add("IDRecord", Idinserted);
                        //param.Add("IDSchemaRef", table.IDSchema);
                        //param.Add("IDTableRef", (int)itemTabel);
                        //param.Add("IDRecordRef", (int)idRecordRef);
                        //param.Add("DataLookUp", json);
                        //_dynamicDBService.InsertData(query, param);
                        i++;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

        }

        #region Select2Helper

        public async Task<ServiceResult> GetTableRecords(Hashtable DATA)
        {
            var paging = new Pagination
            {
                PageIndex = 1,
                PageSize = 20
            };
            var optionsLabel = Utils.GetString(DATA, "DefaultOption");

            var id = Utils.GetInt(DATA, "ID");
            var IDTable = Utils.GetInt(DATA, "IDTable");
            var IDColumn = Utils.GetInt(DATA, "IDColumn");

            var tableInfo = _dasKTNNRepo.TableInfo.Get(IDTable) ?? new TableInfo();
            var schemaInfo = _dasKTNNRepo.SchemaInfo.Get(tableInfo.IDSchema) ?? new SchemaInfo();
            var columnInfo = _dasKTNNRepo.ColumnTableInfo.Get(IDColumn) ?? new ColumnTableInfo();
            if (tableInfo.DbName.IsEmpty() || schemaInfo.Code.IsEmpty())
            {
                return new ServiceResultSuccess();
            }
            //Set cột mặc định
            var dlValueName = SystemField.ID.ToString();
            var isCategoryTable = await TableIsCategory(IDTable);

            var dlTextName = isCategoryTable
                ? CategoryDefaultField.TenDanhMuc.ToString()
                : DefaultField.Ten.ToString();

            //lấy cột theo cấu hình tham chiếu
            if (columnInfo.IDColRef > 0)
            {
                var columnValueRef = _dasKTNNRepo.ColumnTableInfo.Get(columnInfo.IDColRef) ?? new ColumnTableInfo();
                if (columnValueRef.DbName.IsNotEmpty())
                {
                    //Cột giá trị
                    dlValueName = columnValueRef.DbName;
                }
            }
            if (columnInfo.IDColTextRef > 0)
            {
                var columnTextRef = _dasKTNNRepo.ColumnTableInfo.Get(columnInfo.IDColTextRef) ?? new ColumnTableInfo();
                if (columnTextRef.DbName.IsNotEmpty())
                {
                    //Cột hiển thị
                    dlTextName = columnTextRef.DbName;
                }
            }
            var term = Utils.GetString(DATA, "Term");

            if (id > 0)
            {
                //Hiển thị giá trị đã chọn
                var record = _dynamicDBService.GetFirstRowByField(schemaInfo.Code, tableInfo.DbName, dlValueName, id);

                var value = record[dlValueName];
                var text = record[dlTextName];
                var alias = isCategoryTable ? record[CategoryDefaultField.MaDanhMuc.ToString()] : string.Empty;
                return new ServiceResultSuccess()
                {
                    Data = new
                    {
                        Category = new OptionSelect()
                        {
                            Value = value?.ToString(),
                            Text = alias.IsNotEmpty() ? $"{alias} | {text}" : text?.ToString()
                        }
                    }
                };
            }
            else
            {
                //Lấy ds & lọc theo tên truyền vào
                var lstCondParam = new List<CondParam>();
                var items = new List<OptionSelect>();
                if (optionsLabel.IsNotEmpty())
                {
                    items.Add(new OptionSelect { Value = "0", Text = optionsLabel });
                }
                //items.Add(new OptionSelect { Value = "0", Text = "-- Chọn --" });
                if (term.IsNotEmpty())
                {

                    if (isCategoryTable)
                    {
                        lstCondParam.Add(new CondParam
                        {
                            FieldName = dlTextName,
                            Operator = CondOperator.Like,
                            Value = term,
                            Clause = "OR"
                        });
                        lstCondParam.Add(new CondParam
                        {
                            FieldName = CategoryDefaultField.MaDanhMuc.ToString(),
                            Operator = CondOperator.Like,
                            Value = term
                        });
                    }
                    else
                    {
                        lstCondParam.Add(new CondParam
                        {
                            FieldName = dlTextName,
                            Operator = CondOperator.Like,
                            Value = term
                        });
                    }
                }


                var data = _dynamicDBService.Search(lstCondParam, schemaInfo.Code, tableInfo.DbName, page: paging);
                if (data != null && data.Rows.Count > 0)
                {
                    for (int i = 0; i < data.Rows.Count; i++)
                    {
                        var item = data.Rows[i];
                        var value = item[dlValueName];
                        var text = item[dlTextName];
                        var alias = isCategoryTable ? item[CategoryDefaultField.MaDanhMuc.ToString()] : string.Empty;
                        items.Add(new OptionSelect()
                        {
                            Value = value?.ToString(),
                            Text = alias.IsNotEmpty() ? $"{alias} | {text}" : text?.ToString()
                        });
                    }
                }
                return new ServiceResultSuccess()
                {
                    Data = new
                    {
                        Categories = items
                    }
                };

            }
        }


        public async Task<ServiceResult> GetSeparateRecords(Hashtable DATA)
        {
            var paging = new Pagination
            {
                PageIndex = 1,
                PageSize = 20
            };
            var optionsLabel = Utils.GetString(DATA, "DefaultOption");

            var id = Utils.GetInt(DATA, "ID");
            var idExclude = Utils.GetInt(DATA, "IDExclude");
            var IDTable = Utils.GetInt(DATA, "IDTable");
            var IDColumn = Utils.GetInt(DATA, "IDColumn");

            var tableInfo = _dasKTNNRepo.TableInfo.Get(IDTable) ?? new TableInfo();
            var schemaInfo = _dasKTNNRepo.SchemaInfo.Get(tableInfo.IDSchema) ?? new SchemaInfo();
            var columnInfo = _dasKTNNRepo.ColumnTableInfo.Get(IDColumn) ?? new ColumnTableInfo();
            if (tableInfo.DbName.IsEmpty() || schemaInfo.Code.IsEmpty())
            {
                return new ServiceResultSuccess();
            }
            //Set cột mặc định
            var dlValueName = SystemField.ID.ToString();

            var dlTextName = await TableIsCategory(IDTable)
                ? CategoryDefaultField.TenDanhMuc.ToString()
                : DefaultField.Ten.ToString();

            //lấy cột theo cấu hình tham chiếu
            if (columnInfo.IDColRef > 0)
            {
                var columnValueRef = _dasKTNNRepo.ColumnTableInfo.Get(columnInfo.IDColRef) ?? new ColumnTableInfo();
                if (columnValueRef.DbName.IsNotEmpty())
                {
                    //Cột giá trị
                    dlValueName = columnValueRef.DbName;
                }
            }
            if (columnInfo.IDColTextRef > 0)
            {
                var columnTextRef = _dasKTNNRepo.ColumnTableInfo.Get(columnInfo.IDColTextRef) ?? new ColumnTableInfo();
                if (columnTextRef.DbName.IsNotEmpty())
                {
                    //Cột hiển thị
                    dlTextName = columnTextRef.DbName;
                }
            }
            var term = Utils.GetString(DATA, "Term");

            if (id > 0)
            {
                //Hiển thị giá trị đã chọn
                var record = _dynamicDBService.GetFirstRowByField(schemaInfo.Code, tableInfo.DbName, dlValueName, id);

                var value = record[dlValueName];
                var text = record[dlTextName];
                return new ServiceResultSuccess()
                {
                    Data = new
                    {
                        Category = new OptionSelect() { Value = value?.ToString(), Text = text?.ToString() }
                    }
                };
            }
            else
            {
                //Lấy ds & lọc theo tên truyền vào
                var lstCondParam = new List<CondParam>();
                var items = new List<OptionSelect>();
                if (optionsLabel.IsNotEmpty())
                {
                    items.Add(new OptionSelect { Value = "0", Text = optionsLabel });
                }
                //items.Add(new OptionSelect { Value = "0", Text = "-- Chọn --" });
                if (term.IsNotEmpty())
                    lstCondParam.Add(new CondParam
                    {
                        FieldName = dlTextName,
                        Operator = CondOperator.Like,
                        Value = term
                    });

                if (idExclude > 0)
                {
                    lstCondParam.Add(new CondParam
                    {
                        FieldName = SystemField.ID.ToString(),
                        Operator = CondOperator.NotEqual,
                        Value = idExclude.ToOracleStringNumber()
                    });

                    lstCondParam.Add(new CondParam
                    {
                        FieldName = CustomField.IDTach.ToString(),
                        Operator = CondOperator.NotEqual,
                        Value = idExclude.ToOracleStringNumber()
                    });
                }

                var data = _dynamicDBService.Search(lstCondParam, schemaInfo.Code, tableInfo.DbName, page: paging);
                if (data != null && data.Rows.Count > 0)
                {
                    for (int i = 0; i < data.Rows.Count; i++)
                    {
                        var item = data.Rows[i];
                        var value = item[dlValueName];
                        var text = item[dlTextName];
                        items.Add(new OptionSelect() { Value = value?.ToString(), Text = text?.ToString() });
                    }
                }
                return new ServiceResultSuccess()
                {
                    Data = new
                    {
                        Categories = items
                    }
                };

            }
        }


        public async Task<bool> TableIsCategory(int idTable)
        {
            return await _dasKTNNRepo.ColumnTableInfo.AnyAsync(n => n.DbName == CategoryDefaultField.TenDanhMuc.ToString() && n.IDTable == idTable);

        }
        #endregion

        #region Sync Data

        public async Task<VMSyncData> GetColumnMapper(VMSyncData vmSyncData)
        {
            if (!vmSyncData.IDTable.HasValue)
                throw new LogicException("Vui lòng chọn bảng dữ liệu cần đồng bộ");

            var tableInfoTarget = await _dasKTNNRepo.TableInfo.GetAsync(vmSyncData.IDTable ?? 0);
            var tableInfoSource = await _dasKTNNRepo.TableInfo.GetAsync(vmSyncData.IDTableCurrent ?? 0);
            if (tableInfoTarget == null || tableInfoTarget.Status == (int)EnumCommon.Status.InActive)
                throw new LogicException("Bảng dữ liệu này hiện không tồn tại hoặc đã bị xóa");
            if (tableInfoSource == null)
                throw new LogicException("Bảng dữ liệu này hiện không tồn tại hoặc đã bị xóa");

            var model = new VMSyncData();
            var tableColumnsTarget = await _tableInfoServices.GetColumnByIDTable(vmSyncData.IDTable.Value);
            var tableColumnsSource = await _tableInfoServices.GetColumnByIDTable(vmSyncData.IDTableCurrent.Value);
            var columns = tableColumnsSource.Where(x => x.IsSystem == false && x.IsIdentity == false).ToList();

            var vmColumnsMapper = new List<VMSyncDataColumnMapper>();
            foreach (var tableColumnTarget in tableColumnsTarget)
            {
                if (tableColumnTarget.IsSystem)
                    continue;
                var columnMapper = new VMSyncDataColumnMapper()
                {
                    ColumnTableTarget = tableColumnTarget,
                };

                var column = columns.FirstOrDefault(n => n.DbName.ToLower().Trim() == tableColumnTarget.DbName.ToLower().Trim()) ?? new ColumnTableInfo();
                columnMapper.IDColumn = column.ID;
                columnMapper.NameColumn = column.Name;
                vmColumnsMapper.Add(columnMapper);
            }
            model.ColumnTableSources = columns;
            model.ColumnMappers = vmColumnsMapper;
            model.IDRecords = vmSyncData.IDRecords;
            return model;
        }
        #endregion

        #region Sycn dong bo

        public async Task<VMSyncData> SyncData(int? idTable)
        {
            var model = new VMSyncData();
            model.Tables = await _tableInfoServices.GetTables();
            model.Tables = model.Tables.Where(n => !n.IsTempTable).ToList();
            model.IDTableCurrent = idTable;
            return model;
        }

        public async Task<ServiceResult> SyncRecord(VMSyncData vmSyncData)
        {
            try
            {
                var rs = await SyncEachRecord(vmSyncData);
                if (rs.IsSuccess)
                    return new ServiceResultSuccess(rs.Message);
                return new ServiceResultError(rs.Message);
            }
            catch (LogicException ex)
            {
                return new ServiceResultError(ex.Message);
            }
            catch (Exception ex)
            {
                Guid.NewGuid().ToString();
                _logger.LogError(ex);
                return new ServiceResultError(ex.Message);
            }
        }

        public async Task<ServiceResult> Synchronization(VMSyncData vmSyncData, Dictionary<int, int> columns)
        {
            try
            {
                var tableInfoTarget = await _dasKTNNRepo.TableInfo.GetAsync(vmSyncData.IDTable ?? 0);
                var tableInfoSource = await _dasKTNNRepo.TableInfo.GetAsync(vmSyncData.IDTableCurrent ?? 0);
                if (tableInfoTarget == null || tableInfoTarget.Status == (int)EnumCommon.Status.InActive)
                    throw new LogicException("Bảng dữ liệu này hiện không tồn tại hoặc đã bị xóa");
                if (tableInfoSource == null)
                    throw new LogicException("Bảng dữ liệu này hiện không tồn tại hoặc đã bị xóa");

                var schemaTarget = await _dasKTNNRepo.SchemaInfo.GetAsync(tableInfoTarget.IDSchema);
                var schemaSource = await _dasKTNNRepo.SchemaInfo.GetAsync(tableInfoTarget.IDSchema);

                if (schemaTarget == null)
                    throw new LogicException("CSDL không tồn tại");
                if (schemaSource == null)
                    throw new LogicException("CSDL không tồn tại");

                var tableColumns = await _tableInfoServices.GetColumnByIDTable(vmSyncData.IDTable.Value);

                var rs = await SycnData(vmSyncData.IDTableCurrent.Value, vmSyncData.IDTable.Value, vmSyncData.IDRecords, columns);
                if (rs.IsSuccess)
                    return new ServiceResultSuccess(rs.Message);
                return new ServiceResultError(rs.Message);
            }
            catch (LogicException ex)
            {
                return new ServiceResultError(ex.Message);
            }
            catch (Exception ex)
            {
                Guid.NewGuid().ToString();
                _logger.LogError(ex);
                return new ServiceResultError(ex.Message);
            }
        }

        public async Task<VMInputInfo> SyncEachRecord(VMSyncData vmSyncData)
        {
            try
            {
                var model = new VMInputInfo();

                var tableSource = _dasKTNNRepo.TableInfo.Get(vmSyncData.IDTable) ?? new TableInfo() { Name = "Bảng chưa tạo" };
                if (tableSource.ID <= 0)
                {
                    model.IsSuccess = false;
                    model.Message = "Bảng không tồn tại";
                    return model;
                }
                var schemaSource = _dasKTNNRepo.SchemaInfo.Get(tableSource.IDSchema) ?? new SchemaInfo();

                var data = _dynamicDBService.GetFirstById(schemaSource.Code, tableSource.DbName, vmSyncData.IDRecord);
                if (data.Columns.Contains("DADONGBO"))
                {
                    var isdongbo = data.Rows[0]["DADONGBO"].ToString();
                    if (isdongbo == "1")
                    {
                        model.IsSuccess = false;
                        model.Message = $"Dữ liệu đã được đồng bộ";
                        return model;
                    }
                }

                var schemeNameTarget = "";
                var tableNameTarget = "";
                if (data.Columns.Contains("SCHEMANAME"))
                    schemeNameTarget = data.Rows[0]["SCHEMANAME"].ToString().ToUpper();
                if (data.Columns.Contains("TABLENAME"))
                    tableNameTarget = data.Rows[0]["TABLENAME"].ToString().ToUpper();

                var schemaTarget = (from col in _dasKTNNRepo.SchemaInfo.GetAll()
                                    where schemeNameTarget == col.Code.ToUpper()
                                    select col).FirstOrDefault();

                var tableTargets = await _dasKTNNRepo.TableInfo.GetAllListAsync() ?? new List<TableInfo>();
                var tableTarget = tableTargets.Where(x => x.IDSchema == schemaTarget.ID && x.DbName.ToUpper() == (tableNameTarget)).FirstOrDefault() ?? new TableInfo();

                if (tableTarget.ID <= 0)
                {
                    model.IsSuccess = false;
                    model.Message = "Bảng cần đồng bộ không tồn tại";
                    return model;
                }

                var datajson = "";
                var urlCallBack = "";

                if (data.Columns.Contains("DATAJSON"))
                    datajson = data.Rows[0]["DATAJSON"].ToString();

                if (data.Columns.Contains("URLCALLBACK"))
                    urlCallBack = data.Rows[0]["URLCALLBACK"].ToString();



                if (string.IsNullOrEmpty(urlCallBack))
                {
                    model.IsSuccess = false;
                    model.Message = $"Không tìm thấy đường dẫn UrlCallback";
                    return model;
                }
                else if (!urlCallBack.ToUpper().Contains("OLDID"))
                {
                    model.IsSuccess = false;
                    model.Message = $"Không tìm thấy oldid trong đường dẫn UrlCallback";
                    return model;
                }

                var dataResult = JsonConvert.DeserializeObject<Dictionary<string, string>>(datajson);

                var columnbyAllTargets = await GetAllColumnTables(tableTarget.ID);
                var columnTableTargetSystems = columnbyAllTargets.Where(n => n.IsSystem == true).ToList();
                var columnTableTargets = columnbyAllTargets.Where(n => n.IsSystem == false).ToList();
                var columnTableTargetsNotNull = columnbyAllTargets.Where(n => n.IsRequired == true).ToList();
                #region Check tự tăng


                var identityCol = columnTableTargets.FirstOrDefault(n => n.IsIdentity && !n.IsSystem);
                if (identityCol != null && dataResult.ContainsKey(identityCol.DbName))
                {
                    var identityColVal = dataResult[identityCol.DbName];
                    var isIdentity = identityColVal.IsEmpty() || identityColVal == KTNNConst.IdentityValue;
                    if (isIdentity)
                    {
                        var isAllowIdentity = CheckedColAllowIdentity(schemaTarget, tableTarget, identityCol);
                        if (!isAllowIdentity)
                        {
                            model.IsSuccess = false;
                            model.Message = $"Bảng {tableTarget.Name} không thể tự động sinh mã";
                            return model;
                        }
                        //Set là giá trị để trigger SQL nhận diện set tự tăng
                        dataResult[identityCol.DbName] = KTNNConst.IdentityValue;
                    }
                }

                //Set tự tăng cho các trường thêm mới
                var identityCols = columnTableTargets.Where(n => n.IsIdentity && !n.IsSystem && !n.IsDefault);
                if (identityCols.IsNotEmpty())
                {
                    foreach (var item in identityCols)
                    {
                        //Set là giá trị để trigger SQL nhận diện set tự tăng
                        dataResult[item.DbName] = DynamicDBHelper.GetIdentityValue(item);
                    }
                }

                #endregion


                #region CheckTonTai
                if (columnTableTargets.Any(n => n.IsPrimaryKey))
                {
                    var columnPrimaryKey = columnTableTargets.FirstOrNewObj(n => n.IsPrimaryKey).DbName;

                    var datacheckFirst = _dynamicDBService.GetFirstByField(schemaTarget.Code, tableTarget.DbName, columnPrimaryKey, data.Rows[0][columnPrimaryKey]);
                    if (datacheckFirst.Rows.Count > 0)
                    {
                        model.IsSuccess = false;
                        model.Message = $"Dữ liệu của bảng {tableTarget.Name} đã tồn tại";
                        return model;

                    }
                }
                #endregion

                var qrColumnName = new List<string>();
                var values = new List<string>();
                foreach (var item in columnTableTargetSystems)
                {
                    switch (item.DataType)
                    {
                        case (int)ColumnDataType.DateTime:
                            qrColumnName.Add(item.DbName);
                            values.Add(DateTime.Now.ToOracleStringDatetime());
                            break;
                        case (int)ColumnDataType.TableUser:
                            qrColumnName.Add(item.DbName);
                            values.Add($"'{_userPrincipalService.UserId}'");
                            break;
                    }
                }

                int i = 0;
                foreach (var item in dataResult)
                {
                    if (item.Key.Contains("ID"))
                        continue;

                    var rs = columnTableTargets.FirstOrDefault(n => n.DbName.ToUpper() == item.Key.ToUpper());
                    if (rs != null)
                    {
                        var datavalue = item.Value;
                        if (rs.DbName.ToLower() == SystemField.TrangThai.ToString().ToLower())
                        {
                            datavalue = "1";
                        }
                        if (rs.DbName.ToLower() == DefaultField.KieuPhatSinh.ToString().ToLower() && datavalue.IsEmpty())
                        {
                            datavalue = CreateTypeConst.MacDinh.ToString();
                        }

                        switch (rs.DataType)
                        {
                            case (int)ColumnDataType.DateTime:
                                qrColumnName.Add(rs.DbName);
                                if (Utils.IsNotEmpty(datavalue))
                                {
                                    if (Utils.IsDate(Convert.ToDateTime(datavalue)))
                                    {
                                        var date = Convert.ToDateTime(datavalue);
                                        values.Add(date.ToOracleStringDatetime());
                                    }
                                    else
                                    {
                                        if (rs.IsRequired)
                                            values.Add(DateTime.Now.ToOracleStringDatetime());
                                        else
                                            values.Add("NULL");
                                    }
                                }
                                else
                                {
                                    if (rs.IsRequired)
                                        values.Add(DateTime.Now.ToOracleStringDatetime());
                                    else
                                        values.Add("NULL");
                                }
                                break;
                            case (int)ColumnDataType.Decimal:
                            case (int)ColumnDataType.Integer:
                            case (int)ColumnDataType.Boolean:
                                qrColumnName.Add(rs.DbName);
                                if (Utils.IsNotEmpty(datavalue))
                                {
                                    if (Utils.IsNumber(datavalue.ToString()))
                                    {
                                        values.Add($"'{datavalue}'");
                                    }
                                    else
                                        values.Add("'0'");
                                }
                                else
                                    values.Add("'0'");
                                break;
                            default:
                                qrColumnName.Add(rs.DbName);
                                if (Utils.IsNotEmpty(datavalue))
                                {
                                    values.Add($"N'{datavalue}'");
                                }
                                else
                                {
                                    if (rs.IsRequired)
                                        values.Add($"N'{0}'");
                                    else
                                        values.Add("NULL");
                                }
                                break;
                        }
                        i++;
                    }
                }

                if (tableTarget.IsRequireApprove)
                {
                    qrColumnName.Add("DADUYET");
                    values.Add($"'{Status.DaDuyet}'");
                    qrColumnName.Add("NGUOIDUYET");
                    values.Add($"'{_userPrincipalService.UserId}'");
                    qrColumnName.Add("NGAYDUYET");
                    values.Add(DateTime.Now.ToOracleStringDatetime());
                }
                if (!qrColumnName.Exists(t => t.ToUpper() == "MADANHMUC"))
                {
                    qrColumnName.Add("MADANHMUC");
                    values.Add($"'{KTNNConst.IdentityValue}'");
                }

                if (!qrColumnName.Exists(t => t.ToUpper() == SystemField.TrangThai.ToString().ToUpper()))
                {
                    qrColumnName.Add(SystemField.TrangThai.ToString().ToUpper());
                    values.Add($"'1'");
                }
                int newId = 0;
                var column = string.Join(",", qrColumnName);
                var valuedata = string.Join(",", values);
                var sql = $"INSERT INTO {schemaTarget.Code}.{tableTarget.DbName} ({column}) VALUES ({valuedata}) returning ID into :Id";
                var sqlUpdate = $"Update {schemaSource.Code}.{tableSource.DbName}  set DADONGBO='1' WHERE ID={vmSyncData.IDRecord}";
                var param = new DynamicParameters();

                //_logger.LogDebug(sql);
                //_logger.LogDebug(sqlUpdate);

                param.Add(name: "Id", dbType: DbType.Int32, direction: ParameterDirection.Output);
                if (_dynamicDBService.InsertData(sql, param, ref newId))
                {
                    if (newId > 0)
                        _dynamicDBService.UpdateData(sqlUpdate, null);
                    else
                    {
                        model.IsSuccess = false;
                        model.Message = $"Đồng bộ dữ liệu không thành công";
                        return model;
                    }
                }
                else
                {
                    model.IsSuccess = false;
                    model.Message = $"Đồng bộ dữ liệu không thành công";
                    return model;
                }

                if (!string.IsNullOrEmpty(urlCallBack))
                {
                    var result = CallUrlCallBack(urlCallBack, newId, "approve"); // approve: giá trị truyền cho biến status trong UrlCallBack khi thực hiện action đồng bộ
                    if (!result)
                    {
                        model.IsSuccess = true;
                        model.Message = "Lỗi khi gọi tới đường dẫn URL CallBack";
                    }
                }

                model.IsSuccess = true;
                model.Message = "Đồng bộ dữ liệu thành công";
                return model;
            }
            catch (Exception ex)
            {
                _logger.LogError("Đồng bộ: " + ex.ToString());
                return new VMInputInfo()
                {
                    IsSuccess = false,
                    Message = "Error! Hệ thống đã gặp sự cố khi thực hiện quá trình đồng bộ"
                };
            }
        }

        private async Task<VMInputInfo> SycnData(int IDTableSoucre, int IDTableTarget, int[] ids, Dictionary<int, int> columns)
        {
            var model = new VMInputInfo();
            if (ids.Count() <= 0 || Utils.IsEmpty(ids))
            {
                model.IsSuccess = false;
                model.Message = "Không có dữ liệu để đồng bộ";
                return model;
            }
            if (columns.Count() <= 0)
            {
                model.IsSuccess = false;
                model.Message = "Các trường dữ liệu đang map không khớp nhau";
                return model;
            }
            var ID = IDTableSoucre;
            var IDClone = IDTableTarget;
            var tableInfo = _dasKTNNRepo.TableInfo.Get(ID) ?? new TableInfo() { Name = "Bảng chưa tạo" };
            if (tableInfo.ID <= 0)
            {
                model.IsSuccess = false;
                model.Message = "Bảng không tồn tại";
                return model;
            }
            var tableInfoTarget = _dasKTNNRepo.TableInfo.Get(IDClone) ?? new TableInfo() { Name = "Bảng chưa tạo" };
            if (tableInfoTarget.ID <= 0)
            {
                model.IsSuccess = false;
                model.Message = "Bảng cần đồng bộ không tồn tại";
                return model;
            }
            var schemaInfo = _dasKTNNRepo.SchemaInfo.Get(tableInfo.IDSchema) ?? new SchemaInfo();
            var schemaInfoTarget = _dasKTNNRepo.SchemaInfo.Get(tableInfoTarget.IDSchema) ?? new SchemaInfo();
            var sqlInserts = new List<string>();
            var sqlUpdates = new List<string>();
            foreach (var iddata in ids)
            {
                var dataFirst = _dynamicDBService.GetFirstById(schemaInfo.Code, tableInfo.DbName, iddata);

                if (dataFirst.Columns.Contains("DADONGBO"))
                {
                    var isdongbo = dataFirst.Rows[0]["DADONGBO"].ToString();
                    //if (isdongbo == "1")
                    //{
                    //    continue;
                    //}
                }
                else
                    continue;
                var qrColumnName = new List<string>();
                #region Soucre
                var columnbyAlls = await GetAllColumnTables(tableInfo.ID);
                columnbyAlls = columnbyAlls.Where(n => n.IsIdentity == false).ToList();
                var columnTableSoucres = columnbyAlls.ToList();
                #endregion
                #region Target
                var columnbyAllTargets = await GetAllColumnTables(tableInfoTarget.ID);
                columnbyAllTargets = columnbyAllTargets.Where(n => n.IsIdentity == false).ToList();
                var columnTableTargetSystems = columnbyAllTargets.Where(n => n.IsSystem == true).ToList();
                var columnTableTargets = columnbyAllTargets.Where(n => n.IsSystem == false).ToList();
                #endregion
                var ValueNamePrimaryKey = "";
                if (columnTableSoucres.Any(n => n.IsPrimaryKey))
                {
                    var columnPrimaryKey = columnTableSoucres.FirstOrDefault(n => n.IsPrimaryKey).DbName;
                    if (dataFirst.Columns.Contains(columnPrimaryKey))
                    {
                        ValueNamePrimaryKey = dataFirst.Rows[0][columnPrimaryKey].ToString();
                    }
                }
                #region CheckTonTai
                if (columnTableTargets.Any(n => n.IsPrimaryKey) && columnTableSoucres.Any(n => n.IsPrimaryKey))
                {
                    var columnPrimaryKey = columnTableTargets.FirstOrDefault(n => n.IsPrimaryKey).DbName;
                    var datacheckFirst = _dynamicDBService.GetFirstByField(schemaInfoTarget.Code, tableInfoTarget.DbName, columnPrimaryKey, dataFirst.Rows[0][columnPrimaryKey]);
                    if (datacheckFirst.Rows.Count > 0)
                    {
                        model.IsSuccess = false;
                        model.Message = $"Dữ liệu {ValueNamePrimaryKey} của bảng {tableInfoTarget.Name} đã tồn tại";
                        return model;

                    }
                }
                #endregion
                #region Check validate
                foreach (var item in columnTableTargets.Where(n => n.IsRequired || !n.IsNullable).ToList())
                {
                    var idtarget = 0;
                    bool hasValue = columns.TryGetValue(item.ID, out idtarget);
                    if (!hasValue)
                    {
                        model.IsSuccess = false;
                        model.Message = $"Trường dữ liệu {item.Name} của bảng {tableInfoTarget.Name} có bắt buộc không được trống dữ liệu";
                        return model;
                    }
                }
                #endregion
                var values = new List<string>();
                foreach (var item in columnTableTargetSystems)
                {
                    switch (item.DataType)
                    {
                        case (int)ColumnDataType.DateTime:
                            qrColumnName.Add(item.DbName);
                            values.Add(DateTime.Now.ToOracleStringDatetime());
                            break;
                        case (int)ColumnDataType.TableUser:
                            qrColumnName.Add(item.DbName);
                            values.Add($"'{_userPrincipalService.UserId}'");
                            break;

                    }
                }
                int i = 0;
                foreach (var col in columns)
                {
                    var idcolumnSoucre = col.Value;
                    var columnsoucre = columnTableSoucres.FirstOrDefault(n => n.ID == idcolumnSoucre);
                    var item = columnTableTargets.FirstOrDefault(n => n.ID == col.Key);
                    if (Utils.IsEmpty(columnsoucre))
                    {
                        model.IsSuccess = false;
                        model.Message = "Dữ liệu cột không tồn tại";
                        return model;
                    }
                    var datavalue = dataFirst.Rows[0][columnsoucre.DbName];
                    switch (item.DataType)
                    {
                        case (int)ColumnDataType.DateTime:
                            qrColumnName.Add(item.DbName);
                            if (Utils.IsNotEmpty(datavalue))
                            {
                                if (Utils.IsDate((DateTime?)datavalue))
                                {
                                    var date = (DateTime?)datavalue;
                                    values.Add(date.ToOracleStringDatetime());
                                }
                                else
                                {
                                    if (item.IsRequired)
                                        values.Add(DateTime.Now.ToOracleStringDatetime());
                                    else
                                        values.Add("NULL");
                                }
                            }
                            else
                            {
                                if (item.IsRequired)
                                    values.Add(DateTime.Now.ToOracleStringDatetime());
                                else
                                    values.Add("NULL");
                            }
                            break;
                        case (int)ColumnDataType.Decimal:
                        case (int)ColumnDataType.Integer:
                        case (int)ColumnDataType.Boolean:
                            if (Utils.IsNotEmpty(datavalue))
                            {
                                if (Utils.IsNumber(datavalue.ToString()))
                                {
                                    qrColumnName.Add(item.DbName);
                                    values.Add($"'{datavalue}'");
                                }
                                else
                                    values.Add("'0'");
                            }
                            else
                                values.Add("'0'");
                            break;
                        default:
                            if (Utils.IsNotEmpty(datavalue))
                            {
                                qrColumnName.Add(item.DbName);
                                values.Add($"N'{datavalue}'");
                            }
                            else
                            {
                                if (item.IsRequired)
                                    values.Add($"N'{0}'");
                                else
                                    values.Add("NULL");
                            }
                            break;
                    }
                    i++;
                }
                if (tableInfoTarget.IsRequireApprove)
                {
                    qrColumnName.Add("DADUYET");
                    values.Add($"'{Status.DaDuyet}'");
                    qrColumnName.Add("NGUOIDUYET");
                    values.Add($"'{_userPrincipalService.UserId}'");
                    qrColumnName.Add("NGAYDUYET");
                    values.Add(DateTime.Now.ToOracleStringDatetime());
                }
                var column = string.Join(",", qrColumnName);
                var valuedata = string.Join(",", values);
                var sql = $"INSERT INTO {schemaInfoTarget.Code}.{tableInfoTarget.DbName} ({column}) VALUES ({valuedata})";
                var sqlUpdate = $"Update {schemaInfo.Code}.{tableInfo.DbName}  set DADONGBO='1' WHERE ID={iddata}";
                if (_dynamicDBService.InsertData(sql, null))
                {
                    _dynamicDBService.UpdateData(sqlUpdate, null);
                }
                else
                {
                    model.IsSuccess = false;
                    model.Message = $"Đồng bộ dữ liệu {ValueNamePrimaryKey} không thành công";
                    return model;
                }
            }
            model.IsSuccess = true;
            model.Message = "Đồng bộ dữ liệu thành công";
            return model;
        }

        public async Task<VMSyncData> CheckMapColumn(int idTabel, int idRecord)
        {
            var model = new VMSyncData();
            var schemeNameTarget = "";
            var tableNameTarget = "";

            var tableSource = await _dasKTNNRepo.TableInfo.GetAsync(idTabel) ?? new TableInfo() { Name = "Bảng chưa tạo" };
            var schemaSource = await _dasKTNNRepo.SchemaInfo.GetAsync(tableSource.IDSchema) ?? new SchemaInfo();

            var data = _dynamicDBService.GetFirstById(schemaSource.Code, tableSource.DbName, idRecord);

            if (data.Columns.Contains("SCHEMANAME"))
                schemeNameTarget = data.Rows[0]["SCHEMANAME"].ToString().ToUpper();
            if (data.Columns.Contains("TABLENAME"))
                tableNameTarget = data.Rows[0]["TABLENAME"].ToString().ToUpper();

            var schemaTarget = (from col in _dasKTNNRepo.SchemaInfo.GetAll()
                                where schemeNameTarget == (col.Code.ToUpper())
                                select col).FirstOrDefault();

            var tableTargets = await _dasKTNNRepo.TableInfo.GetAllListAsync() ?? new List<TableInfo>();
            var tableTarget = tableTargets.Where(x => x.IDSchema == schemaTarget.ID && x.DbName.ToUpper() == (tableNameTarget)).FirstOrDefault() ?? new TableInfo();

            var datajson = "";

            if (data.Columns.Contains("DATAJSON"))
                datajson = data.Rows[0]["DATAJSON"].ToString();

            var dataResult = JsonConvert.DeserializeObject<Dictionary<string, string>>(datajson);

            var lstColumn = new List<string>();
            var columnbyAllTargets = await GetAllColumnTables(tableTarget.ID);
            columnbyAllTargets = columnbyAllTargets.Where(n => n.IsIdentity == false).ToList();
            var columnTableTargetSystems = columnbyAllTargets.Where(n => n.IsSystem == true).ToList();
            var columnTableTargets = columnbyAllTargets.Where(n => n.IsSystem == false).ToList();
            foreach (var item in dataResult)
            {
                if (item.Key.Contains("ID"))
                    continue;

                var rs = columnbyAllTargets.FirstOrDefault(n => n.DbName.ToUpper() == item.Key);
                if (rs == null)
                    lstColumn.Add(item.Key);
            }
            model.IDTable = tableSource.ID;
            model.IDRecord = idRecord;
            model.ColumnNotMap = lstColumn;
            model.ColumnTableTargets = columnTableTargets;
            return model;
        }
        #endregion

        #region Reject data
        public async Task<ServiceResult> Reject(VMSyncData vmSyncData)
        {
            var tableInfo = await _dasKTNNRepo.TableInfo.GetAsync(vmSyncData.IDTable) ?? new TableInfo() { Name = "Bảng chưa tạo" };
            if (tableInfo.ID <= 0)
                return new ServiceResultError("Không tìm thấy bảng dữ liệu hiện tại");

            var schemaInfo = await _dasKTNNRepo.SchemaInfo.GetAsync(tableInfo.IDSchema) ?? new SchemaInfo();

            var data = _dynamicDBService.GetFirstById(schemaInfo.Code, tableInfo.DbName, vmSyncData.IDRecord);
            if (data.Columns.Contains("DADONGBO"))
            {
                var isdongbo = data.Rows[0]["DADONGBO"].ToString();
                if (isdongbo == "1")
                    return new ServiceResultError("Dữ liệu đã được đồng bộ");
            }

            var urlCallBack = "";
            if (data.Columns.Contains("URLCALLBACK"))
                urlCallBack = data.Rows[0]["URLCALLBACK"].ToString();

            if (string.IsNullOrEmpty(urlCallBack))
                return new ServiceResultError("Không tìm thấy đường dẫn URL CallBack");

            var sqlUpdate = $"Update {schemaInfo.Code}.{tableInfo.DbName}  set DADONGBO='2', REASON='{vmSyncData.Reason}' WHERE ID={vmSyncData.IDRecord}";
            if (_dynamicDBService.UpdateData(sqlUpdate, null))
            {
                var rs = CallUrlCallBack(urlCallBack, 0, "reject", vmSyncData.Reason);
                if (!rs)
                    return new ServiceResultError("Lỗi khi gọi đường dẫn URL CallBack");
            }

            return new ServiceResultSuccess("Từ chối dữ liệu thành công");
        }
        #endregion

        public bool CallUrlCallBack(string UrlCallBack, int? newID, string action, string reason = "")
        {
            try
            {
                var url = "";
                if (newID > 0)
                    url = UrlCallBack + $"?newID={newID}" + $"&status={action}";

                url = UrlCallBack + $"&status={action}" + $"&status={reason}";

                var client = new RestClient(url);
                var request = new RestRequest(url, Method.Get);
                RestResponse response = client.Execute(request);
                if (response != null && response.ErrorException != null)
                {
                    _logger.LogError("URL CallBack: " + response.ErrorException);
                    return false;
                }

                if (response.StatusCode == System.Net.HttpStatusCode.OK || response.IsSuccessful)
                {
                    _logger.LogInfo("URL CallBack Structure: " + url);
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                _logger.LogError("URL CallBack: " + ex.ToString());
                return false;
            }
        }

        public List<DataTable> GetRecord(SchemaInfo schema, TableInfo tableInfo, int[] idRecord)
        {
            try
            {
                var data = _dynamicDBService.GetFirstByIds(schema.Code, $"{tableInfo.DbName}", idRecord);
            }
            catch (Exception ex)
            {
                //Ignore
            }
            return new List<DataTable>();

        }
        /// <summary>
        /// Lấy ra danh sách các phần tự liên kết B-A với với bảng hiện tại
        /// </summary>
        /// <param name="page">page index</param>
        /// <param name="schemaname"> schema hiện tại</param>
        /// <param name="tablename">table hiện tại</param>
        private VMLienKetNguoc GetLienKetNguoc(int page, SchemaInfo schema, TableInfo tablelienket, string IDRecordRef)
        {
            try
            {
                var result = new VMLienKetNguoc();
                var pageLink = new Pagination
                {
                    PageIndex = page,
                    PageSize = 10
                };
                var lstConditionRefer = new List<CondParam>();
                lstConditionRefer.Add(new CondParam
                {
                    FieldName = "IDTABLEREF",
                    Operator = CondOperator.Equal,
                    Value = tablelienket.ID
                });
                lstConditionRefer.Add(new CondParam
                {
                    FieldName = "IDRecordRef",
                    Operator = CondOperator.Equal,
                    Value = IDRecordRef
                });
                var dataRefer = _dynamicDBService.Search(lstConditionRefer, schema.Code, $"{schema.Code}_LIENKET", page: pageLink);// string.Join(",", model.columnTables.Select(n => n.DbName).ToList()));
                if (dataRefer != null && dataRefer.Rows.Count > 0)
                {
                    var lienkets = new List<LienKetNguoc>();
                    for (int i = 0; i < dataRefer.Rows.Count; i++)
                    {
                        var json = dataRefer.Rows[i]["DATALOOKUP"].ToString();
                        lienkets.Add(new LienKetNguoc
                        {
                            MaDanhMuc = Utils.GetValueDataLookup(json, "MADANHMUC"),
                            TableName = dataRefer.Rows[i]["TABLENAME"].ToString(),
                            TenDanhMuc = Utils.GetValueDataLookup(json, "TENDANHMUC") == "" ? Utils.GetValueDataLookup(json, "TEN") : Utils.GetValueDataLookup(json, "TENDANHMUC"),
                        });
                    }
                    result.LienKets = lienkets;
                    result.PageIndex = page;
                    result.IsLoadView = page < pageLink.TotalPage;
                }
                return result;
            }
            catch (Exception)
            {
                return new VMLienKetNguoc();
                throw;
            }
        }
        public async Task<VMLeftTree> GetLienKetNguoc(InputInfoCondition TableInfoCondition)
        {
            var model = new VMLeftTree();
            var tableInfo = await _dasKTNNRepo.TableInfo.GetAsync(TableInfoCondition.IDTable) ?? new TableInfo() { Name = "Bảng chưa tạo" };
            var schemaInfo = await _dasKTNNRepo.SchemaInfo.GetAsync(tableInfo.IDSchema) ?? new SchemaInfo();
            model.InputInfoCondition = TableInfoCondition;
            model.VMLienKetNguoc = GetLienKetNguoc(TableInfoCondition.PageIndex, schemaInfo, tableInfo, TableInfoCondition.IDParent);
            _logger.LogDebug(Utils.Serialize(model.VMLienKetNguoc));
            return model;
        }


        private bool CheckedColAllowIdentity(SchemaInfo schemaInfo, TableInfo tableInfo, ColumnTableInfo identityCol)
        {

            try
            {
                var lastRecord = _dynamicDBService.GetFirst(schemaInfo.Code, tableInfo.DbName, $"{identityCol.DbName} DESC");
                if (lastRecord == null || lastRecord.Rows.Count == 0)
                {
                    return true;
                };

                var colVal = lastRecord.Rows[0][identityCol.DbName]?.ToString();
                return long.TryParse(colVal, out _);

            }
            catch (Exception ex)
            {

                _logger.LogError(Utils.Serialize(ex));
            }
            return false;
        }
    }
}