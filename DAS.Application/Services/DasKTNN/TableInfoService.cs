﻿using AutoMapper;
using Dapper;
using DAS.Application.Constants;
using DAS.Application.Enums;
using DAS.Application.Enums.DasKTNN;
using DAS.Application.Interfaces;
using DAS.Application.Interfaces.DasKTNN;
using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels;
using DAS.Application.Models.ViewModels.DasKTNN;
using DAS.Domain.Enums;
using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Interfaces.DASNotify;
using DAS.Domain.Models.DASKTNN;
using DAS.Infrastructure.Repositories.DASKTNN;
using DAS.Utility;
using DAS.Utility.CustomClass;
using DAS.Utility.LogUtils;
using DocumentFormat.OpenXml.Presentation;
using DocumentFormat.OpenXml.Vml;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DAS.Application.Enums.DasKTNN.EnumTableInfo;

namespace DAS.Application.Services.DasKTNN
{
    public class TableInfoService : BaseMasterService, ITableInfoServices
    {
        #region Properties
        private readonly IMapper _mapper;
        private readonly ILoggerManager _logger;
        private readonly IDynamicDBService _dynamicDBService;
        private readonly IDefaultDataService _defaultDataService;
        private readonly bool IsDanhMucDungChung;

        private IExcelServices _excelService;
        private readonly IHostApplicationLifetime _host;
        private ICacheManagementServices _cacheManagementServices;
        private IWebHostEnvironment _env;

        #endregion

        #region Ctor
        public TableInfoService(IDasRepositoryWrapper dasRepository, IDasKTNNRepositoryWrapper dasKTNN
            , IMapper mapper
            , ILoggerManager logger
            , IExcelServices excelService
            , IDynamicDBService dynamicDBService
            , IDefaultDataService defaultDataService
            , IHostApplicationLifetime host
            , ICacheManagementServices cacheManagementServices, IDasDataDapperRepo dasDapperRepo,
            IWebHostEnvironment env) : base(dasRepository, dasDapperRepo, dasKTNN)
        {
            _mapper = mapper;
            _logger = logger;
            _dynamicDBService = dynamicDBService;
            _cacheManagementServices = cacheManagementServices;
            _defaultDataService = defaultDataService;
            _host = host;
            _env = env;
            _excelService = excelService;

            IsDanhMucDungChung = ConfigUtils.GetAppSetting<bool>("IsDanhMucDungChung");
        }

        #endregion

        #region Gets  

        public async Task<IEnumerable<TableInfo>> GetsList()
        {
            var temp = from ct in _dasKTNNRepo.TableInfo.GetAll()
                       where ct.Status == (int)EnumCommon.Status.Active
                       orderby ct.Name
                       select ct;
            return await temp.ToListAsync();
        }
        public Task<List<SchemaInfo>> GetAllListSchema()
        {
            return _dasKTNNRepo.SchemaInfo.GetAll().OrderBy(n => n.Name).ToListAsync();
        }
        public Task<List<GroupInfo>> GetAllListGroup()
        {
            return _dasKTNNRepo.GroupInfo.GetAll().OrderBy(n => n.Name).ToListAsync();
        }
        public Task<List<SoucreInfo>> GetAllListSource(int? type = null)
        {
            return _dasKTNNRepo.SoucreInfo.GetAll()
                .Where(n => type == null || n.Type == type)
                .OrderBy(n => n.Name).ToListAsync();
        }
        public async Task<IEnumerable<TableInfo>> GetsListShowMenu()
        {
            var temp = from ct in _dasKTNNRepo.TableInfo.GetAll()
                       where ct.Status == (int)EnumCommon.Status.Active && ct.IsMenu == true
                       orderby ct.Name
                       select ct;
            return await temp.ToListAsync();
        }

        public async Task<TableInfo> Get(int id)
        {
            return await _dasKTNNRepo.TableInfo.FirstOrDefaultAsync(n => n.ID == id && n.Status == (int)EnumCommon.Status.Active);
        }
        public async Task<IEnumerable<TableInfo>> GetTables(params int[] ids)
        {
            return await _dasKTNNRepo.TableInfo.GetAllListAsync(n => (ids.IsEmpty() || ids.Contains(n.ID)) && n.Status == (int)EnumCommon.Status.Active);
        }
        public async Task<ColumnTableInfo> GetColumn(int id)
        {
            return await _dasKTNNRepo.ColumnTableInfo.FirstOrDefaultAsync(n => n.ID == id && n.Status == (int)EnumCommon.Status.Active);
        }

        public async Task<PaginatedList<VMTableInfo>> SearchByConditionPagging(TableInfoCondition condition)
        {
            var temp = from tb in _dasKTNNRepo.TableInfo.GetAll()
                       let p = _dasKTNNRepo.GroupTableInfo.GetAll().Where(p2 => tb.ID == p2.IDTable).FirstOrDefault()

                       where (condition.Keyword.IsEmpty() || tb.Name.ToLower().Contains(condition.Keyword.ToLower())) &&
                        (condition.DBName.IsEmpty() || tb.DbName.ToLower().Contains(condition.DBName.ToLower()))
                        && (condition.IDSchema == 0 || tb.IDSchema == condition.IDSchema)
                        && (condition.IDGroup == 0 || p.IDGroup == condition.IDGroup)
                        && (condition.IDSource == 0 || tb.IDSourceInfo == condition.IDSource)
                        && (condition.IDSources.IsEmpty() || condition.IDSources.Contains(tb.IDSourceInfo))
                       && tb.Status == (int)EnumCommon.Status.Active
                       orderby tb.UpdatedDate ?? tb.CreateDate descending
                       select _mapper.Map<VMTableInfo>(tb);

            var total = await temp.LongCountAsync();
            int totalPage = (int)Math.Ceiling(total / (double)condition.PageSize);
            if (totalPage < condition.PageIndex)
            {
                condition.PageIndex = 1;
            }
            var result = await temp.Skip((condition.PageIndex - 1) * condition.PageSize).Take(condition.PageSize).ToListAsync();
            return new PaginatedList<VMTableInfo>(result, (int)total, condition.PageIndex, condition.PageSize);
        }

        #endregion

        #region Create

        public async Task<VMTableInfo> Create()
        {
            var model = new VMTableInfo()
            {
                IsMenu = 1,
                ListType = (int)EnumTableInfo.TableListType.Table
            };
            model.Tables = await GetsList();

            model.DictSchema = await GetDictSchema();
            model.SourceInfos = await GetSourceInfo();
            model.IsDanhMucDungChung = IsDanhMucDungChung;
            //model.DictDataType = Utils.EnumToDic<ColumnDataType>();
            //var defaultColumns = _defaultDataService.GetDefaultTableColumns();
            //model.ColumnTables = _mapper.Map<List<VMColumnTableInfo>>(defaultColumns);
            //Update Module
            return model;
        }

        private async Task<IEnumerable<SoucreInfo>> GetSourceInfo()
        {
            return await _dasKTNNRepo.SoucreInfo.GetAllListAsync();
        }

        public async Task<ServiceResult> Create(VMUpdateTableInfo data)
        {
            try
            {
                UserData userData = await _cacheManagementServices.GetUserDataAndSetCache();
                var tableInfo = _mapper.Map<TableInfo>(data);
                var schema = await _dasKTNNRepo.SchemaInfo.GetAsync(data.IDSchema);
                if (schema == null)
                {
                    throw new LogicException("CSDL không tồn tại");
                }
                var columns = await _defaultDataService.GetDefaultTableColumns(schema.Type, tableInfo);
                data.ColumnTables = _mapper.Map<List<VMUpdateColumnTable>>(columns);

                await ValidateData(data, schema);
                BindUpdate(tableInfo, data);

                var isCategoryTable = schema.Type == (int)EnumSchemaInfo.SchemaType.Category;
                var columnPaths = isCategoryTable ? GetCategoryColumnPath(schema, ref columns) : new List<ColumnPathInfo>();

                AddChildColumns(columns, tableInfo, columnPaths);
                if (IsDanhMucDungChung)
                {
                    tableInfo.IDSourceInfo = 0;
                }

                if (await CrateTableSchema(tableInfo, schema, columnPaths, columns))
                {
                    if (columns.Any(n => n.DataType == (int)ColumnDataType.TableLink))
                        CreateTableLink(schema, tableInfo);

                    await _dasKTNNRepo.TableInfo.InsertAsync(tableInfo);
                    await _dasKTNNRepo.SaveAync();
                    foreach (var column in columns)
                    {
                        column.ID = 0;
                        column.IDTable = tableInfo.ID;
                        column.Status = (int)EnumCommon.Status.Active;
                    }
                    await _dasKTNNRepo.ColumnTableInfo.InsertAsync(columns);
                    await _dasKTNNRepo.SaveAync();

                    //Lưu cấu hình đường dẫn
                    await SaveColumnPaths(tableInfo, columns, isCategoryTable, columnPaths);
                    await _dasKTNNRepo.SaveAync();

                    //Cấu hình tự sinh cột
                    try
                    {
                        //var identityCol = ;
                        //if (identityCol != null)
                        //{
                        _dynamicDBService.UpdateIdentityColumns(schema.Code, tableInfo, columns.Where(n => n.IsIdentity && !n.IsSystem));
                        //}
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(Utils.Serialize(ex));
                    }


                    return new ServiceResultSuccess("Thêm bảng dữ liệu thành công");
                }

                return new ServiceResultError("Thêm bảng dữ liệu không thành công");
            }
            catch (LogicException ex)
            {
                return new ServiceResultError(ex.Message);
            }
            catch (Exception ex)
            {
                Guid.NewGuid().ToString();
                _logger.LogError(ex);
                return new ServiceResultError("Có lỗi khi thêm mới bảng dữ liệu");
            }
        }


        private async Task SaveColumnPaths(TableInfo tableInfo, List<ColumnTableInfo> defaultColumns, bool isCategoryTable, List<ColumnPathInfo> columnPaths)
        {
            if (columnPaths.IsNotEmpty() && isCategoryTable)
            {
                var parentCol = defaultColumns.FirstOrDefault(n => n.DataType == (int)ColumnDataType.Parent);
                if (parentCol != null && parentCol.ID > 0)
                {
                    foreach (var item in columnPaths)
                    {
                        item.IDColumnTableInfo = parentCol.ID;
                        item.Status = (int)EnumCommon.Status.Active;

                        if (item.PathName == KTNNConst.DefautParentIDPath)
                        {
                            item.IDColumnPath = defaultColumns.FirstOrNewObj(n => n.DbName == CategoryDefaultField.MaDanhMuc.ToString()).ID;
                        }
                        else if (item.PathName == KTNNConst.DefautParentNamePath)
                        {
                            item.IDColumnPath = defaultColumns.FirstOrNewObj(n => n.DbName == CategoryDefaultField.TenDanhMuc.ToString()).ID;
                        }
                    }
                    //Cập nhât idRefer
                    parentCol.IDSchemaRef = tableInfo.IDSchema;
                    parentCol.IDTableRef = tableInfo.ID;
                    parentCol.IDColRef = defaultColumns.FirstOrNewObj(n => n.DbName == SystemField.ID.ToString()).ID;
                    parentCol.IDColTextRef = defaultColumns.FirstOrNewObj(n =>
                    n.DbName == CategoryDefaultField.TenDanhMuc.ToString() || n.DbName == DefaultField.Ten.ToString()).ID;
                    await _dasKTNNRepo.ColumnTableInfo.UpdateAsync(parentCol);
                }
                columnPaths.ForEach(x => x.ID = 0);
                await _dasKTNNRepo.ColumnPathInfo.InsertAsync(columnPaths);
            }
        }

        public async Task<VMColumnTableInfo> CreateColumn(int? idTable)
        {
            var tableInfo = await Get(idTable ?? 0);
            if (tableInfo == null || tableInfo.ID == 0)
            {
                throw new LogicException("Bảng dữ liệu không còn tồn tại");
            }
            var cols = await GetColumnByIDTable(tableInfo.ID);

            //var schemaDefault = await _dasKTNNRepo.SchemaInfo.FirstOrDefaultAsync(n => n.Type == (int)EnumSchemaInfo.SchemaType.Dynamic) ?? new SchemaInfo();

            var infoGroupId = (await GetFirstByCode(ColumnTableGroupCode.INFO) ?? new ColumnTableGroup()).ID;

            var model = new VMColumnTableInfo()
            {
                IDTable = tableInfo.ID,
                DataType = (int)ColumnDataType.String,
                Weight = cols.Max(x => x.Weight) + 1,
                IsNullable = 1,
                IDColumnTableGroup = infoGroupId
            };
            model.DlDataType = Utils.EnumToDic<ColumnDataType>().Select(n => new SelectListItem
            {
                Value = n.Key.ToString(),
                Text = n.Value,
                Selected = n.Key == model.DataType
            });
            var schemas = await GetDictSchema();
            model.DlSchemaRef = schemas.Select(n => new SelectListItem
            {
                Value = n.Key.ToString(),
                Text = n.Value,
                Selected = n.Key == model.IDSchemaRef
            });

            var colGroups = await GetColGroups();
            model.DlColGroups = colGroups.Select(n => new SelectListItem
            {
                Value = n.ID.ToString(),
                Text = n.Name,
                Selected = n.ID == model.IDColumnTableGroup
            });
            var tables = await GetTables();
            model.Tables = tables.Where(n => n.ID != tableInfo.ID);

            var friendColumns = await GetColumnByIDTable(tableInfo.ID);
            model.FiendColumns = friendColumns;
            model.ColumnPaths = new List<VMColumnPathInfo>()
            {
                //new VMColumnPathInfo
                //{
                //}
            };
            return model;
        }

        public async Task<ServiceResult> CreateColumn(VMUpdateColumnTable data)
        {
            try
            {
                UserData userData = await _cacheManagementServices.GetUserDataAndSetCache();
                var columnInfo = _mapper.Map<ColumnTableInfo>(data);
                var table = await _dasKTNNRepo.TableInfo.GetAsync(data.IDTable);
                var schema = await _dasKTNNRepo.SchemaInfo.GetAsync(table.IDSchema);
                await ValidateColumnData(data, schema, table, false);
                columnInfo.Status = (int)EnumCommon.Status.Active;
                if (columnInfo.IDTableRef > 0)
                {
                    //Schema ko chọn > theo bảng đã chon
                    var tableRef = await _dasKTNNRepo.TableInfo.GetAsync(columnInfo.IDTableRef) ?? new TableInfo();
                    columnInfo.IDSchemaRef = tableRef.IDSchema;
                }

                try
                {
                    var paths = _mapper.Map<List<ColumnPathInfo>>(data.ColumnPaths);

                    var newColumns = new List<ColumnTableInfo>() { columnInfo };

                    AddChildColumns(newColumns, table, paths);

                    var vmColumns = _mapper.Map<List<VMColumnTableInfo>>(newColumns);
                    foreach (var vmColumn in vmColumns)
                    {
                        if (vmColumn.IDColRef > 0)
                        {
                            var colRef = await GetColumn(vmColumn.IDColRef.Value);
                            if (colRef != null)
                                vmColumn.ColumnRef = _mapper.Map<VMColumnTableInfo>(colRef);
                        }
                    }
                    SetFixedValue(ref columnInfo);

                    var rs = _dynamicDBService.CreateColumn(schema.Code, table, paths, vmColumns.ToArray());
                    if (rs)
                    {
                        if (columnInfo.DataType == (int)ColumnDataType.TableLink)
                            rs = CreateTableLink(schema, table);

                        await UpdateOrder(table, columnInfo);
                        await _dasKTNNRepo.ColumnTableInfo.InsertAsync(newColumns);
                        await _dasKTNNRepo.SaveAync();
                        if (columnInfo.IsIdentity)
                        {
                            var identityCols = await _dasKTNNRepo.ColumnTableInfo
                                .GetAllListAsync(n => n.IsIdentity && n.IDTable == table.ID && !n.IsSystem);
                            _dynamicDBService.UpdateIdentityColumns(schema.Code, table, identityCols);
                        }
                        await SyncColumnPaths(data.ColumnPaths, table, columnInfo);
                        await _dasKTNNRepo.SaveAync();

                        ResetWeb();
                        return new ServiceResultSuccess("Thêm cột dữ liệu thành công");
                    }
                }
                catch (Exception ex)
                {
                    return new ServiceResultError(ex.Message);
                }
                return new ServiceResultError("Thêm cột dữ liệu không thành công");
            }
            catch (LogicException ex)
            {
                return new ServiceResultError(ex.Message);
            }
            catch (Exception ex)
            {
                Guid.NewGuid().ToString();
                _logger.LogError(ex);
                return new ServiceResultError(ex.Message);
            }
        }

        private void SetFixedValue(ref ColumnTableInfo columnInfo)
        {
            if (columnInfo.DbName.ToLowerInvariant() == SystemField.ID.ToString().ToLowerInvariant())
            {
                columnInfo.IsSystem = true;
                columnInfo.IsIdentity = true;
            }
            if (columnInfo.DataType != (int)ColumnDataType.String)
            {
                columnInfo.Suggestion = string.Empty;
                columnInfo.IdentityPrefix = string.Empty;
            }
            if (columnInfo.IsIdentity && (columnInfo.IsSystem || columnInfo.IsDefault))
            {
                //Chỉ những trường tự tăng tự định nghĩa mới có prefix và index tự tăng
                columnInfo.IdentityIndex = 0;
                columnInfo.IdentityPrefix = string.Empty;
            }

            if (columnInfo.DataType != (int)ColumnDataType.String)
            {
            }
            if (columnInfo.DataType != (int)ColumnDataType.String
                && columnInfo.DataType != (int)ColumnDataType.Integer
                && !columnInfo.IsDefault && !columnInfo.IsSystem && columnInfo.IsIdentity) //Nếu là trường tự định nghĩa, tự tăng & khác kiểu dl string/interger thì ko set tự tăng
            {
                columnInfo.IsIdentity = false;
                columnInfo.IdentityIndex = 0;
                columnInfo.IdentityPrefix = string.Empty;
            }


        }

        /// <summary>
        /// Restart web
        /// </summary>
        private void ResetWeb()
        {
            if (!_env.IsDevelopment())
            {
                //TH deploy mới reset web
                //Fix thêm cột/xóa cột trên db Oracle màn InputInfo bị lỗi
                _host.StopApplication();
            }
        }

        #endregion

        #region Update
        public async Task<VMTableInfo> Update(int? id)
        {
            var tableInfo = await Get(id ?? 0);
            if (tableInfo == null || tableInfo.ID == 0 || tableInfo.Status == (int)EnumCommon.Status.InActive)
            {
                throw new LogicException("Bảng dữ liệu không còn tồn tại");
            }

            var model = _mapper.Map<VMTableInfo>(tableInfo);
            model.Tables = await GetsList();

            model.Schema = await _dasKTNNRepo.SchemaInfo.GetAsync(tableInfo.IDSchema) ?? new SchemaInfo();
            model.DictDataType = Utils.EnumToDic<ColumnDataType>();
            model.DictSchema = await GetDictSchema();
            var columnTables = await GetVMColumnTables(model);
            var idTableSelecteds = columnTables.Where(x => x.IDTableRef > 0).Select(x => x.IDTableRef.Value).Distinct().ToArray();
            var columns = await GetColumnByIDTable(idTableSelecteds);
            model.Columns = columns;
            model.ColumnTables = columnTables;
            var colGroups = await GetColGroups();
            model.ColGroups = colGroups;
            model.SourceInfos = await GetSourceInfo();
            model.IsDanhMucDungChung = IsDanhMucDungChung;
            return model;
        }
        public async Task<ServiceResult> Update(VMUpdateTableInfo vmTableInfo)
        {
            try
            {
                UserData userData = await _cacheManagementServices.GetUserDataAndSetCache();


                var tableInfo = await _dasKTNNRepo.TableInfo.GetAsync(vmTableInfo.ID);
                if (tableInfo == null)
                    return new ServiceResultError("Bảng dữ liệu này hiện không tồn tại hoặc đã bị xóa");
                //   var oldParent = vmTableInfo.ParentPath;

                var oldName = tableInfo.DbName;
                var newName = vmTableInfo.DbName;

                //Check có thay đổi IsRequireApprove
                var isChangeRequireApprove = tableInfo.IsRequireApprove != vmTableInfo.IsRequireApprove > 0;

                //Check có thay đổi IsTempTable
                var isTempTable = tableInfo.IsTempTable != vmTableInfo.IsTempTable > 0;

                var schema = await _dasKTNNRepo.SchemaInfo.GetAsync(vmTableInfo.IDSchema);
                await ValidateData(vmTableInfo, schema, false);

                if (oldName != newName)
                {
                    _dynamicDBService.RenameTable(schema.Code, oldName, newName);
                }

                BindUpdate(tableInfo, vmTableInfo);

                if (isChangeRequireApprove)
                    UpdateApproveColumn(schema.Code, tableInfo);

                if (isTempTable)
                    UpdateTempTableColumn(schema.Code, tableInfo);

                if (IsDanhMucDungChung)
                {
                    tableInfo.IDSourceInfo = 0;
                }

                await _dasKTNNRepo.TableInfo.UpdateAsync(tableInfo);
                await _dasKTNNRepo.SaveAync();
                return new ServiceResultSuccess("Cập nhật bảng dữ liệu thành công");
            }
            catch (LogicException ex)
            {
                return new ServiceResultError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return new ServiceResultError(ex.Message);
            }
        }

        /// <summary>
        /// Cập nhật nhiều cột & bảng
        /// </summary>
        /// <param name="vmTableInfo"></param>
        /// <returns></returns>
        public async Task<ServiceResult> UpdateColumns(VMUpdateTableInfo vmTableInfo)
        {
            try
            {
                UserData userData = await _cacheManagementServices.GetUserDataAndSetCache();

                var tableInfo = await _dasKTNNRepo.TableInfo.GetAsync(vmTableInfo.ID);
                if (tableInfo == null)
                    return new ServiceResultError("Bảng dữ liệu này hiện không tồn tại hoặc đã bị xóa");
                //   var oldParent = vmTableInfo.ParentPath;
                var schema = await _dasKTNNRepo.SchemaInfo.GetAsync(vmTableInfo.IDSchema);
                await ValidateData(vmTableInfo, schema);

                BindUpdate(tableInfo, vmTableInfo);

                var oldColumnTables = await GetColumnByIDTable(tableInfo.ID);

                await _dasKTNNRepo.TableInfo.UpdateAsync(tableInfo);

                var updatedCols = await SyncColumnTables(vmTableInfo.ColumnTables, tableInfo);

                //Todo add path config
                // AlterTableSchema(tableInfo, schema, oldColumnTables, updatedCols);
                await _dasKTNNRepo.SaveAync();
                return new ServiceResultSuccess("Cập nhật dữ liệu thành công");
            }
            catch (LogicException ex)
            {
                return new ServiceResultError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return new ServiceResultError(ex.Message);
            }
        }

        /// <summary>
        /// Cập nhật cột
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<VMColumnTableInfo> UpdateColumn(int? id)
        {
            var columnInfo = await GetColumn(id ?? 0);
            if (columnInfo == null || columnInfo.ID == 0)
            {
                throw new LogicException("Cột dữ liệu không còn tồn tại");
            }
            var tableInfo = await _dasKTNNRepo.TableInfo.GetAsync(columnInfo.IDTable);
            if (tableInfo == null || tableInfo.Status == (int)EnumCommon.Status.InActive)
                throw new LogicException("Bảng dữ liệu này hiện không tồn tại hoặc đã bị xóa");

            var model = _mapper.Map<VMColumnTableInfo>(columnInfo);
            model.DlDataType = Utils.EnumToDic<ColumnDataType>().Select(n => new SelectListItem
            {
                Value = n.Key.ToString(),
                Text = n.Value,
                Selected = n.Key == columnInfo.DataType
            });
            var schemas = await GetDictSchema();
            model.DlSchemaRef = schemas.Select(n => new SelectListItem
            {
                Value = n.Key.ToString(),
                Text = n.Value,
                Selected = n.Key == columnInfo.IDSchemaRef
            });
            if (columnInfo.DataType == (int)ColumnDataType.TableRef)
            {
                var tables = await GetTables();
                model.Tables = tables.Where(n => n.ID != tableInfo.ID);
                var columns = await GetColumnByIDTable(columnInfo.IDTableRef);
                model.Columns = columns;
            }
            var schema = await _dasKTNNRepo.SchemaInfo.GetAsync(tableInfo.IDSchema) ?? new SchemaInfo();

            var friendColumns = await GetColumnByIDTable(tableInfo.ID);
            model.FiendColumns = friendColumns.Where(n => n.ID != model.ID);

            var columnPaths = await GetColumnPathByIDColumn(columnInfo.ID);
            model.ColumnPaths = _mapper.Map<List<VMColumnPathInfo>>(columnPaths);

            var colGroups = await GetColGroups();
            model.DlColGroups = colGroups.Select(n => new SelectListItem
            {
                Value = n.ID.ToString(),
                Text = n.Name,
                Selected = n.ID == model.IDColumnTableGroup
            });
            //model.IsUpdate = schema.Type != (int)EnumSchemaInfo.SchemaType.Category;
            //model.IsDetail = schema.Type == (int)EnumSchemaInfo.SchemaType.Category;
            return model;
        }

        public async Task<ServiceResult> UpdateColumn(VMUpdateColumnTable data)
        {
            try
            {
                var columnInfo = await GetColumn(data.ID);
                if (columnInfo == null || columnInfo.ID == 0)
                {
                    throw new LogicException("Cột dữ liệu không còn tồn tại");
                }
                //Check có thay đổi IsIdentity
                var isChangeIsIdentity = columnInfo.IsIdentity != data.IsIdentity > 0;

                var table = await _dasKTNNRepo.TableInfo.GetAsync(data.IDTable);
                var schema = await _dasKTNNRepo.SchemaInfo.GetAsync(table.IDSchema);
                await ValidateColumnData(data, schema, table, true);
                columnInfo.Bind(data.KeyValue());
                columnInfo.Status = (int)EnumCommon.Status.Active;
                if (columnInfo.IDTableRef > 0)
                {
                    //Schema ko chọn > theo bảng đã chon
                    var tableRef = await _dasKTNNRepo.TableInfo.GetAsync(columnInfo.IDTableRef) ?? new TableInfo();

                    columnInfo.IDSchemaRef = tableRef.IDSchema;
                }
                SetFixedValue(ref columnInfo);
                await UpdateOrder(table, columnInfo);
                await _dasKTNNRepo.ColumnTableInfo.UpdateAsync(columnInfo);

                await SyncColumnPaths(data.ColumnPaths, table, columnInfo);

                if (isChangeIsIdentity && columnInfo.IsIdentity)
                {
                    var identityCols = await _dasKTNNRepo.ColumnTableInfo.GetAllListAsync(n => n.IsIdentity && n.IDTable == table.ID && !n.IsSystem);
                    _dynamicDBService.UpdateIdentityColumns(schema.Code, table, identityCols);
                }

                await _dasKTNNRepo.SaveAync();
                return new ServiceResultSuccess("Cập nhật cột dữ liệu thành công");
            }
            catch (LogicException ex)
            {
                return new ServiceResultError(ex.Message);
            }
            catch (Exception ex)
            {
                Guid.NewGuid().ToString();
                _logger.LogError(ex);
                return new ServiceResultError(ex.Message);
            }
        }

        #endregion

        #region Delete
        public async Task<ServiceResult> Delete(int id)
        {
            try
            {
                var tableInfo = await _dasKTNNRepo.TableInfo.GetAsync(id);
                if (tableInfo == null || tableInfo.Status == (int)EnumCommon.Status.InActive)
                    return new ServiceResultError("Bảng dữ liệu này hiện không tồn tại hoặc đã bị xóa");

                var schema = await _dasKTNNRepo.SchemaInfo.GetAsync(tableInfo.IDSchema);
                if (schema == null)
                {
                    return new ServiceResultError("Schema không tồn tại");
                }
                tableInfo.Status = (int)EnumCommon.Status.InActive;
                await _dasKTNNRepo.TableInfo.UpdateAsync(tableInfo);

                var cols = await GetColumnByIDTable(id);
                //if (cols.IsNotEmpty())
                //{
                //    foreach (var col in cols)
                //    {
                //        col.Status = (int)EnumCommon.Status.InActive;
                //    }
                //    await _dasKTNNRepo.ColumnTableInfo.UpdateAsync(cols);
                //}
                await _dasKTNNRepo.SaveAync();
                DropTableSchema(schema, tableInfo, cols);
                //Update Module
                return new ServiceResultSuccess("Xóa bảng dữ liệu thành công");
            }
            catch (LogicException ex)
            {
                return new ServiceResultError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return new ServiceResultError(ex.Message);

            }
        }

        public async Task<ServiceResult> Delete(IEnumerable<int> ids)
        {
            try
            {
                var tableInfoDeletes = await _dasKTNNRepo.TableInfo.GetAllListAsync(n => ids.Contains(n.ID));
                if (tableInfoDeletes == null || tableInfoDeletes.Count() == 0)
                    return new ServiceResultError("Bảng dữ liệu đã chọn hiện không tồn tại hoặc đã bị xóa");

                var idSchemas = tableInfoDeletes.Select(n => n.IDSchema).Distinct().ToArray();
                var schemas = await GetSchemas(idSchemas);
                if (schemas.IsEmpty())
                {
                    return new ServiceResultError("CSDL không tồn tại");
                }
                foreach (var item in tableInfoDeletes)
                {
                    item.Status = (int)EnumCommon.Status.InActive;
                }
                await _dasKTNNRepo.TableInfo.UpdateAsync(tableInfoDeletes);
                var cols = await GetColumnByIDTable(ids.ToArray());
                if (cols.IsNotEmpty())
                {
                    foreach (var col in cols)
                    {
                        col.Status = (int)EnumCommon.Status.InActive;
                    }
                    await _dasKTNNRepo.ColumnTableInfo.UpdateAsync(cols);
                }
                await _dasKTNNRepo.SaveAync();

                foreach (var tableInfo in tableInfoDeletes)
                {
                    var tbCols = cols.Where(n => n.IDTable == tableInfo.ID);
                    var schema = schemas.FirstOrDefault(n => n.ID == tableInfo.IDSchema);
                    if (schema != null)
                        DropTableSchema(schema, tableInfo, tbCols);
                }
                //Update Module
                return new ServiceResultSuccess("Xóa bảng dữ liệu thành công");
            }
            catch (LogicException ex)
            {
                return new ServiceResultError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return new ServiceResultError(ex.Message);
            }
        }
        public async Task<ServiceResult> DeleteColumn(int id)
        {
            try
            {
                var columnInfo = await _dasKTNNRepo.ColumnTableInfo.GetAsync(id);
                if (columnInfo == null || columnInfo.Status == (int)EnumCommon.Status.InActive)
                    return new ServiceResultError("Cột dữ liệu này hiện không tồn tại hoặc đã bị xóa");

                var table = await _dasKTNNRepo.TableInfo.GetAsync(columnInfo.IDTable);
                if (table == null)
                    throw new LogicException("Bảng dữ liệu này hiện không tồn tại hoặc đã bị xóa");

                var schema = await _dasKTNNRepo.SchemaInfo.GetAsync(table.IDSchema);
                if (schema == null)
                {
                    return new ServiceResultError("CSDL không tồn tại");
                }

                var paths = await GetColumnPathByIDColumn(id);

                var columns = new List<ColumnTableInfo>() { columnInfo };
                var allColumn = await GetColumnByIDTable(columnInfo.IDTable);
                DelChildColumns(ref columns, allColumn, paths);

                var rs = _dynamicDBService.DropColumn(schema.Code, table, paths, columns.ToArray());
                if (rs)
                {
                    if (columnInfo.IsIdentity)
                    {
                        var identityCols = await _dasKTNNRepo.ColumnTableInfo.GetAllListAsync(n => n.IsIdentity && n.IDTable == table.ID && n.ID != columnInfo.ID && !n.IsSystem);
                        _dynamicDBService.UpdateIdentityColumns(schema.Code, table, identityCols);
                    }
                    await _dasKTNNRepo.ColumnPathInfo.DeleteAsync(paths);
                    await _dasKTNNRepo.ColumnTableInfo.DeleteAsync(columns);
                    await _dasKTNNRepo.SaveAync();

                    ResetWeb();
                    return new ServiceResultSuccess("Xóa cột dữ liệu thành công");
                }
                else
                {
                    return new ServiceResultError("Xóa cột dữ liệu không thành công");
                }
            }
            catch (LogicException ex)
            {
                return new ServiceResultError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return new ServiceResultError(ex.Message);
            }
        }

        public async Task<ServiceResult> DeleteColumn(IEnumerable<int> ids)
        {
            try
            {
                var columnDeletes = await _dasKTNNRepo.ColumnTableInfo.GetAllListAsync(n => ids.Contains(n.ID));
                if (columnDeletes == null || columnDeletes.Count() == 0)
                    return new ServiceResultError("Cột dữ liệu đã chọn hiện không tồn tại hoặc đã bị xóa");

                var idTables = columnDeletes.Select(n => n.IDTable).Distinct().ToArray();
                var tables = await GetTables(idTables);
                var idSchemas = tables.Select(n => n.IDSchema).Distinct().ToArray();
                var schemas = await GetSchemas(idSchemas);
                if (schemas.IsEmpty())
                {
                    return new ServiceResultError("CSDL không tồn tại");
                }
                var isErr = false;
                foreach (var table in tables)
                {
                    var schema = schemas.FirstOrDefault(n => n.ID == table.IDSchema);
                    if (schema == null)
                        continue;

                    var cols = columnDeletes.Where(n => n.IDTable == table.ID).ToList();
                    var paths = await GetColumnPathByIDColumn(ids.ToArray());
                    var allColumn = await GetColumnByIDTable(table.ID);
                    DelChildColumns(ref cols, allColumn, paths);
                    var rs = _dynamicDBService.DropColumn(schema.Code, table, paths, cols.ToArray());
                    if (!rs)
                    {
                        isErr = true;
                        break;
                    }
                    if (paths.IsNotEmpty())
                    {
                        await _dasKTNNRepo.ColumnPathInfo.DeleteAsync(paths);
                    }

                    if (cols.Any(n => n.IsIdentity))
                    {
                        var identityCols = await _dasKTNNRepo.ColumnTableInfo.GetAllListAsync(n => n.IsIdentity && n.IDTable == table.ID && !ids.Contains(n.ID) && !n.IsSystem);
                        _dynamicDBService.UpdateIdentityColumns(schema.Code, table, identityCols);
                    }

                    await _dasKTNNRepo.ColumnTableInfo.DeleteAsync(cols);
                }

                if (isErr)
                    return new ServiceResultError("Xóa cột dữ liệu không thành công");
                else
                {
                    await _dasKTNNRepo.SaveAync();

                    ResetWeb();
                    return new ServiceResultSuccess("Xóa cột dữ liệu thành công");
                }
            }
            catch (LogicException ex)
            {
                return new ServiceResultError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return new ServiceResultError(ex.Message);
            }
        }
        #endregion

        #region Helper

        /// <summary>
        /// Lấy danh sách bảng theo id schema
        /// </summary>
        /// <param name="idSchemas">ID schema</param>
        /// <returns></returns>
        public async Task<IEnumerable<TableInfo>> GetTableByIDSchema(params int[] idSchemas)
        {
            var temp = from tb in _dasKTNNRepo.TableInfo.GetAll()
                       where idSchemas.IsNotEmpty() && idSchemas.Contains(tb.IDSchema)
                       && tb.Status == (int)EnumCommon.Status.Active
                       orderby tb.Name
                       select tb;
            return await temp.ToListAsync();
        }

        /// <summary>
        /// Lấy danh sách cột theo id bảng
        /// </summary>
        /// <param name="idTables">ID bảng</param>
        /// <returns></returns>
        public async Task<IEnumerable<ColumnTableInfo>> GetColumnByIDTable(params int[] idTables)
        {
            var temp = from col in _dasKTNNRepo.ColumnTableInfo.GetAll()
                       where idTables.IsNotEmpty() && idTables.Contains(col.IDTable)
                       && col.Status == (int)EnumCommon.Status.Active
                       orderby col.Weight
                       select col;
            return await temp.ToListAsync();
        }

        #endregion

        #region Delete By Schema
        public async Task<ServiceResult> DeleteBySchema(params int[] idSchema)
        {
            try
            {
                var tables = await GetTableByIDSchema(idSchema);
                var idTables = tables.IsNotEmpty() ? tables.Select(x => x.ID).ToArray() : new int[] { -1 };
                var columnTableInfos = await GetColumnByIDTable(idTables);
                await _dasKTNNRepo.TableInfo.DeleteAsync(tables);
                await _dasKTNNRepo.ColumnTableInfo.DeleteAsync(columnTableInfos);
                await _dasKTNNRepo.SaveAync();
                return new ServiceResultError("Xóa schema thành công");
            }
            catch (LogicException ex)
            {
                return new ServiceResultError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                return new ServiceResultError("Đã xảy ra lỗi trong quá trình xóa schema");
            }
        }
        #endregion


        #region Clone
        public async Task<VMTableInfo> IsClone(int id)
        {
            var tableInfo = await Get(id);
            if (tableInfo == null || tableInfo.ID == 0 || tableInfo.Status == (int)EnumCommon.Status.InActive)
            {
                throw new LogicException("Bảng dữ liệu không còn tồn tại");
            }
            var model = _mapper.Map<VMTableInfo>(tableInfo);
            return model;
        }
        public async Task<ServiceResult> Clone(VMCloneTableInfo cloneData)
        {
            try
            {
                UserData userData = await _cacheManagementServices.GetUserDataAndSetCache();
                var tableInfo = await _dasKTNNRepo.TableInfo.GetAsync(cloneData.ID);
                if (tableInfo == null || tableInfo.ID == 0 || tableInfo.Status == (int)EnumCommon.Status.InActive)
                {
                    throw new LogicException("Bảng dữ liệu không còn tồn tại");
                }
                var schema = await _dasKTNNRepo.SchemaInfo.GetAsync(tableInfo.IDSchema);
                if (schema == null)
                {
                    throw new LogicException("CSDL không tồn tại");
                }

                if (cloneData.IsCloneAllRecord == 0 && cloneData.IDRecordClones.IsEmpty())
                {
                    throw new LogicException("Dữ liệu sao chép không được để trống");
                }
                if (cloneData.CloneLevel.HasValue && cloneData.CloneLevel < 1)
                {
                    throw new LogicException("Cấp độ sao chép không được nhỏ hơn 1");
                }
                var columns = (await GetColumnByIDTable(tableInfo.ID)).ToList();
                var columnIds = columns.Select(n => n.ID).ToArray();

                var columnPaths = (await GetColumnPathByIDColumn(columnIds)).ToList();

                var cloneTableInfo = Utils.CopyTo<TableInfo>(tableInfo);
                var data = Utils.Bind<VMUpdateTableInfo>(tableInfo.KeyValue());
                data.ColumnTables = _mapper.Map<List<VMUpdateColumnTable>>(columns);
                await ValidateData(data, schema);

                BindUpdate(cloneTableInfo, data);
                cloneTableInfo.ID = 0;
                cloneTableInfo.UpdatedDate = DateTime.Now;
                cloneTableInfo.CreateDate = DateTime.Now;
                cloneTableInfo.Name = cloneData.CloneName;
                cloneTableInfo.DbName = cloneData.CloneDbName;
                var isCategoryTable = schema.Type == (int)EnumSchemaInfo.SchemaType.Category;

                if (!cloneData.CloneLevel.HasValue)
                    cloneData.CloneLevel = 100;


                if (_dynamicDBService.CloneTableCustom(schema.Code, tableInfo.DbName, schema.Code, cloneTableInfo.DbName, cloneData.IsCloneAllRecord > 0, cloneData.IDRecordClones, cloneData.CloneLevel ?? 0))
                {
                    //if (columns.Any(n => n.DataType == (int)ColumnDataType.TableLink))
                    //    _dynamicDBService.CloneTable(schema.Code, $"{schema.Code}{KTNNConst.TableLinkNameExt}", schema.Code, $"{cloneTableInfo.DbName}{KTNNConst.TableLinkNameExt}");

                    await _dasKTNNRepo.TableInfo.InsertAsync(cloneTableInfo);
                    await _dasKTNNRepo.SaveAync();
                    foreach (var column in columns)
                    {
                        column.ID = 0;
                        column.IDTable = cloneTableInfo.ID;
                        column.Status = (int)EnumCommon.Status.Active;
                    }
                    await _dasKTNNRepo.ColumnTableInfo.InsertAsync(columns);
                    await _dasKTNNRepo.SaveAync();

                    //Lưu cấu hình đường dẫn
                    await SaveColumnPaths(cloneTableInfo, columns, isCategoryTable, columnPaths);
                    await _dasKTNNRepo.SaveAync();

                    return new ServiceResultSuccess("Nhân bản bảng dữ liệu thành công");
                }

                return new ServiceResultError("Nhân bản dữ liệu không thành công");
            }
            catch (LogicException ex)
            {
                return new ServiceResultError(ex.Message);
            }
            catch (Exception ex)
            {
                Guid.NewGuid().ToString();
                _logger.LogError(ex);
                return new ServiceResultError("Có lỗi khi thêm mới bảng dữ liệu");
            }
        }

        #endregion
        #region Validate
        private async Task ValidateData(VMUpdateTableInfo vmTableInfo, SchemaInfo schema, bool isValidateColumns = true)
        {
            if (schema == null)
            {
                throw new LogicException("CSDL không tồn tại");
            }
            if (await _dasKTNNRepo.TableInfo.IsNameExist(vmTableInfo.Name, (int)EnumCommon.Status.InActive, vmTableInfo.ID))
            {
                throw new LogicException("Tên bảng dữ liệu đã tồn tại");
            }
            if (await _dasKTNNRepo.TableInfo.IsSqlNameExist(vmTableInfo.DbName, vmTableInfo.IDSchema ?? 0, (int)EnumCommon.Status.InActive, vmTableInfo.ID))
            {
                throw new LogicException("Tên bảng dữ liệu trên CSDL đã tồn tại");
            }

            if (isValidateColumns)
            {
                if (vmTableInfo.ColumnTables.IsEmpty())
                {
                    throw new LogicException("Cột trong bảng không được để trống");
                }
                if (vmTableInfo.ColumnTables.IsEmpty())
                {
                    throw new LogicException("Cột dữ liệu của bảng không được để trống");
                }

                var groupNames = vmTableInfo.ColumnTables.GroupBy(n => n.Name);
                if (groupNames.Any(x => x.Count() > 1))
                {
                    var deplucateCols = groupNames.Where(x => x.Count() > 1).Select(n => n.Key);
                    throw new LogicException($"Tên cột {string.Join(", ", deplucateCols)} không được trùng");
                }

                var groupDbNames = vmTableInfo.ColumnTables.GroupBy(n => n.DbName);
                if (groupDbNames.Any(x => x.Count() > 1))
                {
                    var deplucateCols = groupDbNames.Where(x => x.Count() > 1).Select(n => n.Key);
                    throw new LogicException($"Tên cột trong CSDL {string.Join(", ", deplucateCols)} không được trùng");
                }

                if (vmTableInfo.ColumnTables.Count(n => n.DataType == (int)ColumnDataType.Parent) > 1)
                {
                    throw new LogicException("Trong một bảng chỉ được phép có một cột kiểu dữ liệu \"Cha - Con\"");
                }
                if (vmTableInfo.ColumnTables.Count(n => n.DataType == (int)ColumnDataType.TableLink) > 1)
                {
                    throw new LogicException("Trong một bảng chỉ được phép có một cột kiểu dữ liệu \"Liên kết\"");
                }
            }
        }
        private async Task ValidateColumnData(VMUpdateColumnTable data, SchemaInfo schema, TableInfo table, bool isUpdate)
        {

            if (schema == null)
            {
                throw new LogicException("CSDL không tồn tại");
            }
            if (table == null)
            {
                throw new LogicException("Bảng dữ liệu không tồn tại");
            }

            if (await _dasKTNNRepo.ColumnTableInfo.IsSqlNameExist(data.DbName, data.IDTable, (int)EnumCommon.Status.InActive, data.ID))
            {
                throw new LogicException("Tên cột dữ liệu trên CSDL đã tồn tại");
            }
            if (!isUpdate)
            {
                var dicSysField = Utils.EnumToStringDic<SystemField>();
                var dicDefaultField = Utils.EnumToStringDic<DefaultField>();
                var dicCategoryDefaultField = Utils.EnumToStringDic<CategoryDefaultField>();
                var dicCustomField = Utils.EnumToStringDic<CustomField>();

                if (
                    dicSysField.Any(n => n.Key.ToLower() == data.DbName.ToLower())
                     || dicDefaultField.Any(n => n.Key.ToLower() == data.DbName.ToLower())
                     || dicCategoryDefaultField.Any(n => n.Key.ToLower() == data.DbName.ToLower())
                     || dicCustomField.Any(n => n.Key.ToLower() == data.DbName.ToLower())
                    )
                {
                    throw new LogicException("Tên cột dữ liệu đã bị trùng với tên cột dữ liệu hệ thống");
                }

            }

            if (await _dasKTNNRepo.ColumnTableInfo.IsNameExist(data.DbName, data.IDTable, (int)EnumCommon.Status.InActive, data.ID))
            {
                throw new LogicException("Tên cột dữ liệu đã tồn tại");
            }
            if (data.DataType == (int)ColumnDataType.Parent)
            {
                if (await _dasKTNNRepo.ColumnTableInfo.IsDataTypeExist(data.DataType, data.IDTable, (int)EnumCommon.Status.InActive, data.ID))
                {
                    throw new LogicException("Trong một bảng chỉ được phép có một cột kiểu dữ liệu \"Cha - Con\"");
                }
                var paths = data.ColumnPaths;
                if (paths.IsNotEmpty())
                {
                    if (paths.GroupBy(n => n.IDColumnPath).Any(n => n.Count() > 1))
                    {
                        throw new LogicException("Cột tham chiếu của Cấu hình đường dẫn không được phép trùng nhau");
                    }
                    if (paths.GroupBy(n => n.PathName).Any(n => n.Count() > 1))
                    {
                        throw new LogicException("Đường dẫn Cấu hình đường dẫn không được phép trùng nhau");
                    }
                }
            }
        }
        #endregion


        #region Funtions

        private async Task<IEnumerable<ColumnPathInfo>> GetColumnPathByIDColumn(params int[] idCloumns)
        {
            var temp = from col in _dasKTNNRepo.ColumnPathInfo.GetAll()
                       where idCloumns.IsNotEmpty() && idCloumns.Contains(col.IDColumnTableInfo)
                       && col.Status == (int)EnumCommon.Status.Active
                       orderby col.ID
                       select col;
            return await temp.ToListAsync();
        }


        private void BindUpdate(TableInfo tableInfoUpdate, VMUpdateTableInfo vmTableInfo)
        {
            tableInfoUpdate.Bind(vmTableInfo.KeyValue());
            tableInfoUpdate.Status = (int)EnumCommon.Status.Active;
        }

        private async Task<List<ColumnTableInfo>> SyncColumnTables(IEnumerable<VMUpdateColumnTable> columnTables, TableInfo tableInfo)
        {
            var rs = new List<ColumnTableInfo>();

            UserData userData = await _cacheManagementServices.GetUserDataAndSetCache();

            var inserts = new List<ColumnTableInfo>();
            var updates = new List<ColumnTableInfo>();
            var deletes = new List<ColumnTableInfo>();

            var dbColumnTableInfos = await GetColumnByIDTable(tableInfo.ID) ?? new List<ColumnTableInfo>();

            //var systemColumns = Utils.EnumToStringDic<EnumTableInfo.ColumnSystemField>();
            //var defaultColumns = _defaultDataService.GetDefaultTableColumns(she);

            if (columnTables.IsNotEmpty())
            {
                var dtNow = DateTime.Now;
                foreach (var column in columnTables)
                {
                    column.IDTable = tableInfo.ID;
                    column.Status = (int)EnumCommon.Status.Active;

                    //if (systemColumns.ContainsKey(column.DbName))
                    //{
                    //    //Set các trường cố định
                    //    column.IsSystem = true;
                    //    if (column.DbName == EnumTableInfo.ColumnSystemField.ID.ToString())
                    //    {
                    //        column.IsPrimaryKey = 1;
                    //        column.IsIdentity = 1;
                    //    }
                    //}
                    //if (defaultColumns.Any(x => x.DbName == column.DbName))
                    //{
                    //    //Set các trường mặc định
                    //    column.IsDefault = true;
                    //}

                    if (column.DataType != (int)ColumnDataType.TableRef
                        && column.DataType != (int)ColumnDataType.Parent)
                    {
                        column.IDSchemaRef = 0;
                        column.IDTableRef = 0;
                        column.IDColRef = 0;
                    }
                    var detail = dbColumnTableInfos.FirstOrDefault(n => n.ID == column.ID);
                    if (detail != null)
                    {
                        detail.Bind(column.KeyValue());
                        updates.Add(detail);
                    }
                    else
                    {
                        var newItem = _mapper.Map<ColumnTableInfo>(column);
                        newItem.ID = 0;
                        inserts.Add(newItem);
                    }
                }
                if (Utils.IsNotEmpty(dbColumnTableInfos))
                {
                    var updateIds = columnTables.Select(n => n.ID).ToArray();
                    //lấy ds các bản ghi đc thêm vào db nhưng ko có trong ds id trên form => xóa
                    deletes = dbColumnTableInfos.Where(n => !updateIds.Contains(n.ID)).ToList();
                }
            }
            if (deletes.IsNotEmpty() || updates.IsNotEmpty() || inserts.IsNotEmpty())
            {
                if (deletes.IsNotEmpty())
                {
                    await _dasKTNNRepo.ColumnTableInfo.DeleteAsync(deletes);
                }
                if (updates.IsNotEmpty())
                {
                    rs.AddRange(updates);
                    await _dasKTNNRepo.ColumnTableInfo.UpdateAsync(updates);
                }
                if (inserts.IsNotEmpty())
                {
                    rs.AddRange(inserts);
                    await _dasKTNNRepo.ColumnTableInfo.InsertAsync(inserts);
                }
                return rs;
            }

            return new List<ColumnTableInfo>();
        }
        private async Task<List<ColumnPathInfo>> SyncColumnPaths(IEnumerable<VMColumnPathInfo> columnPaths, TableInfo tableInfo, ColumnTableInfo columnTable)
        {
            var rs = new List<ColumnPathInfo>();
            if (columnTable.DataType == (int)ColumnDataType.Parent)
            {
                UserData userData = await _cacheManagementServices.GetUserDataAndSetCache();
                var inserts = new List<ColumnPathInfo>();
                var updates = new List<ColumnPathInfo>();
                var deletes = new List<ColumnPathInfo>();
                var dbColumnPathInfos = await GetColumnPathByIDColumn(columnTable.ID) ?? new List<ColumnPathInfo>();
                var otherCols = await GetColumnByIDTable(columnTable.IDTable);
                if (columnPaths.IsNotEmpty())
                {
                    var dtNow = DateTime.Now;
                    foreach (var columnPath in columnPaths)
                    {
                        columnPath.IDColumnTableInfo = columnTable.ID;
                        columnPath.Status = (int)EnumCommon.Status.Active;

                        var detail = dbColumnPathInfos.FirstOrDefault(n => n.ID == columnPath.ID);
                        if (detail != null)
                        {
                            detail.Bind(columnPath.KeyValue());
                            updates.Add(detail);
                        }
                        else
                        {
                            var newItem = _mapper.Map<ColumnPathInfo>(columnPath);
                            newItem.ID = 0;
                            inserts.Add(newItem);
                        }
                    }
                    if (Utils.IsNotEmpty(dbColumnPathInfos))
                    {
                        var updateIds = columnPaths.Select(n => n.ID).ToArray();
                        //lấy ds các bản ghi đc thêm vào db nhưng ko có trong ds id trên form => xóa
                        deletes = dbColumnPathInfos.Where(n => !updateIds.Contains(n.ID)).ToList();
                    }
                }
                //Cập nhât idRefer
                columnTable.IDSchemaRef = tableInfo.IDSchema;
                columnTable.IDTableRef = tableInfo.ID;
                columnTable.IDColRef = otherCols.FirstOrNewObj(n => n.DbName == SystemField.ID.ToString()).ID;
                columnTable.IDColTextRef = otherCols.FirstOrNewObj(n =>
                n.DbName == CategoryDefaultField.TenDanhMuc.ToString() || n.DbName == DefaultField.Ten.ToString()).ID;
                await _dasKTNNRepo.ColumnTableInfo.UpdateAsync(columnTable);

                if (deletes.IsNotEmpty() || updates.IsNotEmpty() || inserts.IsNotEmpty())
                {
                    if (deletes.IsNotEmpty())
                    {
                        await _dasKTNNRepo.ColumnPathInfo.DeleteAsync(deletes);
                    }
                    if (updates.IsNotEmpty())
                    {
                        rs.AddRange(updates);
                        await _dasKTNNRepo.ColumnPathInfo.UpdateAsync(updates);
                    }
                    if (inserts.IsNotEmpty())
                    {
                        rs.AddRange(inserts);
                        await _dasKTNNRepo.ColumnPathInfo.InsertAsync(inserts);
                    }
                    return rs;
                }
            }


            return new List<ColumnPathInfo>();
        }
        private async Task<bool> UpdateOrder(TableInfo tableInfo, ColumnTableInfo columnTable)
        {
            if (columnTable.IsOrder)
            {
                var otherColumns = await _dasKTNNRepo.ColumnTableInfo.GetAllListAsync(n => n.IDTable == tableInfo.ID && n.ID != columnTable.ID);
                foreach (var item in otherColumns)
                {
                    item.IsOrder = false;
                    item.OrderType = null;
                }
                await _dasKTNNRepo.ColumnTableInfo.UpdateAsync(otherColumns);
            }
            return true;
        }

        private async Task<List<VMColumnTableInfo>> GetVMColumnTables(VMTableInfo vmTableInfo)
        {
            if (Utils.IsEmpty(vmTableInfo))
                vmTableInfo = new VMTableInfo();

            return await (from col in _dasKTNNRepo.ColumnTableInfo.GetAll()
                          where col.IDTable == vmTableInfo.ID && col.Status == (int)EnumCommon.Status.Active
                          orderby col.Weight
                          select _mapper.Map<VMColumnTableInfo>(col)).ToListAsync();
        }

        private async Task<List<SchemaInfo>> GetSchemas(params int[] ids)
        {
            return await (from schema in _dasKTNNRepo.SchemaInfo.GetAll()
                          where ids.IsEmpty() || (ids.IsNotEmpty() && ids.Contains(schema.ID))
                          select schema).ToListAsync();
        }

        private async Task<Dictionary<int, string>> GetDictSchema()
        {
            var rs = await _dasKTNNRepo.SchemaInfo.GetAllListAsync();
            return rs.OrderBy(n => n.Name).ToDictionary(x => x.ID, x => x.Name);
        }
        private async Task<IEnumerable<ColumnTableGroup>> GetColGroups()
        {
            var rs = await _dasKTNNRepo.ColumnTableGroup.GetAllListAsync();
            return rs.OrderBy(n => n.Name).ToList();
        }

        private async Task<bool> CrateTableSchema(TableInfo tableInfo, SchemaInfo schema, IEnumerable<ColumnPathInfo> columnPaths, IEnumerable<ColumnTableInfo> columns)
        {
            try
            {
                var vmCols = _mapper.Map<List<VMColumnTableInfo>>(columns);
                var columnRefIds = vmCols.Where(n => n.IDColRef > 0).Select(n => n.IDColRef.Value).Distinct().ToArray();
                var columnRefs = await GetColumnByIds(columnRefIds);
                if (columnRefs.IsNotEmpty())
                {
                    foreach (var item in vmCols)
                    {
                        if (item.IDColRef > 0)
                        {
                            var colRef = columnRefs.FirstOrDefault(n => n.ID == item.IDColRef);
                            if (colRef != null)
                            {
                                item.ColumnRef = _mapper.Map<VMColumnTableInfo>(colRef);
                            }
                        }
                    }
                }
                return _dynamicDBService.CreateTable(schema.Code, tableInfo, columnPaths, vmCols.ToArray());
            }
            catch (Exception ex)
            {
                _logger.LogError(Utils.Serialize(ex));
                throw new LogicException(ex.Message);
            }
        }

        private async Task<IEnumerable<ColumnTableInfo>> GetColumnByIds(params int[] refColumnIds)
        {
            return await _dasKTNNRepo.ColumnTableInfo.GetAllListAsync(n => refColumnIds.Contains(n.ID) && n.Status == (int)EnumCommon.Status.Active);
        }

        private void DropTableSchema(SchemaInfo schema, TableInfo tableInfo, IEnumerable<ColumnTableInfo> cols)
        {
            _dynamicDBService.DropTable(schema.Code, tableInfo.DbName);
        }
        //private void AlterTableSchema(TableInfo tableInfo, SchemaInfo schema, IEnumerable<ColumnTableInfo> oldColumns, IEnumerable<ColumnTableInfo> columns)
        //{
        //    //var columns = await GetColumnTables(tableInfo.ID);

        //    var newCols = new List<ColumnTableInfo>();
        //    var delCols = new List<ColumnTableInfo>();

        //    if (columns.IsNotEmpty())
        //    {
        //        //lấy ds cột thêm mới
        //        foreach (var column in columns)
        //        {
        //            if (oldColumns.Count(x => x.DbName.ToLower().Trim() == column.DbName.ToLower().Trim()) == 0)
        //            {
        //                newCols.Add(column);
        //            }
        //        }
        //    }

        //    if (oldColumns.IsNotEmpty())
        //    {
        //        //lấy ds cột xóa
        //        foreach (var column in oldColumns)
        //        {
        //            if (columns.Count(x => x.DbName.ToLower().Trim() == column.DbName.ToLower().Trim()) == 0)
        //            {
        //                delCols.Add(column);
        //            }
        //        }
        //    } 
        //    if (newCols.IsNotEmpty())
        //        _dynamicDBService.CreateColumn(schema.Code, tableInfo, newCols.ToArray());

        //    if (delCols.IsNotEmpty())
        //        _dynamicDBService.DropColumn(schema.Code, tableInfo, delCols.ToArray());
        //}
        #region Delete tale

        /// <summary>
        /// Thêm xóa các cột phục vụ việc duyệt
        /// </summary>
        /// <param name="code"></param>
        /// <param name="tableInfo"></param>
        private async void UpdateApproveColumn(string code, TableInfo tableInfo)
        {
            var sysGroupId = (await GetFirstByCode(ColumnTableGroupCode.SYS) ?? new ColumnTableGroup()).ID;
            var cols = _defaultDataService.GetApproveColumns(sysGroupId);
            try
            {
                if (tableInfo.IsRequireApprove)
                {
                    var vmCols = _mapper.Map<VMColumnTableInfo[]>(cols);
                    _dynamicDBService.CreateColumn(code, tableInfo, null, vmCols);
                }
                else
                {
                    _dynamicDBService.DropColumn(code, tableInfo, null, cols.ToArray());
                }
            }
            catch
            {
                //Tránh TH thêm cột khi db đã có hoặc xóa cột khi db chưa có
                //Ignore
            }
        }

        /// <summary>
        /// Thêm/xóa các cột phục vụ bảng tạm
        /// </summary>
        /// <param name="code"></param>
        /// <param name="tableInfo"></param>
        private async void UpdateTempTableColumn(string code, TableInfo tableInfo)
        {
            var sysGroupId = (await GetFirstByCode(ColumnTableGroupCode.SYS) ?? new ColumnTableGroup()).ID;
            var infoGroupId = (await GetFirstByCode(ColumnTableGroupCode.INFO) ?? new ColumnTableGroup()).ID;
            var cols = _defaultDataService.GetTempTableColumns(sysGroupId, infoGroupId);
            try
            {
                if (tableInfo.IsTempTable)
                {
                    var vmCols = _mapper.Map<VMColumnTableInfo[]>(cols);
                    _dynamicDBService.CreateColumn(code, tableInfo, null, vmCols);
                }
                else
                {
                    _dynamicDBService.DropColumn(code, tableInfo, null, cols.ToArray());
                }
            }
            catch
            {
                //Tránh TH thêm cột khi db đã có hoặc xóa cột khi db chưa có
                //Ignore
            }
        }
        #endregion

        /// <summary>
        /// Lấy cấu hình đường dẫn cho cột Cha -con
        /// </summary>
        /// <param name="schema"></param>
        /// <param name="columnTables"></param>
        /// <returns></returns>
        private List<ColumnPathInfo> GetCategoryColumnPath(SchemaInfo schema, ref List<ColumnTableInfo> columnTables)
        {

            var tempId = -1; //set tạm id để QueryHelper lấy
            foreach (var item in columnTables)
            {
                if (item.DataType == (int)ColumnDataType.Parent)
                {
                    item.ID = tempId;
                    break;
                };
            }

            return new List<ColumnPathInfo>
                  {
                      new ColumnPathInfo
                      {
                          IDColumnTableInfo = tempId,
                          PathName = KTNNConst.DefautParentNamePath,
                      },
                      new ColumnPathInfo
                      {
                          IDColumnTableInfo = tempId,
                          PathName = KTNNConst.DefautParentIDPath,
                      }
                  };
        }

        private bool CreateTableLink(SchemaInfo schema, TableInfo tableInfo)
        {
            var dbName = $"{schema.Code}{KTNNConst.TableLinkNameExt}";
            var checkTable = _dynamicDBService.CheckTableExists(schema.Code, dbName);
            if (checkTable == 0)
            {
                //Ko có > tạo bảng mới
                var cols = GetTableLinkColumns(schema.ID, dbName);
                return _dynamicDBService.CreateTable(schema.Code, dbName, cols.ToArray());
            }
            return false;
        }

        private bool DropTableLink(SchemaInfo schema, TableInfo tableInfo)
        {
            var dbName = $"{schema.Code}{KTNNConst.TableLinkNameExt}";
            return _dynamicDBService.DropTable(schema.Code, dbName);
        }

        public static List<string> GetTableLinkColumns(int idSchema, string dbName)
        {
            var cols = new List<string>()
            {
                //Mục tham chiếu
               $"{nameof(LinkTableModel.ID)} NUMBER(9) GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 9999999999 INCREMENT BY 1 START WITH 283 CACHE 20 NOORDER NOCYCLE NOT NULL",

               $"{nameof(LinkTableModel.IDSchema)} NUMBER(9) NOT NULL",
               $"{nameof(LinkTableModel.SchemaName)} VARCHAR2(200) NOT NULL",
               $"{nameof(LinkTableModel.IDTable)} NUMBER(9) NOT NULL",
               $"{nameof(LinkTableModel.TableName)} VARCHAR2(200) NOT NULL",
               $"{nameof(LinkTableModel.IDRecord)} NUMBER(9) NOT NULL", //ID bản ghi tham chiếu
               $"{nameof(LinkTableModel.DataLookUp)} CLOB", //Chứa Json của bản ghi tham chiếu
               
               //Mục được tham thiếu
               $"{nameof(LinkTableModel.IDSchemaRef)} NUMBER(9) NOT NULL",
               $"{nameof(LinkTableModel.SchemaNameRef)} VARCHAR2(200) NOT NULL",
               $"{nameof(LinkTableModel.IDTableRef)} NUMBER(9) NOT NULL",
               $"{nameof(LinkTableModel.TableNameRef)} VARCHAR2(200) NOT NULL",
               $"{nameof(LinkTableModel.IDRecordRef)} NUMBER(9) NOT NULL", //ID bản ghi được tham chiếu
               $"{nameof(LinkTableModel.DataLookUpRef)} CLOB", //Chứa Json của bản ghi được tham chiếu
               
               
               //Giá trị tham chiếu
               $"CONSTRAINT \"ID_{dbName}{idSchema}_PK\" PRIMARY KEY (\"ID\")"
            };

            return cols;
        }
        public async Task<TableInfo> GetTableByName_Schema(string tbname, string schemaname)
        {
            return await (from tb in _dasKTNNRepo.TableInfo.GetAll()
                          join schema in _dasKTNNRepo.SchemaInfo.GetAll() on tb.IDSchema equals schema.ID
                          where tb.DbName == tbname && schema.Code == schemaname
                          select tb).FirstOrDefaultAsync();
        }

        /// <summary>
        /// Add thêm các côt lien quan
        /// </summary>
        /// <param name="columns"></param>
        /// <param name="tableInfo"></param>
        /// <param name="schema"></param>
        /// <param name="columnPaths"></param>
        private async void AddChildColumns(List<ColumnTableInfo> columns, TableInfo tableInfo, IEnumerable<ColumnPathInfo> columnPaths)
        {
            var addColumn = new List<ColumnTableInfo>();
            var maxWeight = columns.Max(x => x.Weight);
            var sysGroupId = (await GetFirstByCode(ColumnTableGroupCode.SYS) ?? new ColumnTableGroup()).ID;

            foreach (var column in columns)
            {
                var paths = columnPaths.Where(n => n.IDColumnTableInfo == column.ID);
                if (column.DataType == (int)ColumnDataType.Parent)
                {
                    addColumn.Add(new ColumnTableInfo
                    {
                        Name = $"ID {column.Name}",
                        DbName = $"{column.DbName}_ID",
                        DataType = (int)ColumnDataType.Integer,
                        IsNullable = true,
                        IsSystem = true,
                        IsDefault = true,
                        IsSubColumn = true
                    });
                    addColumn.Add(new ColumnTableInfo
                    {
                        Name = $"Cấp dữ liệu",
                        DbName = $"{column.DbName}_Level",
                        DataType = (int)ColumnDataType.Integer,
                        IsNullable = true,
                        Weight = maxWeight,
                        IsSystem = true,
                        IsDefault = true,
                        IsSubColumn = true
                    });
                    addColumn.Add(new ColumnTableInfo
                    {
                        Name = $"Dữ liệu tham chiếu {column.Name}",
                        DbName = $"{column.DbName}_DataLookUp",
                        DataType = (int)ColumnDataType.String,
                        IsNullable = true,
                        MaxLen = 4000,
                        IsSystem = true,
                        IsDefault = true,
                        IsSubColumn = true
                    });

                }
                else if (column.DataType == (int)ColumnDataType.TableRef)
                {
                    addColumn.Add(new ColumnTableInfo
                    {
                        Name = $"ID {column.Name}",
                        DbName = $"{column.DbName}_ID",
                        DataType = (int)ColumnDataType.Integer,
                        IsNullable = true,
                        IsSystem = true,
                        IsDefault = true,
                        IsSubColumn = true
                    });
                    addColumn.Add(new ColumnTableInfo
                    {
                        Name = $"Dữ liệu tham chiếu {column.Name}",
                        DbName = $"{column.DbName}_DataLookUp",
                        DataType = (int)ColumnDataType.String,
                        IsNullable = true,
                        MaxLen = 4000,
                        IsSystem = true,
                        IsDefault = true,
                        IsSubColumn = true
                    });
                }
                if (paths.IsNotEmpty())
                {
                    foreach (var path in paths)
                    {
                        maxWeight++;

                        var refCol = columns.FirstOrNewObj(n => n.ID == path.IDColumnPath);
                        var name = $"Đường dẫn theo {refCol.Name}";
                        if (path.PathName == KTNNConst.DefautParentIDPath)
                        {
                            name = $"Đường dẫn theo mã";
                        }
                        else if (path.PathName == KTNNConst.DefautParentNamePath)
                        {
                            name = $"Đường dẫn theo tên";
                        }
                        addColumn.Add(new ColumnTableInfo
                        {
                            Name = name,
                            DbName = path.PathName,
                            DataType = (int)ColumnDataType.String,
                            IsNullable = true,
                            MaxLen = 4000,
                            IsSystem = true,
                            IsDefault = true,
                            IsSubColumn = true
                        });
                    }
                }
            }
            if (addColumn.Count > 0)
            {
                foreach (var item in addColumn)
                {
                    maxWeight++;
                    item.Weight = maxWeight;
                    item.IDTable = tableInfo.ID;
                    item.IDColumnTableGroup = sysGroupId;
                    item.Status = (int)EnumCommon.Status.Active;
                }

                columns.AddRange(addColumn);
            }
        }
        /// <summary>
        /// Xóa thêm các côt lien quan
        /// </summary>
        /// <param name="columns"></param>
        /// <param name="tableInfo"></param>
        /// <param name="schema"></param>
        /// <param name="columnPaths"></param>
        private void DelChildColumns(ref List<ColumnTableInfo> columns, IEnumerable<ColumnTableInfo> allColumns, IEnumerable<ColumnPathInfo> columnPaths)
        {
            var addColumn = new List<ColumnTableInfo>();
            foreach (var column in columns)
            {
                var paths = columnPaths.Where(n => n.IDColumnTableInfo == column.ID);
                if (column.DataType == (int)ColumnDataType.Parent)
                {
                    var cols = allColumns.Where(n => n.DbName == $"{column.DbName}_ID" || n.DbName == $"{column.DbName}_Level" || n.DbName == $"{column.DbName}_DataLookUp");
                    if (cols.IsNotEmpty())
                        addColumn.AddRange(cols);

                }
                else if (column.DataType == (int)ColumnDataType.TableRef)
                {
                    var cols = allColumns.Where(n => n.DbName == $"{column.DbName}_ID" || n.DbName == $"{column.DbName}_DataLookUp");
                    if (cols.IsNotEmpty())
                        addColumn.AddRange(cols);
                }
                if (paths.IsNotEmpty())
                {
                    foreach (var path in paths)
                    {
                        var cols = allColumns.Where(n => n.DbName == path.PathName);
                        if (cols.IsNotEmpty())
                            addColumn.AddRange(cols);
                    }
                }
            }
            if (addColumn.Count > 0)
            {
                columns.AddRange(addColumn);
            }
        }

        public async Task<ColumnTableGroup> GetFirstByCode(ColumnTableGroupCode code)
        {
            return await _dasKTNNRepo.ColumnTableGroup.FirstOrDefaultAsync(n => n.Code == code.ToString());
        }
        #endregion


        #region Thống kê
        public async Task<VMTableInfoStatistic> Statistic(TableInfoCondition condition)
        {
            var model = new VMTableInfoStatistic();
            var schemas = await GetSchemas();

            var temp = from tb in _dasKTNNRepo.TableInfo.GetAll()
                       let p = _dasKTNNRepo.GroupTableInfo.GetAll().Where(p2 => tb.ID == p2.IDTable).FirstOrDefault()

                       where (condition.Keyword.IsEmpty() || tb.Name.ToLower().Contains(condition.Keyword.ToLower())) &&
                        (condition.DBName.IsEmpty() || tb.DbName.ToLower().Contains(condition.DBName.ToLower()))
                        && (condition.IDSchema == 0 || tb.IDSchema == condition.IDSchema)
                        && (condition.IDGroup == 0 || p.IDGroup == condition.IDGroup)
                        && (condition.IDSource == 0 || tb.IDSourceInfo == condition.IDSource)

                       && tb.Status == (int)EnumCommon.Status.Active
                       orderby tb.UpdatedDate ?? tb.CreateDate descending
                       select _mapper.Map<VMTableInfo>(tb);

            var total = await temp.LongCountAsync();
            int totalPage = (int)Math.Ceiling(total / (double)condition.PageSize);
            if (totalPage < condition.PageIndex)
            {
                condition.PageIndex = 1;
            }
            var tablePaging = await temp.ToListAsync();

            //var allTables = await GetTables();

            var vMTables = new List<VMTableItemStatistic>();
            if (tablePaging.IsNotEmpty())
            {
                var where = "";
                var conditions = new List<string>();
                if (condition.StartDate.HasValue)
                {
                    conditions.Add($"{SystemField.NgayTao} >= {condition.StartDate.Value.ToOracleStringDatetime()}");
                }
                if (condition.EndDate.HasValue)
                {
                    conditions.Add($"{SystemField.NgayTao} <= {condition.EndDate.Value.AddDays(1).AddMinutes(-1).ToOracleStringDatetime()}");
                }
                if (conditions.IsNotEmpty())
                {
                    where = $"Where {string.Join(" AND ", conditions)}";
                }
                var selectors = new List<string>();

                foreach (var table in tablePaging)
                {
                    var schema = schemas.FirstOrNewObj(n => n.ID == table.IDSchema);
                    if (schema.Code.IsNotEmpty() && table.DbName.IsNotEmpty())
                    {
                        selectors.Add($"(select count(1) from {schema.Code}.{table.DbName} {where} ) as {table.DbName}");
                    }
                }
                if (selectors.IsNotEmpty())
                {
                    var query = $"select {string.Join(",", selectors)} FROM dual";
                    var _dt = _dynamicDBService.ExecuteSql(query);
                    if (_dt != null && _dt.Rows.Count > 0)
                    {
                        foreach (var table in tablePaging)
                        {
                            var vmTable = Utils.Bind<VMTableItemStatistic>(table.KeyValue());
                            vmTable.RecordCount = Utils.GetInt(_dt, 0, table.DbName);
                            vMTables.Add(vmTable);
                        }
                    }
                }
            }
            model.TableStatistics = vMTables;
            var chartTables = vMTables.Where(n => n.RecordCount > 0);
            var order = vMTables.OrderByDescending(n => n.RecordCount).Select(n => n.ID).ToList();
            var chartData = new
            {
                labels = chartTables.Select(n => n.Name).ToArray(),
                data = chartTables.Select(n => n.RecordCount).ToArray(),
            };
            tablePaging = tablePaging.OrderBy(n => order.IndexOf(n.ID)).Skip((condition.PageIndex - 1) * condition.PageSize).Take(condition.PageSize).ToList();
            model.Tables = new PaginatedList<VMTableInfo>(tablePaging, (int)total, condition.PageIndex, condition.PageSize);
            model.ChartData = chartData;
            return model;
        }
        public async Task<ServiceResult> StatisticExport(TableInfoCondition condition)
        {
            try
            {
                var model = await Statistic(condition);
                var hearsers = new List<Header>
                {
                    new Header("STT", 8),
                    new Header("Tên bảng dữ liệu"),
                    new Header("Tên bảng dữ liệu trong SQL"),
                    new Header("Số bản ghi"),
                };

                var export = new ExportExtend
                {
                    Data = model.TableStatistics.Cast<dynamic>().ToList(),
                    Cols = new List<Col>
                {
                    new Col{
                        DataType = 5
                    },
                    new Col("Name"),
                    new Col("DbName"),
                    new Col("RecordCount"),
                },
                    Headers = hearsers
                };
                var rs = await _excelService.ExportExcel(export, "Thống kê bảng dữ liệu");
                return rs;
            }
            catch (LogicException ex)
            {
                return new ServiceResultError(ex.Message);
            }
            catch (Exception ex)
            {
                Guid.NewGuid().ToString();
                _logger.LogError(ex);
                return new ServiceResultError("Có lỗi khi tải file biểu mẫu import");
            }

        }

        public Task<ColumnTableInfo> GetColumnOrderDefaul(int idtable)
        {
            return _dasKTNNRepo.ColumnTableInfo.FirstOrDefaultAsync(n => n.IsOrder == true && n.IDTable == idtable);
        }

        #endregion
    }
}