﻿using System;

namespace DAS.Application.Constants
{
    public static class KTNNConst
    {
        #region TableConfig
        public static readonly string DefautParentIDPath = "DuongDanTheoMa";
        public static readonly string DefautParentNamePath = "DuongDanTheoTen";


        /// <summary>
        /// Đuôi của bảng liên kết
        /// </summary>
        public static readonly string TableLinkNameExt = "_LienKet";
        #endregion

        #region Import
        public static readonly int ImportHeaderIndex = 1;
        public static readonly int ImportDataIndex = 2;
        public static readonly int ImportDefaultSheet = 1;

        #endregion

        public static readonly string IdentityValue = "xxyyzz123identity";
    }

    public static class CreateTypeConst
    {
        public static readonly int MacDinh = 10;
        public static readonly int ChiaTach = 30;
        public static readonly int SapNhap = 40;

    }
}
