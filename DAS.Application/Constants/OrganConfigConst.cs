﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAS.Application.Constants
{
    public class OrganConfigConst
    {
        public static string DEFAULT_PWD_SSO = "DEFAULT_PWD_SSO";
        public static string DEFAULT_ORGAN_CODE_SSO = "DEFAULT_ORGAN_CODE_SSO";
        public static string DEFAULT_AGENCY_CODE_SSO = "DEFAULT_AGENCY_CODE_SSO";
        public static string DEFAULT_PERMISSION_SSO = "DEFAULT_PERMISSION_SSO";

        public static string CONFIG_ORGAN_EXPIRYDATE = "CONFIG_ORGAN_EXPIRYDATE";
        public static string CONFIG_ORGAN_PROCESS = "CONFIG_ORGAN_PROCESS";
        public static string AUTO_APPROVE_DIGITAL_PROFILE = "AUTO_APPROVE_DIGITAL_PROFILE";
        public static string ENABLE_ACESS_PROFILE_BY_ARCHIVE_AGENCY = "ENABLE_ACESS_PROFILE_BY_ARCHIVE_AGENCY";
    }
}
