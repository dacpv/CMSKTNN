﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAS.Application.Constants
{
    public static class CommonConst
    {
        public static readonly string Success = "Success";
        public static readonly string Error = "Error";
        public static readonly string Warning = "Warning";
        public static readonly string DfDateFormat = "dd/MM/yyyy";
        public static readonly string AdminOrgan = "Admin cơ quan";
        public static readonly string BorrowCart = "BorrowCart";
        public static readonly string DestructionProfile = "DestructionProfile";
    }
}