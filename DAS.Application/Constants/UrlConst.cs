﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAS.Application.Constants
{
    public static class UrlConst
    {
        public static readonly string AccessDenied = "/Error/AccessDenied";
    }
}
