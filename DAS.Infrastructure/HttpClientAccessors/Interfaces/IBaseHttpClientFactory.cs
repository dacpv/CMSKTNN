﻿namespace DAS.Infrastructure.HttpClientAccessors.Interfaces
{
    public interface IBaseHttpClientFactory
    {
        IBaseHttpClient Create();
    }
}
