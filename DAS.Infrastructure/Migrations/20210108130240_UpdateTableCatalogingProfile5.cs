﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAS.Infrastructure.Migrations
{
    public partial class UpdateTableCatalogingProfile5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "InUsing",
                table: "CatalogingProfile",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "InUsing",
                table: "CatalogingProfile");
        }
    }
}
