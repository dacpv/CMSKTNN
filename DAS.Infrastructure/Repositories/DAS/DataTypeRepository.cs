﻿using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Models.DAS;
using DAS.Infrastructure.Contexts;

namespace DAS.Infrastructure.Repositories.DAS
{
    public class DataTypeRepository : DasBaseRepository<DataType>, IDataTypeRepository
    {
        public DataTypeRepository(DASContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}