﻿using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Models.DAS;
using DAS.Infrastructure.Contexts;
namespace DAS.Infrastructure.Repositories.DAS
{
    public class ReaderInOrganRepository : DasBaseRepository<ReaderInOrgan>, IReaderInOrganRepository
    {
        public ReaderInOrganRepository(DASContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
