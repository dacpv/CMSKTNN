﻿using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Models.DAS;
using DAS.Infrastructure.Contexts;

namespace DAS.Infrastructure.Repositories.DAS
{
    public class CatalogingDocRepository : DasBaseRepository<CatalogingDoc>, ICatalogingDocRepository
    {
        public CatalogingDocRepository(DASContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
