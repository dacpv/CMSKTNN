﻿using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Models.DAS;
using DAS.Infrastructure.Contexts;

namespace DAS.Infrastructure.Repositories.DAS
{
    public class DeliveryRecordRepository : DasBaseRepository<DeliveryRecord>, IDeliveryRecordRepository
    {
        public DeliveryRecordRepository(DASContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
