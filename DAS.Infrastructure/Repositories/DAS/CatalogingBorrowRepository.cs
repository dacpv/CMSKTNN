﻿using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Models.DAS;
using DAS.Infrastructure.Contexts;

namespace DAS.Infrastructure.Repositories.DAS
{
    public class CatalogingBorrowRepository : DasBaseRepository<CatalogingBorrow>, ICatalogingBorrowRepository
    {
        public CatalogingBorrowRepository(DASContext repositoryContext)
            : base(repositoryContext)
        {
        }
         
    }
}
