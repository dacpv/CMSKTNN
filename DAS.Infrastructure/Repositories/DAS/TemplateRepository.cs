﻿using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Models.DAS;
using DAS.Infrastructure.Contexts;

namespace DAS.Infrastructure.Repositories.DAS
{
    public class TemplateRepository : DasBaseRepository<Template>, ITemplateRepository
    {
        public TemplateRepository(DASContext repositoryContext)
            : base(repositoryContext)
        {

        }
    }
}
