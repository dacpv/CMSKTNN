﻿using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Models.DAS;
using DAS.Infrastructure.Contexts;

namespace DAS.Infrastructure.Repositories.DAS
{
    public class CatalogingBorrowDocRepository : DasBaseRepository<CatalogingBorrowDoc>, ICatalogingBorrowDocRepository
    {
        public CatalogingBorrowDocRepository(DASContext repositoryContext)
            : base(repositoryContext)
        {
        }
         
    }
}
