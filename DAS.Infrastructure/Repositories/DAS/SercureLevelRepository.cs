﻿using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Models.DAS;
using DAS.Infrastructure.Contexts;

namespace DAS.Infrastructure.Repositories.DAS
{
    public class SercureLevelRepository : DasBaseRepository<SercureLevel>, ISercureLevelRepository
    {
        public SercureLevelRepository(DASContext repositoryContext) : base(repositoryContext)
        {
        }
    }
}
