﻿using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Models.DAS;
using DAS.Infrastructure.Contexts;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace DAS.Infrastructure.Repositories.DAS
{
    public class CategoryFieldRepository : DasBaseRepository<CategoryField>, ICategoryFieldRepository
    {
        public CategoryFieldRepository(DASContext repositoryContext)
            : base(repositoryContext)
        {
        } 
    }
}