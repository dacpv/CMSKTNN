﻿using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Models.DAS;
using DAS.Infrastructure.Contexts;

namespace DAS.Infrastructure.Repositories.DAS
{
    public class PlanAgencyRepository : DasBaseRepository<PlanAgency>, IPlanAgencyRepository
    {
        public PlanAgencyRepository(DASContext repositoryContext)
            : base(repositoryContext)
        {
        }

    }
}