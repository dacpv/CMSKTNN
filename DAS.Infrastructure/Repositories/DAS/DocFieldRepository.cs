﻿using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Models.DAS;
using DAS.Infrastructure.Contexts;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace DAS.Infrastructure.Repositories.DAS
{
    public class DocFieldRepository : DasBaseRepository<DocField>, IDocFieldRepository
    {
        public DocFieldRepository(DASContext repositoryContext)
            : base(repositoryContext)
        {
        }
        public async Task<bool> IsCodeExist(string code, int status, int id = 0)
        {
            return await DasContext.CategoryType.AnyAsync(n => n.Code.Trim() == code.Trim() && n.ID != id && n.Status == status);
        }
    }
}