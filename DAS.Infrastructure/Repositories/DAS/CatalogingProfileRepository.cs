﻿using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Models.DAS;
using DAS.Infrastructure.Contexts;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace DAS.Infrastructure.Repositories.DAS
{
    public class CatalogingProfileRepository : DasBaseRepository<CatalogingProfile>, ICatalogingProfileRepository
    {
        public CatalogingProfileRepository(DASContext repositoryContext)
            : base(repositoryContext)
        {
        }

        public async Task<bool> IsCodeExist(string code, int status, int id = 0)
        {
            return await DasContext.PlanProfile.AnyAsync(n => n.FileCode.Trim() == code.Trim() && n.ID != id && n.Status == status);
        }
    }
}