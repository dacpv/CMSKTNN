﻿using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Models.DAS;
using DAS.Infrastructure.Contexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAS.Infrastructure.Repositories.DAS
{
    public class DocTypeRepository : DasBaseRepository<DocType>, IDocTypeRepository
    {
        public DocTypeRepository(DASContext repositoryContext)
            : base(repositoryContext)
        {
        }

        public async Task<bool> IsCodeExist(string code, int status, int id = 0)
        {
            return await DasContext.DocType.AnyAsync(n => n.Code.Trim() == code.Trim() && n.ID != id && n.Status == status);
        }
    }
}