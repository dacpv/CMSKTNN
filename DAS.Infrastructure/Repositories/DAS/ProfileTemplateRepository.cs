﻿using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Models.DAS;
using DAS.Infrastructure.Contexts;

namespace DAS.Infrastructure.Repositories.DAS
{
    public class ProfileTemplateRepository : DasBaseRepository<ProfileTemplate>, IProfileTemplateRepository
    {
        public ProfileTemplateRepository(DASContext repositoryContext) : base(repositoryContext)
        {
        }
    }
}
