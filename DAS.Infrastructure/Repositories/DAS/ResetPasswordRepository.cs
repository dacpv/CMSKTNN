﻿using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Models.DAS;
using DAS.Infrastructure.Contexts;

namespace DAS.Infrastructure.Repositories.DAS
{
    public class ResetPasswordRepository : DasBaseRepository<ResetPassword>, IResetPasswordRepository
    {
        public ResetPasswordRepository(DASContext repositoryContext) : base(repositoryContext)
        {

        }
    }
}
