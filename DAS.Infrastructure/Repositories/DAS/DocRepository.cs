﻿using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Models.DAS;
using DAS.Infrastructure.Contexts;

namespace DAS.Infrastructure.Repositories.DAS
{
    public class DocRepository : DasBaseRepository<Doc>, IDocRepository
    {
        public DocRepository(DASContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
