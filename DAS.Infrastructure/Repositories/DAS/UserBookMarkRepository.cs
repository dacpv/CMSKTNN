﻿using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Models.DAS;
using DAS.Infrastructure.Contexts;

namespace DAS.Infrastructure.Repositories.DAS
{
    public class UserBookMarkRepository : DasBaseRepository<UserBookmark>, IUserBookMarkRepository
    {
        public UserBookMarkRepository(DASContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
