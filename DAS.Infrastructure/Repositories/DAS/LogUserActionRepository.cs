﻿using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Models.DAS;
using DAS.Infrastructure.Contexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAS.Infrastructure.Repositories.DAS
{
    public class LogUserActionRepository : DasBaseRepository<LogUserAction>, ILogUserActionRepository
    {
        public LogUserActionRepository(DASContext repositoryContext)
            : base(repositoryContext)
        {
        }

       
    }
}