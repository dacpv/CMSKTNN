﻿using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Models.DAS;
using DAS.Infrastructure.Contexts;

namespace DAS.Infrastructure.Repositories.DAS
{
    public class ExpiryDateRepository : DasBaseRepository<ExpiryDate>, IExpiryDateRepository
    {
        public ExpiryDateRepository(DASContext repositoryContext) : base(repositoryContext)
        {
        }
    }
}
