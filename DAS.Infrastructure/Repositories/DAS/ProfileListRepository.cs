﻿using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Models.DAS;
using DAS.Infrastructure.Contexts;

namespace DAS.Infrastructure.Repositories.DAS
{
    public class ProfileListRepository : DasBaseRepository<ProfileList>, IProfileListRepository
    {
        public ProfileListRepository(DASContext repositoryContext) : base(repositoryContext)
        {
        }
    }
}
