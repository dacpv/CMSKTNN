﻿using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Models.DAS;
using DAS.Infrastructure.Contexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace DAS.Infrastructure.Repositories.DAS
{
    public class DestructionProfileRepository : DasBaseRepository<DestructionProfile>, IDestructionProfileRepository
    {
        public DestructionProfileRepository(DASContext repositoryContext)
           : base(repositoryContext)
        {
        }
    }
}
