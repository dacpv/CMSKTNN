﻿using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Models.DAS;
using DAS.Infrastructure.Contexts;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace DAS.Infrastructure.Repositories.DAS
{
    public class PositionRepository : DasBaseRepository<Position>, IPositionRepository
    {
        public PositionRepository(DASContext repositoryContext)
            : base(repositoryContext)
        {
        }

        public async Task<bool> IsCodeExist(string code, int id = 0)
        {

            return await DasContext.Position.AnyAsync(n => n.Code.Trim() == code.Trim() && n.ID != id && n.Status >0 /*Todo enum*/) ;
        }
    }
}