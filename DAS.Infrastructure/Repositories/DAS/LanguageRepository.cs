﻿using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Models.DAS;
using DAS.Infrastructure.Contexts;

namespace DAS.Infrastructure.Repositories.DAS
{
    public class LanguageRepository : DasBaseRepository<Language>, ILanguageRepository
    {
        public LanguageRepository(DASContext repositoryContext) : base(repositoryContext)
        {
        }
    }
}
