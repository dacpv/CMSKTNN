﻿using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Models.DAS;
using DAS.Infrastructure.Contexts;

namespace DAS.Infrastructure.Repositories.DAS
{
    public class ProfileDestroyedRepository : DasBaseRepository<ProfileDestroyed>, IProfileDestroyedRepository
    {
        public ProfileDestroyedRepository(DASContext repositoryContext) : base(repositoryContext)
        {
        }
    }
}
