﻿using System;
using System.Collections.Generic;
using System.Text;
using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Models.DAS;
using DAS.Infrastructure.Contexts;

namespace DAS.Infrastructure.Repositories.DAS
{
    public class TemplateParamRepository : DasBaseRepository<TemplateParam>, ITemplateParamRepository
    {
        public TemplateParamRepository(DASContext repositoryContext)
            : base(repositoryContext)
        {

        }
    }
}
