﻿using DAS.Domain.Interfaces;
using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Interfaces.DASCongViec;
using DAS.Domain.Interfaces.DASNotify;
using DAS.Domain.Models.DAS;
using DAS.Domain.Models.DasCongViec;
using DAS.Domain.Models.DASNotify;
using DAS.Infrastructure.Contexts;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace DAS.Infrastructure.Repositories.DASCongViec
{
    public class NhatKyCongViecRepository : DasCongViecBaseRepository<NhatKyCongViec>, INhatKyCongViecRepository
    {
        public NhatKyCongViecRepository(DASCongViecContext repositoryContext)
            : base(repositoryContext)
        {

        }

    }

}
