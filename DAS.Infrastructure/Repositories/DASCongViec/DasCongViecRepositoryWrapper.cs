﻿using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Interfaces.DASCongViec;
using DAS.Domain.Interfaces.DASNotify;
using DAS.Infrastructure.Contexts;
using DAS.Infrastructure.Repositories.DASCongViec;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAS.Infrastructure.Repositories.DASNotify
{
    public class DasCongViecRepositoryWrapper: IDasCongViecRepositoryWrapper
    {
        #region ctor
        private readonly DASCongViecContext _repoContext;

        public DasCongViecRepositoryWrapper(DASCongViecContext repositoryContext)
        {
            _repoContext = repositoryContext;
        }
        #endregion

        #region properties
        private IChiTietNhatKyRepository _chiTietNhatKy;

        public IChiTietNhatKyRepository ChiTietNhatKy
        {
            get
            {
                if (_chiTietNhatKy == null)
                {
                    _chiTietNhatKy = new ChiTietNhatKyRepository(_repoContext);
                }
                return _chiTietNhatKy;
            }
        }
        private INhatKyCongViecRepository _NhatKyCongViec;

        public INhatKyCongViecRepository NhatKyCongViec
        {
            get
            {
                if (_NhatKyCongViec == null)
                {
                    _NhatKyCongViec = new NhatKyCongViecRepository(_repoContext);
                }
                return _NhatKyCongViec;
            }
        }
        private IChiTietNhatKyNguoiXLRepository _ChiTietNhatKyNguoiXL;

        public IChiTietNhatKyNguoiXLRepository ChiTietNhatKyNguoiXL
        {
            get
            {
                if (_ChiTietNhatKyNguoiXL == null)
                {
                    _ChiTietNhatKyNguoiXL = new ChiTietNhatKyNguoiXLRepository(_repoContext);
                }
                return _ChiTietNhatKyNguoiXL;
            }
        }
        private IStaticBaoCaoRepository _ChiTietTon;

        public IStaticBaoCaoRepository StaticBaoCao
        {
            get
            {
                if (_ChiTietTon == null)
                {
                    _ChiTietTon = new StaticBaoCaoRepository(_repoContext);
                }
                return _ChiTietTon;
            }
        }
        #endregion

        public async Task SaveAync()
        {
            await _repoContext.SaveChangesAsync();
        }
    }
}
