﻿using DAS.Domain.Interfaces.DASCongViec;
using DAS.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAS.Infrastructure.Repositories.DASCongViec
{
    public class DasCongViecBaseRepository<T> : BaseRepository<T> where T : class
    {
        protected DASCongViecContext DASCongViecContext { get; set; }

        public DasCongViecBaseRepository(DASCongViecContext repositoryContext) : base(repositoryContext)
        {
            DASCongViecContext = (DASCongViecContext)base.Context;
        }
        
    }
}
