﻿using DAS.Domain.Interfaces.DASKTNN;
using DAS.Domain.Models.DASKTNN;
using DAS.Infrastructure.Contexts;

namespace DAS.Infrastructure.Repositories.DASKTNN
{
    public class TableInfoApiConfigRepository : DasTabeTestBaseRepository<TableInfoApiConfig>, ITableInfoApiConfigRepository
    {
        public TableInfoApiConfigRepository(DASKTNNContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }

}
