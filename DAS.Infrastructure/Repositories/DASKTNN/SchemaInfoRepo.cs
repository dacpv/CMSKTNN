﻿using DAS.Domain.Interfaces.DasKTNN;
using DAS.Domain.Models.DASKTNN;
using DAS.Infrastructure.Contexts;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace DAS.Infrastructure.Repositories.DASKTNN
{
    public class SchemaInfoRepo : DasTabeTestBaseRepository<SchemaInfo>, ISchemaInfoRepo
    {
        public SchemaInfoRepo(DASKTNNContext repositoryContext) : base(repositoryContext)
        {
        }
        public async Task<bool> IsCodeExist(string code, int id = 0)
        {
            return await DASKTNNContext.SchemaInfo.AnyAsync(n => n.Code.Trim() == code.Trim() && n.ID != id);
        }
    }
}
