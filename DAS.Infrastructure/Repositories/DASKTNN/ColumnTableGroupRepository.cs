﻿using DAS.Domain.Interfaces;
using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Interfaces.DASKTNN;
using DAS.Domain.Interfaces.DASNotify;
using DAS.Domain.Models.DAS;
using DAS.Domain.Models.DASKTNN;
using DAS.Domain.Models.DASNotify;
using DAS.Infrastructure.Contexts;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace DAS.Infrastructure.Repositories.DASKTNN
{
    public class ColumnTableGroupRepository : DasTabeTestBaseRepository<ColumnTableGroup>, IColumnTableGroupRepository
    {
        public ColumnTableGroupRepository(DASKTNNContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }

}
