﻿using DAS.Domain.Interfaces.DasKTNN;
using DAS.Domain.Models.DASKTNN;
using DAS.Infrastructure.Contexts;

namespace DAS.Infrastructure.Repositories.DASKTNN
{
    public class CollectionReportRepository : DasTabeTestBaseRepository<CollectionReport>, ICollectionReportRepository
    {
        public CollectionReportRepository(DASKTNNContext repositoryContext) 
            : base(repositoryContext)
        {
        }
    }
}
