﻿using DAS.Domain.Interfaces.DASKTNN;
using DAS.Domain.Models.DASKTNN;
using DAS.Infrastructure.Contexts;

namespace DAS.Infrastructure.Repositories.DASKTNN
{
    public class TableTestRepository : DasTabeTestBaseRepository<TableTest>, ITableTestRepository
    {
        public TableTestRepository(DASKTNNContext repositoryContext)
            : base(repositoryContext)
        {
        }

    }

}
