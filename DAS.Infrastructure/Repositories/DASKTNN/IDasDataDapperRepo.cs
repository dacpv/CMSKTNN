﻿using DAS.Infrastructure.DapperORM;

namespace DAS.Infrastructure.Repositories.DASKTNN
{
    public interface IDasDataDapperRepo: IDapperRepo
    {
    }

    public interface IDynamicDBDapperRepo : IDapperRepo
    {

    }
    public interface IAPIManageDBDapperRepo : IDapperRepo
    {

    }
}
