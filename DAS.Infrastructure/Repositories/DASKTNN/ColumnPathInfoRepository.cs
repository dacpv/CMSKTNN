﻿using DAS.Domain.Interfaces.DASKTNN;
using DAS.Domain.Models.DASKTNN;
using DAS.Infrastructure.Contexts;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace DAS.Infrastructure.Repositories.DASKTNN
{
    public class ColumnPathInfoRepository : DasTabeTestBaseRepository<ColumnPathInfo>, IColumnPathInfoRepository
    {
        public ColumnPathInfoRepository(DASKTNNContext repositoryContext)
            : base(repositoryContext)
        {
        }

        public async Task<bool> IsPathNameExist(string name, int idColumnTable, int ignoreStatus, int id = 0)
        {
            return await DASKTNNContext.ColumnPathInfo.AnyAsync(n => n.PathName.Trim().ToLower() == name.Trim().ToLower()
          && n.IDColumnTableInfo == idColumnTable && n.Status != ignoreStatus && n.ID != id);
        }

        public async Task<bool> IsIDColumnPathExist(int idColumn, int idColumnTable, int ignoreStatus, int id = 0)
        {
            return await DASKTNNContext.ColumnPathInfo.AnyAsync(n => n.IDColumnPath == idColumnTable
             && n.IDColumnTableInfo == idColumnTable && n.Status != ignoreStatus && n.ID != id);
        }
    }
}
