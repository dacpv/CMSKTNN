﻿using DAS.Domain.Interfaces;
using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Interfaces.DASKTNN;
using DAS.Domain.Interfaces.DASNotify;
using DAS.Domain.Models.DAS;
using DAS.Domain.Models.DASKTNN;
using DAS.Domain.Models.DASNotify;
using DAS.Infrastructure.Contexts;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace DAS.Infrastructure.Repositories.DASKTNN
{
    public class TableInfoRepository : DasTabeTestBaseRepository<TableInfo>, ITableInfoRepository
    {
        public TableInfoRepository(DASKTNNContext repositoryContext)
            : base(repositoryContext)
        {
        }

      
        public async Task<bool> IsNameExist(string name, int ignoreStatus, int id = 0)
        {
            return await DASKTNNContext.TableInfo.AnyAsync(n => n.Name.Trim() == name.Trim() && n.ID != id  && n.Status != ignoreStatus);
        }
        public async Task<bool> IsSqlNameExist(string name, int idSchema, int ignoreStatus, int id = 0)
        {
            return await DASKTNNContext.TableInfo.AnyAsync(n => n.DbName.Trim().ToLower() == name.Trim().ToLower()
            && n.IDSchema == idSchema
            && n.ID != id 
            && n.Status != ignoreStatus);
        }
       
    }

}
