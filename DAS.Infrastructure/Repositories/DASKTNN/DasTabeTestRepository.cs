﻿using DAS.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAS.Infrastructure.Repositories.DASKTNN
{
    public class DasTabeTestBaseRepository<T> : BaseRepository<T> where T : class
    {
        protected DASKTNNContext DASKTNNContext { get; set; }

        public DasTabeTestBaseRepository(DASKTNNContext repositoryContext) : base(repositoryContext)
        {
            DASKTNNContext = (DASKTNNContext)base.Context;
        }
    }
}
