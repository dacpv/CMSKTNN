﻿using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Interfaces.DasKTNN;
using DAS.Domain.Interfaces.DASKTNN;
using DAS.Domain.Interfaces.DASNotify;
using DAS.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAS.Infrastructure.Repositories.DASKTNN
{
    public class DasTabeTestRepositoryWrapper : IDasKTNNRepositoryWrapper
    {
        #region ctor
        private readonly DASKTNNContext _repoContext;

        public DasTabeTestRepositoryWrapper(DASKTNNContext repositoryContext)
        {
            _repoContext = repositoryContext;
        }
        #endregion

        #region properties
        private ITableTestRepository _tableTest;

        public ITableTestRepository TableTest
        {
            get
            {
                if (_tableTest == null)
                {
                    _tableTest = new TableTestRepository(_repoContext);
                }   
                return _tableTest;
            }
        }
        private ISoucreInfoRepository _SoucreInfo;

        public ISoucreInfoRepository SoucreInfo
        {
            get
            {
                if (_SoucreInfo == null)
                {
                    _SoucreInfo = new SoucreInfoRepository(_repoContext);
                }
                return _SoucreInfo;
            }
        }
        private IGroupInfoRepository _groupInfo;

        public IGroupInfoRepository GroupInfo
        {
            get
            {
                if (_groupInfo == null)
                {
                    _groupInfo = new GroupInfoRepository(_repoContext);
                }   
                return _groupInfo;
            }
        }
        private IGroupTableInfoRepository _groupTableInfo;
        public IGroupTableInfoRepository GroupTableInfo
        {
            get
            {
                if (_groupTableInfo == null)
                {
                    _groupTableInfo = new GroupTableInfoRepository(_repoContext);
                }
                return _groupTableInfo;
            }
        }
        private ITableInfoRepository _tableInfo;

        public ITableInfoRepository TableInfo
        {
            get
            {
                if (_tableInfo == null)
                {
                    _tableInfo = new TableInfoRepository(_repoContext);
                }
                return _tableInfo;
            }
        }
        private IColumnTableInfoRepository _columnTableInfo;

        public IColumnTableInfoRepository ColumnTableInfo
        {
            get
            {
                if (_columnTableInfo == null)
                {
                    _columnTableInfo = new ColumnTableInfoRepository(_repoContext);
                }   
                return _columnTableInfo;
            }
        }
         
        private IColumnPathInfoRepository _columnPathInfo;

        public IColumnPathInfoRepository ColumnPathInfo
        {
            get
            {
                if (_columnPathInfo == null)
                {
                    _columnPathInfo = new ColumnPathInfoRepository(_repoContext);
                }   
                return _columnPathInfo;
            }
        }

        private ISchemaInfoRepo _schemaInfo;

        public ISchemaInfoRepo SchemaInfo
        {
            get
            {
                if (_schemaInfo == null)
                {
                    _schemaInfo = new SchemaInfoRepo(_repoContext);
                }
                return _schemaInfo;
            }
        }
        
        private ITableInfoApiConfigRepository _tableInfoApiConfig;

        public ITableInfoApiConfigRepository TableInfoApiConfig
        {
            get
            {
                if (_tableInfoApiConfig == null)
                {
                    _tableInfoApiConfig = new TableInfoApiConfigRepository(_repoContext);
                }
                return _tableInfoApiConfig;
            }
        }


        private IColumnTableGroupRepository _columnTableGroup;

        public IColumnTableGroupRepository ColumnTableGroup
        {
            get
            {
                if (_columnTableGroup == null)
                {
                    _columnTableGroup = new ColumnTableGroupRepository(_repoContext);
                }
                return _columnTableGroup;
            }
        }

        private ISharedAppRepository _sharedApp;
        public ISharedAppRepository SharedApp
        {
            get
            {
                if (_sharedApp == null)
                {
                    _sharedApp = new SharedAppRepository(_repoContext);
                }
                return _sharedApp;
            }
        }
        
        private ITableInfoApiConfigSharedAppRepository _tableInfoApiConfigSharedApp;
        public ITableInfoApiConfigSharedAppRepository TableInfoApiConfigSharedApp
        {
            get
            {
                if (_tableInfoApiConfigSharedApp == null)
                {
                    _tableInfoApiConfigSharedApp = new TableInfoApiConfigSharedAppRepository(_repoContext);
                }
                return _tableInfoApiConfigSharedApp;
            }
        }

        
        private ITanXuatTruyCapAPIRepository _TanXuatTruyCapAPI;
        public ITanXuatTruyCapAPIRepository TanXuatTruyCapAPI
        {
            get
            {
                if (_TanXuatTruyCapAPI == null)
                {
                    _TanXuatTruyCapAPI = new TanXuatTruyCapAPIRepository(_repoContext);
                }
                return _TanXuatTruyCapAPI;
            }
        }

        public ICollectionReportRepository _collectionReport;
        public ICollectionReportRepository CollectionReport
        {
            get
            {
                if (_collectionReport == null)
                {
                    _collectionReport = new CollectionReportRepository(_repoContext);
                }
                return _collectionReport;
            }
        }

        #endregion

        public async Task SaveAync()
        {
            await _repoContext.SaveChangesAsync();
        }
    }
}
