﻿using DAS.Domain.Interfaces;
using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Interfaces.DASKTNN;
using DAS.Domain.Interfaces.DASNotify;
using DAS.Domain.Models.DAS;
using DAS.Domain.Models.DASKTNN;
using DAS.Domain.Models.DASNotify;
using DAS.Infrastructure.Contexts;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace DAS.Infrastructure.Repositories.DASKTNN
{
    public class GroupInfoRepository : DasTabeTestBaseRepository<GroupInfo>, IGroupInfoRepository
    {
        public GroupInfoRepository(DASKTNNContext repositoryContext)
            : base(repositoryContext)
        {
        }

      
        public async Task<bool> IsNameExist(string name,  int id = 0)
        {
            return await DASKTNNContext.GroupInfo.AnyAsync(n => n.Name.Trim() == name.Trim() && n.ID != id );
        }
    }
    public class GroupTableInfoRepository : DasTabeTestBaseRepository<GroupTableInfo>, IGroupTableInfoRepository
    {
        public GroupTableInfoRepository(DASKTNNContext repositoryContext)
            : base(repositoryContext)
        {
        }


        public async Task<bool> IsNameExist(string name, int id = 0)
        {
            return await DASKTNNContext.GroupInfo.AnyAsync(n => n.Name.Trim() == name.Trim() && n.ID != id);
        }
    }
}
