﻿using DAS.Domain.Interfaces.DASKTNN;
using DAS.Domain.Models.DASKTNN;
using DAS.Infrastructure.Contexts;

namespace DAS.Infrastructure.Repositories.DASKTNN
{
    public class TableInfoApiConfigSharedAppRepository : DasTabeTestBaseRepository<TableInfoApiConfigSharedApp>, ITableInfoApiConfigSharedAppRepository
    {
        public TableInfoApiConfigSharedAppRepository(DASKTNNContext repositoryContext)
            : base(repositoryContext)
        {
        }
         
    }
}
