﻿using DAS.Domain.Interfaces;
using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Interfaces.DASKTNN;
using DAS.Domain.Interfaces.DASNotify;
using DAS.Domain.Models.DAS;
using DAS.Domain.Models.DASKTNN;
using DAS.Domain.Models.DASNotify;
using DAS.Infrastructure.Contexts;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace DAS.Infrastructure.Repositories.DASKTNN
{
    public class SoucreInfoRepository : DasTabeTestBaseRepository<SoucreInfo>, ISoucreInfoRepository
    {
        public SoucreInfoRepository(DASKTNNContext repositoryContext)
            : base(repositoryContext)
        {
        }
      
        public async Task<bool> IsNameExist(string name,  int id = 0)
        {
            return await DASKTNNContext.SoucreInfo.AnyAsync(n => n.Name.Trim() == name.Trim() && n.ID != id );
        }
        public async Task<bool> IsCodeExist(string code, int id = 0)
        {
            return await DASKTNNContext.SoucreInfo.AnyAsync(n => n.Code.Trim() == code.Trim() && n.ID != id);
        }
    }
    
}
