﻿using DAS.Domain.Interfaces.DASKTNN;
using DAS.Domain.Models.DASKTNN;
using DAS.Infrastructure.Contexts;

namespace DAS.Infrastructure.Repositories.DASKTNN
{
    public class TanXuatTruyCapAPIRepository : DasTabeTestBaseRepository<TanXuatTruyCapAPI>, ITanXuatTruyCapAPIRepository
    {
        public TanXuatTruyCapAPIRepository(DASKTNNContext repositoryContext)
            : base(repositoryContext)
        {
        }
         
    }
}
