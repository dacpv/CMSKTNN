﻿using DAS.Domain.Interfaces.DASKTNN;
using DAS.Domain.Models.DASKTNN;
using DAS.Infrastructure.Contexts;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace DAS.Infrastructure.Repositories.DASKTNN
{
    public class ColumnTableInfoRepository : DasTabeTestBaseRepository<ColumnTableInfo>, IColumnTableInfoRepository
    {
        public ColumnTableInfoRepository(DASKTNNContext repositoryContext)
            : base(repositoryContext)
        {
        }

        public async Task<bool> IsDataTypeExist(int dataType, int idTable, int ignoreStatus, int id = 0)
        {
            return await DASKTNNContext.ColumnTableInfo.AnyAsync(n => n.DataType == dataType
          && n.IDTable == idTable
          && n.Status != ignoreStatus && n.ID != id);
        }

        public async Task<bool> IsNameExist(string name, int idTable, int ignoreStatus, int id = 0)
        {
            return await DASKTNNContext.ColumnTableInfo.AnyAsync(n => n.Name.Trim().ToLower() == name.Trim().ToLower()
            && n.IDTable == idTable

            && n.Status != ignoreStatus && n.ID != id);

        }
        public async Task<bool> IsSqlNameExist(string name, int idTable, int ignoreStatus, int id = 0)
        {
            return await DASKTNNContext.ColumnTableInfo.AnyAsync(n => n.DbName.Trim().ToLower() == name.Trim().ToLower()
            && n.IDTable == idTable
            && n.Status != ignoreStatus && n.ID != id);
        }
    }
}
