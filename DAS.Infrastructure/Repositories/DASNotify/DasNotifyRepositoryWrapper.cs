﻿using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Interfaces.DASNotify;
using DAS.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAS.Infrastructure.Repositories.DASNotify
{
    public class DasNotifyRepositoryWrapper : IDasNotifyRepositoryWrapper
    {
        #region ctor
        private readonly DASNotifyContext _repoContext;

        public DasNotifyRepositoryWrapper(DASNotifyContext repositoryContext)
        {
            _repoContext = repositoryContext;
        }
        #endregion

        #region properties
        private INotificationRepository _notification;

        public INotificationRepository Notification
        {
            get
            {
                if (_notification == null)
                {
                    _notification = new NotificationRepository(_repoContext);
                }
                return _notification;
            }
        }

        #endregion

        public async Task SaveAync()
        {
            await _repoContext.SaveChangesAsync();
        }
    }
}
