﻿using DAS.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAS.Infrastructure.Repositories.DASNotify
{
    public class DasNotifyBaseRepository<T> : BaseRepository<T> where T : class
    {
        protected DASNotifyContext DasNotifyContext { get; set; }

        public DasNotifyBaseRepository(DASNotifyContext repositoryContext) : base(repositoryContext)
        {
            DasNotifyContext = (DASNotifyContext)base.Context;
        }
    }
}
