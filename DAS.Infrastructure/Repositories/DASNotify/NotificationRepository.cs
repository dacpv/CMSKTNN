﻿using DAS.Domain.Interfaces;
using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Interfaces.DASNotify;
using DAS.Domain.Models.DAS;
using DAS.Domain.Models.DASNotify;
using DAS.Infrastructure.Contexts;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace DAS.Infrastructure.Repositories.DASNotify
{
    public class NotificationRepository : DasNotifyBaseRepository<Notification>, INotificationRepository
    {
        public NotificationRepository(DASNotifyContext repositoryContext)
            : base(repositoryContext)
        {
        }

    }

}
