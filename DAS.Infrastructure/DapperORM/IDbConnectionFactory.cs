﻿using System.Data;

namespace DAS.Infrastructure.DapperORM
{
    public interface IDbConnectionFactory
    {
        IDbConnection CreateDbConnection(DatabaseConnectionName connectionName,bool IsSQLServer);

    }
}
