﻿using DAS.Domain.Models.DASNotify;
using DAS.Infrastructure.Constants;
using DAS.Utility;
using Microsoft.EntityFrameworkCore;

namespace DAS.Infrastructure.Contexts
{
    public class DASNotifyContext: DbContext
    {
        public DASNotifyContext(DbContextOptions<DASNotifyContext> options) : base(options)
        {
        }

        public DASNotifyContext() : base()
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseOracle(ConfigUtils.GetConnectionString("DASNotify"));
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            //builder.Entity<User>().HasIndex(u => u.Email).IsUnique();
            //builder.Entity<Job>().HasIndex(u => new { u.Name, u.ProjectId }).IsUnique();
            //builder.Entity<Project>().HasIndex(u => u.Name).IsUnique();
            builder.NamesToSnakeCase();
            //builder.HasDefaultSchema("DASTEST");
            builder.HasDefaultSchema(ConfigUtils.GetKeyValue("SchemaOracle"));
        }

        public DbSet<Notification> Notification { get; set; }
       // public DbSet<NotificationType> NotificationType { get; set; }
    }

   
}
