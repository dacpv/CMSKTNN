﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Models.Abstractions;
using DAS.Domain.Models.CustomModels;
using DAS.Domain.Models.DAS;
using DAS.Domain.Models.DasCongViec;
using DAS.Domain.Models.DASKTNN;
using DAS.Infrastructure;
using DAS.Infrastructure.Constants;
using DAS.Infrastructure.ContextAccessors;
using DAS.Utility;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace DAS.Infrastructure.Contexts
{
    public class DASKTNNContext : DbContext
    {
        private readonly IUserPrincipalService _userPrincipalService;
        private readonly ILogBySqlRepository _logBySql;

        public static readonly ILoggerFactory loggerFactory = LoggerFactory.Create(builder =>
        {
            builder
                   .AddFilter(DbLoggerCategory.Database.Command.Name, LogLevel.Warning)
                   .AddFilter(DbLoggerCategory.Query.Name, LogLevel.Debug)
                   .AddConsole();
        }
        );

        public DASKTNNContext(DbContextOptions<DASKTNNContext> options, IUserPrincipalService userPrincipalService, ILogBySqlRepository logBySql) : base(options)
        {
            _userPrincipalService = userPrincipalService;
            _logBySql = logBySql;
        }

        public DASKTNNContext() : base()
        {
        }

        public DASKTNNContext(IUserPrincipalService userPrincipalService) : base()
        {
            _userPrincipalService = userPrincipalService;
        }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseOracle(ConfigUtils.GetConnectionString("DASKTNN"));
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            //builder.Entity<User>().HasIndex(u => u.Email).IsUnique();
            //builder.Entity<Job>().HasIndex(u => new { u.Name, u.ProjectId }).IsUnique();
            //builder.Entity<Project>().HasIndex(u => u.Name).IsUnique();
            builder.NamesToSnakeCase();
            //builder.HasDefaultSchema("DASTEST");
            builder.HasDefaultSchema(ConfigUtils.GetKeyValue("SchemaOracle"));
        }

        public virtual DbSet<SchemaInfo> SchemaInfo { get; set; }

        public DbSet<NhatKyCongViec> NhatKyCongViec { get; set; }
        private object CreateWithValues(EntityEntry values)
        {
            object entity = Activator.CreateInstance(values.Entity.GetType());
            foreach (var property in values.Entity.GetType().GetProperties())
            {
                //var property = type.GetProperty(propname.);
                property.SetValue(entity, values.Property(property.Name).OriginalValue);
            }

            return entity;
        }
        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {


            try
            {
                var modifiedEntries = ChangeTracker.Entries()
                    .Where(x => x.Entity is IAuditable
                                && (x.State == EntityState.Added || x.State == EntityState.Modified)).ToList();

                if (modifiedEntries.Any())
                {
                    foreach (var entry in modifiedEntries)
                    {
                        var entity = entry.Entity as IAuditable;
                        if (entity == null) continue;

                        if (entry.State == EntityState.Added)
                        {
                            entity.CreatedBy = _userPrincipalService.UserId;
                            entity.CreateDate = DateTime.UtcNow;
                        }
                        else
                        {
                            entity.UpdatedBy = _userPrincipalService.UserId;
                            entity.UpdatedDate = DateTime.UtcNow;
                        }
                    }
                }
            }
            catch
            {
                // ignored
            }
            //Log
            if (ConfigUtils.GetKeyValue("LogConfig", "LogCRUD") == "true")
            {
                try
                {
                    ChangeTracker.DetectChanges();
                    var changeEntries = ChangeTracker.Entries().Where(x => x.State == EntityState.Added || x.State == EntityState.Modified || x.State == EntityState.Deleted).ToList();
                    if (changeEntries.Any())
                    {
                        foreach (var entry in changeEntries)
                        {
                            var entityObj = entry.Entity;
                            LogInfo logInfo;
                            switch (entry.State)
                            {
                                case EntityState.Deleted:
                                    logInfo = new LogInfo(entityObj.GetType().GetProperty("ID") != null ? entityObj.GetType().GetProperty("ID").GetValue(entityObj).ToString() : string.Empty, entityObj.GetType().Name, LogStateConst.DELETE, entityObj, null);
                                    await _logBySql.InsertCRUDLog(logInfo);
                                    break;
                                case EntityState.Modified:
                                    if (entityObj.GetType().GetProperty("Status") != null && entityObj.GetType().GetProperty("Status").GetValue(entityObj).ToString() == "0")
                                        logInfo = new LogInfo(entityObj.GetType().GetProperty("ID") != null ? entityObj.GetType().GetProperty("ID").GetValue(entityObj).ToString() : string.Empty, entityObj.GetType().Name, LogStateConst.DELETELOGIC, CreateWithValues(entry), entityObj);
                                    else
                                        logInfo = new LogInfo(entityObj.GetType().GetProperty("ID") != null ? entityObj.GetType().GetProperty("ID").GetValue(entityObj).ToString() : string.Empty, entityObj.GetType().Name, LogStateConst.UPDATE, CreateWithValues(entry), entityObj);

                                    await _logBySql.InsertCRUDLog(logInfo);

                                    break;
                                case EntityState.Added:
                                    logInfo = new LogInfo(entityObj.GetType().GetProperty("ID") != null ? entityObj.GetType().GetProperty("ID").GetValue(entityObj).ToString() : string.Empty, entityObj.GetType().Name, LogStateConst.INSERT, null, entityObj);
                                    await _logBySql.InsertCRUDLog(logInfo);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }

            return await base.SaveChangesAsync(true, cancellationToken);
        }

        public DbSet<TableInfo> TableInfo { get; set; }
        public DbSet<GroupInfo> GroupInfo { get; set; }
        public DbSet<SoucreInfo> SoucreInfo { get; set; }
        public DbSet<CollectionReport> CollectionReport { get; set; }
        public DbSet<GroupTableInfo> GroupTableInfo { get; set; }
        public DbSet<ColumnTableInfo> ColumnTableInfo { get; set; }
        public DbSet<ColumnPathInfo> ColumnPathInfo { get; set; }
        public DbSet<TableInfoApiConfig> TableInfoApiConfig { get; set; }
        public DbSet<ColumnTableGroup> ColumnTableGroup { get; set; }
        public DbSet<SharedApp> SharedApp { get; set; }
        public DbSet<TableInfoApiConfigSharedApp> TableInfoApiConfigSharedApp { get; set; }
        public DbSet<TanXuatTruyCapAPI> TanXuatTruyCapAPI { get; set; }
    }
}