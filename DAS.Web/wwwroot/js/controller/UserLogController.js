﻿var SystemLogConfig = {

    initChart: function () {
        let dict = JSON.parse($("#tableStatisticData").val());


        if (!CommonJs.IsEmpty(dict)) {
            var ctx = document.getElementById('archive_chart');
            var chartOptions = {
                scales: {
                    xAxes: [{
                        barPercentage: 1,
                        categoryPercentage: 0.6
                    }]

                }
            };

            new Chart(ctx, {
                type: 'bar',
                data: dict,
                options: chartOptions
            });
        }
          
    },
    getRandomColorHex: function () {
        var hex = "0123456789ABCDEF",
            color = "#";
        for (var i = 1; i <= 6; i++) {
            color += hex[Math.floor(Math.random() * 16)];
        }
        return color;
    }
};
var InitSystemLogConfigStatistic = function () {
    SystemLogConfig.initChart();
}