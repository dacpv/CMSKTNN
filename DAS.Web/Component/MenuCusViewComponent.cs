﻿using AutoMapper;
using DAS.Application.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using DAS.Web.Models;
using System.Linq;
using DAS.Application.Interfaces.DasKTNN;

namespace DAS.Web.Component
{
    public class MenuCusViewComponent : ViewComponent
    {
        private readonly IModuleService _module;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;
        private readonly ITableInfoServices _tableinfo;
        private readonly ISchemaInfoService _schemainfo;
        private readonly IGroupInfoService _groupinfo;
        public MenuCusViewComponent(IModuleService module, IConfiguration configuration, IMapper mapper, ITableInfoServices tableInfo, ISchemaInfoService schema, IGroupInfoService groupinfo)
        {
            _module = module;
            _configuration = configuration;
            _mapper = mapper;
            _tableinfo = tableInfo;
            _schemainfo = schema;
            _groupinfo = groupinfo;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            //var moduleExclude = _configuration["ModuleExclude"].Split("|", StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToList();
            var model = new List<LeftMenuComponentModel>();
            var tables = await _tableinfo.GetsListShowMenu();
            var schemas = await _schemainfo.GetAllList();
            var groupinfos = await _groupinfo.GetsList();
            var grouptables = await _groupinfo.GetsListTable();
            ViewData["TableAll"] = tables.ToList();
            ViewData["SchemaAll"] = schemas.ToList();
            ViewData["GroupInfoAll"] = groupinfos.ToList();
            ViewData["GroupTableInfoAll"] = grouptables.ToList();
            var isDMDC = _configuration["IsDanhMucDungChung"];
            ViewBag.IsDanhMucDungChung = isDMDC;
            int maxIdLeftMenu = 0;
            var lstMenu = await _module.GetModuleForCurrentUser();
            if (lstMenu != null)
            {
                foreach (var item in lstMenu)
                {

                    var menu = new LeftMenuComponentModel
                    {
                        Id = item.ID + maxIdLeftMenu,
                        Href = item.Url,
                        Name = item.Name,
                        Controller = item.Controller,
                        Action = "",
                        Icon = "rounded-circle bg-primary is-icons-fix " + item.Icon,
                        ParentId = item.ParentId != 0 ? item.ParentId + maxIdLeftMenu : item.ParentId,
                        Pattern = "",
                        Code=item.Code,
                        //RouteName = !item.Url.Contains("/Position") ? item.RouterName : CustomsConfig.RouterDanhMucChuVu,
                        SortOrder = item.SortOrder
                    };

                    model.Add(menu);
                }
            }

            return View(model.AsEnumerable());
        }
    }
}
