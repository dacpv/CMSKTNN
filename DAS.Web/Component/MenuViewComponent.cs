﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DAS.Web.Models;
using DAS.Application.Interfaces;
using Microsoft.AspNetCore.Authorization;
using DAS.Application.Models.CustomModels;
using DAS.Utility.LogUtils;
using DAS.Infrastructure.ContextAccessors;
using DAS.Application.Interfaces.DasKTNN;

namespace DAS.Web.Component
{
    public class MenuViewComponent : ViewComponent
    {
        private readonly IModuleService _module;
        private readonly ITableInfoServices _tableinfo;
        private readonly ISchemaInfoService _schemainfo;
        private readonly IUserBookMarkServices _userBookMark;
        private readonly IGroupInfoService _groupinfo;
        public MenuViewComponent(IModuleService module, IUserBookMarkServices userBookMark,ITableInfoServices tableInfo,ISchemaInfoService schema, IGroupInfoService groupinfo)
        {
            _module = module;
            _userBookMark = userBookMark;
            _tableinfo = tableInfo;
            _schemainfo = schema;
            _groupinfo = groupinfo;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var listBookMark = ((await _userBookMark.GetBookMark())?.Modules) ?? new List<int>();
            var dictBookmark = new Dictionary<string, int[]>();
            var model = await _module.GetsActive();
            var dicMenu = model.Where(x => x.Url != null && x.Url != "" && x.Url != "#").ToDictionary(x => x.Url.ToLower(), v => v.Name);
            var dicModule = model.Where(x => x.Url != null && x.Url != "" && x.Url != "#").ToDictionary(x => x.Url.ToLower(), v => v.ID);
            foreach (var item in dicMenu)
            {
                string keyurl = item.Key;
                int idModule = dicModule.GetValueOrDefault(keyurl);
                int statusBookMark = listBookMark.Contains(idModule) ? 1 : 0;
                dictBookmark.Add(keyurl, new int[] { idModule, statusBookMark });
            }
            TempData["ModuleBookmark"] = dictBookmark;
            if (ViewData["Breadcrumb"] != null && ViewData["Breadcrumb"] is Dictionary<string, string>)
            {
                var listKeyBreadCrumb = ((Dictionary<string, string>)ViewData["Breadcrumb"]).Keys.ToList();
                foreach (string key in listKeyBreadCrumb)
                {
                    if (dicMenu.ContainsKey(key.ToLower()))
                    {
                        ((Dictionary<string, string>)ViewData["Breadcrumb"])[key] = dicMenu.GetValueOrDefault(key.ToLower());
                    }
                }
            }

            //------------------Làm tạm để hiển thị menu
           var tables = await _tableinfo.GetsList();
            var schemas = await _schemainfo.GetAllList();
            var groupinfos = await _groupinfo.GetsList();
            var grouptables = await _groupinfo.GetsListTable();
            ViewData["TableAll"] = tables.ToList();
            ViewData["SchemaAll"] = schemas.ToList();
            ViewData["GroupInfoAll"] = groupinfos.ToList();
            ViewData["GroupTableInfoAll"] = grouptables.ToList();
            //--------------------------------------------
            return View(model);
        }
    }
}
