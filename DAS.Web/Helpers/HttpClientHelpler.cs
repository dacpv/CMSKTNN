﻿using DAS.Utility;
using DAS.Utility.LogUtils;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace DAS.Web.Customs.Helper
{
    public class HttpClientHelpler<T>
    {
        private static ILogger logger = LogManager.GetCurrentClassLogger();
        public async Task<T> Excute(string url, Dictionary<string, string> parameters)
        {
            T model = Activator.CreateInstance<T>();
            if (url.IsEmpty())
                return model;
            var content = new FormUrlEncodedContent(parameters);
            using (var client = new HttpClient())
            {
                try
                {
                    client.Timeout = TimeSpan.FromSeconds(60);
                    var response = await client.PostAsync(url, content).Result.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<T>(response);
                    return result;
                }
                catch (Exception ex)
                {
                    logger.Error(Utils.Serialize(ex));
                    return model;
                }
            }
        }
        private static XElement RemoveAllNamespaces(XElement xmlDocument)
        {
            if (!xmlDocument.HasElements)
            {
                XElement xElement = new XElement(xmlDocument.Name.LocalName);
                xElement.Value = xmlDocument.Value;

                foreach (XAttribute attribute in xmlDocument.Attributes())
                    xElement.Add(attribute);

                return xElement;
            }
            return new XElement(xmlDocument.Name.LocalName, xmlDocument.Elements().Select(el => RemoveAllNamespaces(el)));
        }
        public async Task<T> ExcuteXML(string url, string strXML)
        {
            T model = Activator.CreateInstance<T>();
            if (url.IsEmpty())
                return model;
            try
            {
                var httpClient = new HttpClient();
                var stringContent = new StringContent(strXML, Encoding.UTF8, "application/xml");
                var respone = await httpClient.PostAsync(url, stringContent).Result.Content.ReadAsStringAsync();
                XElement xmlDocumentWithoutNs = RemoveAllNamespaces(XElement.Parse(respone));
                var xmlWithoutNs = xmlDocumentWithoutNs.ToString();
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xmlWithoutNs);
                var json = JsonConvert.SerializeXmlNode(doc.ChildNodes[0].ChildNodes[0].ChildNodes[0], Newtonsoft.Json.Formatting.None, true);
                model = JsonConvert.DeserializeObject<T>(json);
            }
            catch (HttpRequestException ex)
            {
                logger.Error(Utils.Serialize(ex));
                return model;
            }
            return model;
        }
    }
}