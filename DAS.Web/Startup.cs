﻿using DAS.Application.AutoMapper;
using DAS.Application.Interfaces;
using DAS.Application.Middwares;
using DAS.Application.Services;
using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Interfaces.DASNotify;
using DAS.Infrastructure.ContextAccessors;
using DAS.Infrastructure.Contexts;
using DAS.Infrastructure.Notifications;
using DAS.Infrastructure.Repositories.DAS;
using DAS.Infrastructure.Repositories.DASNotify;
using DAS.Utility;
using DAS.Utility.LogUtils;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using DAS.Web.Configuration;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using DAS.Utility.Binding;
using DAS.Domain.Interfaces.DASCongViec;
using DAS.Infrastructure.Repositories.DASCongViec;
using DAS.Infrastructure.Repositories.DASKTNN;
using DAS.Infrastructure.DapperORM;
using System.Collections.Generic;
using DAS.Domain.Models.DAS;
using DAS.Application.Enums;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using DAS.Web.Helpers;
using Microsoft.AspNetCore.Authentication.WsFederation;
using System.Net.Http;
using System.Security.Claims;
using DAS.Application.Constants;
using DAS.Domain.Enums;
//using LinqToDB.DataProvider.Oracle;
///using Oracle.EntityFrameworkCore
namespace DAS.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            //Config NLog
            var appBasePath = string.IsNullOrEmpty(Configuration["LogsFolder"]) ? Directory.GetCurrentDirectory() : Configuration["LogsFolder"];
            NLog.GlobalDiagnosticsContext.Set("appbasepath", appBasePath);
            NLog.LogManager.LoadConfiguration(String.Concat(Directory.GetCurrentDirectory(), "/nlog.config")).GetCurrentClassLogger();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var authServerConfiguration = Configuration.GetSection(nameof(AuthServerConfiguration)).Get<AuthServerConfiguration>();

            services.AddDbContext<DASContext>(o => o.UseOracle(ConfigUtils.GetConnectionString("DASContext")));
            services.AddDbContext<DASNotifyContext>(o => o.UseOracle(ConfigUtils.GetConnectionString("DASNotify")));
            services.AddDbContext<DASCongViecContext>(o => o.UseOracle(ConfigUtils.GetConnectionString("DASCongViec")));
            services.AddDbContext<DASKTNNContext>(o => o.UseOracle(ConfigUtils.GetConnectionString("DASKTNN")));
            services.AddDbContext<DASKTNNContext>(o => o.UseOracle(ConfigUtils.GetConnectionString("DASKTNN")));

            //Add dapper connection
            ConfigDapperContext(services);
            services.AddSingleton<ILoggerManager, LoggerManager>();

            // Config HttpContextAccessor
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            //if (Configuration["UseAuthServer"] == "false" || Configuration["UseAuthServer"] == null)
            //{
            //    services.AddAuthentication(options =>
            //        {
            //            options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            //        })
            //        .AddCookie(options =>
            //        {
            //            options.LoginPath = "/Account/Login";
            //            options.AccessDeniedPath = "/Error/Error/";
            //            options.ExpireTimeSpan = TimeSpan.FromHours(1);
            //            options.SlidingExpiration = true;
            //        });
            //}
            //else
            //{
            //    services.AddAuthentication(options =>
            //        {
            //            options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            //            options.DefaultChallengeScheme = OpenIdConnectDefaults.AuthenticationScheme;
            //        })
            //        .AddCookie(options =>
            //        {
            //            options.LoginPath = "/Account/Login";
            //            options.AccessDeniedPath = "/Error/Error/";
            //            options.ExpireTimeSpan = TimeSpan.FromHours(1);
            //            options.SlidingExpiration = true;
            //        })
            //        .AddOpenIdConnect(OpenIdConnectDefaults.AuthenticationScheme, options =>
            //        {
            //            options.SignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            //            options.Authority = authServerConfiguration.IdentityServerBaseUrl;
            //            options.RequireHttpsMetadata = authServerConfiguration.RequireHttpsMetadata;

            //            options.ClientId = authServerConfiguration.ClientId;
            //            options.ClientSecret = authServerConfiguration.ClientSecret;
            //            options.ResponseType = "code id_token";

            //            options.SaveTokens = true;
            //            options.GetClaimsFromUserInfoEndpoint = true;

            //            // add ClientScopes
            //            foreach (var clientScope in authServerConfiguration.ClientScopes)
            //            {
            //                options.Scope.Add(clientScope);
            //            }
            //        });
            //}

            //SSO
            ConfigAuthen_SSO_Cookie(services);

            services.AddCors(o => o.AddPolicy("DASCorsPolicy", b =>
            {
                b.AllowAnyHeader()
                    .AllowAnyMethod()
                    .SetIsOriginAllowed(_ => true)
                    .AllowCredentials();
            }));

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "DAS API",
                    Version = "v1",
                    Description = "A DAS API",
                    Contact = new OpenApiContact
                    {
                        Name = "huytd",
                        Email = "huytd@fsivietnam.com.vn"
                    }
                });
            });

            //DistributedCache
            //services.AddDistributedSqlServerCache(o =>
            //{
            //    o.ConnectionString = ConfigUtils.GetConnectionString("DASCache");
            //    o.SchemaName = "dbo";
            //    o.TableName = "CacheTable";
            //});
            services.AddDistributedMemoryCache();

            services.AddControllersWithViews().AddRazorRuntimeCompilation();
            services.AddSignalR();
            //Inject repos
            services.AddScoped<IDasCongViecRepositoryWrapper, DasCongViecRepositoryWrapper>();
            services.AddScoped<IDasNotifyRepositoryWrapper, DasNotifyRepositoryWrapper>();
            services.AddScoped<IDasRepositoryWrapper, DasRepositoryWrapper>();
            services.AddScoped<IUserPrincipalService, UserPrincipalService>();
            services.AddScoped<ILogBySqlRepository, LogBySqlRepository>();
            services.AddScoped<IDasKTNNRepositoryWrapper, DasTabeTestRepositoryWrapper>();

            services.AddSingleton<IConnectionManager, ConnectionManager>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IHubNotificationHelper, HubNotificationHelper>();


            //Inject logic services
            services.DependencyInjectionService();

            //bind datetime
            services.AddMvc(options =>
            {
                options.ModelBinderProviders.Insert(0, new DateTimeModelBinderProvider());
            });
            //AutoMapper
            DasAutoMapper.Configure(services);

            ////Generate Html To PDF
            //services.AddWkhtmltopdf();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider serviceProvider)
        {
            if ( env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                //app.UseExceptionHandler("/Home/Error");
                //// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                //app.UseHsts();
                app.UseMiddleware<ExceptionMiddleware>();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseRouting();

            app.UseAuthorization();

            app.UseSwagger();
            app.UseSwaggerUI(s =>
            {
                s.SwaggerEndpoint("/swagger/v1/swagger.json", "Project Api v1.1");
                s.RoutePrefix = "swagger";
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapHub<NotificationHub>("/NotificationHub");
            });

            var provider = new FileExtensionContentTypeProvider();
            provider.Mappings[".properties"] = "application/octet-stream";
            app.UseStaticFiles(new StaticFileOptions
            {
                ContentTypeProvider = provider
            });
            // UpdatePermissonTable
            UpdatePermissonTable(app);

            //Load cache
            var permissionService = (IPermissionService)serviceProvider.GetService(typeof(IPermissionService));
            permissionService.LoadCacheAllPermission().Wait();
        }

        private void ConfigAuthen_SSO_Cookie(IServiceCollection services)
        {
            var isSSO = Configuration["IntergratedSso"] == "true";
            var typeSSO = Configuration["SSOType"]; //ADFS - Open
            if (!isSSO) //chỉ sử dụng cookie để đăng nhập
            {
                services.AddAuthentication(options =>
                {
                    options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                    //options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                    //options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    //options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                    .AddCookie(options =>
                    {
                        options.LoginPath = "/Account/Login";
                        options.AccessDeniedPath = "/Error/Error/";
                        options.ExpireTimeSpan = TimeSpan.FromDays(1);
                        options.SlidingExpiration = true;
                        //options.Cookie.Name = ".AspNet.SharedCookie";
                        options.Cookie.Path = "/";
                        //options.Cookie.Domain = ".contoso.com";     // Share cookies across subdomains such as first_subdomain.contoso.com, second_subdomain.contoso.com
                    })
                    .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, cfg =>
                    {
                        cfg.RequireHttpsMetadata = false;
                        cfg.SaveToken = true;

                        cfg.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidIssuer = Configuration["Tokens:Issuer"],
                            ValidAudience = Configuration["Tokens:Audience"],
                            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Tokens:Key"])),
                            //Do not check the expiry of token
                            ValidateLifetime = false
                        };
                    })
                    .AddJwtBearer("Bearer_Ltls", cfg =>    // verify token for LTLS
                    {
                        cfg.RequireHttpsMetadata = false;
                        cfg.SaveToken = true;

                        cfg.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidIssuer = Configuration["DASLTLSConfiguration:Tokens:Issuer"],
                            ValidAudience = Configuration["DASLTLSConfiguration:Tokens:Audience"],
                            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["DASLTLSConfiguration:Tokens:Key"])),
                            //Do not check the expiry of token
                            ValidateLifetime = false
                        };
                    });
            }
            else if (isSSO && typeSSO == "WsFederation") //ADFS + cookie
            {
                services.Configure<CookiePolicyOptions>(options =>
                {
                    options.MinimumSameSitePolicy = SameSiteMode.Unspecified;
                    options.Secure = CookieSecurePolicy.SameAsRequest;
                    options.OnAppendCookie = cookieContext =>
                        AuthenticationHelpers.CheckSameSite(cookieContext.Context, cookieContext.CookieOptions);
                    options.OnDeleteCookie = cookieContext =>
                        AuthenticationHelpers.CheckSameSite(cookieContext.Context, cookieContext.CookieOptions);
                });

                services.AddAuthentication(options =>
                {
                    options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                    options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = WsFederationDefaults.AuthenticationScheme;

                })
                    .AddWsFederation(authenticationScheme: "WsFederation",
                         displayName: "Single Signin",
                         configureOptions: cfg =>
                         {
                             //https://docs.microsoft.com/en-us/aspnet/core/security/authentication/ws-federation?view=aspnetcore-5.0#add-ws-federation-as-an-external-login-provider-for-aspnet-core-identity
                             cfg.Wtrealm = Configuration["Wtrealm"];
                             cfg.MetadataAddress = Configuration["MetadataAddress"]; //địa chỉ đăng nhập
                             cfg.SignOutWreply = Configuration["SignOutWreply"];
                             cfg.UseTokenLifetime = true;
                             //cfg.Events = OnAffterSSOLoginWsFederation();
                             cfg.AccessDeniedPath = "/Error/Error/";
                             cfg.BackchannelHttpHandler = new HttpClientHandler() { ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator };

                         })
                    .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme, options =>
                    {
                        options.LoginPath = "/Account/Login";
                        options.AccessDeniedPath = "/Error/Error/";
                        options.ExpireTimeSpan = TimeSpan.FromDays(1);
                        options.SlidingExpiration = true;
                        //options.Cookie.Name = ".AspNet.SharedCookie";
                        options.Cookie.Path = "/";
                        //options.Cookie.Domain = ".contoso.com";     // Share cookies across subdomains such as first_subdomain.contoso.com, second_subdomain.contoso.com
                    })
                .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, cfg =>
                {
                    cfg.RequireHttpsMetadata = false;
                    cfg.SaveToken = true;
                    cfg.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidIssuer = Configuration["Tokens:Issuer"],
                        ValidAudience = Configuration["Tokens:Audience"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Tokens:Key"])),
                        //Do not check the expiry of token
                        ValidateLifetime = false
                    };
                });
            }
        }

        private void ConfigDapperContext(IServiceCollection services)
        {
            //For one Database
            //services.AddTransient<IDbConnection>(o => new SqlConnection(Configuration.GetConnectionString("DASContext")));

            //ForMutipleDatabase
            var connectionDict = new Dictionary<DatabaseConnectionName, string>
            {
                { DatabaseConnectionName.DasDataConnection, this.Configuration.GetConnectionString("DASKTNN") },

                { DatabaseConnectionName.ExcuteGenrateDBConnection, this.Configuration.GetConnectionString("DASOracleExcute") },
                { DatabaseConnectionName.APIManageDBConnection, this.Configuration.GetConnectionString("DASAPIManage") },
                //{ DatabaseConnectionName.DbThoConnetion, this.Configuration.GetConnectionString("DbThoContext") }
           
            };

            // Inject this dict
            services.AddSingleton<IDictionary<DatabaseConnectionName, string>>(connectionDict);

            // Inject the factory
            services.AddScoped<IDbConnectionFactory, DapperDbConnectionFactory>();
            services.AddScoped<IDasDataDapperRepo, DasDataDapperRepo>();
            services.AddScoped<IDynamicDBDapperRepo, DynamicDBDapperRepo>();
            services.AddScoped<IAPIManageDBDapperRepo, APIManageDBDapperRepo>();

            //services.AddScoped<IDbThoDapperRepo, DbThoDapperRepo>();

        }

        /// <summary>
        /// Cấu hình sự kiện sau khi login thành công
        /// </summary>
        /// <returns></returns>
        private WsFederationEvents OnAffterSSOLoginWsFederation()
        {
            return new WsFederationEvents
            {
                OnSecurityTokenValidated = async (securityTokenValidatedContext) =>
                {
                    var context = securityTokenValidatedContext.HttpContext;
                    var _userPrincipalService = context.RequestServices.GetService<IUserPrincipalService>();
                    var _userService = context.RequestServices.GetService<IUserService>();
                    var _permissionService = context.RequestServices.GetService<IPermissionService>();
                    var _organConfigServices = context.RequestServices.GetService<IOrganConfigServices>();
                    var _organServices = context.RequestServices.GetService<IOrganServices>();
                    var _agencyServices = context.RequestServices.GetService<IAgencyServices>();
                    var _groupPermissionService = context.RequestServices.GetService<IGroupPermissionService>();

                    var userName = context.User.Identity.Name;
                    if (!string.IsNullOrEmpty(userName))
                    {
                        var existedUser = await _userService.GetByAccountName(userName);
                        if (existedUser == null)
                        {
                            string pwd = "123456";
                            //string pwd = (await _organConfigServices.GetConfigByCode(OrganConfigConst.DEFAULT_PWD_SSO) ?? "123456").ToString();
                            //string organCode = (await _organConfigServices.GetConfigByCode(OrganConfigConst.DEFAULT_ORGAN_CODE_SSO) ?? "").ToString();
                            //string agencyCode = (await _organConfigServices.GetConfigByCode(OrganConfigConst.DEFAULT_AGENCY_CODE_SSO) ?? "").ToString();
                            var groupPermission = await _permissionService.GetPermisstionSSO();

                            // get OrganId & AgencyId
                            //int organId = 0;
                            //if (!string.IsNullOrEmpty(organCode))
                            //{
                            //    var organ = await _organServices.GetByCode(organCode);
                            //    organId = organ.ID;
                            //}

                            //int agencyId = 0;
                            //if (!string.IsNullOrEmpty(agencyCode))
                            //{
                            //    var agency = await _agencyServices.GetByCodeAndOrgan(agencyCode, organId);
                            //    agencyId = agency.ID;
                            //}

                            //int groupPermissionId = 0;
                            //if (!string.IsNullOrEmpty(groupPermission.Name))
                            //{
                            //    var groupPermission = await _groupPermissionService.GetByName(permissionSSOName);
                            //    groupPermissionId = groupPermission.ID;
                            //}

                            var user = new User
                            {
                                AccountName = _userPrincipalService.UserName,
                                Name = _userPrincipalService.UserName,
                                Email = _userPrincipalService.Email,
                                Password = StringUltils.Md5Encryption(pwd),
                                //IDOrgan = organId,
                                //IDAgency = agencyId,
                                Status = (int)EnumCommon.Status.Active,
                                TypeAccount = (int)EnumCommon.TypeAccount.SSO,
                                Description = "User SSO",
                                CreateDate = DateTime.Now,
                            };

                            /* 
                             * 1. tạo mới User
                             * 2. cập nhật lại quyền user -> gán quyền mặc định cho trước
                             */
                            var createSSOResult = await _userService.CreateSSOUser(user, groupPermission.ID);

                            //3 update claim current UserId
                            _userPrincipalService.AddUpdateClaim(ClaimTypes.NameIdentifier, user.ID.ToString());
                            _userPrincipalService.AddUpdateClaim(CustomClaimTypes.Username, user.AccountName);
                            _userPrincipalService.AddUpdateClaim(CustomClaimTypes.FullName, user.Name);
                            _userPrincipalService.AddUpdateClaim(ClaimTypes.Email, user.Email);
                            _userPrincipalService.AddUpdateClaim(CustomClaimTypes.IDOrgan, user.IDOrgan.ToString());
                            _userPrincipalService.AddUpdateClaim(CustomClaimTypes.IDAgency, user.IDAgency.ToString());

                            if (createSSOResult.Code == CommonConst.Success)
                            {
                                // update CreatedDate
                                user.CreatedBy = user.ID;
                                await _userService.Update(user);

                                //4.Cập nhật lại cache
                                await _permissionService.UpdateCachePermission(user.ID);
                                await _userService.UpdateCacheUser(user.ID);
                            }
                        }
                        else
                        {
                            //3 update claim current UserId
                            _userPrincipalService.AddUpdateClaim(ClaimTypes.NameIdentifier, existedUser.ID.ToString());
                            _userPrincipalService.AddUpdateClaim(CustomClaimTypes.Username, existedUser.AccountName);
                            _userPrincipalService.AddUpdateClaim(CustomClaimTypes.FullName, existedUser.Name);
                            _userPrincipalService.AddUpdateClaim(ClaimTypes.Email, existedUser.Email);
                            _userPrincipalService.AddUpdateClaim(CustomClaimTypes.IDOrgan, existedUser.IDOrgan.ToString());
                            _userPrincipalService.AddUpdateClaim(CustomClaimTypes.IDAgency, existedUser.IDAgency.ToString());
                        }

                    }

                }
            };
        }

        /// <summary>
        /// Update Permission table
        /// </summary>
        /// <returns></returns>
        private void UpdatePermissonTable(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var ct = serviceScope.ServiceProvider.GetRequiredService<DASContext>();
                //Get Module
                var moduleHasCode = ct.Module.Where(s => s.Code != 0).ToList();
                if (moduleHasCode.Count == 0)
                    return;
                var lstPermission = new List<Permission>();
                //Ma trận phân quyền
                var dictMockData = new Dictionary<int, List<EnumPermission.Type>>();
                dictMockData = MapEnum.MatrixPermission;
                foreach (var module in moduleHasCode)
                {
                    var dicType = dictMockData.GetValueOrDefault(module.Code);
                    if (dicType != null && dicType.Count > 0)
                    {
                        foreach (var type in dicType)
                        {
                            lstPermission.Add(new Permission
                            {
                                IDModule = module.ID,
                                Name = StringUltils.GetEnumDescription(type),
                                Type = (int)type
                            });
                        }
                    }
                }
                var oldPer = ct.Permission.ToList();
                var listDelete = oldPer.Where(x => !lstPermission.Any(n => n.IDModule == x.IDModule && n.Type == x.Type)).ToList();
                var listInsert = lstPermission.Where(x => !oldPer.Any(n => n.IDModule == x.IDModule && n.Type == x.Type)).ToList();

                if (listDelete.Count > 0)
                {
                    ct.Permission.RemoveRange(listDelete);
                }

                if (listInsert.Count > 0)
                {
                    ct.Permission.AddRange(listInsert);
                }
                ct.SaveChanges();
            }
        }
    }

}
