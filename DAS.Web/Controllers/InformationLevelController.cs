﻿using AutoMapper;
using DAS.Application.Enums;
using DAS.Application.Interfaces;
using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels.DasKTNN;
using DAS.Utility;
using DAS.Web.Attributes;
using DAS.Utility.LogUtils;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace DAS.Web.Controllers
{
    public class InformationLevelController : BaseController
    {
        #region Properties
        private readonly IInputInfoServices _inputTypeService;
        private readonly IMapper _mapper;
        private readonly ILoggerManager _logger;
        private readonly IConfiguration _configuration;
        #endregion Properties

        #region Ctor
        public InformationLevelController(IMapper mapper, IConfiguration configuration, ILoggerManager logger, IInputInfoServices inputInfoService)
        {
            _inputTypeService = inputInfoService;
            _mapper = mapper;
            _logger = logger;
            _configuration = configuration;
        }
        #endregion

        #region Gets
        [HasPermission((int)EnumModule.Code.INPUTINFO, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> Index(InputInfoCondition condition)
        {
            var isDMDC = _configuration["IsDanhMucDungChung"];
            ViewBag.IsDanhMucDungChung = isDMDC;
            var result = await _inputTypeService.GetTableLeftTree(condition);
            ViewBag.TagActive = condition.IDTable;
            return View(result);
        }
        public async Task<IActionResult> SearchByCondition(InputInfoCondition condition)
        {
            var model = new VMLeftTree();
            if (string.IsNullOrEmpty(condition.Keyword))
            {
                model = await _inputTypeService.GetTableLeftTree(condition);
                return PartialView("Index_InformationLevel", model); 
            }    
            else
                model = await _inputTypeService.SearchByCondition(condition);
            ViewBag.TagActive = condition.IDTable;
            return PartialView("_SearchTreeView", model);
        }
        public async Task<IActionResult> GetDataSeeMore(InputInfoCondition condition)
        {
            var result = new VMLeftTree();
            result = await _inputTypeService.GetTableLeftTree(condition);
            if (condition.isSearch == 1 && !string.IsNullOrEmpty(condition.Keyword))
                result = await _inputTypeService.SearchByCondition(condition);
            return PartialView("_AddSeeMore", result);
        }
        public async Task<IActionResult> GetBackLink(InputInfoCondition condition)
        {
            var result = await _inputTypeService.GetLienKetNguoc(condition);
            return PartialView("_TablesBackLink", result);
        }
        public async Task<IActionResult> GetDataExpand(InputInfoCondition condition)
        {
            var result = await _inputTypeService.GetTableLeftTree(condition);
            return PartialView("Index_InformationLevelExpand", result);
        }
        public async Task<IActionResult> GetInfoDetail(InputInfoCondition condition)
        {
            var model = await _inputTypeService.GetInfoDataDetail(condition);
            return PartialView("Detail", model);
        }
        public async Task<IActionResult> GetInfoDetailV2(InputInfoCondition condition)
        {
            var model = await _inputTypeService.GetInfoDataDetail(condition);
            return PartialView("DetailV2", model);
        }
        #endregion

        #region Create
        [HasPermission((int)EnumModule.Code.INPUTINFO, new int[] { (int)EnumPermission.Type.Create })]
        public async Task<IActionResult> Create()
        {
            var idTable = Utils.GetInt(DATA, "IDTable");
            var model = await _inputTypeService.CreateInInformationLevel(idTable);
            return View("Create", model);
        }
        [HasPermission((int)EnumModule.Code.INPUTINFO, new int[] { (int)EnumPermission.Type.Create })]
        public async Task<IActionResult> Save()
        {
            var model = await _inputTypeService.Save(DATA);
            if (!model.IsSuccess)
                return JSErrorResult(model.Message);
            return CustJSonResult(new ServiceResultSuccess(model.Message));
        }
        #endregion

        #region Update
        public async Task<IActionResult> Change()
        {
            var model = await _inputTypeService.Update(DATA);
            if (!model.IsSuccess)
                return JSErrorResult(model.Message);
            return CustJSonResult(new ServiceResultSuccess(model.Message));
        }
        #endregion

        #region Delete
        public async Task<IActionResult> Delete()
        {
            var model = await _inputTypeService.Delete(DATA);
            if (!model.IsSuccess)
                return JSErrorResult(model.Message);
            return CustJSonResult(new ServiceResultSuccess(model.Message));
        }
        #endregion
    }
}
