﻿using AutoMapper;
using DAS.Application.Enums;
using DAS.Application.Interfaces;
using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels.DasKTNN;
using DAS.Application.Services.DasKTNN;
using DAS.Utility;
using DAS.Utility.CustomClass;
using DAS.Utility.LogUtils;
using DAS.Web.Attributes;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAS.Web.Controllers
{
    public class InputInfoTempController : BaseController
    {
        #region Properties

        private readonly ILoggerManager _logger;
        private readonly ITableInfoServices _tableInfoService;
        private readonly IInputInfoServices _inputTypeService;
        private readonly IMapper _mapper;
        private readonly IUserService _userService;

        #endregion Properties

        #region Ctor

        public InputInfoTempController(IMapper mapper, IInputInfoServices inputInfoService, ITableInfoServices tableInfoServices, IUserService userService, ILoggerManager logger)
        {
            _inputTypeService = inputInfoService;
            _tableInfoService = tableInfoServices;
            _mapper = mapper;
            _userService = userService;
            _logger = logger;
        }

        #endregion Ctor

        #region List

        // GET: InputInfoTemp
        [HasPermission((int)EnumModule.Code.INPUTINFOTEMP, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> Index(InputInfoCondition condition)
        {
            var ID = Utils.GetInt(DATA, "IDTable");
            var model = await _inputTypeService.Index(condition, ID, DATA);
            model.inputInfoCondition = condition;
            ViewBag.Keyword = condition.Keyword;
            ViewBag.TagActive = ID;
            return PartialView(model);
        }

        [HasPermission((int)EnumModule.Code.INPUTINFOTEMP, new int[] { (int)EnumPermission.Type.Read })]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SearchByCondition(InputInfoCondition condition)
        {
            var ID = Utils.GetInt(DATA, "IDTable");
            ViewBag.Keyword = condition.Keyword;
            var model = await _inputTypeService.Index(condition, ID, DATA);
            model.inputInfoCondition = condition;
            DATA.Add("IDTableSoucre", "11");
            DATA.Add("IDTableTarget", "13");
            // var tad = await _inputTypeService.Sycn(DATA, new int[] { 283, 284, 285 }, new int[] { 144, 145 }, new int[] { 175, 176 });
            return PartialView("Index_InputInfos", model);
        }
        #endregion List

        #region Sync (Đồng bộ nhiều bản ghi)
        [HasPermission((int)EnumModule.Code.INPUTINFOTEMP, new int[] { (int)EnumPermission.Type.Create })]
        public async Task<IActionResult> SyncView(int[] IDRecords)
        {
            var idTable = Utils.GetInt(DATA, "IDTable");
            var model = await _inputTypeService.SyncData(idTable);
            return PartialView("ViewSync", model);
        }

        public async Task<IActionResult> Synchronization(VMSyncData data)
        {
            try
            {
                var columns = new Dictionary<int, int>();
                var columnIDTagets = Utils.GetInts(DATA, "ColumnID");
                foreach (var itemColumnTarget in columnIDTagets)
                {
                    var columnSource = Utils.GetInt(DATA, "IDColumn_" + itemColumnTarget);
                    if (columnSource > 0)
                    {
                        columns.Add(itemColumnTarget, columnSource);
                    }
                }
                var idRecordStr = Utils.GetString(DATA, "IDRecords");

                string[] arrIdRecord = idRecordStr.Split(",");
                data.IDRecords = Array.ConvertAll(arrIdRecord, s => int.Parse(s));
                var rs = await _inputTypeService.Synchronization(data, columns);
                return CustJSonResult(rs);
            }
            catch (LogicException ex)
            {
                return JSErrorResult(ex.Message);
            }
            catch (Exception ex)
            {
                return JSErrorResult("Có lỗi khi đồng bộ dữ liệu");
            }
        }

        public async Task<IActionResult> GetColumnMapper(VMSyncData data)
        {
            if (data.IDRecords.IsEmpty())
                return JSErrorResult("Chưa chọn dữ liệu để đồng bộ");
            var model = await _inputTypeService.GetColumnMapper(data);
            return PartialView("ColumnMapper", model);
        }
        #endregion

        #region SyncEachRecord (Đồng bộ từng bản ghi)
        public async Task<IActionResult> ViewCheckColumn()
        {
            var idTable = Utils.GetInt(DATA, "IDTable");
            var idRecord = Utils.GetInt(DATA, "IDRecord");
            var model = await _inputTypeService.CheckMapColumn(idTable, idRecord);
            return PartialView("ViewCheckColumn", model);
        }

        public async Task<IActionResult> SyncRecord(VMSyncData data)
        {
            try
            {
                var rs = await _inputTypeService.SyncRecord(data);
                return CustJSonResult(rs);
            }
            catch (LogicException ex)
            {
                return JSErrorResult(ex.Message);
            }
            catch (Exception e)
            {
                _logger.LogError("SyncRecord: " + e.ToString());
                return JSErrorResult("Có lỗi khi đồng bộ dữ liệu");
            }
        }
        #endregion

        #region Reject
        public async Task<IActionResult> ConfirmReject()
        {
            var idTbale = Utils.GetInt(DATA, "IDTable");
            var idRecord = Utils.GetInt(DATA, "IDRecord");
            var model = await _inputTypeService.GetDataTemp(idTbale, idRecord);
            return PartialView("_Reject", model);
        }

        public async Task<IActionResult> Reject(VMSyncData data)
        {
            try
            {
                var rs = await _inputTypeService.Reject(data);
                return CustJSonResult(rs);
            }
            catch (LogicException ex)
            {
                return JSErrorResult(ex.Message);
            }
            catch (Exception ex)
            {
                return JSErrorResult("Có lỗi khi từ chối dữ liệu");
            }
        }
        #endregion

        #region Delete
        public async Task<IActionResult> Delete()
        {
            var model = await _inputTypeService.Delete(DATA);
            if (!model.IsSuccess)
                return JSErrorResult(model.Message);
            return CustJSonResult(new ServiceResultSuccess(model.Message));
        }
        #endregion
    }
}
