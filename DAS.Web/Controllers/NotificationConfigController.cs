﻿using AutoMapper;
using DAS.Application.Enums;
using DAS.Application.Interfaces;
using DAS.Application.Interfaces.DasKTNN;
using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels;
using DAS.Application.Models.ViewModels.DasKTNN;
using DAS.Application.Services;
using DAS.Utility;
using DAS.Web.Attributes;
using DocumentFormat.OpenXml.Presentation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;

namespace DAS.Web.Controllers
{
    public class NotificationConfigController : BaseController
    {
        #region Properties

        private readonly INotificationConfigService _notificationConfigService;
        private readonly IPermissionService _permissionService;
        private readonly IMapper _mapper;
        private readonly IExcelServices _excelService;
        private readonly IDistributedCache _cache;
        private readonly IModuleService _module;

        #endregion Properties

        #region Ctor

        public NotificationConfigController(IMapper mapper
          , INotificationConfigService notificationConfigService
          , IPermissionService permissionService
          , IDistributedCache cache
          , IModuleService module
          , IExcelServices excel)
        {
            _notificationConfigService = notificationConfigService;
            _permissionService = permissionService;
            _mapper = mapper;
            _cache = cache;
            _module = module;
            _excelService = excel;
        }

        #endregion Ctor
        public async Task<IActionResult> Index(NotificationConfigCondition condition)
        {
            var model = await _notificationConfigService.SearchByCondition(condition);
            ViewBag.Keyword = condition.Keyword;
            return View(model);
        }

        public async Task<IActionResult> SerchCondition(NotificationConfigCondition condition)
        {
            var model = await _notificationConfigService.SearchByCondition(condition);
            ViewBag.Keyword = condition.Keyword;
            return PartialView("Index_Records", model);
        }

        public async Task<IActionResult> CreateOrEdit()
        {
            var model = new NotificationConfig();
            var action = Utils.GetInt(DATA, "Action");
            var id = Utils.GetInt(DATA, "ID");
            if (action > 0)
            {
                if (id <= 0)
                    return NotFound();
                model = await _notificationConfigService.GetByID(id);
            }
            model.Action = action;
            model.Url = $"/NotificationConfig/SaveOrUpdate?Action={action}";
            var lstNotificationConfig = await _notificationConfigService.GetList();
            ViewBag.NotificationConfig = lstNotificationConfig;
            return PartialView("Create", model);
        }

        public async Task<IActionResult> Detail()
        {
            var model = new NotificationConfig();
            var action = Utils.GetInt(DATA, "Action");
            var id = Utils.GetInt(DATA, "ID");
            if (id <= 0)
                return NotFound();
            model = await _notificationConfigService.GetByID(id);
            model.Action = action;
            ViewBag.NotificationConfig = await _notificationConfigService.GetList();
            return PartialView("Create", model);
        }

        public async Task<IActionResult> SaveOrUpdate(NotificationConfig model)
        {
            var rs = new ServiceResult();
            var action = Utils.GetInt(DATA, "Action");
            if (action > 0)
            {
                var id = model.ID;
                if (id <= 0)
                    return NotFound();

                rs = await _notificationConfigService.Update(model);
            }
            else
                rs = await _notificationConfigService.Save(model);
            return CustJSonResult(rs);
        }

        public async Task<IActionResult> Delete()
        {
            var id = Utils.GetInt(DATA, "ID");
            if (id <= 0)
                return NotFound();

            var rs = await _notificationConfigService.Delete(id);
            return CustJSonResult(rs);
        }

        public async Task<IActionResult> Deletes(int[] ids)
        {
            if (ids == null || ids.Length == 0)
                return JSErrorResult("Vui lòng chọn thông báo cần xoá!");

            var rs = await _notificationConfigService.Deletes(ids);
            return CustJSonResult(rs);
        }

        [HttpPost]
        public async Task<IActionResult> EnableNotification()
        {
            var rs = await _notificationConfigService.EnableNotification(DATA);
            return CustJSonResult(rs);
        }
    }
}
