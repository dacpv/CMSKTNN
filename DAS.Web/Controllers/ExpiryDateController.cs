﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DAS.Web.Models;
using DAS.Application.Interfaces;
using DAS.Application.Models.ViewModels;
using DAS.Domain.Models.DAS;
using Microsoft.AspNetCore.Mvc.Rendering;
using AutoMapper;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.IO;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using DAS.Application.Models.CustomModels;
using DAS.Application.Enums;
using DAS.Web.Attributes;

namespace DAS.Web.Controllers
{
    [Authorize]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ExpiryDateController : BaseController
    {
        #region Properties
        private readonly IExpiryDateServices _expiryDateService;
        private readonly IMapper _mapper;
        private readonly IExcelServices _excelService;
        private readonly ISystemConfigServices _systemConfigServices;
        #endregion

        #region Ctor
        public ExpiryDateController(IExpiryDateServices expiryDateService
            , IMapper mapper
            , IExcelServices excel
            , ISystemConfigServices systemConfigServices)
        {
            _expiryDateService = expiryDateService;
            _mapper = mapper;
            _excelService = excel;
            _systemConfigServices = systemConfigServices;
        }
        #endregion

        #region List
        [HasPermission((int)EnumModule.Code.TGBQ, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> Index(ExpiryDateCondition condition)
        {
            var breadcrum = new Dictionary<string, string>
            {
                { "/ExpiryDate", "Thời hạn bảo quản" }
            };
            ViewData["Breadcrumb"] = breadcrum;
            ViewBag.Keyword = condition.Keyword;
            var model = await _expiryDateService.SearchByConditionPagging(condition);
            return PartialView(model);
        }

        [HasPermission((int)EnumModule.Code.TGBQ, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> SearchByConditionPagging(ExpiryDateCondition condition)
        {
            ViewBag.Keyword = condition.Keyword;
            PaginatedList<VMExpiryDate> paging = await _expiryDateService.SearchByConditionPagging(condition);
            return PartialView("Index_ExpiryDate", paging);
        }

        [HasPermission((int)EnumModule.Code.TGBQ, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> Detail(int? id)
        {
            if (id == null)
                return NotFound();
            var model = await _expiryDateService.GetExpiryDate(id.Value);
            if (model == null)
                return NotFound();
            return PartialView("Index_Detail", model);
        }

        [HasPermission((int)EnumModule.Code.TGBQ, new int[] { (int)EnumPermission.Type.Export })]
        [HttpGet]
        public async Task<IActionResult> Export(ExpiryDateCondition condition)
        {
            var list = await _expiryDateService.GetListByCondition(condition);
            var export = new ExportExtend
            {
                Data = list.Cast<dynamic>().ToList(),
                Cols = new List<Col>
                {
                    new Col{
                        DataType = 5
                    },
                    new Col("Code"),
                    new Col("Name"),
                    new Col("Description"),
                },
                Headers = new List<Header>
                {
                    new Header("STT",8),
                    new Header("Mã thời hạn bảo quản"),
                    new Header("Tên thời hạn bảo quản"),
                    new Header("Mô tả",70), 
                }
            };
            var rs = await _excelService.ExportExcel(export, "Danh mục thời hạn bảo quản");
            if (rs is ServiceResultError)
            {
                return NotFound();
            }
            else
            {
                var fileName = "DanhMucThoiHanBaoQuan.xlsx";
                var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                return File((byte[])rs.Data, contentType, fileName);
            }
        }
        #endregion

        #region Edit
        [HasPermission((int)EnumModule.Code.TGBQ, new int[] { (int)EnumPermission.Type.Update })]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();
            var model = await _expiryDateService.GetExpiryDate(id.Value);
            if (model == null)
                return NotFound();
            return PartialView("Index_Edit", model);
        }

        [HasPermission((int)EnumModule.Code.TGBQ, new int[] { (int)EnumPermission.Type.Update })]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update([Bind("ID, IDChannel, Code, Name, Description, Status, Value")] VMExpiryDate vMExpiryDate)
        {
            if (!ModelState.IsValid)
                return JSErrorModelStateByLine();
            var rs = await _expiryDateService.UpdateExpiryDate(vMExpiryDate);
            return CustJSonResult(rs);
        }
        #endregion

        #region Create
        [HasPermission((int)EnumModule.Code.TGBQ, new int[] { (int)EnumPermission.Type.Create })]
        public async Task<IActionResult> Create()
        {
            //VMExpiryDate vmExpiryDate = new VMExpiryDate();
            return PartialView("Index_Edit", await _expiryDateService.GetNewExpiryDate());
        }

        [HasPermission((int)EnumModule.Code.TGBQ, new int[] { (int)EnumPermission.Type.Create })]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID, IDChannel, Code, Name, Description, Status, Value")] VMExpiryDate vMExpiryDate)
        {
            if (!ModelState.IsValid)
                return JSErrorModelStateByLine();
            var rs = await _expiryDateService.CreateExpiryDate(vMExpiryDate);
            return CustJSonResult(rs);
        }
        #endregion

        #region Delete
        [HasPermission((int)EnumModule.Code.TGBQ, new int[] { (int)EnumPermission.Type.Deleted })]
        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var rs = await _expiryDateService.DeleteExpiryDate(id);
            return CustJSonResult(rs);
        }

        [HasPermission((int)EnumModule.Code.TGBQ, new int[] { (int)EnumPermission.Type.Deleted })]
        [HttpPost]
        public async Task<IActionResult> Deletes(int[] ids)
        {
            if (ids == null || ids.Length == 0)
                return JSErrorResult("Vui lòng chọn thời hạn bảo quản cần xóa");
            var rs = await _expiryDateService.DeleteMultiExpiryDate(ids);
            return CustJSonResult(rs);
        }
        #endregion
    }

}
