﻿using AutoMapper;
using DAS.Application.Interfaces;
using DAS.Application.Interfaces.DasKTNN;
using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels.DasKTNN;
using DAS.Utility.LogUtils;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using DAS.Web.Attributes;
using DAS.Application.Enums;

namespace DAS.Web.Controllers
{
    public class CollectionReportController : BaseController
    {
        #region Properties

        private readonly ICollectionReportService _collectionReportService;
        private readonly IExcelServices _excelService;
        private readonly IMapper _mapper;
        private readonly ILoggerManager _logger;
        #endregion Properties

        #region Ctor

        public CollectionReportController(IMapper mapper
            , ICollectionReportService collectionReportService
            , ISchemaInfoService schemainfoService
            , IExcelServices excel
            , ILoggerManager logger)
        {
            _collectionReportService = collectionReportService;
            _mapper = mapper;
            _excelService = excel;
            _logger = logger;
        }

        #endregion Ctor

        #region Index
        [HasPermission((int)EnumModule.Code.TABLERECORDSTATS, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> Index(CollectionReportCondition condition)
        {
            var model = await _collectionReportService.StatisticBySoucre(condition);
            return View(model);
        }

        [HasPermission((int)EnumModule.Code.TABLERECORDSTATS, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> SearchByCondition(CollectionReportCondition condition)
        {
            var model = await _collectionReportService.StatisticBySoucre(condition);
            return PartialView("Index_Record", model);
        }
        #endregion

        #region Export
        [HasPermission((int)EnumModule.Code.TABLERECORDSTATS, new int[] { (int)EnumPermission.Type.Read })]
        [HttpGet]
        public async Task<IActionResult> ExportToExcel(CollectionReportCondition condition)
        {
            var model = await _collectionReportService.StatisticBySoucre(condition);
            var export = new ExportExtend
            {
                Data = model.Tables.Cast<dynamic>().ToList(),
                Cols = new List<Col>
                {
                    new Col{
                        DataType = 5
                    },
                    new Col("Name"),
                    new Col("Month"),
                    new Col("Year"),
                    new Col("RecordCount"),
                },
                Headers = new List<Header>
                {
                  new Header("STT", 8),
                    new Header("Nguồn dữ liệu"),
                    new Header("Tháng"),
                    new Header("Năm"),
                    new Header("Số bản ghi"),
                }
            };
            var rs = await _excelService.ExportExcel(export, "TanSuatTruyCapDuLieu");
            if (rs is ServiceResultError)
                return NotFound();
            else
            {
                var fileName = "Thống kê tần sất thu thập.xlsx";
                var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                return File((byte[])rs.Data, contentType, fileName);
            }
        }
        #endregion
    }
}
