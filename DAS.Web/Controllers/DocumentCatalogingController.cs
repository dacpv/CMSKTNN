﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using DAS.Application.Enums;
using DAS.Application.Interfaces;
using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels;
using DAS.Infrastructure.ContextAccessors;
using DAS.Utility;
using DAS.Web.Attributes;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authorization;

namespace DAS.Web.Controllers
{
    /// <summary>
    /// Biên mục hồ sơ tài lại
    /// </summary>
    ///

    [Authorize]
    [ApiExplorerSettings(IgnoreApi = true)] 
    public class DocumentCatalogingController : BaseController
    {
        #region Properties
        private readonly ICatalogingProfileService _catalogingProfileServices;
        //private readonly IPlanServices _planService;
        private readonly IUserService _userService;
        private readonly IExcelServices _excelService;
        private readonly IUserPrincipalService _iUserPrincipalService;
        private readonly string _apiFile;

        #endregion

        #region Ctor
        public DocumentCatalogingController(   IConfiguration configuration,  ICatalogingProfileService catalogingProfileServices, IUserService userService, IExcelServices excel, IUserPrincipalService userPrincipalService)
        {
            _catalogingProfileServices = catalogingProfileServices;
            _excelService = excel;
            _userService = userService;
            _iUserPrincipalService = userPrincipalService;
            _apiFile = configuration["FileDomain"];

        }
        #endregion Ctor

        #region List & Search
        [HasPermission((int)EnumModule.Code.E5020, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> Index(CatalogingProfileCondition condition)
        {
            var model = await _catalogingProfileServices.DocumentCatalogingSearch(condition);
            return PartialView(model);
        }
        [HasPermission((int)EnumModule.Code.E5020, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> SearchByCondition(CatalogingProfileCondition condition)
        {
            var model = await _catalogingProfileServices.DocumentCatalogingSearch(condition);
            return PartialView("Index_Profiles", model);
        }

        [HasPermission((int)EnumModule.Code.E5020, new int[] { (int)EnumPermission.Type.Read })]
        [HttpPost]
        public async Task<IActionResult> DocQuickDetailIndex(CatalogingDocCondition condition)
        {
            var model = await _catalogingProfileServices.CatalogingDocIndex(condition, true);
            //model.VMPlanProfile = await _planService.GetPlanProfile(condition.IDProfile);
            //ViewBag.IsReceived = Utils.GetBool(DATA, "IsReceived");
            //ViewBag.AgencyName = (await _agencyServices.Get(model.VMPlanProfile.IDAgency) ?? new Domain.Models.DAS.Agency()).Name;
            model.IsQuickDetail = true;
            return PartialView("_DocQuickDetailIndex", model);
        }
        #endregion List & Search

        #region Create

        [HasPermission((int)EnumModule.Code.E5020, new int[] { (int)EnumPermission.Type.Create })]
        public async Task<IActionResult> CreateCatalogingProfile()
        {
            var rs = await _catalogingProfileServices.CreateCatalogingProfile();
            return PartialView("Index_ProfileUpdate", rs);
        }

        [HasPermission((int)EnumModule.Code.E5020, new int[] { (int)EnumPermission.Type.Create })]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateCatalogingProfile(VMUpdateCatalogingProfile vMCatalogingProfile)
        {
            //Validate
            if (!ModelState.IsValid)
                return JSErrorModelStateByLine();

            //CallService
            var rs = await _catalogingProfileServices.CreateCatalogingProfile(vMCatalogingProfile);
            return CustJSonResult(rs);
        }
        #endregion Create

        #region Edit
        [HasPermission((int)EnumModule.Code.E5020, new int[] { (int)EnumPermission.Type.Update })]
        public async Task<IActionResult> UpdateCatalogingProfile(int? id)
        {
            var rs = await _catalogingProfileServices.UpdateCatalogingProfile(id);
            return PartialView("Index_ProfileUpdate", rs);
        }

        [HasPermission((int)EnumModule.Code.E5020, new int[] { (int)EnumPermission.Type.Update })]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UpdateCatalogingProfile(VMUpdateCatalogingProfile vMCatalogingProfile)
        {
            //Validate
            if (!ModelState.IsValid)
                return JSErrorModelStateByLine();
            //CallService
            var rs = await _catalogingProfileServices.UpdateCatalogingProfile(vMCatalogingProfile);
            return CustJSonResult(rs);
        }
        #endregion Edit

        #region Delete
        [HasPermission((int)EnumModule.Code.E5020, new int[] { (int)EnumPermission.Type.Deleted })]
        public async Task<IActionResult> DeleteCatalogingProfile(int id)
        {
            var rs = await _catalogingProfileServices.DeleteCatalogingProfile(id);
            return CustJSonResult(rs);
        }

        [HasPermission((int)EnumModule.Code.E5020, new int[] { (int)EnumPermission.Type.Deleted })]
        public async Task<IActionResult> DeleteCatalogingProfiles(int[] ids)
        {
            if (ids == null || ids.Length == 0)
                return JSErrorResult("Vui lòng chọn hồ sơ cần xoá!");
            var rs = await _catalogingProfileServices.DeleteCatalogingProfiles(ids);
            return CustJSonResult(rs);
        }

        #endregion Delete

        #region Documents

        #region Index & Search

        [HasPermission((int)EnumModule.Code.E5020, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> ListDocument(CatalogingDocCondition condition)
        {
            var model = await _catalogingProfileServices.CatalogingDocIndex(condition);
            return View(model);
        }

        [HasPermission((int)EnumModule.Code.E5020, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> SearchDocument(CatalogingDocCondition condition)
        {
            var model = await _catalogingProfileServices.CatalogingDocIndex(condition);
            return PartialView("ListDocument_Documents", model);
        }

        [HttpGet]
        public async Task<IActionResult> ExportDoc(CatalogingDocCondition condition)
        {
            var model = await _catalogingProfileServices.CatalogingDocIndex(condition);

            var docs = new List<object>();
            var docStatus = Utils.EnumToDic<EnumDocCollect.Status>();
            if (model.VMCatalogingDocs.IsNotEmpty())
                foreach (var doc in model.VMCatalogingDocs)
                {
                    docs.Add(new
                    {

                        DocCode = doc.dictCodeValue["DocCode"],
                        IssuedDate = doc.dictCodeValue["IssuedDate"],
                        TypeNameSubject = doc.dictCodeValue["TypeName"] + Environment.NewLine + doc.dictCodeValue["Subject"],
                        OrganName = doc.dictCodeValue["OrganName"],
                        PageAmountDocOrdinal = doc.dictCodeValue["PageAmount"] + "/" + doc.dictCodeValue["DocOrdinal"],
                        Description = doc.dictCodeValue["Description"],
                        Status = docStatus.GetValueOrDefault(doc.Status)
                    });
                }

            var export = new ExportExtend
            {
                Data = docs.Cast<dynamic>().ToList(),
                Cols = new List<Col>
                {
                    new Col{
                        DataType = 5
                    },
                    new Col("DocCode"),
                    new Col("IssuedDate"),
                    new Col("TypeNameSubject"),
                    new Col("OrganName"),
                    new Col("PageAmountDocOrdinal"),
                    new Col("Description"),
                    new Col("Status"),
                },
                Headers = new List<Header>
                {
                    new Header("STT",8),
                    new Header("Số, ký hiệu văn bản"),
                    new Header("Ngày tháng năm văn bản"),
                    new Header("Tên loại và trích yếu nội dung văn bản"),
                    new Header("Tác giả văn bản"),
                    new Header("Số tờ/ Trang số"),
                    new Header("Ghi chú"),
                    new Header("Trạng thái")
                }
            };
            var rs = await _excelService.ExportExcel(export, "Danh sách thành phần hồ sơ");
            if (rs is ServiceResultError)
            {
                return NotFound();
            }
            else
            {
                var fileName = "DanhSachThanhPhanHoSo.xlsx";
                var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                return File((byte[])rs.Data, contentType, fileName);
            }
        }


        #endregion

        #region Create & Update
        [HasPermission((int)EnumModule.Code.E5020, new int[] { (int)EnumPermission.Type.Update, (int)EnumPermission.Type.Create })]
        public async Task<IActionResult> CreateDoc(int? IDProfile)
        {
            if (IDProfile == null)
                return NotFound();
            var rs = await _catalogingProfileServices.CreateCatalogingDoc(IDProfile.GetValueOrDefault(0));

            ViewBag.CData = GetCData(_iUserPrincipalService, _apiFile);
            return PartialView("ListDocument_Update", rs);
        }


        public async Task<IActionResult> ChangeDocType(int? IDProfile, int IDDocType)
        {
            if (IDProfile == null)
                return NotFound();
            var rs = await _catalogingProfileServices.CreateCatalogingDoc(IDProfile.GetValueOrDefault(0), IDDocType);
            return PartialView("ListDocument_Metadata", rs);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveDoc(int IsComplete = 0)
        {
            //Validate
            if (!ModelState.IsValid)
                return JSErrorModelStateByLine();

            //CallService

            var rs = await _catalogingProfileServices.CreateCatalogingDoc(DATA, IsComplete > 0);
            return CustJSonResult(rs);
        }

        [HasPermission((int)EnumModule.Code.E5020, new int[] { (int)EnumPermission.Type.Update, (int)EnumPermission.Type.Create })]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangeDoc(int IsComplete = 0)
        {
            //Validate
            if (!ModelState.IsValid)
                return JSErrorModelStateByLine();

            //CallService
            var rs = await _catalogingProfileServices.UpdateCatalogingDoc(DATA, IsComplete > 0);
            return CustJSonResult(rs);
        }

        #endregion Create & Update

        #region ViewDoc
        [HasPermission((int)EnumModule.Code.E5020, new int[] { (int)EnumPermission.Type.Update, (int)EnumPermission.Type.Create, (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> UpdateDoc(int? id)
        {
            if (id == null)
                return NotFound();
            var rs = await _catalogingProfileServices.GetDocCollect(id.GetValueOrDefault(0));
            ViewBag.CData = GetCData(_iUserPrincipalService, _apiFile);
            return PartialView("ListDocument_Update", rs);
        }

        [HasPermission((int)EnumModule.Code.E5020, new int[] { (int)EnumPermission.Type.Update, (int)EnumPermission.Type.Create, (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> DocView(int? id)
        {
            if (id == null)
                return NotFound();
            var rs = await _catalogingProfileServices.GetDocCollect(id.GetValueOrDefault(0));
            return View("ListDocument_Detail", rs);
        }

        #endregion ViewDoc

        #region Delete
        [HasPermission((int)EnumModule.Code.E5020, new int[] { (int)EnumPermission.Type.Update, (int)EnumPermission.Type.Create, (int)EnumPermission.Type.Deleted })]
        public async Task<IActionResult> DeleteDoc(int id)
        {
            var rs = await _catalogingProfileServices.DeleteDoc(id);
            return CustJSonResult(rs);
        }
        public async Task<IActionResult> DeleteDocs(int[] ids)
        {
            if (ids == null || ids.Length == 0)
                return JSErrorResult("Vui lòng chọn danh mục cần xoá!");
            var rs = await _catalogingProfileServices.DeleteDocs(ids);
            return CustJSonResult(rs);
        }
        #endregion Delete

        #endregion Documents

        #region SendApprove

        [HasPermission((int)EnumModule.Code.E5020, new int[] { (int)EnumPermission.Type.Approve })]
        public async Task<IActionResult> IsSendApprove(int id = 0, int[] ids = null)
        {

            ViewBag.LstUser = (await _userService.GetActive()).Select(n => new SelectListItem
            {
                Value = n.ID.ToString(),
                Text = n.Name
            }).ToList();
            ViewBag.ID = id;
            ViewBag.IDs = ids;

            return PartialView("_SendApprove");
        }
        [HasPermission((int)EnumModule.Code.E5020, new int[] { (int)EnumPermission.Type.Approve })]
        public async Task<IActionResult> SendApprove(int[] IDs = null, int ID = 0)
        {
            var ids = IDs;
            if (Utils.IsEmpty(ids))
            {
                ids = new[] { ID };
            }
            var rs = await _catalogingProfileServices.SendApprove(ids, Utils.GetInt(DATA, "ApprovedBy"));
            return CustJSonResult(rs);
        }
        #endregion SendApprove

        #region SaveFile
        public async Task<IActionResult> SaveFile(VMCatalogingDoc vMCatalogingDoc)
        {
            var rs = await _catalogingProfileServices.SaveFile(vMCatalogingDoc, "/DocumentCataloging/ViewFile?IDFile=");
            return CustJSonResult(rs);
        }
        #endregion SaveFile

        #region ViewFIle
        public IActionResult ViewFile(string IDFile)
        {
            var path = FileUltils.DecryptPathFile(IDFile, _iUserPrincipalService.UserId);
            if (System.IO.File.Exists(path))
            {
                var bytes = System.IO.File.ReadAllBytes(path);
                return File(bytes, "application/pdf");
            }
            return JSErrorResult("File không tồn tại");
        }
        public async Task<IActionResult> AutoOCR(int? IDFile)
        {
            var rs = await _catalogingProfileServices.AutoOCR(IDFile ?? 0);
            return CustJSonResult(rs);
        }
        #endregion ViewFIle

        #region Scan

        public IActionResult ScanDoc()
        {
            return View();
        }

        [AllowAnonymous]
        public async Task<IActionResult> SaveScanDoc(VMCatalogingDoc vMCatalogingDoc)
        {
            var rs = await _catalogingProfileServices.SaveScanFile(vMCatalogingDoc, "/DocumentCataloging/ViewFile?IDFile=");
            return CustJSonResult(rs);
        }
        #endregion Scan
    }
}