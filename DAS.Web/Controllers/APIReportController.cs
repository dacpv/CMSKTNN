﻿using DAS.Application.Enums;
using DAS.Application.Interfaces;
using DAS.Application.Interfaces.DasCongViec;
using DAS.Application.Interfaces.DasKTNN;
using DAS.Application.Models.Param;
using DAS.Application.Models.ViewModels;
using DAS.Domain.Enums;
using DAS.Infrastructure.ContextAccessors;
using DAS.Utility;
using DAS.Web.Attributes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAS.Web.Controllers
{
    [Authorize]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class APIReportController : BaseController
    {
        private readonly ITemplateServices _templateServices;
        private readonly IUserPrincipalService _userPrincipalService;
        private readonly IAPIManageService _APIManageService;
        private readonly IUserService _iUserService;
        public APIReportController(IAPIManageService APIManageService, ITemplateServices templateServices,
            IUserPrincipalService userPrincipalService, IUserService iUserService)
        {
            _templateServices = templateServices;
            _userPrincipalService = userPrincipalService;
            _APIManageService = APIManageService;
            _iUserService = iUserService;
        }
        [HasPermission((int)EnumModule.Code.REPORTAPI, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> Index(APIReportCondition condition)
        {
            var result = _APIManageService.Search(condition);
            return PartialView(result);
        }
        [HasPermission((int)EnumModule.Code.REPORTAPI, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> SearchByCondition(APIReportCondition condition)
        {
            var result = _APIManageService.Search(condition);
            return PartialView("Index_APIReport", result);
        }
    }
}
