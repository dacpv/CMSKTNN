﻿using AutoMapper;
using DAS.Application.Enums;
using DAS.Application.Interfaces;
using DAS.Application.Models.ViewModels;
using DAS.Web.Attributes;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using DAS.Application.Models.CustomModels;
using DAS.Utility;
using System.Dynamic;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace DAS.Web.Controllers
{
    [Authorize]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class StorageController : BaseController
    {
        #region Properties

        private readonly IStorageServices _storageService;
        private readonly IOrganServices _organService;
        private readonly IExcelServices _excelService;
        private readonly IMapper _mapper;
        private readonly IProfileTemplateServices _profileTemplateService;

        #endregion Properties

        #region Ctor

        public StorageController(IMapper mapper
            , IStorageServices storageService
            , IOrganServices organServices
            , IExcelServices excel
            , IProfileTemplateServices profileTemplateService)
        {
            _organService = organServices;
            _storageService = storageService;
            _mapper = mapper;
            _excelService = excel;
            _profileTemplateService = profileTemplateService;
        }

        #endregion Ctor

        #region SearchProfile
        #region List
        [HasPermission((int)EnumModule.Code.TCHS, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> IndexSearchProfile(SearchProfileCondition condition)
        {
            ViewBag.Keyword = condition.Keyword;
            var model = await _storageService.SearchProfileAndDocByConditionPaging(condition);
            var ids = new List<int>();
            if (model.IsNotEmpty())
                ids = model.VMPlanDoc.IsEmpty() ? new List<int>() : model.VMPlanDoc.GroupBy(m => m.IDProfileTemplate).Select(m => m.FirstOrDefault().IDProfileTemplate).ToList();
            var fonds = await _storageService.GetListProfileTemplate(ids);
            if (fonds.IsEmpty())
                ViewBag.ListProfileTemplate = new List<SelectListItem>();
            else
                ViewBag.ListProfileTemplate = fonds.Select(s => new SelectListItem()
                {
                    Value = s.ID.ToString(),
                    Text = s.FondName,
                    Selected = s.ID == condition.IDProfileTemplate
                }).ToList();
            return View(model);
        }

        [HasPermission((int)EnumModule.Code.TCHS, new int[] { (int)EnumPermission.Type.Read })]
        [HttpPost]
        public async Task<IActionResult> SearchByConditionSearchProfile(SearchProfileCondition condition)
        {
            var breadcrum = new Dictionary<string, string>
            {
                { "/Storage", "Tra cứu hồ sơ" }
            };
            ViewData["Breadcrumb"] = breadcrum;
            var model = await _storageService.SearchProfileAndDocByConditionPaging(condition);

            return PartialView("_IndexSearchProfile", model);
        }
        #endregion List

        #region Edit

        #endregion Edit

        #region Create

        #endregion Create

        #region Detail
        [HasPermission((int)EnumModule.Code.TCHS, new int[] { (int)EnumPermission.Type.Read })]
        [HttpPost]
        public async Task<IActionResult> DocQuickDetailIndex(SearchProfileCondition condition)
        {
            var model = await _storageService.PlanDocDetailIndexNoPaging(condition);
            return PartialView("_DocQuickDetailIndex", model);
        }

        [HasPermission((int)EnumModule.Code.TCHS, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> DocDetailIndex(SearchProfileCondition condition)
        {
            VMIndexDocCatalogingProfile searchProfile = await _storageService.PlanDocDetailIndex(condition);
            return View("DocDetailIndex", searchProfile);
        }

        [HasPermission((int)EnumModule.Code.TCHS, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> DocListSearch(SearchProfileCondition condition)
        {
            var model = await _storageService.PlanDocDetailIndex(condition);
            return PartialView("DocDetailIndex_Docs", model);
        }

        [HasPermission((int)EnumModule.Code.TCHS, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> DocView(int? id)
        {
            if (id == null)
                return NotFound();
            var rs = await _storageService.GetDocCollect(id.GetValueOrDefault(0));
            return View(rs);
        }
        #endregion Detail

        #region Export
        [HasPermission((int)EnumModule.Code.QLTT, new int[] { (int)EnumPermission.Type.Export })]
        [HttpGet]
        public async Task<IActionResult> ExportDocs(SearchProfileCondition condition)
        {
            var model = await _storageService.PlanDocDetailIndex(condition);
            //STT
            var header = new List<Header>
            {
                new Header("STT",8),
                new Header("Số, ký hiệu văn bản"),
                new Header("Ngày tháng năm văn bản"),
                new Header("Tên loại và trích yếu nội dung văn bản"),
                new Header("Tác giả văn bản"),
                new Header("Số tờ / Trang số"),
                new Header("Ghi chú"),
                new Header("Trạng thái"),
            };
            var enumDoc = StringUltils.GetEnumDictionary<EnumDocCollect.Status>();
            var col = new List<Col> {
                new Col{ DataType = 5},
                new Col("col1"),
                new Col("col2"),
                new Col("col3"),
                new Col("col4"),
                new Col("col5"),
                new Col("col6"),
                new Col{Field = "Status",DataType = 2,DefineEnum = enumDoc,}
            };

            var data = new List<dynamic>();
            foreach (var doc in model.vMPlanDocs)
            {
                dynamic item = new ExpandoObject();
                var itemDic = (ICollection<KeyValuePair<string, object>>)item;
                var dict = new Dictionary<string, object>()
                {
                    {"col1", ""},
                    {"col2", ""},
                    {"col3", ""},
                    {"col4", ""},
                    {"col5", ""},
                    {"col6", ""},
                    {"Status", ""},
                };
                //Lấy data theo type
                var type = doc.VMDocType.Type;
                switch (type)
                {
                    case (int)EnumDocType.Type.Doc:
                        dict["col1"] = doc.dictCodeValue["DocCode"];
                        dict["col2"] = doc.dictCodeValue["IssuedDate"];
                        dict["col3"] = doc.dictCodeValue["TypeName"] + "\n" + doc.dictCodeValue["Subject"];
                        dict["col4"] = doc.dictCodeValue["OrganName"];
                        dict["col5"] = doc.dictCodeValue["PageAmount"] + "/" + doc.dictCodeValue["DocOrdinal"];
                        dict["col6"] = doc.dictCodeValue["Description"];
                        dict["Status"] = doc.Status;
                        break;
                    case (int)EnumDocType.Type.Photo:
                        dict["col1"] = doc.dictCodeValue["Identifier"];
                        dict["col2"] = doc.dictCodeValue["PhotoTime"];
                        dict["col3"] = "";
                        dict["col4"] = doc.dictCodeValue["Photographer"];
                        dict["col5"] = "";
                        dict["col6"] = doc.dictCodeValue["Description"];
                        dict["Status"] = doc.Status;
                        break;
                    case (int)EnumDocType.Type.Video:
                        dict["col1"] = doc.dictCodeValue["Identifier"];
                        dict["col2"] = doc.dictCodeValue["RecordDate"];
                        dict["col3"] = "";
                        dict["col4"] = doc.dictCodeValue["Recorder"];
                        dict["col5"] = "";
                        dict["col6"] = doc.dictCodeValue["Description"];
                        dict["Status"] = doc.Status;
                        break;
                    default:
                        break;
                }
                foreach (var defineRow in dict)
                {
                    itemDic.Add(defineRow);
                }
                data.Add(item);

            }
            var export = new ExportExtend
            {
                Data = data,
                Cols = col,
                Headers = header,
            };
            var rs = await _excelService.ExportExcel(export, "DanhMucThanhPhanHoSo");
            if (rs is ServiceResultError)
            {
                return NotFound();
            }
            else
            {
                var fileName = "DanhMucThanhPhanHoSo" + Regex.Replace(model.VMUpdateCatalogingProfile.FileNotation, @"\s+", "") + ".xlsx";
                var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                return File((byte[])rs.Data, contentType, fileName);
            }
        }
        #endregion Export

        #region Delete

        #endregion Delete

        #region Private method

        #endregion Private method
        #endregion SearchProfile
    }
}