﻿using AutoMapper;
using DAS.Application.Enums;
using DAS.Application.Interfaces;
using DAS.Application.Interfaces.DasKTNN;
using DAS.Application.Models.ViewModels.DasKTNN;
using DAS.Utility;
using DAS.Utility.CustomClass;
using DAS.Utility.LogUtils;
using DAS.Web.Attributes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static DAS.Application.Enums.DasKTNN.EnumTableInfo;

namespace DAS.Web.Controllers
{
    [Authorize]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class TableInfoApiConfigController : BaseController
    {
        #region Properties

        private readonly ITableInfoApiConfigServices _tableInfoApiConfigService;
        private readonly IMapper _mapper;
        private readonly ILoggerManager _logger;
        private readonly string defaultPath = "/TableInfoApiConfig";
        #endregion Properties

        #region Ctor

        public TableInfoApiConfigController(IMapper mapper, ITableInfoApiConfigServices TableInfoApiConfigService, ISchemaInfoService schemainfoService, ILoggerManager logger)
        {
            _tableInfoApiConfigService = TableInfoApiConfigService;
            _mapper = mapper;
            _logger = logger;
        }

        #endregion Ctor

        #region List

        // GET: TableInfoApiConfigFields
        [HasPermission((int)EnumModule.Code.TABLEINFO, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> Index(TableInfoApiConfigCondition condition)
        {
            var model = await _tableInfoApiConfigService.SearchByConditionPagging(condition);
            return PartialView(model);
        }

        [HasPermission((int)EnumModule.Code.TABLEINFO, new int[] { (int)EnumPermission.Type.Read })]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SearchByCondition(TableInfoApiConfigCondition condition)
        {
            try
            {
                ViewBag.Keyword = condition.Keyword;
                var model = await _tableInfoApiConfigService.SearchByConditionPagging(condition);
                return PartialView("Index_TableInfoApiConfigs", model);
            }
            catch (LogicException ex)
            {
                SetError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                SetError("Có lỗi khi lấy dữ liệu");
            }
            return Redirect(defaultPath);
        }
        #endregion List

        #region Create
        [HasPermission((int)EnumModule.Code.TABLEINFO, new int[] { (int)EnumPermission.Type.Create })]
        public async Task<IActionResult> Create()
        {
            try
            {
                var model = await _tableInfoApiConfigService.Create();
                return PartialView("Index_Update", model);
            }
            catch (LogicException ex)
            {
                SetError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                SetError("Có lỗi khi khởi tạo cấu hình api");
            }
            return Redirect(defaultPath);
        }

        [HasPermission((int)EnumModule.Code.TABLEINFO, new int[] { (int)EnumPermission.Type.Create })]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Save(VMUpdateTableInfoApiConfig data)
        {
            //Validate
            if (!ModelState.IsValid)
                return JSErrorModelStateByLine();

            if (data.Type == (int)TableInfoApiConfigType.RunScriptSQL && data.ScriptSQL.IsEmpty())
            {
                var errObj = new List<object>
                {
                    new
                    {
                        Field = nameof(VMUpdateTableInfoApiConfig.ScriptSQL),
                        Mss = "Giá trị không được để trống"
                    } 
                };
                return JSErrorResult("Giá trị không được để trống", errObj);
            }

            //CallService
            var viewColumns = Utils.GetInts(DATA, "ViewColumn");
            data.ViewColumn = viewColumns.IsNotEmpty() ? string.Join(",", viewColumns) : null;
            var rs = await _tableInfoApiConfigService.Save(data);
            return CustJSonResult(rs);
        }

        #endregion

        #region Edit
        [HasPermission((int)EnumModule.Code.TABLEINFO, new int[] { (int)EnumPermission.Type.Update })]
        public async Task<IActionResult> Edit(int? id)
        {
            try
            {
                var model = await _tableInfoApiConfigService.Update(id);
                return PartialView("Index_Update", model);
            }
            catch (LogicException ex)
            {
                SetError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                SetError("Có lỗi khi lấy thông tin cập nhật cấu hình api");
            }
            return Redirect(defaultPath);
        }

        [HasPermission((int)EnumModule.Code.TABLEINFO, new int[] { (int)EnumPermission.Type.Update })]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Change(VMUpdateTableInfoApiConfig data)
        {
            data.IDSharedApps = Utils.GetInts(DATA, "slIDSharedApps");
            //Validate
            if (!ModelState.IsValid)
                return JSErrorModelStateByLine();

            if (data.Type == (int)TableInfoApiConfigType.RunScriptSQL && data.ScriptSQL.IsEmpty())
            {
                var errObj = new List<object>
                {
                    new
                    {
                        Field = nameof(VMUpdateTableInfoApiConfig.ScriptSQL),
                        Mss = "Giá trị không được để trống"
                    }
                };
                return JSErrorResult("Giá trị không được để trống", errObj);
            }
            //CallService
            var viewColumns = Utils.GetInts(DATA, "ViewColumn");
            data.ViewColumn = viewColumns.IsNotEmpty() ? string.Join(",", viewColumns) : null;
            var rs = await _tableInfoApiConfigService.Change(data);
            return CustJSonResult(rs);
        }
        #endregion

        #region Delete 
        [HasPermission((int)EnumModule.Code.TABLEINFO, new int[] { (int)EnumPermission.Type.Deleted })]
        public async Task<IActionResult> Delete(int id)
        {
            var rs = await _tableInfoApiConfigService.Delete(id);
            return CustJSonResult(rs);
        }

        [HasPermission((int)EnumModule.Code.TABLEINFO, new int[] { (int)EnumPermission.Type.Deleted })]
        public async Task<IActionResult> Deletes(int[] ids)
        {
            if (ids == null || ids.Length == 0)
                return JSErrorResult("Vui lòng chọn cấu hình api cần xoá!");
            var rs = await _tableInfoApiConfigService.Delete(ids);
            return CustJSonResult(rs);
        }
        #endregion Delete

        #region Details
        [HasPermission((int)EnumModule.Code.TABLEINFO, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> Details(int? id)
        {
            try
            {
                var model = await _tableInfoApiConfigService.Update(id);
                return PartialView("Index_Detail", model);
            }
            catch (LogicException ex)
            {
                SetError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                SetError("Có lỗi khi lấy thông tin cấu hình api");
            }
            return Redirect(defaultPath);
        }

        #endregion

        #region Ajax api

        public async Task<IActionResult> GetConfigColumns()
        {
            var columns = await _tableInfoApiConfigService.GetColumnConfig(DATA);
            return PartialView("Update_ColumnTable", columns);
        }

        #endregion
    }
}
