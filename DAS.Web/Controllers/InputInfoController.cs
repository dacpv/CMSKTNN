﻿using AutoMapper;
using DAS.Application.Enums;
using DAS.Application.Interfaces;
using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels.DasKTNN;
using DAS.Utility;
using DAS.Utility.CustomClass;
using DAS.Utility.LogUtils;
using DAS.Web.Attributes;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAS.Web.Controllers
{
    public class InputInfoController : BaseController
    {
        #region Properties

        private readonly ILoggerManager _logger;
        private readonly ITableInfoServices _tableInfoService;
        private readonly IInputInfoServices _inputTypeService;
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly IExcelServices _excelService;
        private readonly IConfiguration _configuration;

        #endregion Properties

        #region Ctor

        public InputInfoController(IMapper mapper, IConfiguration configuration, IInputInfoServices inputInfoService, ITableInfoServices tableInfoServices, IUserService userService, ILoggerManager logger, IExcelServices excel)
        {
            _inputTypeService = inputInfoService;
            _tableInfoService = tableInfoServices;
            _mapper = mapper;
            _excelService = excel;
            _userService = userService;
            _logger = logger;
            _configuration = configuration;
        }

        #endregion Ctor

        #region List

        // GET: InputInfo
        [HasPermission((int)EnumModule.Code.INPUTINFO, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> Index(InputInfoCondition condition)
        {
            var ID = Utils.GetInt(DATA, "IDTable");
            //lay thong tin order mac dinh
            if(string.IsNullOrEmpty(condition.OrderBy))
            {
                var cldefault = await _tableInfoService.GetColumnOrderDefaul(ID);
                if(Utils.IsNotEmpty(cldefault))
                {
                    condition.OrderBy = cldefault.DbName;
                    condition.OrderType = cldefault.OrderType;
                }
            }
            var model = await _inputTypeService.Index(condition, ID, DATA);
            model.inputInfoCondition = condition;
            ViewBag.Keyword = condition.Keyword;
            ViewBag.TagActive = ID;
            var isDMDC = _configuration["IsDanhMucDungChung"];
            ViewBag.IsDanhMucDungChung = isDMDC;
            return PartialView(model);
        }

        [HasPermission((int)EnumModule.Code.INPUTINFO, new int[] { (int)EnumPermission.Type.Read })]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SearchByCondition(InputInfoCondition condition)
        {
            var ID = Utils.GetInt(DATA, "IDTable");
            ViewBag.Keyword = condition.Keyword;
            var model = await _inputTypeService.Index(condition, ID, DATA);
            model.inputInfoCondition = condition;
            return PartialView("Index_InputInfos", model);
        }
        #endregion List

        #region Create
        [HasPermission((int)EnumModule.Code.INPUTINFO, new int[] { (int)EnumPermission.Type.Create })]
        public async Task<IActionResult> Create()
        {
            var ID = Utils.GetInt(DATA, "IDTable");
            var model = await _inputTypeService.Create(ID);
            return View("_Create", model);
        }
        [HasPermission((int)EnumModule.Code.INPUTINFO, new int[] { (int)EnumPermission.Type.Create })]
        public async Task<IActionResult> Save()
        {
            try
            {
                var model = await _inputTypeService.Save(DATA);
                if (!model.IsSuccess)
                    return JSErrorResult(model.Message);
                return CustJSonResult(new ServiceResultSuccess(model.Message));
            }
            catch (LogicException ex)
            {
                return CustJSonResult(new ServiceResultError(ex.Message));
            }
            catch (Exception ex)
            {
                Guid.NewGuid().ToString();
                _logger.LogError(ex);
                return CustJSonResult(new ServiceResultError("Có lỗi khi thêm mới dữ liệu"));
            }
        }
        #endregion

        #region Update
        [HasPermission((int)EnumModule.Code.INPUTINFO, new int[] { (int)EnumPermission.Type.Update })]
        public async Task<IActionResult> Update()
        {
            var IDTable = Utils.GetInt(DATA, "IDTable");
            var ID = Utils.GetInt(DATA, "ID");
            var model = await _inputTypeService.Update(IDTable, ID);
            return View("_Update", model);
        }
        public async Task<IActionResult> Detach()
        {
            var IDTable = Utils.GetInt(DATA, "IDTable");
            var ID = Utils.GetInt(DATA, "ID");
            var model = await _inputTypeService.Update(IDTable, ID);
            return View("_Detach", model);
        }  
        public async Task<IActionResult> SingleDetach()
        {
            var IDTable = Utils.GetInt(DATA, "IDTable");
            var ID = Utils.GetInt(DATA, "ID");
            var model = await _inputTypeService.Update(IDTable, ID);
            return View("_SingleDetach", model);
        }
        [HasPermission((int)EnumModule.Code.INPUTINFO, new int[] { (int)EnumPermission.Type.Update })]
        public async Task<IActionResult> Change()
        {

            var model = await _inputTypeService.Update(DATA);
            if (!model.IsSuccess)
                return JSErrorResult(model.Message);
            return CustJSonResult(new ServiceResultSuccess(model.Message));
        }

        public async Task<IActionResult> SaveDetach()
        {
            var model = await _inputTypeService.SaveDetach(DATA);
            if (!model.IsSuccess)
                return JSErrorResult(model.Message);
            return CustJSonResult(new ServiceResultSuccess(model.Message));
        }

        #endregion

        #region Delete
        [HasPermission((int)EnumModule.Code.INPUTINFO, new int[] { (int)EnumPermission.Type.Deleted })]
        public async Task<IActionResult> Delete()
        {
            var model = await _inputTypeService.Delete(DATA);
            if (!model.IsSuccess)
                return JSErrorResult(model.Message);
            return CustJSonResult(new ServiceResultSuccess(model.Message));
        }
        [HasPermission((int)EnumModule.Code.INPUTINFO, new int[] { (int)EnumPermission.Type.Deleted })]
        public async Task<IActionResult> Deletes(int[] ids)
        {
            if (ids == null || ids.Length == 0)
                return JSErrorResult("Vui lòng chọn dữ liệu cần xoá!");
            var model = await _inputTypeService.Deletes(DATA, ids);
            if (!model.IsSuccess)
                return JSErrorResult(model.Message);
            return CustJSonResult(new ServiceResultSuccess(model.Message));
        }
        #endregion

        #region Detail
        public async Task<IActionResult> ViewDetail()
        {
            var IDTable = Utils.GetInt(DATA, "IDTable");
            var ID = Utils.GetInt(DATA, "ID");
            var model = await _inputTypeService.Update(IDTable, ID);
            return View("_Detail", model);
        }
        #endregion

        #region Import and Download Excel
        public async Task<IActionResult> DownloadTempalte()
        {
            var ID = Utils.GetInt(DATA, "IDTable");
            var model = await _inputTypeService.DownloadTemplate(ID);
            var fileName = "DownloadTempalte.xlsx";
            var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            return File((byte[])model.Data, contentType, fileName);
        }
        public async Task<IActionResult> Import()
        {
            var ID = Utils.GetInt(DATA, "IDTable");
            var model = await _inputTypeService.Create(ID);
            return View("_Import_InputInfo", model);
        }
        //[HttpPost]
        //[HasPermission((int)EnumModule.Code.M20140, new int[] { (int)EnumPermission.Type.Import })]
        //public async Task<IActionResult> ImportExcel(VMIndexImportInputInfo model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return JSErrorModelStateByLine();
        //    }
        //    var rs = await _agencyService.ImportAgencies(model);
        //    var fileName = "KetQuaImport.xlsx";

        //    if (rs.Data != null)
        //    {
        //        var xx = new
        //        {
        //            FileContents = (byte[])rs.Data,
        //            FileName = fileName,
        //            Type = rs.Code,
        //            Message = rs.Message
        //        };
        //        return new ObjectResult(xx);
        //    }
        //    else
        //    {
        //        return CustJSonResult(rs);
        //    }

        //    //return CustJSonResult(rs);
        //}

        #endregion

        #region HopNhat/PhanTach

        public async Task<IActionResult> Merge()
        {
            var ID = Utils.GetInt(DATA, "IDTable");
            var model = await _inputTypeService.Create(ID);
            return View("_Merge", model);
        }

        public async Task<IActionResult> SaveMerge()
        {
            try
            {
                var model = await _inputTypeService.SaveMerge(DATA);
                if (!model.IsSuccess)
                    return JSErrorResult(model.Message);
                return CustJSonResult(new ServiceResultSuccess(model.Message));
            }
            catch (LogicException ex)
            {
                return CustJSonResult(new ServiceResultError(ex.Message));
            }
            catch (Exception ex)
            {
                Guid.NewGuid().ToString();
                _logger.LogError(ex);
                return CustJSonResult(new ServiceResultError("Có lỗi khi thêm mới dữ liệu"));
            }
        }
        #endregion

        #region Autocomplete

        public async Task<IActionResult> GetTableRecords()
        {
            var rs = await _inputTypeService.GetTableRecords(DATA);
            return CustJSonResult(rs);
        }

        /// <summary>
        /// Lấy danh sách bản ghi cần phân tách
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> GetSeparateRecords()
        {
            var rs = await _inputTypeService.GetSeparateRecords(DATA);
            return CustJSonResult(rs);
        }

        public async Task<IActionResult> GetTableBySchema()
        {
            var id = Utils.GetInt(DATA, "ID");
            var selectedId = Utils.GetInt(DATA, "SelectedID");
            var defaultText = Utils.GetString(DATA, "DefaultText");
            var html = string.Empty;

            var categories = await _tableInfoService.GetTableByIDSchema(selectedId);

            categories = categories.Where(n => n.ID != id);

            if (categories.IsNotEmpty())
                html = Utils.RenderOptions(categories, 0, true, defaultText, "");
            return new JsonResult(new
            {
                Type = Application.Constants.CommonConst.Success,
                Data = html
            });
        }
        #endregion
        public async Task<IActionResult> GetUsers()
        {
            var items = new List<OptionSelect>();
            var options = await _userService.GetListAll();
            items.Add(new OptionSelect { Value = "0", Text = "-- Chọn --" });
            foreach (var item in options)
            {
                items.Add(new OptionSelect() { Value = item.ID.ToString(), Text = item.Name });
            }
            var rs = new ServiceResultSuccess()
            {
                Data = new
                {
                    Categories = items
                }
            };
            return CustJSonResult(rs);
        }

        public async Task<IActionResult> ApproveData()
        {
            var model = await _inputTypeService.ApproveData(DATA);
            if (!model.IsSuccess)
                return JSErrorResult(model.Message);
            return CustJSonResult(new ServiceResultSuccess(model.Message));
        }


        #region Export
        [HttpGet]
        public async Task<IActionResult> ExportData(InputInfoCondition condition)
        {
            var ID = Utils.GetInt(DATA, "IDTable");
            //condition.PageSize = 10000000;// măcđịnh 10 triệu
            //condition.PageIndex = 0;
            var model = await _inputTypeService.Index(condition, ID, DATA);
            var headers = new List<Header>
                {
                  new Header("STT", 8),
                };
            var cols = new List<Col>
                {
                    new Col{
                        DataType = 5
                    },
                };
            var datas = new List<Hashtable>();
            var count = model.datas.Rows.Count;

            foreach (var item in model.columnTables)
            {
                headers.Add(new Header(item.Name));
                cols.Add(new Col(item.DbName));
            }


            for (int i = 0; i < count; i++)
            {
                var data = new Hashtable();
                foreach (var item in model.columnTables)
                {
                    var valueconver = model.datas.Rows[i][item.DbName + "_ValueShow"].ToString();
                    data.Add(item.DbName, valueconver);
                }
                datas.Add(data);
            }
            var export = new ExportExtend
            {
                Data = datas.Cast<dynamic>().ToList(),
                Cols = cols,
                Headers = headers
            };
            var rs = await _excelService.ExportExcel(export, "Dữ liệu");
            if (rs is ServiceResultError)
            {
                return NotFound();
            }
            else
            {
                var fileName = model.tableInfo.Name + ".xlsx";
                var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                return File((byte[])rs.Data, contentType, fileName);
            }
        }

        #endregion
    }
}
