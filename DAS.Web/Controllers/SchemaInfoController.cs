﻿using DAS.Application.Enums;
using DAS.Application.Interfaces;
using DAS.Application.Interfaces.DasKTNN;
using DAS.Application.Models.ViewModels;
using DAS.Application.Services;
using DAS.Infrastructure.ContextAccessors;
using DAS.Utility;
using DAS.Web.Attributes;
using DocumentFormat.OpenXml.Presentation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAS.Web.Controllers
{
    [Authorize]
    [ApiExplorerSettings(IgnoreApi = true)]
  
    public class SchemaInfoController : BaseController
    {
        #region Properties
        private readonly ITemplateServices _templateServices;
        private readonly IUserPrincipalService _userPrincipalService;
        private readonly ISchemaInfoService _schemaInfoService;
        private readonly IUserService _iUserService;
        #endregion

        #region Ctor
        public SchemaInfoController(ISchemaInfoService schemaInfoService, ITemplateServices templateServices,
        IUserPrincipalService userPrincipalService, IUserService iUserService)
        {
            _templateServices = templateServices;
            _userPrincipalService = userPrincipalService;
            _schemaInfoService = schemaInfoService;
            _iUserService = iUserService;
        }
        #endregion

        #region List
        [HasPermission((int)EnumModule.Code.SCHEMA, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> Index(SchemaInfoCondition condition)
        {
            var model = await _schemaInfoService.SearchByConditionPagging(condition);
            ViewBag.Keyword = condition.Keyword;
            return View(model);
        }
        [HasPermission((int)EnumModule.Code.SCHEMA, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> SearchByCondition(SchemaInfoCondition condition)
        {
            ViewBag.Keyword = condition.Keyword;
            var model = await _schemaInfoService.SearchByConditionPagging(condition);
            return PartialView("Index_SchemaInfos", model);
        }
        #endregion

        #region Create
        [HasPermission((int)EnumModule.Code.SCHEMA, new int[] { (int)EnumPermission.Type.Create })]
        public async Task<IActionResult> Create()
        {
            return PartialView("Create", new VMSchemaInfo());
        }
        [HasPermission((int)EnumModule.Code.SCHEMA, new int[] { (int)EnumPermission.Type.Create })]
        public async Task<IActionResult> Save(VMSchemaInfo model)
        {
            if (!CheckValidate(model, out string mss))
                return JSErrorResult(mss);

            var rs = await _schemaInfoService.Save(model);
            return CustJSonResult(rs);
        }
        #endregion

        #region Update
        [HasPermission((int)EnumModule.Code.SCHEMA, new int[] { (int)EnumPermission.Type.Update })]
        public async Task<IActionResult> Edit(int? id)
        {
            var model = await _schemaInfoService.GetByID(id);

            return PartialView("Create", model);
        }
        [HasPermission((int)EnumModule.Code.SCHEMA, new int[] { (int)EnumPermission.Type.Update })]
        public async Task<IActionResult> Change(VMSchemaInfo vmScheMaInfo)
        {
            if (!ModelState.IsValid)
                return JSErrorModelStateByLine();

            if (!CheckValidate(vmScheMaInfo, out string mss))
                return JSErrorResult(mss);

            var rs = await _schemaInfoService.Change(vmScheMaInfo);
            return CustJSonResult(rs);
        }
        #endregion

        #region Delete
        [HasPermission((int)EnumModule.Code.SCHEMA, new int[] { (int)EnumPermission.Type.Deleted })]
        public async Task<IActionResult> Delete(int id)
        {
            var rs = await _schemaInfoService.Delete(id);
            return CustJSonResult(rs);
        }
        [HasPermission((int)EnumModule.Code.SCHEMA, new int[] { (int)EnumPermission.Type.Deleted })]
        public async Task<IActionResult> Deletes(int[] ids)
        {
            if (ids == null || ids.Length == 0)
                return JSErrorResult("Vui lòng chọn schema cần xoá!");
            var rs = await _schemaInfoService.Delete(ids);
            return CustJSonResult(rs);
        }
        #endregion

        #region Private Method
        private bool CheckValidate(VMSchemaInfo vmSchemaInfo, out string mss)
        {
            mss = string.Empty;
            if (!vmSchemaInfo.Password.IsNotEmpty())
                mss = "Mật khẩu không được để trống";
            if (!vmSchemaInfo.Name.IsNotEmpty())
                mss = "Tên schema (hiển thị) không được để trống";
            if (!vmSchemaInfo.Code.IsNotEmpty())
                mss = "Tên schema trong CSDL không được để trống";
            return mss.IsEmpty();
        }
        #endregion
    }
}
