﻿using DAS.Application.Enums;
using DAS.Application.Interfaces;
using DAS.Application.Models.ViewModels.DasKTNN;
using DAS.Infrastructure.ContextAccessors;
using DAS.Utility;
using DAS.Utility.CustomClass;
using DAS.Web.Attributes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAS.Web.Controllers
{
    [Authorize]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ImportDataController : BaseController
    {
        #region Properties
        private readonly ITemplateServices _templateServices;
        private readonly IUserPrincipalService _userPrincipalService;
        private readonly IImportDataServices _importDataService;
        private readonly ITableInfoServices _tableInfoService;
        private readonly IUserService _iUserService;
        #endregion

        #region Ctor
        public ImportDataController(IImportDataServices importDataService, ITemplateServices templateServices,
        IUserPrincipalService userPrincipalService, IUserService iUserService, ITableInfoServices tableInfoService)
        {
            _templateServices = templateServices;
            _userPrincipalService = userPrincipalService;
            _importDataService = importDataService;
            _iUserService = iUserService;
            _tableInfoService = tableInfoService;
        }
        #endregion
        [HasPermission((int)EnumModule.Code.INPUTINFO, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> Index(int? idTable)
        {
            var model = await _importDataService.Index(idTable);
     

            return View(model);
        }

        public async Task<IActionResult> DownloadTemplate()
        {
            var idTable = Utils.GetInt(DATA, "IDTable");
            var model = await _importDataService.DownloadTemplate(idTable);
            var table = await _tableInfoService.Get(idTable);
            var fileName = table != null ? $"{table.DbName}_Import.xlsx" : "ImportTempalte.xlsx";
            var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            return File((byte[])model.Data, contentType, fileName);
        }

        [HttpPost]
        public async Task<IActionResult> GetHeader(VMImportData data)
        {
            try
            {
                var model = await _importDataService.GetHeader(data);
                return PartialView("Index_HeaderMapper", model);
            }
            catch (LogicException ex)
            {
                return JSErrorResult(ex.Message);
            }
            catch (Exception ex)
            {
                return JSErrorResult("Có lỗi khi đọc file import");
            }

        }



        [HttpPost]
        [HasPermission((int)EnumModule.Code.INPUTINFO, new int[] { (int)EnumPermission.Type.Create })]
        public async Task<IActionResult> Import(VMImportData data)
        {
            try
            {
                var columns = await _tableInfoService.GetColumnByIDTable(data.IDTable ?? 0);
                if (!HeaderMapperFieldValidate(data, columns, out string mss, out List<object> errObj))
                {
                    if (errObj.IsNotEmpty())
                        return JSErrorResult(mss, errObj);
                    return JSErrorResult(mss);
                }

                var rs = await _importDataService.Import(data);
                return CustJSonResult(rs);
            }
            catch (LogicException ex)
            {
                return JSErrorResult(ex.Message);
            }
            catch (Exception ex)
            {
                return JSErrorResult("Có lỗi khi khởi tạo dữ liệu import");
            }

        }

        private bool HeaderMapperFieldValidate(VMImportData data, IEnumerable<Domain.Models.DASKTNN.ColumnTableInfo> columns, out string mss, out List<object> errObj)
        {
            errObj = new List<object>();
            mss = string.Empty;
            if (data.HeaderMappers.IsNotEmpty())
            {
                var i = 0;
                foreach (var item in data.HeaderMappers)
                {
                    var column = columns.FirstOrDefault(n => n.ID == item.ColumnID);
                    if (column != null)
                        if (column.IDColRef > 0 && item.IDColumnRef == 0)
                        {
                            errObj.Add(new
                            {
                                Field = $"HeaderMappers[{i}].IDColumnRef",
                                Mss = "Cột tham chiếu không được để trống"
                            });
                        }

                    i++;
                }
            }
            return mss.IsEmpty() && errObj.IsEmpty();
        }

        #region Autocomplete

        public async Task<IActionResult> GetColumnByTable()
        {
            var selectedId = Utils.GetInt(DATA, "SelectedID");
            var defaultText = Utils.GetString(DATA, "DefaultText");
            var name = Utils.GetString(DATA, "Name");

            var html = string.Empty;

            var categories = await _tableInfoService.GetColumnByIDTable(selectedId);

            var selected = categories.FirstOrNewObj(n => n.DbName == name); //Mặc định focus vào trường tên
            if (categories.IsNotEmpty())
                html = Utils.RenderOptions(categories, selected.ID, true, defaultText, "");
            return new JsonResult(new
            {
                Type = Application.Constants.CommonConst.Success,
                Data = html
            });
        }

        #endregion


        public async Task<IActionResult> Sync(VMSyncData data)
        {
            var model = await _importDataService.Sync(data);
            if (data.IDTable > 0)
                return PartialView("Sync_Column", model);
            return View(model);
        }


        [HttpPost]
        public async Task<IActionResult> SaveSync(VMSyncData data)
        {
            try
            {
                var model = await _importDataService.SaveSync(data);
                if (model.IsSuccess)
                    return JSSuccessResult(model.Message);
                else
                    return JSErrorResult(model.Message);
            }
            catch (LogicException ex)
            {
                return JSErrorResult(ex.Message);
            }
            catch (Exception ex)
            {
                return JSErrorResult("Có lỗi khi đồng bộ dữ liệu");
            }

             
        }

    }
}
