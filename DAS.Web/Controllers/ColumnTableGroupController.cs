﻿using AutoMapper;
using DAS.Application.Enums;
using DAS.Application.Interfaces;
using DAS.Application.Interfaces.DasKTNN;
using DAS.Application.Models.ViewModels.DasKTNN;
using DAS.Utility.CustomClass;
using DAS.Utility.LogUtils;
using DAS.Web.Attributes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace DAS.Web.Controllers
{
    [Authorize]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ColumnTableGroupController : BaseController
    {
        #region Properties

        private readonly IColumnTableGroupServices _columnTableGroupService;
        private readonly IMapper _mapper;
        private readonly ILoggerManager _logger;
        private readonly string defaultPath = "/ColumnTableGroup";
        #endregion Properties

        #region Ctor

        public ColumnTableGroupController(IMapper mapper, IColumnTableGroupServices ColumnTableGroupService, ISchemaInfoService schemainfoService, ILoggerManager logger)
        {
            _columnTableGroupService = ColumnTableGroupService;
            _mapper = mapper;
            _logger = logger;
        }

        #endregion Ctor

        #region List

        // GET: ColumnTableGroupFields
        [HasPermission((int)EnumModule.Code.TABLEINFO, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> Index(ColumnTableGroupCondition condition)
        {
            var model = await _columnTableGroupService.SearchByConditionPagging(condition);
            return PartialView(model);
        }

        [HasPermission((int)EnumModule.Code.TABLEINFO, new int[] { (int)EnumPermission.Type.Read })]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SearchByCondition(ColumnTableGroupCondition condition)
        {
            try
            {
                ViewBag.Keyword = condition.Keyword;
                var model = await _columnTableGroupService.SearchByConditionPagging(condition);
                return PartialView("Index_ColumnTableGroups", model);
            }
            catch (LogicException ex)
            {
                SetError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                SetError("Có lỗi khi lấy dữ liệu");
            }
            return Redirect(defaultPath);
        }
        #endregion List

        #region Create
        [HasPermission((int)EnumModule.Code.TABLEINFO, new int[] { (int)EnumPermission.Type.Create })]
        public async Task<IActionResult> Create()
        {
            try
            {
                var model = await _columnTableGroupService.Create();
                return PartialView("Index_Update", model);
            }
            catch (LogicException ex)
            {
                SetError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                SetError("Có lỗi khi khởi tạo nhóm cột");
            }
            return Redirect(defaultPath);
        }

        [HasPermission((int)EnumModule.Code.TABLEINFO, new int[] { (int)EnumPermission.Type.Create })]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Save(VMUpdateColumnTableGroup vmColumnTableGroup)
        {
            //Validate
            if (!ModelState.IsValid)
                return JSErrorModelStateByLine();

            //CallService
            var rs = await _columnTableGroupService.Save(vmColumnTableGroup);
            return CustJSonResult(rs);
        }

        #endregion

        #region Edit
        [HasPermission((int)EnumModule.Code.TABLEINFO, new int[] { (int)EnumPermission.Type.Update })]
        public async Task<IActionResult> Edit(int? id)
        {
            try
            {
                var model = await _columnTableGroupService.Update(id);
                return PartialView("Index_Update", model);
            }
            catch (LogicException ex)
            {
                SetError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                SetError("Có lỗi khi lấy thông tin cập nhật nhóm cột");
            }
            return Redirect(defaultPath);
        }

        [HasPermission((int)EnumModule.Code.TABLEINFO, new int[] { (int)EnumPermission.Type.Update })]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Change(VMUpdateColumnTableGroup columnTableGroup)
        {
            //Validate
            if (!ModelState.IsValid)
                return JSErrorModelStateByLine();

            //CallService
            var rs = await _columnTableGroupService.Change(columnTableGroup);
            return CustJSonResult(rs);
        }
        #endregion

        #region Delete 
        [HasPermission((int)EnumModule.Code.TABLEINFO, new int[] { (int)EnumPermission.Type.Deleted })]
        public async Task<IActionResult> Delete(int id)
        {
            var rs = await _columnTableGroupService.Delete(id);
            return CustJSonResult(rs);
        }

        [HasPermission((int)EnumModule.Code.TABLEINFO, new int[] { (int)EnumPermission.Type.Deleted })]
        public async Task<IActionResult> Deletes(int[] ids)
        {
            if (ids == null || ids.Length == 0)
                return JSErrorResult("Vui lòng chọn nhóm cột cần xoá!");
            var rs = await _columnTableGroupService.Delete(ids);
            return CustJSonResult(rs);
        }
        #endregion Delete
         
        #region Details
        [HasPermission((int)EnumModule.Code.TABLEINFO, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> Details(int? id)
        {
            try
            {
                var model = await _columnTableGroupService.Update(id);
                return PartialView("Index_Detail", model);
            }
            catch (LogicException ex)
            {
                SetError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                SetError("Có lỗi khi lấy thông tin nhóm cột");
            }
            return Redirect(defaultPath);
        }

        #endregion
 
    }
}
