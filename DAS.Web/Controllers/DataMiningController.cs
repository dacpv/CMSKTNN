﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAS.Application.Enums;
using DAS.Application.Interfaces;
using DAS.Application.Models.ViewModels;
using DAS.Utility;
using Microsoft.AspNetCore.Mvc;

namespace DAS.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DataMiningController : BaseController
    {

        #region Properties
        private readonly ICategoryServices _categoryServices;
        #endregion
        #region Ctor
        public DataMiningController(ICategoryServices categoryServices)
        {
            _categoryServices = categoryServices;
        }
        #endregion
        /// <summary>
        /// Danh sách kho
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("StorageList")]
        public async Task<IActionResult> StorageList()
        {
            var condition = new CategoryCondition()
            {
                CodeType = EnumCategoryType.Code.DM_Kho.ToString()
            };
            return JSSuccessResult(string.Empty, await GetCategories(condition));
        }

        /// <summary>
        /// Danh sách giá
        /// </summary>
        /// <param name="IDStorage">ID kho</param>
        /// <returns></returns>
        [HttpGet]
        [Route("ShelveList")]
        public async Task<IActionResult> ShelveList(int IDStorage)
        {
            var condition = new CategoryCondition()
            {
                CodeType = EnumCategoryType.Code.DM_Gia.ToString(),
                IDParentCategory = IDStorage
            };
            return JSSuccessResult(string.Empty, await GetCategories(condition));
        }

        /// <summary>
        /// Danh sách hộp
        /// </summary>
        /// <param name="IDShelve">ID giá, kệ</param>
        /// <returns></returns>
        [HttpGet]
        [Route("BoxList")]
        public async Task<IActionResult> BoxList(int IDShelve)
        {
            var condition = new CategoryCondition()
            {
                CodeType = EnumCategoryType.Code.DM_HopSo.ToString(),
                IDParentCategory = IDShelve
            };
            return JSSuccessResult(string.Empty, await GetCategories(condition));
        }


        #region Functions

        private async Task<object> GetCategories(CategoryCondition condition)
        {
            condition.PageSize = -1; //No paging
            var model = await _categoryServices.SearchByConditionPagging(condition, DATA);
            var stt = model.VMCategorys.PageSize * (model.VMCategorys.PageIndex - 1);
            var storages = new List<object>();
            var gridFields = model.VMCategoryTypeFields.Where(n => n.IsShowGrid).OrderBy(n => n.Priority);
            foreach (var cate in model.VMCategorys)
            {
                stt++;
                var _item = new Hashtable()
                {
                    {"ID", cate.ID }
                };
                foreach (var field in gridFields)
                {
                    var data = cate.VMCategoryFields.FirstOrDefault(n => n.IDCategoryTypeField == field.ID);
                    if (data != null)
                    {
                        var key = field.Name.ConvertToCamelString();
                        if (!_item.ContainsKey(key))
                            _item.Add(key, data.DisplayVal);
                    }

                }
                storages.Add(_item);
            }

            return storages;
        }

        #endregion Functions
    }
}
