﻿using AutoMapper;
using DAS.Application.Enums;
using DAS.Application.Interfaces;
using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels.DasKTNN;
using DAS.Utility;
using DAS.Utility.CustomClass;
using DAS.Utility.LogUtils;
using DAS.Web.Attributes;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAS.Web.Controllers
{
    public class StatisticController : BaseController
    {
        #region Properties

        private readonly ILoggerManager _logger;
        private readonly ISharedAppService _sharedAppService;
        private readonly IInputInfoServices _inputTypeService;
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly IExcelServices _excelService;

        private readonly string defaultPath = "/Statistic";
        #endregion Properties

        #region Ctor

        public StatisticController(IMapper mapper, IInputInfoServices inputInfoService, ISharedAppService sharedAppService, IUserService userService, ILoggerManager logger, IExcelServices excel)
        {
            _inputTypeService = inputInfoService;
            _sharedAppService = sharedAppService;
            _mapper = mapper;
            _excelService = excel;
            _userService = userService;
            _logger = logger;
        }

        #endregion Ctor

        #region  Báo cáo tần xuất ứng dụng chia sẻ
        [HasPermission((int)EnumModule.Code.SHAREDAPPFREQUENCYSTATS, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> SharedApp(SharedAppStatsCondition condition)
        {
            var model = await _sharedAppService.SharedAppStats(condition);
            return View("SharedApp/Index", model);
        }

        [HasPermission((int)EnumModule.Code.SHAREDAPPFREQUENCYSTATS, new int[] { (int)EnumPermission.Type.Read })]
        [HttpPost]
        public async Task<IActionResult> SharedAppSearch(SharedAppStatsCondition condition)
        {
            try
            {
                var model = await _sharedAppService.SharedAppStats(condition);
                ViewBag.Keyword = condition.Keyword;
                ViewBag.StartDate = condition.StartDate;
                ViewBag.EndDate = condition.EndDate;
                return PartialView("SharedApp/Index_Table", model);
            }
            catch (LogicException ex)
            {
                SetError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                SetError("Có lỗi khi lấy dữ liệu");
            }
            return Redirect(defaultPath);
        }

        [HasPermission((int)EnumModule.Code.SHAREDAPPFREQUENCYSTATS, new int[] { (int)EnumPermission.Type.Export })]
        [HttpGet]
        public async Task<IActionResult> SharedAppExport(SharedAppStatsCondition condition)
        {
            condition.PageSize = 5000;
            var model = await _sharedAppService.SharedAppStats(condition);
            var export = new ExportExtend
            {
                Data = model.SharedAppStatistics.Select(item => new
                {
                    APIName = item.APIName,
                    NoiDung = item.NoiDung,
                    Count = item.Count,
                    IDSharedApp = model.SharedApps.FirstOrNewObj(n => n.ID == item.IDSharedApp).Name
                }).Cast<dynamic>().ToList(),
                Cols = new List<Col>
                {
                    new Col{
                        DataType = 5
                    },
                    new Col("APIName"),
                    new Col("NoiDung"),
                    new Col("IDSharedApp"),
                    new Col("Count"),
                },
                Headers = new List<Header>
                {
                  new Header("STT", 8),
                    new Header("Tên api"),
                    new Header("Nội dung"),
                    new Header("Ứng dụng"),
                    new Header("Số lần truy cập"),
                }
            };
            var rs = await _excelService.ExportExcel(export, "Báo cáo");
            if (rs is ServiceResultError)
            {
                return NotFound();
            }
            else
            {
                var fileName = "Báo cáo tần xuất ứng dụng chia sẻ.xlsx";
                var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                return File((byte[])rs.Data, contentType, fileName);
            }
        }

        #endregion

        #region  Báo cáo thống kê ứng dụng
        [HasPermission((int)EnumModule.Code.SHAREDAPPFREQUENCYSTATS, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> AppByApi(SharedAppStatsCondition condition)
        {
            var model = await _sharedAppService.AppByApiStats(condition);
            return View("AppByApi/Index", model);
        }

        [HasPermission((int)EnumModule.Code.SHAREDAPPFREQUENCYSTATS, new int[] { (int)EnumPermission.Type.Read })]
        [HttpPost]
        public async Task<IActionResult> AppByApiSearch(SharedAppStatsCondition condition)
        {
            try
            {
                var model = await _sharedAppService.AppByApiStats(condition);
                ViewBag.Keyword = condition.Keyword;
                ViewBag.StartDate = condition.StartDate;
                ViewBag.EndDate = condition.EndDate;
                return PartialView("AppByApi/Index_Table", model);
            }
            catch (LogicException ex)
            {
                SetError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                SetError("Có lỗi khi lấy dữ liệu");
            }
            return Redirect(defaultPath);
        }

        [HasPermission((int)EnumModule.Code.SHAREDAPPFREQUENCYSTATS, new int[] { (int)EnumPermission.Type.Export })]
        [HttpGet]
        public async Task<IActionResult> AppByApiExport(SharedAppStatsCondition condition)
        {
            condition.PageSize = 5000;
            var model = await _sharedAppService.AppByApiStats(condition);
            var export = new ExportExtend
            {
                Data = model.SharedAppPagings.Select(app =>
                {
                    return new
                    {
                        AppName = app.Name,
                        AppCode = app.Code,
                        Count = model.SharedAppStatistics.Where(n => n.IDSharedApp == app.ID).Sum(n => n.Count),
                    };
                }).Cast<dynamic>().ToList(),
                Cols = new List<Col>
                {
                    new Col{
                        DataType = 5
                    },
                    new Col("AppName"),
                    new Col("AppCode"),
                    new Col("Count"),
                },
                Headers = new List<Header>
                {
                  new Header("STT", 8),
                    new Header("Ứng dụng"),
                    new Header("Mã ứng dụng"),
                    new Header("Số nguồn dữ liệu được chia sẻ"),
                }
            };
            var rs = await _excelService.ExportExcel(export, "Báo cáo");
            if (rs is ServiceResultError)
            {
                return NotFound();
            }
            else
            {
                var fileName = "Báo cáo thống kê ứng dụng.xlsx";
                var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                return File((byte[])rs.Data, contentType, fileName);
            }
        }

        #endregion

        #region Báo cáo thống kê ứng dụng sử dụng dữ liệu
        [HasPermission((int)EnumModule.Code.SHAREDAPPFREQUENCYSTATS, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> SharedAppFrequency(SharedAppStatsCondition condition)
        {
            var model = await _sharedAppService.SharedAppFrequencyStats(condition);
            return View("SharedAppFrequency/Index", model);
        }

        [HasPermission((int)EnumModule.Code.SHAREDAPPFREQUENCYSTATS, new int[] { (int)EnumPermission.Type.Read })]
        [HttpPost]
        public async Task<IActionResult> SharedAppFrequencySearch(SharedAppStatsCondition condition)
        {
            try
            {
                var model = await _sharedAppService.SharedAppFrequencyStats(condition);
                ViewBag.Keyword = condition.Keyword;
                ViewBag.StartDate = condition.StartDate;
                ViewBag.EndDate = condition.EndDate;
                return PartialView("SharedAppFrequency/Index_Table", model);
            }
            catch (LogicException ex)
            {
                SetError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                SetError("Có lỗi khi lấy dữ liệu");
            }
            return Redirect(defaultPath);
        }

        [HasPermission((int)EnumModule.Code.SHAREDAPPFREQUENCYSTATS, new int[] { (int)EnumPermission.Type.Export })]
        [HttpGet]
        public async Task<IActionResult> SharedAppFrequencyExport(SharedAppStatsCondition condition)
        {
            condition.PageSize = 5000;
            var model = await _sharedAppService.SharedAppFrequencyStats(condition);
            var export = new ExportExtend
            {
                Data = model.SharedAppPagings.Select(app => new
                {
                    AppCode = app.Code,
                    AppName = app.Code,
                    Count = model.SharedAppStatistics.Where(n => n.IDSharedApp == app.ID).Sum(n => n.Count),
                }).Cast<dynamic>().ToList(),
                Cols = new List<Col>
                {
                    new Col{
                        DataType = 5
                    },
                    new Col("AppCode"),
                    new Col("AppName"),
                    new Col("Count"),
                },
                Headers = new List<Header>
                {
                  new Header("STT", 8),
                    new Header("Mã ứng dụng"),
                    new Header("Tên ứng dụng"),
                    new Header("Lượt truy cập"),
                }
            };
            var rs = await _excelService.ExportExcel(export, "Báo cáo");
            if (rs is ServiceResultError)
            {
                return NotFound();
            }
            else
            {
                var fileName = "Báo cáo thống kê ứng dụng sử dụng dữ liệu";
                var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                return File((byte[])rs.Data, contentType, fileName);
            }
        }

        #endregion
    }
}
