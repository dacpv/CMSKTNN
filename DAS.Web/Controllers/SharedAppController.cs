﻿using DAS.Application.Enums;
using DAS.Application.Interfaces;
using DAS.Application.Interfaces.DasKTNN;
using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels;
using DAS.Application.Models.ViewModels.DasKTNN;
using DAS.Application.Services;
using DAS.Infrastructure.ContextAccessors;
using DAS.Utility;
using DAS.Web.Attributes;
using DocumentFormat.OpenXml.Presentation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAS.Web.Controllers
{
    [Authorize]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class SharedAppController : BaseController
    {
        #region Properties
        private readonly ITemplateServices _templateServices;
        private readonly IUserPrincipalService _userPrincipalService;
        private readonly ISharedAppService _schemaInfoService;
        private readonly IUserService _iUserService;
        #endregion
        #region Ctor
        public SharedAppController(ISharedAppService schemaInfoService, ITemplateServices templateServices,
        IUserPrincipalService userPrincipalService, IUserService iUserService)
        {
            _templateServices = templateServices;
            _userPrincipalService = userPrincipalService;
            _schemaInfoService = schemaInfoService;
            _iUserService = iUserService;
        }
        #endregion
        #region List
        [HasPermission((int)EnumModule.Code.SHAREDAPP, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> Index(SharedAppCondition condition)
        {
            var model = await _schemaInfoService.SearchByConditionPagging(condition);
            ViewBag.Keyword = condition.Keyword;
            return View(model);
        }
        [HasPermission((int)EnumModule.Code.SHAREDAPP, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> SearchByCondition(SharedAppCondition condition)
        {
            ViewBag.Keyword = condition.Keyword;
            var model = await _schemaInfoService.SearchByConditionPagging(condition);
            return PartialView("Index_SharedApps", model);
        }
        #endregion

        #region Create
        [HasPermission((int)EnumModule.Code.SHAREDAPP, new int[] { (int)EnumPermission.Type.Create })]
        public async Task<IActionResult> Create()
        {
            var model = await _schemaInfoService.Create();
            return PartialView("Create", model);
        }
        public async Task<IActionResult> Save()
        {
            var ID = Utils.GetInt(DATA, "IDTable");
            var model = await _schemaInfoService.Create(DATA);
            if (!model.IsSuccess)
                return JSErrorResult(model.Message);
            return CustJSonResult(new ServiceResultSuccess(model.Message));
        }
        #endregion
        #region
        [HasPermission((int)EnumModule.Code.SHAREDAPP, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> Detail(int? id)
        {
            var model = await _schemaInfoService.Update(id);

            return PartialView("Detail", model);
        }
        #endregion
        #region Update
        [HasPermission((int)EnumModule.Code.SHAREDAPP, new int[] { (int)EnumPermission.Type.Update })]
        public async Task<IActionResult> Edit(int? id)
        {
            var model = await _schemaInfoService.Update(id);

            return PartialView("Create", model);
        }
        public async Task<IActionResult> Change()
        {
            var model = await _schemaInfoService.Update(DATA);
            if (!model.IsSuccess)
                return JSErrorResult(model.Message);
            return CustJSonResult(new ServiceResultSuccess(model.Message));
        }
        #endregion

        #region Delete
        [HasPermission((int)EnumModule.Code.SHAREDAPP, new int[] { (int)EnumPermission.Type.Deleted })]
        public async Task<IActionResult> Delete(int id)
        {
            var rs = await _schemaInfoService.Delete(id);
            return CustJSonResult(rs);
        }
        [HasPermission((int)EnumModule.Code.SHAREDAPP, new int[] { (int)EnumPermission.Type.Deleted })]
        public async Task<IActionResult> Deletes(int[] ids)
        {
            if (ids == null || ids.Length == 0)
                return JSErrorResult("Vui lòng chọn ứng dụng cần xoá!");
            var rs = await _schemaInfoService.Delete(ids);
            return CustJSonResult(rs);
        }
        #endregion
    }
}
