﻿using DAS.Application.Constants;
using DAS.Application.Interfaces;
using DAS.Application.Interfaces.DasCongViec;
using DAS.Application.Interfaces.DasKTNN;
using DAS.Application.Models.CustomModels;
using DAS.Application.Models.Param;
using DAS.Application.Models.ViewModels;
using DAS.Application.Models.ViewModels.DasKTNN;
using DAS.Domain.Enums;
using DAS.Infrastructure.ContextAccessors;
using DAS.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAS.Web.Controllers
{
  //  [Authorize]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class APIController : BaseController
    {
        private readonly ITemplateServices _templateServices;
        private readonly IUserPrincipalService _userPrincipalService;
        private readonly ITableTestService _tableTestService;
        private readonly IUserService _iUserService;
        private readonly IDynamicDBService _iDynamicDBService;

        public APIController(ITableTestService tableTestService, ITemplateServices templateServices,
            IUserPrincipalService userPrincipalService, IUserService iUserService, IDynamicDBService iDynamicDBService)
        {
            _templateServices = templateServices;
            _userPrincipalService = userPrincipalService;
            _tableTestService = tableTestService;
            _iUserService = iUserService;
            _iDynamicDBService = iDynamicDBService;
        }
        public async Task<IActionResult> GetItems()
        {
            try
            {
                VMParamAPI data = Utils.Bind<VMParamAPI>(DATA);
                data.query = Utils.Deserialize<List<QueryAPI>>(Utils.GetString(DATA, "query"));
                data.viewColumns = Utils.Deserialize<List<string>>(Utils.GetString(DATA, "viewColumns"));
                var servicereult = new ServiceResult();
                servicereult.Code = CommonConst.Success;
                var result = await _iDynamicDBService.Getitems(data);
                if (result.IsSuccess)
                    return CustJSonResult(new ServiceResultSuccess
                    {
                        Data = new
                        {
                            Items = Utils.ConvertDataTabletoString(result.Result),
                            TotalItems = result.TotalItems
                        }
                    });
                else
                    return CustJSonResult(new ServiceResultError(result.Message));
            }
            catch (Exception ex)
            {
                return CustJSonResult(new ServiceResultError(ex.Message));
            }
        }
        public async Task<IActionResult> GetItem()
        {
            try
            {
                VMParamAPI data = Utils.Bind<VMParamAPI>(DATA);
                data.query = Utils.Deserialize<List<QueryAPI>>(Utils.GetString(DATA, "query"));
                data.viewColumns = Utils.Deserialize<List<string>>(Utils.GetString(DATA, "viewColumns"));
                var servicereult = new ServiceResult();
                servicereult.Code = CommonConst.Success;
                var result = await _iDynamicDBService.Getitem(data);
                if (result.IsSuccess)
                    return CustJSonResult(new ServiceResultSuccess
                    {
                        Data = new
                        {
                            Items = Utils.ConvertDataTabletoString(result.Result),
                            TotalItems = result.TotalItems
                        }
                    });
                else
                    return CustJSonResult(new ServiceResultError(result.Message));
            }
            catch (Exception ex)
            {
                return CustJSonResult(new ServiceResultError(ex.Message));
            }
        }
    }
}
