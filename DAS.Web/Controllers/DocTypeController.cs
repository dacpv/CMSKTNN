﻿using AutoMapper;
using DAS.Application.Enums;
using DAS.Application.Interfaces;
using DAS.Application.Models.ViewModels;
using DAS.Infrastructure.ContextAccessors;
using DAS.Utility;
using DAS.Web.Attributes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAS.Web.Controllers
{ 
    [Authorize]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class DocTypeController : BaseController
    {
        #region Properties

        private readonly IDocTypeServices _docTypeService;
        private readonly IMapper _mapper;
        private readonly IUserPrincipalService _userPrincipalService;

        #endregion Properties

        #region Ctor

        public DocTypeController(IMapper mapper, IDocTypeServices DocTypeService, IUserPrincipalService userPrincipalService)
        {
            _docTypeService = DocTypeService;
            _mapper = mapper;
            _userPrincipalService = userPrincipalService;
        }

        #endregion Ctor


        #region List
        [HasPermission((int)EnumModule.Code.M20130, new int[] { (int)EnumPermission.Type.Read })]
        // GET: DocTypeFields
        public async Task<IActionResult> Index(DocTypeCondition condition)
        {
            var model = await _docTypeService.SearchByConditionPagging(condition);
            ViewBag.Keyword = condition.Keyword;
            return PartialView(model);
        }

        [HasPermission((int)EnumModule.Code.M20130, new int[] { (int)EnumPermission.Type.Read })]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SearchByCondition(DocTypeCondition condition)
        {
            ViewBag.Keyword = condition.Keyword;
            var model = await _docTypeService.SearchByConditionPagging(condition);
            return PartialView("Index_DocTypes", model);
        }
        #endregion List

        #region Create
        // GET: Users/Create
        [HasPermission((int)EnumModule.Code.M20130, new int[] { (int)EnumPermission.Type.Create })]
        public async Task<IActionResult> Create()
        {
            var model = await _docTypeService.Create();
            return PartialView("Index_Update", model);
        }

        [HasPermission((int)EnumModule.Code.M20130, new int[] { (int)EnumPermission.Type.Create })]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(VMUpdateDocType vmDocType)
        {
            //Validate
            if (!ModelState.IsValid)
                return JSErrorModelStateByLine();

            if (!CheckDocTypeFieldValidate(vmDocType, out string mss, out List<object> errObj))
            {
                if (errObj.IsNotEmpty())
                    return JSErrorResult(mss, errObj);
                return JSErrorResult(mss);
            }

            //CallService
            var rs = await _docTypeService.Create(vmDocType);
            return CustJSonResult(rs);
        }

        #endregion

        #region Edit
        [HasPermission((int)EnumModule.Code.M20130, new int[] { (int)EnumPermission.Type.Update })]
        public async Task<IActionResult> Edit(int? id)
        {
            var model = await _docTypeService.Update(id);
            if (model == null || (model.IDOrgan == 0 && _userPrincipalService.IDOrgan != 0))
                return Redirect("/DocType");

            return PartialView("Index_Update", model);
        }

        [HasPermission((int)EnumModule.Code.M20130, new int[] { (int)EnumPermission.Type.Update })]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(VMUpdateDocType vmDocType)
        {
            //Validate
            if (!ModelState.IsValid)
                return JSErrorModelStateByLine();

            if (!CheckDocTypeFieldValidate(vmDocType, out string mss, out List<object> errObj))
            {
                if (errObj.IsNotEmpty())
                    return JSErrorResult(mss, errObj);
                return JSErrorResult(mss);
            }
            //CallService
            var rs = await _docTypeService.Update(vmDocType);
            return CustJSonResult(rs);
        }

        public async Task<IActionResult> ChangeType(int id = 0, int type = 0)
        {
            var rs = await _docTypeService.ChangeType(id, type);
            return PartialView("Update_DocTypeFields", rs);
        }
        #endregion

        #region Delete 
        [HasPermission((int)EnumModule.Code.M20130, new int[] { (int)EnumPermission.Type.Deleted })]
        public async Task<IActionResult> Delete(int id)
        {
            var rs = await _docTypeService.Delete(id);
            return CustJSonResult(rs);
        }


        [HasPermission((int)EnumModule.Code.M20130, new int[] { (int)EnumPermission.Type.Deleted })]
        public async Task<IActionResult> Deletes(int[] ids)
        {
            if (ids == null || ids.Length == 0)
                return JSErrorResult("Vui lòng chọn loại danh mục cần xoá!");
            var rs = await _docTypeService.Delete(ids);
            return CustJSonResult(rs);
        }
        #endregion Delete

        #region Details
        [HasPermission((int)EnumModule.Code.M20130, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> Details(int? id)
        {
            var model = await _docTypeService.Update(id);
            return PartialView("Index_Detail", model);
        }
        #endregion
        #region Functions
        private bool CheckDocTypeFieldValidate(VMUpdateDocType vmDocType, out string mss, out List<object> errObj)
        {
            errObj = new List<object>();
            mss = string.Empty;
            if (vmDocType.DocTypeFields.Any(n => n.InputType == (int)EnumDocType.InputType.CategoryType))
            {
                var i = 0;
                foreach (var item in vmDocType.DocTypeFields)
                {
                    if (item.InputType == (int)EnumDocType.InputType.CategoryType && item.IDCategoryTypeRelated == 0)
                    {
                        errObj.Add(new
                        {
                            Field = $"DocTypeFields[{i}].IDCategoryTypeRelated",
                            Mss = "Danh mục không được để trống"
                        });
                    }
                    i++;
                }
            }
            var codes = vmDocType.DocTypeFields.Where(n => n.Code.IsNotEmpty()).Select(n => n.Code).ToList();
            if (codes.Count() > codes.Distinct().Count())
            {
                mss = "Mã trường thông tin đã bị trùng";
            }
            return mss.IsEmpty() && errObj.IsEmpty();
        }
        #endregion
    }
}
