﻿using AutoMapper;
using DAS.Application.Enums;
using DAS.Application.Enums.DasKTNN;
using DAS.Application.Interfaces;
using DAS.Application.Models.CustomModels;
using DAS.Application.Models.ViewModels.DasKTNN;
using DAS.Utility;
using DAS.Utility.CustomClass;
using DAS.Utility.LogUtils;
using DAS.Web.Attributes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAS.Web.Controllers
{
    [Authorize]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class TableInfoController : BaseController
    {
        #region Properties

        private readonly ITableInfoServices _tableInfoService;
        private readonly IMapper _mapper;
        private readonly ILoggerManager _logger;
        private readonly IExcelServices _excelService;
        private readonly string defaultPath = "/TableInfo";
        private readonly bool IsDanhMucDungChung = ConfigUtils.GetAppSetting<bool>("IsDanhMucDungChung");
        #endregion Properties

        #region Ctor

        public TableInfoController(IMapper mapper, ITableInfoServices TableInfoService, ILoggerManager logger, IExcelServices excel)
        {
            _tableInfoService = TableInfoService;
            _mapper = mapper;
            _excelService = excel;
            _logger = logger;
        }

        #endregion Ctor

        #region List

        // GET: TableInfoFields
        [HasPermission((int)EnumModule.Code.TABLEINFO, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> Index(TableInfoCondition condition)
        {
            var model = await _tableInfoService.SearchByConditionPagging(condition);
            var lstSchema = await _tableInfoService.GetAllListSchema();
            var lstGroup = await _tableInfoService.GetAllListGroup();
            var lstSource = IsDanhMucDungChung ? new List<Domain.Models.DASKTNN.SoucreInfo>() : await _tableInfoService.GetAllListSource();
            ViewBag.Keyword = condition.Keyword;
            ViewBag.LstSchema = lstSchema;
            ViewBag.LstGroup = lstGroup;
            ViewBag.LstSource = lstSource;
            ViewBag.IDSchema = condition.IDSchema;
            ViewBag.IDGroup = condition.IDGroup;
            ViewBag.IDSource = condition.IDSource;
            ViewBag.IsDanhMucDungChung = IsDanhMucDungChung;
            return PartialView(model);
        }

        [HasPermission((int)EnumModule.Code.TABLEINFO, new int[] { (int)EnumPermission.Type.Read })]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SearchByCondition(TableInfoCondition condition)
        {
            try
            {
                if (condition.IDSource < 0)
                {
                    var sourceInfos = await _tableInfoService.GetAllListSource(condition.IDSource * -1);
                    if(sourceInfos.IsNotEmpty())
                    {
                        condition.IDSources = sourceInfos.Select(n => n.ID).ToArray();
                        condition.IDSource = 0;
                    }    
                }
                var model = await _tableInfoService.SearchByConditionPagging(condition);
                return PartialView("Index_TableInfos", model);
            }
            catch (LogicException ex)
            {
                SetError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                SetError("Có lỗi khi lấy dữ liệu");
            }
            return Redirect(defaultPath);
        }

        [HasPermission((int)EnumModule.Code.TABLERECORDSTATS, new int[] { (int)EnumPermission.Type.Export })]
        [HttpGet]
        public async Task<IActionResult> Export(TableInfoCondition condition)
        {
            condition.PageSize = 5000; 
            if (condition.IDSource < 0)
            {
                var sourceInfos = await _tableInfoService.GetAllListSource(condition.IDSource * -1);
                if (sourceInfos.IsNotEmpty())
                {
                    condition.IDSources = sourceInfos.Select(n => n.ID).ToArray();
                    condition.IDSource = 0;
                }
            }
            var listTypes = Utils.EnumToDic<EnumTableInfo.TableListType>();
            var model = await _tableInfoService.SearchByConditionPagging(condition);
            var export = new ExportExtend
            {
                Data = model.Select(n=>new
                {
                    Name= n.Name,
                    DbName = n.DbName,
                    Title = n.Title,
                    Description = n.Description,
                    IsMenu = n.IsMenu > 0 ? "Hiển thị" : "Ẩn",
                    ListType= listTypes.GetValueOrDefault(n.ListType)
                }).Cast<dynamic>().ToList(),
                Cols = new List<Col>
                {
                    new Col{
                        DataType = 5
                    },
                    new Col("Name"),
                    new Col("DbName"),
                    new Col("Title"),
                    new Col("Description"),
                    new Col("IsMenu"),
                    new Col("ListType"),
                },
                Headers = new List<Header>
                {
                  new Header("STT", 8),
                    new Header("Tên bảng dữ liệu"),
                    new Header("Tên bảng dữ liệu trong SQL"),
                    new Header("Nhãn hiển thị"),
                    new Header("Mô tả"),
                    new Header("Hiển thị ở menu"),
                    new Header("Kiểu hiển thị"),
                }
            };
            var rs = await _excelService.ExportExcel(export, "Thống kê bảng dữ liệu");
            if (rs is ServiceResultError)
            {
                return NotFound();
            }
            else
            {
                var fileName = "ThongKeBangDuLieu.xlsx";
                var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                return File((byte[])rs.Data, contentType, fileName);
            }
        }

        #endregion List

        #region Create
        [HasPermission((int)EnumModule.Code.TABLEINFO, new int[] { (int)EnumPermission.Type.Create })]
        public async Task<IActionResult> Create()
        {
            try
            {
                var model = await _tableInfoService.Create();
                return PartialView("Index_Update", model);
            }
            catch (LogicException ex)
            {
                SetError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                SetError("Có lỗi khi khởi tạo bảng dữ liệu");
            }
            return Redirect(defaultPath);
        }

        [HasPermission((int)EnumModule.Code.TABLEINFO, new int[] { (int)EnumPermission.Type.Create })]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(VMUpdateTableInfo vmTableInfo)
        {
            //Validate
            if (!ModelState.IsValid)
                return JSErrorModelStateByLine();

            //CallService
            var rs = await _tableInfoService.Create(vmTableInfo);
            return CustJSonResult(rs);
        }

        #endregion

        #region Edit
        [HasPermission((int)EnumModule.Code.TABLEINFO, new int[] { (int)EnumPermission.Type.Update })]
        public async Task<IActionResult> Edit(int? id)
        {
            try
            {
                var model = await _tableInfoService.Update(id);
                return PartialView("Index_Update", model);
            }
            catch (LogicException ex)
            {
                SetError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                SetError("Có lỗi khi lấy thông tin cập nhật bảng dữ liệu");
            }
            return Redirect(defaultPath);
        }

        [HasPermission((int)EnumModule.Code.TABLEINFO, new int[] { (int)EnumPermission.Type.Update })]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(VMUpdateTableInfo tableInfo)
        {
            //Validate
            if (!ModelState.IsValid)
                return JSErrorModelStateByLine();

            //CallService
            var rs = await _tableInfoService.Update(tableInfo);
            return CustJSonResult(rs);
        }
        #endregion

        #region Clone
        [HasPermission((int)EnumModule.Code.TABLEINFO, new int[] { (int)EnumPermission.Type.Create })]
        public async Task<IActionResult> IsClone(int id)
        {
            try
            {
                var model = await _tableInfoService.IsClone(id);
                return PartialView("Index_Clone", model);
            }
            catch (LogicException ex)
            {
                SetError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                SetError("Có lỗi khi lấy thông tin cập nhật bảng dữ liệu");
            }
            return Redirect(defaultPath);
        }

        [HasPermission((int)EnumModule.Code.TABLEINFO, new int[] { (int)EnumPermission.Type.Create })]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Clone(VMCloneTableInfo tableInfo)
        {
            //Validate
            if (!ModelState.IsValid)
                return JSErrorModelStateByLine();

            tableInfo.IDRecordClones = Utils.GetInts(DATA, "IDRecords");
            //CallService
            var rs = await _tableInfoService.Clone(tableInfo);
            return CustJSonResult(rs);
        }
        #endregion

        #region Delete 
        [HasPermission((int)EnumModule.Code.TABLEINFO, new int[] { (int)EnumPermission.Type.Deleted })]
        public async Task<IActionResult> Delete(int id)
        {
            var rs = await _tableInfoService.Delete(id);
            return CustJSonResult(rs);
        }

        [HasPermission((int)EnumModule.Code.TABLEINFO, new int[] { (int)EnumPermission.Type.Deleted })]
        public async Task<IActionResult> Deletes(int[] ids)
        {
            if (ids == null || ids.Length == 0)
                return JSErrorResult("Vui lòng chọn bảng dữ liệu cần xoá!");
            var rs = await _tableInfoService.Delete(ids);
            return CustJSonResult(rs);
        }
        #endregion Delete

        #region CreateColumn
        public async Task<IActionResult> CreateColumn(int? idTable)
        {
            try
            {
                var model = await _tableInfoService.CreateColumn(idTable);
                return PartialView("Index_UpdateColumn", model);
            }
            catch (LogicException ex)
            {
                SetError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                SetError("Có lỗi khi xóa cột dữ liệu");
            }
            return Redirect(defaultPath);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateColumn(VMUpdateColumnTable data)
        {
            data.IdentityIndex = data.IdentityIndex ?? 0;

            //Validate
            if (!ModelState.IsValid)
                return JSErrorModelStateByLine();

            //CallService
            var rs = await _tableInfoService.CreateColumn(data);
            return CustJSonResult(rs);
        }

        #endregion

        #region EditColumn
        public async Task<IActionResult> EditColumn(int? id)
        {
            try
            {
                var model = await _tableInfoService.UpdateColumn(id);
                return PartialView("Index_UpdateColumn", model);
            }
            catch (LogicException ex)
            {
                SetError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                SetError("Có lỗi khi lấy thông tin cập nhật cột dữ liệu");
            }
            return Redirect(defaultPath);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditColumn(VMUpdateColumnTable data)
        {
            data.IdentityIndex = data.IdentityIndex ?? 0;

            //Validate
            if (!ModelState.IsValid)
                return JSErrorModelStateByLine();

            var rs = await _tableInfoService.UpdateColumn(data);
            return CustJSonResult(rs);
        }
        #endregion

        #region DeleteColumn
        public async Task<IActionResult> DeleteColumn(int id)
        {
            var rs = await _tableInfoService.DeleteColumn(id);
            return CustJSonResult(rs);
        }

        public async Task<IActionResult> DeletesColumn(int[] ids)
        {

            if (ids == null || ids.Length == 0)
                return JSErrorResult("Vui lòng chọn cột dữ liệu cần xoá!");
            var rs = await _tableInfoService.DeleteColumn(ids);
            return CustJSonResult(rs);

        }
        #endregion Delete

        #region UpdateColumns
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UpdateColumns(VMUpdateTableInfo tableInfo)
        { 
            //Validate
            if (!ModelState.IsValid)
                return JSErrorModelStateByLine();


            var rs = await _tableInfoService.UpdateColumns(tableInfo);
            return CustJSonResult(rs);
        }
        #endregion

        #region Details
        [HasPermission((int)EnumModule.Code.TABLEINFO, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> Detail(int? id)
        {
            try
            {
                var model = await _tableInfoService.Update(id);
                model.IsReadOnly = true;
                return PartialView("Index_Detail", model);
            }
            catch (LogicException ex)
            {
                SetError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                SetError("Có lỗi khi lấy thông tin bảng dữ liệu");
            }
            return Redirect(defaultPath);
        }
        [HasPermission((int)EnumModule.Code.TABLEINFO, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> ConfigColumn(int? id)
        {
            try
            {
                var model = await _tableInfoService.Update(id);
                return PartialView("Index_Detail", model);
            }
            catch (LogicException ex)
            {
                SetError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                SetError("Có lỗi khi lấy thông tin bảng dữ liệu");
            }
            return Redirect(defaultPath);
        }

        #endregion

        #region Functions
        #endregion

        #region Autocomplete

        public async Task<IActionResult> GetTableBySchema()
        {
            var id = Utils.GetInt(DATA, "ID");
            var selectedId = Utils.GetInt(DATA, "SelectedID");
            var defaultText = Utils.GetString(DATA, "DefaultText");

            var html = string.Empty;

            var categories = await _tableInfoService.GetTableByIDSchema(selectedId);

            categories = categories.Where(n => n.ID != id);

            if (categories.IsNotEmpty())
                html = Utils.RenderOptions(categories, 0, true, defaultText, "");
            return new JsonResult(new
            {
                Type = Application.Constants.CommonConst.Success,
                Data = html
            });
        }
        public async Task<IActionResult> GetColumnByTable()
        {
            var selectedId = Utils.GetInt(DATA, "SelectedID");
            var defaultText = Utils.GetString(DATA, "DefaultText");

            var names = Utils.GetStrings(DATA, "Name");

            var html = string.Empty;

            var categories = await _tableInfoService.GetColumnByIDTable(selectedId);

            var selected = categories.FirstOrNewObj(n => names.IsNotEmpty() && names.Contains(n.DbName)); //Mặc định focus vào trường tên
            if (categories.IsNotEmpty())
                html = Utils.RenderOptions(categories, selected.ID, true, defaultText, "");
            return new JsonResult(new
            {
                Type = Application.Constants.CommonConst.Success,
                Data = html
            });
        }

        #endregion

        #region Statistic
        [HasPermission((int)EnumModule.Code.TABLERECORDSTATS, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> Statistic(TableInfoCondition condition)
        {
            var lstSchema = await _tableInfoService.GetAllListSchema();
            var lstGroup = await _tableInfoService.GetAllListGroup();
            ViewBag.Keyword = condition.Keyword;
            ViewBag.LstSchema = lstSchema;
            ViewBag.LstGroup = lstGroup;
            ViewBag.IDSchema = condition.IDSchema;
            ViewBag.IDGroup = condition.IDGroup;

            var model = await _tableInfoService.Statistic(condition);
            return View("Statistic/Statistic", model);
        }

        [HasPermission((int)EnumModule.Code.TABLERECORDSTATS, new int[] { (int)EnumPermission.Type.Read })]
        [HttpPost]
        public async Task<IActionResult> StatisticSearch(TableInfoCondition condition)
        {
            try
            {
                var model = await _tableInfoService.Statistic(condition);
                return PartialView("Statistic/Statistic_Table", model);
            }
            catch (LogicException ex)
            {
                SetError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                SetError("Có lỗi khi lấy dữ liệu");
            }
            return Redirect(defaultPath);
        }

        [HasPermission((int)EnumModule.Code.TABLERECORDSTATS, new int[] { (int)EnumPermission.Type.Export })]
        [HttpGet]
        public async Task<IActionResult> StatisticExport(TableInfoCondition condition)
        {
            condition.PageSize = 5000;
            var model = await _tableInfoService.Statistic(condition);
            var export = new ExportExtend
            {
                Data = model.TableStatistics.OrderByDescending(n => n.RecordCount).Cast<dynamic>().ToList(),
                Cols = new List<Col>
                {
                    new Col{
                        DataType = 5
                    },
                    new Col("Name"),
                    new Col("DbName"),
                    new Col("RecordCount"),
                },
                Headers = new List<Header>
                {
                  new Header("STT", 8),
                    new Header("Tên bảng dữ liệu"),
                    new Header("Tên bảng dữ liệu trong SQL"),
                    new Header("Số bản ghi"),
                }
            };
            var rs = await _excelService.ExportExcel(export, "Thống kê bảng dữ liệu");
            if (rs is ServiceResultError)
            {
                return NotFound();
            }
            else
            {
                var fileName = "ThongKeBangDuLieu.xlsx";
                var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                return File((byte[])rs.Data, contentType, fileName);
            }
        } 
       
        [HasPermission((int)EnumModule.Code.TABLEINFO, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> CollectStatistic(TableInfoCondition condition)
        {
            var lstSource = await _tableInfoService.GetAllListSource();
            ViewBag.Keyword = condition.Keyword;
            ViewBag.LstSource = lstSource;
            ViewBag.IDSource = condition.IDSource;
            ViewBag.StartDate = condition.StartDate;
            ViewBag.EndDate = condition.EndDate;

            var model = await _tableInfoService.Statistic(condition);
            return View("Statistic/CollectStatistic", model);

        }

        [HasPermission((int)EnumModule.Code.TABLERECORDSTATS, new int[] { (int)EnumPermission.Type.Read })]
        [HttpPost]
        public async Task<IActionResult> CollectStatisticSearch(TableInfoCondition condition)
        {
            try
            {
                var model = await _tableInfoService.Statistic(condition);
                return PartialView("Statistic/CollectStatistic_Table", model);
            }
            catch (LogicException ex)
            {
                SetError(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex);
                SetError("Có lỗi khi lấy dữ liệu");
            }
            return Redirect(defaultPath);
        }

        [HasPermission((int)EnumModule.Code.TABLEINFO, new int[] { (int)EnumPermission.Type.Export })]
        [HttpGet]
        public async Task<IActionResult> CollectStatisticExport(TableInfoCondition condition)
        {
            condition.PageSize = 5000;
            var model = await _tableInfoService.Statistic(condition);
            var export = new ExportExtend
            {
                Data = model.TableStatistics.OrderByDescending(n => n.RecordCount).Cast<dynamic>().ToList(),
                Cols = new List<Col>
                {
                    new Col{
                        DataType = 5
                    },
                    new Col("Name"),
                    new Col("DbName"),
                    new Col("RecordCount"),
                },
                Headers = new List<Header>
                {
                  new Header("STT", 8),
                    new Header("Tên bảng dữ liệu"),
                    new Header("Tên bảng dữ liệu trong SQL"),
                    new Header("Số bản ghi"),
                }
            };
            var rs = await _excelService.ExportExcel(export, "Thống kê bảng dữ liệu");
            if (rs is ServiceResultError)
            {
                return NotFound();
            }
            else
            {
                var fileName = "ThongKeBangDuLieu.xlsx";
                var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                return File((byte[])rs.Data, contentType, fileName);
            }
        }

        #endregion
    }
}
