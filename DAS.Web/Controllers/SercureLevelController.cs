﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DAS.Web.Models;
using DAS.Application.Interfaces;
using DAS.Application.Models.ViewModels;
using DAS.Domain.Models.DAS;
using Microsoft.AspNetCore.Mvc.Rendering;
using AutoMapper;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.IO;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using DAS.Application.Models.CustomModels;
using DAS.Application.Enums;
using DAS.Web.Attributes;

namespace DAS.Web.Controllers
{
    [Authorize]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class SercureLevelController : BaseController
    {
        #region Properties
        private readonly ISercureLevelServices _sercureLevelService;
        private readonly IMapper _mapper;
        private readonly IExcelServices _excelService;
        #endregion

        #region Ctor
        public SercureLevelController(ISercureLevelServices sercureLevelServices
            , IMapper mapper
            , IExcelServices excel)
        {
            _sercureLevelService = sercureLevelServices;
            _mapper = mapper;
            _excelService = excel;
        }
        #endregion

        #region List
        [HasPermission((int)EnumModule.Code.CDBM, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> Index(SercureLevelCondition condition)
        {
            var breadcrum = new Dictionary<string, string>();
            breadcrum.Add("/SercureLevel", "Cấp độ bảo mật");
            ViewData["Breadcrumb"] = breadcrum;
            ViewBag.Keyword = condition.Keyword;
            var model = await _sercureLevelService.SearchByConditionPagging(condition);
            return PartialView(model);
        }

      
        public async Task<IActionResult> SearchByConditionPagging(SercureLevelCondition condition)
        {
            ViewBag.Keyword = condition.Keyword;
            PaginatedList<VMSercureLevel> paging = await _sercureLevelService.SearchByConditionPagging(condition);
            return PartialView("Index_SercureLevel", paging);
        }

        [HasPermission((int)EnumModule.Code.CDBM, new int[] { (int)EnumPermission.Type.Read })]
        public async Task<IActionResult> Detail(int? id)
        {
            if (id == null)
                return NotFound();
            var model = await _sercureLevelService.GetSercureLevel(id.Value);
            if (model == null)
                return NotFound();
            return PartialView("Index_Detail", model);
        }


        [HasPermission((int)EnumModule.Code.CDBM, new int[] { (int)EnumPermission.Type.Export })]
        [HttpGet]
        public async Task<IActionResult> Export(SercureLevelCondition condition)
        {
            var list = await _sercureLevelService.GetListByCondition(condition);
            var export = new ExportExtend
            {
                Data = list.Cast<dynamic>().ToList(),
                Cols = new List<Col>
                {
                    new Col{
                        DataType = 5
                    },
                    new Col("Code"),
                    new Col("Name"),
                    new Col("Description"),
                },
                Headers = new List<Header>
                {
                    new Header("STT",8),
                    new Header("Mã cấp độ bảo mật"),
                    new Header("Tên cấp độ bảo mật"),
                    new Header("Mô tả",70),
                }
            };
            var rs = await _excelService.ExportExcel(export, "Danh mục cấp độ bảo mật");
            if (rs is ServiceResultError)
            {
                return NotFound();
            }
            else
            {
                var fileName = "DanhMucCDBM.xlsx";
                var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                return File((byte[])rs.Data, contentType, fileName);
            }
        }
        #endregion

        #region Edit

        [HasPermission((int)EnumModule.Code.CDBM, new int[] { (int)EnumPermission.Type.Update })]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return NotFound();
            var model = await _sercureLevelService.GetSercureLevel(id.Value);
            if (model == null)
                return NotFound();
            return PartialView("Index_Edit", model);
        }


        [HasPermission((int)EnumModule.Code.CDBM, new int[] { (int)EnumPermission.Type.Update })]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update([Bind("ID, IDChannel, Code, Name, Description, Status")] VMSercureLevel vMSecureLevel)
        {
            if (!ModelState.IsValid)
                return JSErrorModelStateByLine();
            var rs = await _sercureLevelService.UpdateSercureLevel(vMSecureLevel);
            return CustJSonResult(rs);
        }
        #endregion

        #region Create

        [HasPermission((int)EnumModule.Code.CDBM, new int[] { (int)EnumPermission.Type.Create })]
        public async Task<IActionResult> Create()
        {
            VMSercureLevel vMSecureLevel = new VMSercureLevel();
            return PartialView("Index_Edit", vMSecureLevel);
        }


        [HasPermission((int)EnumModule.Code.CDBM, new int[] { (int)EnumPermission.Type.Create })]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID, IDChannel, Code, Name, Description, Status")] VMSercureLevel vMSecureLevel)
        {
            if (!ModelState.IsValid)
                return JSErrorModelStateByLine();
            var rs = await _sercureLevelService.CreateSercureLevel(vMSecureLevel);
            return CustJSonResult(rs);
        }
        #endregion

        #region Delete

        [HasPermission((int)EnumModule.Code.CDBM, new int[] { (int)EnumPermission.Type.Deleted })]
        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var rs = await _sercureLevelService.DeleteSercureLevel(id);
            return CustJSonResult(rs);
        }


        [HasPermission((int)EnumModule.Code.CDBM, new int[] { (int)EnumPermission.Type.Deleted })]
        [HttpPost]
        public async Task<IActionResult> Deletes(int[] ids)
        {
            if (ids == null || ids.Length == 0)
                return JSErrorResult("Vui lòng chọn cấp độ bảo mật cần xóa");
            var rs = await _sercureLevelService.DeleteMultiSercureLevel(ids);
            return CustJSonResult(rs);
        }
        #endregion
    }
}
