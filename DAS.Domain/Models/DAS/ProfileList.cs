﻿using DAS.Domain.Models.Abstractions;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAS.Domain.Models.DAS
{
    public class ProfileList : Auditable
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        [MaxLength(20)]
        public string Code { get; set; }

        [Required]
        [MaxLength(250)]
        public string Name { get; set; }

        [MaxLength(250)]
        public string Description { get; set; }

        [Required]
        public int IDStorage { get; set; }

        [Required]
        public int IDProfileTemplate { get; set; }

        public int Status { get; set; } = 1;
        public int IDAgency { get; set; } = 0;
    }
}
