﻿using DAS.Domain.Models.Abstractions;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAS.Domain.Models.DAS
{
    public class DocType : Auditable
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int IDChannel { get; set; } = 0;
        public int IDOrgan { get; set; } = 0;

        [MaxLength(20)]
        [Required]
        public string Code { get; set; }

        [Required]
        [MaxLength(250)]
        public string Name { get; set; }

        [MaxLength(250)]
        public string Description { get; set; }

        public int Status { get; set; } = 1;
        public int Type { get; set; }
        public bool IsBase { get; set; }

    }
}
