﻿using DAS.Domain.Models.Abstractions;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAS.Domain.Models.DASKTNN
{
    public class TableInfo : Auditable
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        //public int IDChannel { get; set; } = 0;

        [Required]
        public int IDSchema { get; set; }

        [Required]
        [MaxLength(200)]
        public string Name { get; set; }

        
        [Required]
        [MaxLength(100)]
        public string DbName { get; set; }
         
        [Required]
        [MaxLength(300)]
        public string Title { get; set; }


        [MaxLength(500)]
        public string Description { get; set; }

        public int ListType { get; set; }
        public int Status { get; set; }
        public bool IsMenu { get; set; }

        /// <summary>
        /// Cần phê duyệt 
        /// </summary>
        public bool IsRequireApprove { get; set; } 

        /// <summary>
        /// Là bảng tạm 
        /// </summary>
        public bool IsTempTable { get; set; }

        /// <summary>
        /// Nguồn dữ liệu
        /// </summary>
        public int IDSourceInfo { get; set; }
    }
}
