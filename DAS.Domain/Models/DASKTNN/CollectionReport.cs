﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAS.Domain.Models.DASKTNN
{
    public class CollectionReport
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [MaxLength(200)]
        public string CodeSource {get; set; }

        [MaxLength(250)]
        public string DataSource { get; set; }

        public int TotalRecord { get; set; }

        public int Month { get; set; }

        public int Year { get; set; }
    }
}
