﻿using DAS.Domain.Models.Abstractions;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAS.Domain.Models.DASKTNN
{
    public class ColumnTableInfo : Auditable
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        //public int IDChannel { get; set; } = 0;

        [Required]
        public int IDTable { get; set; }

        [Required]
        [MaxLength(200)]
        public string Name { get; set; }

        [Required]
        [MaxLength(100)]
        public string DbName { get; set; }

        public int DataType { get; set; }
        public bool IsNullable { get; set; }
        public bool IsMultiple { get; set; }
        public bool IsPrimaryKey { get; set; }
        public bool IsIdentity { get; set; }

        public int IDSchemaRef { get; set; }
        public int IDTableRef { get; set; }
        public int IDColRef { get; set; }
        public int IDColTextRef { get; set; }

        public int Weight { get; set; }
        
        [MaxLength(500)]
        public string Description { get; set; }

        public bool IsSearchable { get; set; }

        public bool IsShowOnList { get; set; }

        public bool IsRequired { get; set; }

        public int? MinVal { get; set; }

        public int? MaxVal { get; set; }

        public int? MinLen { get; set; }

        public int? MaxLen { get; set; }

        [MaxLength(250)]
        public string DefaultValue { get; set; }

        /// <summary>
        /// Là trường hệ thống (không đc phép nhập, vd: id, người tạo, ngày tạo....)
        /// </summary>
        public bool IsSystem { get; set; } 

        /// <summary>
        /// Là các trường mặc định đc khởi tạo
        /// </summary>
        public bool IsDefault { get; set; }
        
        public bool IsSubColumn { get; set; }
        public int IDColumnTableGroup { get; set; }
        public int Status { get; set; }

        public string IdentityPrefix { get; set; }
        public string OrderType { get; set; }
        public bool IsOrder { get; set; }
        public string Suggestion { get; set; }

        public int IdentityIndex { get; set; }
    }
}
