﻿using DAS.Domain.Models.Abstractions;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAS.Domain.Models.DASKTNN
{
    public class GroupTableInfo : Auditable
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Required]
        public int IDGroup { get; set; }
        [Required]
        public int IDTable { get; set; }
    }
}
