﻿using DAS.Domain.Models.Abstractions;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAS.Domain.Models.DASKTNN
{
    public class TableInfoApiConfig : Auditable
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        public int IDSchema { get; set; }


        [Required]
        public int IDTable { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string ViewColumn { get; set; }

        public string Condition { get; set; }

        [MaxLength(300)]
        public string OrderBy { get; set; }

        public int Limit { get; set; }
        public int Type { get; set; }
        public int IDTempTable { get; set; }
        public int Status { get; set; }
        public string ParamJson { get; set; }

    }
}
