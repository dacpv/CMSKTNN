﻿using DAS.Domain.Models.Abstractions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAS.Domain.Models.DASKTNN
{
    public class APIMangeReport
    {
        public int API_ID { get; set; }
        public string API_NAME { get; set; }
        public string API_VERSION { get; set; }
        public string CONTEXT { get; set; }
        public string NAME { get; set; }
        public string APPLICATION_TIER { get; set; }
        public string STATUS { get; set; }
        public string APPLICATION_STATUS { get; set; }
        public string HITCOUNTER { get; set; }

    }
    public class API
    {
        public int API_ID { get; set; }
        public string API_NAME { get; set; }

    }
}