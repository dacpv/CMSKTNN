﻿using DAS.Domain.Models.Abstractions;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAS.Domain.Models.DASKTNN
{
    public class SoucreInfo : Auditable
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Required]
        [MaxLength(200)]
        public string Code { get; set; }
        [Required]
        [MaxLength(500)]
        public string Name { get; set; }
        public int Type { get; set; }
        public string IP { get; set; }
        public string Url { get; set; }
        public string DatabaseName { get; set; }
        public string Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Describe { get; set; }
        public int Status { get; set; }
         
        public string FileLocation { get; set; }
        public string FileName { get; set; }
        public string DBConnection { get; set; }
        public string TokenUrl { get; set; }
    }
}
