﻿using DAS.Domain.Models.Abstractions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DAS.Domain.Models.DASKTNN
{
    public class ColumnPathInfo : Auditable
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        public int IDColumnTableInfo { get; set; }

        [Required]
        public int IDColumnPath { get; set; } 

        public string PathName { get; set; }
        public int Status { get; set; }
    }
}
