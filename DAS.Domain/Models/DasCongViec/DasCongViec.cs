﻿using DAS.Domain.Models.Abstractions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAS.Domain.Models.DasCongViec
{
    public class NhatKyCongViec : Auditable
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int IDCaTruoc { get; set; }
        public DateTime Ngay { get; set; } //dd/MM/yyyy
        public int Ca { get; set; }
        public int Status { get; set; }
        public DateTime? Approved { get; set; }
        public int ApprovedBy { get; set; }
        public string ApprovedNote { get; set; }
        public string NoteTon { get; set; }
        [NotMapped]
        public string TenCongViec { get; set; }
    }
    public class ChiTietNhatKy : Auditable
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int IDNhatKyCongViec { get; set; }
        public int IDCongViec { get; set; }
        public int KetQua { get; set; }
        public string NoiDung { get; set; }
        public string Note { get; set; }
        public int ApprovedStatus { get; set; }
        [NotMapped]
        public int Index { get; set; } = 0;
    }
    public class ChiTietNhatKyNguoiXL : Auditable
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int IDNhatKyCongViec { get; set; }
        public int IDChiTietNhatKy { get; set; }
        public int IDUser { get; set; }
    }
    public class StaticBaoCao : Auditable
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int IDNhatKyCongViec { get; set; }
        public int IDCongViec { get; set; }
        public DateTime Ngay { get; set; }
        public int Ca { get; set; }
        public int IDUser { get; set; }
        public string NoiDungTacDong { get; set; }
        public int ApprovedBy { get; set; }
        public int ApprovedStatus { get; set; }
        public string ApprovedNote { get; set; }
        public DateTime? Approved { get; set; }
        public int Status { get; set; }
    }
}