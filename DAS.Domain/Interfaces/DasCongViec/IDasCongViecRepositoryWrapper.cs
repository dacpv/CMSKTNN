﻿using DAS.Domain.Interfaces.DAS;
using DAS.Domain.Interfaces.DASCongViec;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAS.Domain.Interfaces.DASNotify
{
    public interface IDasCongViecRepositoryWrapper
    {
        INhatKyCongViecRepository NhatKyCongViec { get; }
        IChiTietNhatKyRepository ChiTietNhatKy { get; }
        IChiTietNhatKyNguoiXLRepository ChiTietNhatKyNguoiXL { get; }
        IStaticBaoCaoRepository StaticBaoCao { get; }

        Task SaveAync();
    }
}
