﻿using DAS.Domain.Models.DAS;
using DAS.Domain.Models.DasCongViec;

namespace DAS.Domain.Interfaces.DASCongViec
{
    public interface INhatKyCongViecRepository : IBaseRepository<NhatKyCongViec>
    {
    }
    public interface IChiTietNhatKyRepository : IBaseRepository<ChiTietNhatKy>
    {
    }
    public interface IChiTietNhatKyNguoiXLRepository : IBaseRepository<ChiTietNhatKyNguoiXL>
    {
    }
    public interface IStaticBaoCaoRepository : IBaseRepository<StaticBaoCao>
    {
    }
   
}
