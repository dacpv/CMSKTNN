﻿using DAS.Domain.Models.DAS;
using DAS.Domain.Models.DasCongViec;
using DAS.Domain.Models.DASKTNN;
using System.Threading.Tasks;

namespace DAS.Domain.Interfaces.DASKTNN
{
    public interface IColumnTableGroupRepository : IBaseRepository<ColumnTableGroup>
    {
    }

}
