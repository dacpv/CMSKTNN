﻿using DAS.Domain.Models.DASKTNN;
using System.Threading.Tasks;

namespace DAS.Domain.Interfaces.DASKTNN
{
    public interface IColumnTableInfoRepository : IBaseRepository<ColumnTableInfo>
    {
        Task<bool> IsNameExist(string name, int idTable, int ignoreStatus, int id = 0);
        Task<bool> IsSqlNameExist(string name, int idTable, int ignoreStatus, int id = 0);
        Task<bool> IsDataTypeExist(int dataType, int idTable, int ignoreStatus, int id = 0);
    }
}
