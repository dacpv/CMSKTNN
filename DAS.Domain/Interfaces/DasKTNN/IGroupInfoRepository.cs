﻿using DAS.Domain.Models.DAS;
using DAS.Domain.Models.DasCongViec;
using DAS.Domain.Models.DASKTNN;
using System.Threading.Tasks;

namespace DAS.Domain.Interfaces.DASKTNN
{
    public interface IGroupInfoRepository : IBaseRepository<GroupInfo>
    {
        Task<bool> IsNameExist(string name,  int id = 0);
    }
    public interface IGroupTableInfoRepository : IBaseRepository<GroupTableInfo>
    {
        Task<bool> IsNameExist(string name, int id = 0);
    }

}
