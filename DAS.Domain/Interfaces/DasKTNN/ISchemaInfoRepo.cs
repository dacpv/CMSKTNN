﻿using DAS.Domain.Models.DASKTNN;
using System.Threading.Tasks;

namespace DAS.Domain.Interfaces.DasKTNN
{
    public interface ISchemaInfoRepo : IBaseRepository<SchemaInfo>
    {
        Task<bool> IsCodeExist(string code, int id = 0);
    }
}
