﻿using DAS.Domain.Models.DAS;
using DAS.Domain.Models.DasCongViec;
using DAS.Domain.Models.DASKTNN;
using System.Threading.Tasks;

namespace DAS.Domain.Interfaces.DASKTNN
{
    public interface ITableInfoRepository : IBaseRepository<TableInfo>
    {
        Task<bool> IsNameExist(string name, int ignoreStatus, int id = 0);
        Task<bool> IsSqlNameExist(string name, int idSchema, int ignoreStatus, int id = 0);
    }

}
