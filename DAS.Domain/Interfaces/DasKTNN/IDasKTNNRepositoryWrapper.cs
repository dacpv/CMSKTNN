﻿using DAS.Domain.Interfaces.DasKTNN;
using DAS.Domain.Interfaces.DASKTNN;
using System.Threading.Tasks;

namespace DAS.Domain.Interfaces.DASNotify
{
    public interface IDasKTNNRepositoryWrapper
    {
        ITableTestRepository TableTest { get; }
        IColumnTableInfoRepository ColumnTableInfo { get; }
        IColumnTableGroupRepository ColumnTableGroup { get; }
        ITableInfoRepository TableInfo { get; }
        ITableInfoApiConfigRepository TableInfoApiConfig { get; }
        IGroupInfoRepository GroupInfo { get; }
        ISoucreInfoRepository SoucreInfo { get; }
        IGroupTableInfoRepository GroupTableInfo { get; }
        IColumnPathInfoRepository ColumnPathInfo { get; }
        ISchemaInfoRepo SchemaInfo { get; }
        ITableInfoApiConfigSharedAppRepository TableInfoApiConfigSharedApp { get; }
        ISharedAppRepository SharedApp { get; }
        ITanXuatTruyCapAPIRepository TanXuatTruyCapAPI { get; }
        ICollectionReportRepository CollectionReport { get; }
        Task SaveAync();
    }
}
