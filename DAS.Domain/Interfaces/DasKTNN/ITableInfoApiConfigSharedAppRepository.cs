﻿using DAS.Domain.Models.DASKTNN;

namespace DAS.Domain.Interfaces.DASKTNN
{
    public interface ITableInfoApiConfigSharedAppRepository : IBaseRepository<TableInfoApiConfigSharedApp>
    {

    }
}
