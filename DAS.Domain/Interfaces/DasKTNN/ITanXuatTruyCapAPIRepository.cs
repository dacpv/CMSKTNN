﻿using DAS.Domain.Models.DASKTNN;

namespace DAS.Domain.Interfaces.DASKTNN
{
    public interface ITanXuatTruyCapAPIRepository : IBaseRepository<TanXuatTruyCapAPI>
    {

    }
}
