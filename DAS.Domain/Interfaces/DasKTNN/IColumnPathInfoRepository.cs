﻿using DAS.Domain.Models.DASKTNN;
using System.Threading.Tasks;

namespace DAS.Domain.Interfaces.DASKTNN
{
    public interface IColumnPathInfoRepository : IBaseRepository<ColumnPathInfo>
    {
        Task<bool> IsPathNameExist(string name, int idColumnTable, int ignoreStatus, int id = 0);
        Task<bool> IsIDColumnPathExist(int idColumn, int idColumnTable, int ignoreStatus, int id = 0);
    }
}
