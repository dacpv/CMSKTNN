﻿using DAS.Domain.Models.DASKTNN;

namespace DAS.Domain.Interfaces.DasKTNN
{
    public interface ICollectionReportRepository : IBaseRepository<CollectionReport>
    {
    }
}
