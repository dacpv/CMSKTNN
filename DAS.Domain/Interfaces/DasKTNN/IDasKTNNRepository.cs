﻿using DAS.Domain.Models.DAS;
using DAS.Domain.Models.DasCongViec;
using DAS.Domain.Models.DASKTNN;

namespace DAS.Domain.Interfaces.DASKTNN
{
    public interface ITableTestRepository : IBaseRepository<TableTest>
    {
    }
   
}
