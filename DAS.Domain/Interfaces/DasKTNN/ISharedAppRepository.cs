﻿using DAS.Domain.Models.DASKTNN;
using System.Threading.Tasks;

namespace DAS.Domain.Interfaces.DASKTNN
{
    public interface ISharedAppRepository : IBaseRepository<SharedApp>
    {
        Task<bool> IsNameExist(string name, int id = 0);
        Task<bool> IsCodeExist(string code, int id = 0);
        Task<bool> IsUserNameExist(string username, int id = 0);
    }

}
