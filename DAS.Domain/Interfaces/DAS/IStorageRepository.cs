﻿using DAS.Domain.Models.DAS;
using System.Threading.Tasks;

namespace DAS.Domain.Interfaces.DAS
{
    public interface IStorageRepository : IBaseRepository<Storage>
    {
        Task<bool> IsCodeExist(string email, int id = 0);
    }
}
