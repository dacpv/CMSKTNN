﻿using DAS.Domain.Models.DAS;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAS.Domain.Interfaces.DAS
{
    public interface ICodeBoxRepository : IBaseRepository<CodeBox>
    {
        //Task<bool> IsEmailExist(string email);
    }
}