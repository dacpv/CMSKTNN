﻿using DAS.Domain.Models.DAS;

namespace DAS.Domain.Interfaces.DAS
{
    public interface IProfileListRepository : IBaseRepository<ProfileList>
    {
    }
}
