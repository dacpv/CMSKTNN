﻿using DAS.Domain.Models.DAS;
using System.Threading.Tasks;

namespace DAS.Domain.Interfaces.DAS
{
    public interface IOrganConfigRepository : IBaseRepository<OrganConfig>
    {
        Task<object> GetConfigByCode(string code, int idOrgan = 0);
    }
}
