﻿using DAS.Domain.Models.DAS;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAS.Domain.Interfaces.DAS
{
    public interface IPermissionGroupPerRepository : IBaseRepository<PermissionGroupPer>
    {
        //Task<bool> IsEmailExist(string email);
    }
}