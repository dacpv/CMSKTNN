﻿using System;
using System.Collections.Generic;
using System.Text;
using DAS.Domain.Models.DAS;

namespace DAS.Domain.Interfaces.DAS
{
    public interface IProfileTemplateRepository : IBaseRepository<ProfileTemplate>
    {
    }
}
