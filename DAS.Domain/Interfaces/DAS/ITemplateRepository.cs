﻿using DAS.Domain.Models.DAS;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;


namespace DAS.Domain.Interfaces.DAS
{
    public interface ITemplateRepository : IBaseRepository<Template>
    {
    }
}
