﻿using DAS.Domain.Models.DAS;
using System.Threading.Tasks;

namespace DAS.Domain.Interfaces.DAS
{
    public interface IProfileDestroyedRepository : IBaseRepository<ProfileDestroyed>
    {
    }
}
