﻿using DAS.Domain.Models.DAS;

namespace DAS.Domain.Interfaces.DAS
{
    public interface ISercureLevelRepository : IBaseRepository<SercureLevel>
    {
    }
}
