﻿using DAS.Domain.Interfaces.DAS;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAS.Domain.Interfaces.DASNotify
{
    public interface IDasNotifyRepositoryWrapper
    {
        INotificationRepository Notification { get; }
        Task SaveAync();
    }
}
