﻿namespace DASNotify.Application.Models.CustomModels
{
    public class UserPermissionModel
    {
        public int IdPermission { get; set; }
        public int IdModule { get; set; }
        public int CodeModule { get; set; }
        public int Type { get; set; }
    }

    //public class UserPermissionModel
    //{
    //    public int IdPermission { get; set; }
    //    public int IdModule { get; set; }
    //    public string CodeModule { get; set; }
    //    public int Type { get; set; }
    //}

}
