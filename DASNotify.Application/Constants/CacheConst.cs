﻿namespace DASNotify.Application.Constants
{
    public static class CacheConst
    {
        public static string USER_PERMISSION = "USER_PEMISSION";
        public static string USER_DATA = "USER_DATA";
        public static string USER_COUNT_LOGIN_FAIL = "COUNT_LOGIN_FAIL_"; // Ghép thêm userName
        public static string USER_CHANGE_PWD_FAIL = "USER_CHANGE_PWD_FAIL_"; // Ghép thêm user
        public static string READER_COUNT_LOGIN_FAIL = "READER_COUNT_LOGIN_FAIL_"; // Ghép thêm readerName
        public static string READER_CHANGE_PWD_FAIL = "READER_CHANGE_PWD_FAIL_"; // Ghép thêm readerName
    }
}
