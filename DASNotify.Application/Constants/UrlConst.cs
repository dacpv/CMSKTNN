﻿namespace DASNotify.Application.Constants
{
    public static class UrlConst
    {
        public static readonly string AccessDenied = "/Error/AccessDenied";
    }
}
