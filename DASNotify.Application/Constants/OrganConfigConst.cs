﻿namespace DASNotify.Application.Constants
{
    public static class OrganConfigConst
    {
        public static string DEFAULT_PWD_SSO = "DEFAULT_PWD_SSO";
        public static string DEFAULT_ORGAN_CODE_SSO = "DEFAULT_ORGAN_CODE_SSO";
        public static string DEFAULT_AGENCY_CODE_SSO = "DEFAULT_AGENCY_CODE_SSO";
    }
}
